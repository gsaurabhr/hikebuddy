<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.3.1">
<drawing>
<settings>
<setting alwaysvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="16" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="14" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="6" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="58" name="bCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="yes" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="no" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="no" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="no" active="no"/>
<layer number="134" name="silk_top" color="7" fill="1" visible="no" active="no"/>
<layer number="135" name="silk_bottom" color="7" fill="1" visible="no" active="no"/>
<layer number="136" name="silktop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="137" name="silkbottom" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="145" name="DrillLegend_01-16" color="7" fill="1" visible="yes" active="yes"/>
<layer number="146" name="DrillLegend_01-20" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="166" name="AntennaArea" color="7" fill="1" visible="yes" active="yes"/>
<layer number="168" name="4mmHeightArea" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="252" name="BR-BS" color="4" fill="9" visible="yes" active="yes"/>
<layer number="253" name="BR-LS" color="1" fill="2" visible="yes" active="yes"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S" xrefpart="/%S.%C%R">
<libraries>
<library name="ESP32-PICO-D4">
<packages>
<package name="PQFN50P700X700X104-49N" urn="urn:adsk.eagle:footprint:9948343/1">
<wire x1="-3.55" y1="-3.55" x2="-3.55" y2="3.55" width="0.127" layer="51"/>
<wire x1="-3.55" y1="3.55" x2="3.55" y2="3.55" width="0.127" layer="51"/>
<wire x1="3.55" y1="3.55" x2="3.55" y2="-3.55" width="0.127" layer="51"/>
<wire x1="3.55" y1="-3.55" x2="-3.55" y2="-3.55" width="0.127" layer="51"/>
<wire x1="-3.55" y1="3.11" x2="-3.55" y2="3.55" width="0.127" layer="21"/>
<wire x1="-3.55" y1="3.55" x2="-3.11" y2="3.55" width="0.127" layer="21"/>
<wire x1="-3.55" y1="-3.11" x2="-3.55" y2="-3.55" width="0.127" layer="21"/>
<wire x1="-3.55" y1="-3.55" x2="-3.11" y2="-3.55" width="0.127" layer="21"/>
<wire x1="3.55" y1="-3.11" x2="3.55" y2="-3.55" width="0.127" layer="21"/>
<wire x1="3.55" y1="-3.55" x2="3.11" y2="-3.55" width="0.127" layer="21"/>
<wire x1="3.55" y1="3.11" x2="3.55" y2="3.55" width="0.127" layer="21"/>
<wire x1="3.55" y1="3.55" x2="3.11" y2="3.55" width="0.127" layer="21"/>
<wire x1="-3.75" y1="-3.75" x2="3.75" y2="-3.75" width="0.05" layer="39"/>
<wire x1="3.75" y1="-3.75" x2="3.75" y2="3.75" width="0.05" layer="39"/>
<wire x1="3.75" y1="3.75" x2="-3.75" y2="3.75" width="0.05" layer="39"/>
<wire x1="-3.75" y1="3.75" x2="-3.75" y2="-3.75" width="0.05" layer="39"/>
<text x="-3.70105" y="4.201190625" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.70055" y="-5.4907" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<circle x="-4.2" y="2.8" radius="0.1" width="0.2" layer="21"/>
<circle x="-4.2" y="2.8" radius="0.1" width="0.2" layer="51"/>
<rectangle x1="0.395" y1="0.395" x2="2.305" y2="2.305" layer="31"/>
<rectangle x1="-2.305" y1="0.395" x2="-0.395" y2="2.305" layer="31" rot="R90"/>
<rectangle x1="-2.305" y1="-2.305" x2="-0.395" y2="-0.395" layer="31" rot="R180"/>
<rectangle x1="0.395" y1="-2.305" x2="2.305" y2="-0.395" layer="31" rot="R270"/>
<smd name="1" x="-3.265" y="2.75" dx="0.45" dy="0.35" layer="1" roundness="9" rot="R180"/>
<smd name="2" x="-3.265" y="2.25" dx="0.45" dy="0.35" layer="1" roundness="9" rot="R180"/>
<smd name="3" x="-3.265" y="1.75" dx="0.45" dy="0.35" layer="1" roundness="9" rot="R180"/>
<smd name="4" x="-3.265" y="1.25" dx="0.45" dy="0.35" layer="1" roundness="9" rot="R180"/>
<smd name="5" x="-3.265" y="0.75" dx="0.45" dy="0.35" layer="1" roundness="9" rot="R180"/>
<smd name="6" x="-3.265" y="0.25" dx="0.45" dy="0.35" layer="1" roundness="9" rot="R180"/>
<smd name="7" x="-3.265" y="-0.25" dx="0.45" dy="0.35" layer="1" roundness="9" rot="R180"/>
<smd name="8" x="-3.265" y="-0.75" dx="0.45" dy="0.35" layer="1" roundness="9" rot="R180"/>
<smd name="9" x="-3.265" y="-1.25" dx="0.45" dy="0.35" layer="1" roundness="9" rot="R180"/>
<smd name="10" x="-3.265" y="-1.75" dx="0.45" dy="0.35" layer="1" roundness="9" rot="R180"/>
<smd name="11" x="-3.265" y="-2.25" dx="0.45" dy="0.35" layer="1" roundness="9" rot="R180"/>
<smd name="12" x="-3.265" y="-2.75" dx="0.45" dy="0.35" layer="1" roundness="9" rot="R180"/>
<smd name="13" x="-2.75" y="-3.265" dx="0.45" dy="0.35" layer="1" roundness="9" rot="R270"/>
<smd name="14" x="-2.25" y="-3.265" dx="0.45" dy="0.35" layer="1" roundness="9" rot="R270"/>
<smd name="15" x="-1.75" y="-3.265" dx="0.45" dy="0.35" layer="1" roundness="9" rot="R270"/>
<smd name="16" x="-1.25" y="-3.265" dx="0.45" dy="0.35" layer="1" roundness="9" rot="R270"/>
<smd name="17" x="-0.75" y="-3.265" dx="0.45" dy="0.35" layer="1" roundness="9" rot="R270"/>
<smd name="18" x="-0.25" y="-3.265" dx="0.45" dy="0.35" layer="1" roundness="9" rot="R270"/>
<smd name="19" x="0.25" y="-3.265" dx="0.45" dy="0.35" layer="1" roundness="9" rot="R270"/>
<smd name="20" x="0.75" y="-3.265" dx="0.45" dy="0.35" layer="1" roundness="9" rot="R270"/>
<smd name="21" x="1.25" y="-3.265" dx="0.45" dy="0.35" layer="1" roundness="9" rot="R270"/>
<smd name="22" x="1.75" y="-3.265" dx="0.45" dy="0.35" layer="1" roundness="9" rot="R270"/>
<smd name="23" x="2.25" y="-3.265" dx="0.45" dy="0.35" layer="1" roundness="9" rot="R270"/>
<smd name="24" x="2.75" y="-3.265" dx="0.45" dy="0.35" layer="1" roundness="9" rot="R270"/>
<smd name="25" x="3.265" y="-2.75" dx="0.45" dy="0.35" layer="1" roundness="9"/>
<smd name="26" x="3.265" y="-2.25" dx="0.45" dy="0.35" layer="1" roundness="9"/>
<smd name="27" x="3.265" y="-1.75" dx="0.45" dy="0.35" layer="1" roundness="9"/>
<smd name="28" x="3.265" y="-1.25" dx="0.45" dy="0.35" layer="1" roundness="9"/>
<smd name="29" x="3.265" y="-0.75" dx="0.45" dy="0.35" layer="1" roundness="9"/>
<smd name="30" x="3.265" y="-0.25" dx="0.45" dy="0.35" layer="1" roundness="9"/>
<smd name="31" x="3.265" y="0.25" dx="0.45" dy="0.35" layer="1" roundness="9"/>
<smd name="32" x="3.265" y="0.75" dx="0.45" dy="0.35" layer="1" roundness="9"/>
<smd name="33" x="3.265" y="1.25" dx="0.45" dy="0.35" layer="1" roundness="9"/>
<smd name="34" x="3.265" y="1.75" dx="0.45" dy="0.35" layer="1" roundness="9"/>
<smd name="35" x="3.265" y="2.25" dx="0.45" dy="0.35" layer="1" roundness="9"/>
<smd name="36" x="3.265" y="2.75" dx="0.45" dy="0.35" layer="1" roundness="9"/>
<smd name="37" x="2.75" y="3.265" dx="0.45" dy="0.35" layer="1" roundness="9" rot="R90"/>
<smd name="38" x="2.25" y="3.265" dx="0.45" dy="0.35" layer="1" roundness="9" rot="R90"/>
<smd name="39" x="1.75" y="3.265" dx="0.45" dy="0.35" layer="1" roundness="9" rot="R90"/>
<smd name="40" x="1.25" y="3.265" dx="0.45" dy="0.35" layer="1" roundness="9" rot="R90"/>
<smd name="41" x="0.75" y="3.265" dx="0.45" dy="0.35" layer="1" roundness="9" rot="R90"/>
<smd name="42" x="0.25" y="3.265" dx="0.45" dy="0.35" layer="1" roundness="9" rot="R90"/>
<smd name="43" x="-0.25" y="3.265" dx="0.45" dy="0.35" layer="1" roundness="9" rot="R90"/>
<smd name="44" x="-0.75" y="3.265" dx="0.45" dy="0.35" layer="1" roundness="9" rot="R90"/>
<smd name="45" x="-1.25" y="3.265" dx="0.45" dy="0.35" layer="1" roundness="9" rot="R90"/>
<smd name="46" x="-1.75" y="3.265" dx="0.45" dy="0.35" layer="1" roundness="9" rot="R90"/>
<smd name="47" x="-2.25" y="3.265" dx="0.45" dy="0.35" layer="1" roundness="9" rot="R90"/>
<smd name="48" x="-2.75" y="3.265" dx="0.45" dy="0.35" layer="1" roundness="9" rot="R90"/>
<smd name="49" x="0" y="0" dx="5.4" dy="5.4" layer="1" cream="no"/>
</package>
</packages>
<packages3d>
<package3d name="PQFN50P700X700X104-49N" urn="urn:adsk.eagle:package:9948345/2" type="model">
<packageinstances>
<packageinstance name="PQFN50P700X700X104-49N"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="ESP32-PICO-D4">
<wire x1="15.24" y1="-35.56" x2="-15.24" y2="-35.56" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-35.56" x2="-15.24" y2="35.56" width="0.254" layer="94"/>
<wire x1="-15.24" y1="35.56" x2="15.24" y2="35.56" width="0.254" layer="94"/>
<wire x1="15.24" y1="35.56" x2="15.24" y2="-35.56" width="0.254" layer="94"/>
<text x="-15.2452" y="35.5722" size="1.778609375" layer="95" ratio="10">&gt;NAME</text>
<text x="-15.2569" y="-38.1422" size="1.77996875" layer="96" ratio="10">&gt;VALUE</text>
<pin name="VDDA" x="20.32" y="33.02" length="middle" direction="pwr" rot="R180"/>
<pin name="LNA_IN" x="20.32" y="12.7" length="middle" rot="R180"/>
<pin name="VDDA3P3" x="20.32" y="30.48" length="middle" direction="pwr" rot="R180"/>
<pin name="SENSOR_VP" x="-20.32" y="17.78" length="middle" direction="in"/>
<pin name="SENSOR_CAPP" x="-20.32" y="15.24" length="middle" direction="in"/>
<pin name="SENSOR_CAPN" x="-20.32" y="12.7" length="middle" direction="in"/>
<pin name="SENSOR_VN" x="-20.32" y="10.16" length="middle" direction="in"/>
<pin name="EN" x="-20.32" y="7.62" length="middle" direction="in"/>
<pin name="IO34" x="-20.32" y="2.54" length="middle" direction="in"/>
<pin name="IO35" x="-20.32" y="5.08" length="middle" direction="in"/>
<pin name="IO32" x="-20.32" y="-5.08" length="middle"/>
<pin name="IO33" x="-20.32" y="-2.54" length="middle"/>
<pin name="IO25" x="-20.32" y="-12.7" length="middle"/>
<pin name="IO26" x="-20.32" y="-10.16" length="middle"/>
<pin name="IO27" x="-20.32" y="-7.62" length="middle"/>
<pin name="IO14" x="20.32" y="-20.32" length="middle" rot="R180"/>
<pin name="IO12" x="20.32" y="-15.24" length="middle" rot="R180"/>
<pin name="VDD3P3_RTC" x="20.32" y="27.94" length="middle" direction="pwr" rot="R180"/>
<pin name="IO13" x="20.32" y="-17.78" length="middle" rot="R180"/>
<pin name="IO15" x="20.32" y="-22.86" length="middle" rot="R180"/>
<pin name="IO2" x="20.32" y="-7.62" length="middle" rot="R180"/>
<pin name="IO0" x="20.32" y="-5.08" length="middle" rot="R180"/>
<pin name="IO4" x="20.32" y="-10.16" length="middle" rot="R180"/>
<pin name="IO16" x="20.32" y="-25.4" length="middle" rot="R180"/>
<pin name="VDD_SDIO" x="20.32" y="25.4" length="middle" direction="pwr" rot="R180"/>
<pin name="IO17" x="-20.32" y="-27.94" length="middle"/>
<pin name="SD2" x="20.32" y="2.54" length="middle" rot="R180"/>
<pin name="SD3" x="20.32" y="5.08" length="middle" rot="R180"/>
<pin name="CMD" x="20.32" y="7.62" length="middle" rot="R180"/>
<pin name="CLK" x="20.32" y="10.16" length="middle" rot="R180"/>
<pin name="SD0" x="20.32" y="-2.54" length="middle" rot="R180"/>
<pin name="SD1" x="20.32" y="0" length="middle" rot="R180"/>
<pin name="IO5" x="20.32" y="-12.7" length="middle" rot="R180"/>
<pin name="IO18" x="-20.32" y="-25.4" length="middle"/>
<pin name="IO23" x="-20.32" y="-15.24" length="middle"/>
<pin name="VDD3P3_CPU" x="20.32" y="22.86" length="middle" direction="pwr" rot="R180"/>
<pin name="IO19" x="-20.32" y="-22.86" length="middle"/>
<pin name="IO22" x="-20.32" y="-17.78" length="middle"/>
<pin name="U0RXD" x="20.32" y="15.24" length="middle" rot="R180"/>
<pin name="U0TXD" x="20.32" y="17.78" length="middle" rot="R180"/>
<pin name="IO21" x="-20.32" y="-20.32" length="middle"/>
<pin name="GND" x="20.32" y="-33.02" length="middle" direction="pwr" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ESP32-PICO-D4" prefix="U">
<description>The ESP32-PICO-D4 is a System-in-Package (SIP) module that is based on ESP32, providing complete Wi-Fi
and Bluetooth functionalities. The module has a size as small as 7.0±0.1 mm×7.0±0.1 mm×0.94±0.1 mm, thus
requiring minimal PCB area. The module integrates a 4-MB SPI flash</description>
<gates>
<gate name="G$1" symbol="ESP32-PICO-D4" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PQFN50P700X700X104-49N">
<connects>
<connect gate="G$1" pin="CLK" pad="31"/>
<connect gate="G$1" pin="CMD" pad="30"/>
<connect gate="G$1" pin="EN" pad="9"/>
<connect gate="G$1" pin="GND" pad="49"/>
<connect gate="G$1" pin="IO0" pad="23"/>
<connect gate="G$1" pin="IO12" pad="18"/>
<connect gate="G$1" pin="IO13" pad="20"/>
<connect gate="G$1" pin="IO14" pad="17"/>
<connect gate="G$1" pin="IO15" pad="21"/>
<connect gate="G$1" pin="IO16" pad="25"/>
<connect gate="G$1" pin="IO17" pad="27"/>
<connect gate="G$1" pin="IO18" pad="35"/>
<connect gate="G$1" pin="IO19" pad="38"/>
<connect gate="G$1" pin="IO2" pad="22"/>
<connect gate="G$1" pin="IO21" pad="42"/>
<connect gate="G$1" pin="IO22" pad="39"/>
<connect gate="G$1" pin="IO23" pad="36"/>
<connect gate="G$1" pin="IO25" pad="14"/>
<connect gate="G$1" pin="IO26" pad="15"/>
<connect gate="G$1" pin="IO27" pad="16"/>
<connect gate="G$1" pin="IO32" pad="12"/>
<connect gate="G$1" pin="IO33" pad="13"/>
<connect gate="G$1" pin="IO34" pad="10"/>
<connect gate="G$1" pin="IO35" pad="11"/>
<connect gate="G$1" pin="IO4" pad="24"/>
<connect gate="G$1" pin="IO5" pad="34"/>
<connect gate="G$1" pin="LNA_IN" pad="2"/>
<connect gate="G$1" pin="SD0" pad="32"/>
<connect gate="G$1" pin="SD1" pad="33"/>
<connect gate="G$1" pin="SD2" pad="28"/>
<connect gate="G$1" pin="SD3" pad="29"/>
<connect gate="G$1" pin="SENSOR_CAPN" pad="7"/>
<connect gate="G$1" pin="SENSOR_CAPP" pad="6"/>
<connect gate="G$1" pin="SENSOR_VN" pad="8"/>
<connect gate="G$1" pin="SENSOR_VP" pad="5"/>
<connect gate="G$1" pin="U0RXD" pad="40"/>
<connect gate="G$1" pin="U0TXD" pad="41"/>
<connect gate="G$1" pin="VDD3P3_CPU" pad="37"/>
<connect gate="G$1" pin="VDD3P3_RTC" pad="19"/>
<connect gate="G$1" pin="VDDA" pad="1 43 46"/>
<connect gate="G$1" pin="VDDA3P3" pad="3 4"/>
<connect gate="G$1" pin="VDD_SDIO" pad="26"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:9948345/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="AVAILABILITY" value="Unavailable"/>
<attribute name="DESCRIPTION" value=" WIFi/Bluetooth SIP "/>
<attribute name="MF" value="Espressif Systems"/>
<attribute name="MP" value="ESP32-PICO-D4"/>
<attribute name="PACKAGE" value="SMD-48 Espressif Systems"/>
<attribute name="PRICE" value="None"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-PowerSymbols" urn="urn:adsk.eagle:library:530">
<description>&lt;h3&gt;SparkFun Power Symbols&lt;/h3&gt;
This library contains power, ground, and voltage-supply symbols.
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
</packages>
<symbols>
<symbol name="VCC" urn="urn:adsk.eagle:symbol:39418/1" library_version="1">
<description>&lt;h3&gt;VCC Voltage Supply&lt;/h3&gt;</description>
<wire x1="0.762" y1="1.27" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-0.762" y2="1.27" width="0.254" layer="94"/>
<pin name="VCC" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
<text x="0" y="2.794" size="1.778" layer="96" align="bottom-center">&gt;VALUE</text>
</symbol>
<symbol name="DGND" urn="urn:adsk.eagle:symbol:39415/1" library_version="1">
<description>&lt;h3&gt;Digital Ground Supply&lt;/h3&gt;</description>
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
<text x="0" y="-0.254" size="1.778" layer="96" align="top-center">&gt;VALUE</text>
</symbol>
<symbol name="V_USB" urn="urn:adsk.eagle:symbol:39429/1" library_version="1">
<description>&lt;h3&gt;USB Voltage Supply&lt;/h3&gt;</description>
<wire x1="0.762" y1="1.27" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-0.762" y2="1.27" width="0.254" layer="94"/>
<pin name="V_USB" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
<text x="0" y="2.794" size="1.778" layer="96" align="bottom-center">&gt;VALUE</text>
</symbol>
<symbol name="V_BATT" urn="urn:adsk.eagle:symbol:39428/1" library_version="1">
<description>&lt;h3&gt;Battery Voltage Supply&lt;/h3&gt;</description>
<wire x1="0.762" y1="1.27" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-0.762" y2="1.27" width="0.254" layer="94"/>
<pin name="V_BATT" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
<text x="0" y="2.794" size="1.778" layer="96" align="bottom-center">&gt;VALUE</text>
</symbol>
<symbol name="5V" urn="urn:adsk.eagle:symbol:39412/1" library_version="1">
<description>&lt;h3&gt;5V Voltage Supply&lt;/h3&gt;</description>
<wire x1="0.762" y1="1.27" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-0.762" y2="1.27" width="0.254" layer="94"/>
<pin name="5V" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
<text x="0" y="2.794" size="1.778" layer="96" align="bottom-center">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="VCC" urn="urn:adsk.eagle:component:39449/1" prefix="SUPPLY" library_version="1">
<description>&lt;h3&gt;VCC Voltage Supply&lt;/h3&gt;
&lt;p&gt;Positive voltage supply (traditionally for a BJT device, C=collector).&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="VCC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" urn="urn:adsk.eagle:component:39439/1" prefix="GND" library_version="1">
<description>&lt;h3&gt;Ground Supply Symbol&lt;/h3&gt;
&lt;p&gt;Generic signal ground supply symbol.&lt;/p&gt;</description>
<gates>
<gate name="1" symbol="DGND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="V_USB" urn="urn:adsk.eagle:component:39450/1" prefix="SUPPLY" library_version="1">
<description>&lt;h3&gt;USB Voltage Supply&lt;/h3&gt;</description>
<gates>
<gate name="G$1" symbol="V_USB" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="V_BATT" urn="urn:adsk.eagle:component:39448/1" prefix="SUPPLY" library_version="1">
<description>&lt;h3&gt;Battery Voltage Supply&lt;/h3&gt;
&lt;p&gt;Generic symbol for the battery input to a system.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="V_BATT" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="5V" urn="urn:adsk.eagle:component:39433/1" prefix="SUPPLY" library_version="1">
<description>&lt;h3&gt;5V Supply Symbol&lt;/h3&gt;
&lt;p&gt;Power supply symbol for a specifically-stated 5V source.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Coils">
<description>&lt;h3&gt;SparkFun Coils&lt;/h3&gt;
In this library you'll find magnetics.

&lt;p&gt;&lt;b&gt;SparkFun Products:&lt;/b&gt;
&lt;ul&gt;&lt;li&gt;Inductors&lt;/li&gt;
&lt;li&gt;Ferrite Beads&lt;/li&gt;
&lt;li&gt;Transformers&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;

&lt;br&gt;
&lt;p&gt;We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.&lt;/p&gt;</description>
<packages>
<package name="L0603" urn="urn:adsk.eagle:footprint:5829375/1" locally_modified="yes">
<description>&lt;b&gt;INDUCTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<wire x1="-1.397" y1="0.635" x2="1.397" y2="0.635" width="0.0508" layer="39"/>
<wire x1="1.397" y1="0.635" x2="1.397" y2="-0.635" width="0.0508" layer="39"/>
<wire x1="1.397" y1="-0.635" x2="-1.397" y2="-0.635" width="0.0508" layer="39"/>
<wire x1="-1.397" y1="-0.635" x2="-1.397" y2="0.635" width="0.0508" layer="39"/>
<wire x1="0" y1="0.508" x2="0" y2="-0.508" width="0.127" layer="21"/>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-0.889" y="0.889" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.889" y="-2.032" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="L0603" urn="urn:adsk.eagle:package:5829817/3" locally_modified="yes" type="model">
<description>&lt;b&gt;INDUCTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<packageinstances>
<packageinstance name="L0603"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="FERRITE_BEAD">
<description>&lt;h3&gt;Ferrite Bead (blocks, cores, rings, chokes, etc.)&lt;/h3&gt;
&lt;p&gt;Inductor with layers of ferrite used to suppress high frequencies. Often used to isolate high frequency noise.&lt;/p&gt;</description>
<text x="1.27" y="2.54" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="1.27" y="-2.54" size="1.778" layer="96" font="vector" align="top-left">&gt;VALUE</text>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<wire x1="0" y1="2.54" x2="0" y2="1.27" width="0.1524" layer="94" curve="-180"/>
<wire x1="0" y1="1.27" x2="0" y2="0" width="0.1524" layer="94" curve="-180"/>
<wire x1="0" y1="0" x2="0" y2="-1.27" width="0.1524" layer="94" curve="-180"/>
<wire x1="0" y1="-1.27" x2="0" y2="-2.54" width="0.1524" layer="94" curve="-180"/>
<wire x1="0.889" y1="2.54" x2="0.889" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="1.143" y1="2.54" x2="1.143" y2="-2.54" width="0.1524" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="FERRITE_BEAD" prefix="FB">
<description>&lt;h3&gt;Ferrite Bead (blocks, cores, rings, chokes, etc.)&lt;/h3&gt;
&lt;p&gt;Inductor with layers of ferrite used to suppress high frequencies. Often used to isolate high frequency noise.&lt;/p&gt;
&lt;p&gt;SparkFun Products:
&lt;ul&gt;&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/13613"&gt;IOIO-OTG - V2.2&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/13664"&gt;SparkFun SAMD21 Mini Breakout&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/13339"&gt;SparkFun 6 Degrees of Freedom Breakout - LSM6DS3&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/13672"&gt;SparkFun SAMD21 Dev Breakout&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="FERRITE_BEAD" x="0" y="0"/>
</gates>
<devices>
<device name="-0603" package="L0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:5829817/3"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="NDUC-13805"/>
<attribute name="VALUE" value="30Ω/1.8A"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Capacitors">
<description>&lt;h3&gt;SparkFun Capacitors&lt;/h3&gt;
This library contains capacitors. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="C0603" urn="urn:adsk.eagle:footprint:23123/1" locally_modified="yes">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.524" y1="0.635" x2="1.524" y2="0.635" width="0.0508" layer="39"/>
<wire x1="1.524" y1="0.635" x2="1.524" y2="-0.635" width="0.0508" layer="39"/>
<wire x1="1.524" y1="-0.635" x2="-1.524" y2="-0.635" width="0.0508" layer="39"/>
<wire x1="-1.524" y1="-0.635" x2="-1.524" y2="0.635" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="CAP-PTH-10MM">
<description>2 PTH spaced 10mm apart</description>
<wire x1="-0.5" y1="0.635" x2="-0.5" y2="0" width="0.2032" layer="21"/>
<pad name="1" x="-5" y="0" drill="0.9" diameter="1.651"/>
<pad name="2" x="5" y="0" drill="0.9" diameter="1.651"/>
<text x="0" y="1" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-1" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
<wire x1="-0.5" y1="0" x2="-0.5" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="0.5" y1="0.635" x2="0.5" y2="0" width="0.2032" layer="21"/>
<wire x1="0.5" y1="0" x2="0.5" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-0.5" y1="0" x2="-3.5" y2="0" width="0.2032" layer="21"/>
<wire x1="0.5" y1="0" x2="3.5" y2="0" width="0.2032" layer="21"/>
</package>
<package name="0402">
<description>&lt;p&gt;&lt;b&gt;Generic 1005 (0402) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<wire x1="-0.2704" y1="0.2286" x2="0.2704" y2="0.2286" width="0.1524" layer="51"/>
<wire x1="0.2704" y1="-0.2286" x2="-0.2704" y2="-0.2286" width="0.1524" layer="51"/>
<wire x1="-1.2" y1="0.65" x2="1.2" y2="0.65" width="0.0508" layer="39"/>
<wire x1="1.2" y1="0.65" x2="1.2" y2="-0.65" width="0.0508" layer="39"/>
<wire x1="1.2" y1="-0.65" x2="-1.2" y2="-0.65" width="0.0508" layer="39"/>
<wire x1="-1.2" y1="-0.65" x2="-1.2" y2="0.65" width="0.0508" layer="39"/>
<smd name="1" x="-0.58" y="0" dx="0.85" dy="0.9" layer="1"/>
<smd name="2" x="0.58" y="0" dx="0.85" dy="0.9" layer="1"/>
<text x="0" y="0.762" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.762" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.3048" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.3048" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="0805">
<description>&lt;p&gt;&lt;b&gt;Generic 2012 (0805) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<smd name="1" x="-0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<text x="0" y="0.889" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.889" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="-1.5" y1="0.8" x2="1.5" y2="0.8" width="0.0508" layer="39"/>
<wire x1="1.5" y1="0.8" x2="1.5" y2="-0.8" width="0.0508" layer="39"/>
<wire x1="1.5" y1="-0.8" x2="-1.5" y2="-0.8" width="0.0508" layer="39"/>
<wire x1="-1.5" y1="-0.8" x2="-1.5" y2="0.8" width="0.0508" layer="39"/>
</package>
<package name="1210">
<description>&lt;p&gt;&lt;b&gt;Generic 3225 (1210) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<wire x1="-1.5365" y1="1.1865" x2="1.5365" y2="1.1865" width="0.127" layer="51"/>
<wire x1="1.5365" y1="1.1865" x2="1.5365" y2="-1.1865" width="0.127" layer="51"/>
<wire x1="1.5365" y1="-1.1865" x2="-1.5365" y2="-1.1865" width="0.127" layer="51"/>
<wire x1="-1.5365" y1="-1.1865" x2="-1.5365" y2="1.1865" width="0.127" layer="51"/>
<smd name="1" x="-1.755" y="0" dx="1.27" dy="2.06" layer="1"/>
<smd name="2" x="1.755" y="0" dx="1.27" dy="2.06" layer="1"/>
<text x="0" y="1.397" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.397" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="-2.59" y1="1.45" x2="2.59" y2="1.45" width="0.0508" layer="39"/>
<wire x1="2.59" y1="1.45" x2="2.59" y2="-1.45" width="0.0508" layer="39"/>
<wire x1="2.59" y1="-1.45" x2="-2.59" y2="-1.45" width="0.0508" layer="39"/>
<wire x1="-2.59" y1="-1.45" x2="-2.59" y2="1.45" width="0.0508" layer="39"/>
</package>
<package name="CAP-PTH-SMALL-KIT">
<description>&lt;h3&gt;CAP-PTH-SMALL-KIT&lt;/h3&gt;
Commonly used for small ceramic capacitors. Like our 0.1uF (http://www.sparkfun.com/products/8375) or 22pF caps (http://www.sparkfun.com/products/8571).&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of this package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.</description>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.254" layer="21"/>
<wire x1="-2.667" y1="1.27" x2="2.667" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.667" y1="1.27" x2="2.667" y2="-1.27" width="0.254" layer="21"/>
<wire x1="2.667" y1="-1.27" x2="-2.667" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-2.667" y1="-1.27" x2="-2.667" y2="1.27" width="0.254" layer="21"/>
<pad name="1" x="-1.397" y="0" drill="1.016" diameter="2.032" stop="no"/>
<pad name="2" x="1.397" y="0" drill="1.016" diameter="2.032" stop="no"/>
<polygon width="0.127" layer="30">
<vertex x="-1.4021" y="-0.9475" curve="-90"/>
<vertex x="-2.357" y="-0.0178" curve="-90.011749"/>
<vertex x="-1.4046" y="0.9576" curve="-90"/>
<vertex x="-0.4546" y="-0.0204" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-1.4046" y="-0.4395" curve="-90.012891"/>
<vertex x="-1.8491" y="-0.0153" curve="-90"/>
<vertex x="-1.4046" y="0.452" curve="-90"/>
<vertex x="-0.9627" y="-0.0051" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="1.397" y="-0.9475" curve="-90"/>
<vertex x="0.4421" y="-0.0178" curve="-90.011749"/>
<vertex x="1.3945" y="0.9576" curve="-90"/>
<vertex x="2.3445" y="-0.0204" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="1.3945" y="-0.4395" curve="-90.012891"/>
<vertex x="0.95" y="-0.0153" curve="-90"/>
<vertex x="1.3945" y="0.452" curve="-90"/>
<vertex x="1.8364" y="-0.0051" curve="-90.012967"/>
</polygon>
</package>
<package name="C1206" urn="urn:adsk.eagle:footprint:23125/1" locally_modified="yes">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.286" y1="1.016" x2="2.286" y2="1.016" width="0.0508" layer="39"/>
<wire x1="2.286" y1="-1.016" x2="-2.286" y2="-1.016" width="0.0508" layer="39"/>
<wire x1="-2.286" y1="-1.016" x2="-2.286" y2="1.016" width="0.0508" layer="39"/>
<wire x1="2.286" y1="1.016" x2="2.286" y2="-1.016" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
</packages>
<packages3d>
<package3d name="C0603" urn="urn:adsk.eagle:package:23616/2" locally_modified="yes" type="model">
<description>CAPACITOR</description>
<packageinstances>
<packageinstance name="C0603"/>
</packageinstances>
</package3d>
<package3d name="C1206" urn="urn:adsk.eagle:package:23618/2" locally_modified="yes" type="model">
<description>CAPACITOR</description>
<packageinstances>
<packageinstance name="C1206"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="CAP">
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<text x="1.524" y="2.921" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="4.7PF" prefix="C">
<description>&lt;h3&gt;4.7pF ceramic capacitors&lt;/h3&gt;
&lt;p&gt;A capacitor is a passive two-terminal electrical component used to store electrical energy temporarily in an electric field.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="-0603-50V-5%" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23616/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-07880"/>
<attribute name="VALUE" value="4.7pF"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="0.1UF" prefix="C">
<description>&lt;h3&gt;0.1µF ceramic capacitors&lt;/h3&gt;
&lt;p&gt;A capacitor is a passive two-terminal electrical component used to store electrical energy temporarily in an electric field.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="-0402-16V-10%" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-12416"/>
<attribute name="VALUE" value="0.1uF"/>
</technology>
</technologies>
</device>
<device name="-0603-25V-(+80/-20%)" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23616/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-00810"/>
<attribute name="VALUE" value="0.1uF"/>
</technology>
</technologies>
</device>
<device name="-0603-25V-5%" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23616/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-08604"/>
<attribute name="VALUE" value="0.1uF"/>
</technology>
</technologies>
</device>
<device name="-KIT-EZ-50V-20%" package="CAP-PTH-SMALL-KIT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-08370"/>
<attribute name="VALUE" value="0.1uF"/>
</technology>
</technologies>
</device>
<device name="-0603-100V-10%" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23616/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-08390"/>
<attribute name="VALUE" value="0.1uF"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="47PF" prefix="C">
<description>&lt;h3&gt;47pF ceramic capacitors&lt;/h3&gt;
&lt;p&gt;A capacitor is a passive two-terminal electrical component used to store electrical energy temporarily in an electric field.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="-0603-50V-5%" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23616/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-08913"/>
<attribute name="VALUE" value="47pF"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="2.2UF" prefix="C">
<description>&lt;h3&gt;2.2µF ceramic capacitors&lt;/h3&gt;
&lt;p&gt;A capacitor is a passive two-terminal electrical component used to store electrical energy temporarily in an electric field.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="-0603-10V-20%" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23616/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-07888" constant="no"/>
<attribute name="VALUE" value="2.2uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0805-25V-(+80/-20%)" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-11624"/>
<attribute name="VALUE" value="2.2uF"/>
</technology>
</technologies>
</device>
<device name="-1206-50V-10%" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23618/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-10009"/>
<attribute name="VALUE" value="2.2uF"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="10UF" prefix="C">
<description>&lt;h3&gt;10.0µF ceramic capacitors&lt;/h3&gt;
&lt;p&gt;A capacitor is a passive two-terminal electrical component used to store electrical energy temporarily in an electric field.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="-0603-6.3V-20%" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23616/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-11015"/>
<attribute name="VALUE" value="10uF"/>
</technology>
</technologies>
</device>
<device name="-1206-6.3V-20%" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23618/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-10057"/>
<attribute name="VALUE" value="10uF"/>
</technology>
</technologies>
</device>
<device name="-0805-10V-10%" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-11330"/>
<attribute name="VALUE" value="10uF"/>
</technology>
</technologies>
</device>
<device name="-1210-50V-20%" package="1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-09824"/>
<attribute name="VALUE" value="10uF"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="1.0UF" prefix="C">
<description>&lt;h3&gt;1µF ceramic capacitors&lt;/h3&gt;
&lt;p&gt;A capacitor is a passive two-terminal electrical component used to store electrical energy temporarily in an electric field.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="-0603-16V-10%" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23616/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-00868"/>
<attribute name="VALUE" value="1.0uF"/>
</technology>
</technologies>
</device>
<device name="-0402-16V-10%" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-12417"/>
<attribute name="VALUE" value="1.0uF"/>
</technology>
</technologies>
</device>
<device name="-0805-25V-(+80/-20%)" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-11625"/>
<attribute name="VALUE" value="1.0uF"/>
</technology>
</technologies>
</device>
<device name="-1206-50V-10%" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23618/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-09822"/>
<attribute name="VALUE" value="1.0uF"/>
</technology>
</technologies>
</device>
<device name="-0805-25V-10%" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-08064"/>
<attribute name="VALUE" value="1.0uF"/>
</technology>
</technologies>
</device>
<device name="-0603-16V-10%-X7R" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23616/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-13930"/>
<attribute name="VALUE" value="1.0uF"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="10NF" prefix="C">
<description>&lt;h3&gt;0.01uF/10nF/10,000pF ceramic capacitors&lt;/h3&gt;
&lt;p&gt;A capacitor is a passive two-terminal electrical component used to store electrical energy temporarily in an electric field.&lt;/p&gt;

CAP-09321</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="-PTH-10MM-10000V-1-%" package="CAP-PTH-10MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-09321"/>
<attribute name="VALUE" value="10nF"/>
</technology>
</technologies>
</device>
<device name="-0603-50V-10%" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23616/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-00867"/>
<attribute name="VALUE" value="10nF"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="4.7UF" prefix="C">
<description>&lt;h3&gt;4.7µF ceramic capacitors&lt;/h3&gt;
&lt;p&gt;A capacitor is a passive two-terminal electrical component used to store electrical energy temporarily in an electric field.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="-2.54"/>
</gates>
<devices>
<device name="-0603-6.3V-(10%)" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23616/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-08280"/>
<attribute name="VALUE" value="4.7uF"/>
</technology>
</technologies>
</device>
<device name="-1206-16V-(+80/-20%)" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23618/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-10300" constant="no"/>
<attribute name="VALUE" value="4.7uF" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="0.22UF" prefix="C">
<description>&lt;h3&gt;0.22µF ceramic capacitors&lt;/h3&gt;
&lt;p&gt;A capacitor is a passive two-terminal electrical component used to store electrical energy temporarily in an electric field.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="-0603-25V-10%" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23616/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-07822" constant="no"/>
<attribute name="VALUE" value="0.22uF" constant="no"/>
</technology>
</technologies>
</device>
<device name="-0603-50V-(-20/+80%)" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23616/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CAP-09885"/>
<attribute name="VALUE" value="0.22uF"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="PRO-OB-440">
<description>&lt;OnBoard SMD  2400 antenna&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="PRO-OB-440_1" urn="urn:adsk.eagle:footprint:9989955/1">
<description>&lt;b&gt;PRO-OB-440&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="1.865" y="-2.65" dx="2" dy="1.8" layer="1"/>
<smd name="2" x="1.865" y="-4.85" dx="2" dy="1.8" layer="1"/>
<smd name="3" x="-1.865" y="-2.65" dx="2.6" dy="2.25" layer="1"/>
<smd name="4" x="-1.865" y="-13.35" dx="2.8" dy="2.45" layer="1"/>
<text x="-0.366" y="-6.368" size="1.27" layer="27" align="center">&gt;VALUE</text>
<text x="-0.366" y="-6.368" size="1.27" layer="25" align="center">&gt;NAME</text>
<wire x1="-2.615" y1="0" x2="2.615" y2="0" width="0.2" layer="51"/>
<wire x1="2.615" y1="0" x2="2.615" y2="-13.75" width="0.2" layer="51"/>
<wire x1="2.615" y1="-13.75" x2="-2.615" y2="-13.75" width="0.2" layer="51"/>
<wire x1="-2.615" y1="-13.75" x2="-2.615" y2="0" width="0.2" layer="51"/>
<wire x1="2.615" y1="-13.75" x2="2.615" y2="-6.35" width="0.2" layer="21"/>
<wire x1="2.615" y1="0" x2="-2.615" y2="0" width="0.2" layer="21"/>
<wire x1="-2.615" y1="-4.126" x2="-2.615" y2="-11.766" width="0.2" layer="21"/>
<wire x1="2.615" y1="-13.75" x2="0" y2="-13.75" width="0.2" layer="21"/>
</package>
</packages>
<packages3d>
<package3d name="PRO-OB-440_1" urn="urn:adsk.eagle:package:9989957/2" type="model">
<description>&lt;b&gt;PRO-OB-440&lt;/b&gt;&lt;br&gt;
</description>
<packageinstances>
<packageinstance name="PRO-OB-440_1"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="PRO-OB-440">
<wire x1="5.08" y1="2.54" x2="27.94" y2="2.54" width="0.254" layer="94"/>
<wire x1="27.94" y1="-5.08" x2="27.94" y2="2.54" width="0.254" layer="94"/>
<wire x1="27.94" y1="-5.08" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<text x="29.21" y="7.62" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="29.21" y="5.08" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="GND" x="0" y="0" length="middle"/>
<pin name="RF_FEED" x="0" y="-2.54" length="middle"/>
<pin name="NC_1" x="33.02" y="0" length="middle" rot="R180"/>
<pin name="NC_2" x="33.02" y="-2.54" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PRO-OB-440" prefix="ANT">
<description>&lt;b&gt;OnBoard SMD  2400 antenna&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://www.proant.se/files/user/Datasheet - OnBoard SMD 2400 MHz rev 2.0.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="PRO-OB-440" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PRO-OB-440_1">
<connects>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="NC_1" pad="3"/>
<connect gate="G$1" pin="NC_2" pad="4"/>
<connect gate="G$1" pin="RF_FEED" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:9989957/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="OnBoard SMD  2400 antenna" constant="no"/>
<attribute name="HEIGHT" value="mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Proant AB" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="PRO-OB-440" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="" constant="no"/>
<attribute name="RS_PART_NUMBER" value="" constant="no"/>
<attribute name="RS_PRICE-STOCK" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Resistors">
<description>&lt;h3&gt;SparkFun Resistors&lt;/h3&gt;
This library contains resistors. Reference designator:R. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="R0603" urn="urn:adsk.eagle:footprint:23044/1" locally_modified="yes">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<wire x1="-1.397" y1="0.635" x2="1.397" y2="0.635" width="0.0508" layer="39"/>
<wire x1="1.397" y1="0.635" x2="1.397" y2="-0.635" width="0.0508" layer="39"/>
<wire x1="1.397" y1="-0.635" x2="-1.397" y2="-0.635" width="0.0508" layer="39"/>
<wire x1="-1.397" y1="-0.635" x2="-1.397" y2="0.635" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="0402">
<description>&lt;p&gt;&lt;b&gt;Generic 1005 (0402) package&lt;/b&gt;&lt;/p&gt;
&lt;p&gt;0.2mm courtyard excess rounded to nearest 0.05mm.&lt;/p&gt;</description>
<wire x1="-0.2704" y1="0.2286" x2="0.2704" y2="0.2286" width="0.1524" layer="51"/>
<wire x1="0.2704" y1="-0.2286" x2="-0.2704" y2="-0.2286" width="0.1524" layer="51"/>
<wire x1="-1.2" y1="0.65" x2="1.2" y2="0.65" width="0.0508" layer="39"/>
<wire x1="1.2" y1="0.65" x2="1.2" y2="-0.65" width="0.0508" layer="39"/>
<wire x1="1.2" y1="-0.65" x2="-1.2" y2="-0.65" width="0.0508" layer="39"/>
<wire x1="-1.2" y1="-0.65" x2="-1.2" y2="0.65" width="0.0508" layer="39"/>
<smd name="1" x="-0.58" y="0" dx="0.85" dy="0.9" layer="1"/>
<smd name="2" x="0.58" y="0" dx="0.85" dy="0.9" layer="1"/>
<text x="0" y="0.762" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.762" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.3048" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.3048" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="AXIAL-0.3">
<description>&lt;h3&gt;AXIAL-0.3&lt;/h3&gt;
&lt;p&gt;Commonly used for 1/4W through-hole resistors. 0.3" pitch between holes.&lt;/p&gt;</description>
<wire x1="-2.54" y1="0.762" x2="2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0.762" x2="2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="-2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="3.81" y="0" drill="0.9" diameter="1.8796"/>
<text x="0" y="1.016" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-1.016" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
</package>
<package name="AXIAL-0.1">
<description>&lt;h3&gt;AXIAL-0.1&lt;/h3&gt;
&lt;p&gt;Commonly used for 1/4W through-hole resistors. 0.1" pitch between holes.&lt;/p&gt;</description>
<wire x1="0" y1="-0.762" x2="0" y2="0" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="0.762" width="0.2032" layer="21"/>
<wire x1="0.254" y1="0" x2="0" y2="0" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="-0.254" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-1.27" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="1.27" y="0" drill="0.9" diameter="1.8796"/>
<text x="0" y="1.143" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-1.143" size="0.6096" layer="21" font="vector" ratio="20" align="top-center">&gt;Value</text>
</package>
<package name="AXIAL-0.1-KIT">
<description>&lt;h3&gt;AXIAL-0.1-KIT&lt;/h3&gt;
&lt;p&gt;Commonly used for 1/4W through-hole resistors. 0.1" pitch between holes.&lt;/p&gt;
&lt;p&gt;&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of the AXIAL-0.1 package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.&lt;/p&gt;</description>
<wire x1="0" y1="-0.762" x2="0" y2="0" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="0.762" width="0.2032" layer="21"/>
<wire x1="0.254" y1="0" x2="0" y2="0" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="-0.254" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-1.27" y="0" drill="0.9" diameter="1.8796" stop="no"/>
<pad name="P$2" x="1.27" y="0" drill="0.9" diameter="1.8796" stop="no"/>
<text x="0" y="1.143" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-1.143" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
<circle x="-1.27" y="0" radius="0.4572" width="0" layer="29"/>
<circle x="-1.27" y="0" radius="1.016" width="0" layer="30"/>
<circle x="1.27" y="0" radius="1.016" width="0" layer="30"/>
<circle x="-1.27" y="0" radius="0.4572" width="0" layer="29"/>
<circle x="1.27" y="0" radius="0.4572" width="0" layer="29"/>
</package>
<package name="AXIAL-0.3-KIT">
<description>&lt;h3&gt;AXIAL-0.3-KIT&lt;/h3&gt;
&lt;p&gt;Commonly used for 1/4W through-hole resistors. 0.3" pitch between holes.&lt;/p&gt;
&lt;p&gt;&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of the AXIAL-0.3 package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.&lt;/p&gt;</description>
<wire x1="-2.54" y1="1.27" x2="2.54" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="0" width="0.254" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="-2.54" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="0" width="0.254" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.254" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.254" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="1.016" diameter="2.032" stop="no"/>
<pad name="P$2" x="3.81" y="0" drill="1.016" diameter="2.032" stop="no"/>
<text x="0" y="1.524" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.524" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<polygon width="0.127" layer="30">
<vertex x="3.8201" y="-0.9449" curve="-90"/>
<vertex x="2.8652" y="-0.0152" curve="-90.011749"/>
<vertex x="3.8176" y="0.9602" curve="-90"/>
<vertex x="4.7676" y="-0.0178" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="3.8176" y="-0.4369" curve="-90.012891"/>
<vertex x="3.3731" y="-0.0127" curve="-90"/>
<vertex x="3.8176" y="0.4546" curve="-90"/>
<vertex x="4.2595" y="-0.0025" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="-3.8075" y="-0.9525" curve="-90"/>
<vertex x="-4.7624" y="-0.0228" curve="-90.011749"/>
<vertex x="-3.81" y="0.9526" curve="-90"/>
<vertex x="-2.86" y="-0.0254" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-3.81" y="-0.4445" curve="-90.012891"/>
<vertex x="-4.2545" y="-0.0203" curve="-90"/>
<vertex x="-3.81" y="0.447" curve="-90"/>
<vertex x="-3.3681" y="-0.0101" curve="-90.012967"/>
</polygon>
</package>
</packages>
<packages3d>
<package3d name="R0603" urn="urn:adsk.eagle:package:23555/3" locally_modified="yes" type="model">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R0603"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="0" y="1.524" size="1.778" layer="95" font="vector" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.524" size="1.778" layer="96" font="vector" align="top-center">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="10KOHM" prefix="R">
<description>&lt;h3&gt;10kΩ resistor&lt;/h3&gt;
&lt;p&gt;A resistor is a passive two-terminal electrical component that implements electrical resistance as a circuit element. Resistors act to reduce current flow, and, at the same time, act to lower voltage levels within circuits. - Wikipedia&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="-HORIZ-1/4W-1%" package="AXIAL-0.3">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-12183" constant="no"/>
<attribute name="VALUE" value="10k" constant="no"/>
</technology>
</technologies>
</device>
<device name="-VERT-1/4W-1%" package="AXIAL-0.1">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-12183"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
<device name="-VERT_KIT-1/4W-1%" package="AXIAL-0.1-KIT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-12183" constant="no"/>
<attribute name="VALUE" value="10k" constant="no"/>
</technology>
</technologies>
</device>
<device name="-VERT-1/4W-5%" package="AXIAL-0.1">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-09435"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
<device name="-VERT_KIT-1/4W-5%" package="AXIAL-0.1-KIT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-09435"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
<device name="-HORIZ-1/4W-5%" package="AXIAL-0.3">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-09435"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
<device name="-HORIZ_KIT-1/4W-5%" package="AXIAL-0.3-KIT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-09435"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
<device name="-HORIZ_KIT-1/4W-1%" package="AXIAL-0.3-KIT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-12183"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
<device name="-VERT-1/6W-5%" package="AXIAL-0.1">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-08375"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
<device name="-VERT_KIT-1/6W-5%" package="AXIAL-0.1-KIT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-08375"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
<device name="-HORIZ-1/6W-5%" package="AXIAL-0.3">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-08375"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
<device name="-HORIZ_KIT-1/6W-5%" package="AXIAL-0.3-KIT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-08375"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
<device name="-0603-1/10W-1%" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23555/3"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-00824"/>
<attribute name="VALUE" value="10k"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="27OHM" prefix="R">
<description>&lt;h3&gt;27Ω resistor&lt;/h3&gt;
&lt;p&gt;A resistor is a passive two-terminal electrical component that implements electrical resistance as a circuit element. Resistors act to reduce current flow, and, at the same time, act to lower voltage levels within circuits. - Wikipedia&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="-0603-1/10W-1%" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23555/3"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-09334"/>
<attribute name="VALUE" value="27"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="1KOHM" prefix="R">
<description>&lt;h3&gt;1kΩ resistor&lt;/h3&gt;
&lt;p&gt;A resistor is a passive two-terminal electrical component that implements electrical resistance as a circuit element. Resistors act to reduce current flow, and, at the same time, act to lower voltage levels within circuits. - Wikipedia&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="-HORIZ-1/4W-1%" package="AXIAL-0.3">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-12182"/>
<attribute name="VALUE" value="1k"/>
</technology>
</technologies>
</device>
<device name="-VERT-1/4W-1%" package="AXIAL-0.1">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-12182"/>
<attribute name="VALUE" value="1k"/>
</technology>
</technologies>
</device>
<device name="-VERT_KIT-1/4W-1%" package="AXIAL-0.1-KIT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-12182"/>
<attribute name="VALUE" value="1k"/>
</technology>
</technologies>
</device>
<device name="-HORIZ_KIT-1/4W-1%" package="AXIAL-0.3-KIT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-12182"/>
<attribute name="VALUE" value="1k"/>
</technology>
</technologies>
</device>
<device name="-VERT-1/4W-5%" package="AXIAL-0.1">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-08380"/>
<attribute name="VALUE" value="1k"/>
</technology>
</technologies>
</device>
<device name="-VERT_KIT-1/4W-5%" package="AXIAL-0.1-KIT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-08380"/>
<attribute name="VALUE" value="1k"/>
</technology>
</technologies>
</device>
<device name="-HORIZ-1/4W-5%" package="AXIAL-0.3">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-08380"/>
<attribute name="VALUE" value="1k"/>
</technology>
</technologies>
</device>
<device name="-HORIZ_KIT-1/4W-5%" package="AXIAL-0.3-KIT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-08380"/>
<attribute name="VALUE" value="1k"/>
</technology>
</technologies>
</device>
<device name="-VERT-1/6W-5%" package="AXIAL-0.1">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-09769"/>
<attribute name="VALUE" value="1k"/>
</technology>
</technologies>
</device>
<device name="-0603-1/10W-1%" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23555/3"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-07856"/>
<attribute name="VALUE" value="1k"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="330KOHM" prefix="R">
<description>&lt;h3&gt;330kΩ resistor&lt;/h3&gt;
&lt;p&gt;A resistor is a passive two-terminal electrical component that implements electrical resistance as a circuit element. Resistors act to reduce current flow, and, at the same time, act to lower voltage levels within circuits. - Wikipedia&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="-0603-1/10W-1%" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23555/3"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-08968"/>
<attribute name="VALUE" value="330k"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="47KOHM" prefix="R">
<description>&lt;h3&gt;47kΩ resistor&lt;/h3&gt;
&lt;p&gt;A resistor is a passive two-terminal electrical component that implements electrical resistance as a circuit element. Resistors act to reduce current flow, and, at the same time, act to lower voltage levels within circuits. - Wikipedia&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="-0603-1/10W-1%" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23555/3"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-07871"/>
<attribute name="VALUE" value="47k"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="200KOHM" prefix="R">
<description>&lt;h3&gt;200kΩ resistor&lt;/h3&gt;
&lt;p&gt;A resistor is a passive two-terminal electrical component that implements electrical resistance as a circuit element. Resistors act to reduce current flow, and, at the same time, act to lower voltage levels within circuits. - Wikipedia&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="-0603-1/10W-1%" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23555/3"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-09385"/>
<attribute name="VALUE" value="200k"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="22OHM" prefix="R">
<description>&lt;h3&gt;22Ω resistor&lt;/h3&gt;
&lt;p&gt;A resistor is a passive two-terminal electrical component that implements electrical resistance as a circuit element. Resistors act to reduce current flow, and, at the same time, act to lower voltage levels within circuits. - Wikipedia&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="-0402-1/10W-1%" package="0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-12427"/>
<attribute name="VALUE" value="22"/>
</technology>
</technologies>
</device>
<device name="-0603-1/10W-1%" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23555/3"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="RES-08698"/>
<attribute name="VALUE" value="22"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="FT231XS-U">
<packages>
<package name="SOP20" urn="urn:adsk.eagle:footprint:30925/1">
<description>&lt;b&gt;SMALL OUTLINE PACKAGE&lt;/b&gt;</description>
<wire x1="-6.9" y1="4.9" x2="6.9" y2="4.9" width="0.1998" layer="39"/>
<wire x1="6.9" y1="4.9" x2="6.9" y2="-4.9" width="0.1998" layer="39"/>
<wire x1="-6.9" y1="-4.9" x2="-6.9" y2="4.9" width="0.1998" layer="39"/>
<wire x1="6.88" y1="3.01" x2="6.88" y2="-3.01" width="0.2032" layer="51"/>
<wire x1="6.88" y1="-3.01" x2="-6.88" y2="-3.01" width="0.2032" layer="51"/>
<wire x1="-6.88" y1="-3.01" x2="-6.88" y2="3.01" width="0.2032" layer="51"/>
<wire x1="-6.88" y1="3.01" x2="6.88" y2="3.01" width="0.2032" layer="51"/>
<wire x1="6.9" y1="-4.9" x2="-6.9" y2="-4.9" width="0.1998" layer="39"/>
<smd name="1" x="-5.715" y="-3.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="2" x="-4.445" y="-3.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="3" x="-3.175" y="-3.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="4" x="-1.905" y="-3.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="5" x="-0.635" y="-3.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="6" x="0.635" y="-3.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="7" x="1.905" y="-3.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="8" x="3.175" y="-3.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="9" x="4.445" y="-3.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="10" x="5.715" y="-3.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="11" x="5.715" y="3.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="12" x="4.445" y="3.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="13" x="3.175" y="3.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="14" x="1.905" y="3.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="15" x="0.635" y="3.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="16" x="-0.635" y="3.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="17" x="-1.905" y="3.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="18" x="-3.175" y="3.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="19" x="-4.445" y="3.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="20" x="-5.715" y="3.6" dx="0.6" dy="2.2" layer="1"/>
<text x="-5.715" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.715" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<text x="-5.08" y="-2.54" size="1.27" layer="21" rot="R90">1</text>
<rectangle x1="-5.9649" y1="-4.4399" x2="-5.4651" y2="-3.11" layer="51"/>
<rectangle x1="-4.6949" y1="-4.4399" x2="-4.1951" y2="-3.11" layer="51"/>
<rectangle x1="-3.4249" y1="-4.4399" x2="-2.9251" y2="-3.11" layer="51"/>
<rectangle x1="-2.1549" y1="-4.4399" x2="-1.6551" y2="-3.11" layer="51"/>
<rectangle x1="-0.8849" y1="-4.4399" x2="-0.3851" y2="-3.11" layer="51"/>
<rectangle x1="0.3851" y1="-4.4399" x2="0.8849" y2="-3.11" layer="51"/>
<rectangle x1="1.6551" y1="-4.4399" x2="2.1549" y2="-3.11" layer="51"/>
<rectangle x1="2.9251" y1="-4.4399" x2="3.4249" y2="-3.11" layer="51"/>
<rectangle x1="4.1951" y1="-4.4399" x2="4.6949" y2="-3.11" layer="51"/>
<rectangle x1="5.4651" y1="-4.4399" x2="5.9649" y2="-3.11" layer="51"/>
<rectangle x1="5.4651" y1="3.11" x2="5.9649" y2="4.4399" layer="51"/>
<rectangle x1="4.1951" y1="3.11" x2="4.6949" y2="4.4399" layer="51"/>
<rectangle x1="2.9251" y1="3.11" x2="3.4249" y2="4.4399" layer="51"/>
<rectangle x1="1.6551" y1="3.11" x2="2.1549" y2="4.4399" layer="51"/>
<rectangle x1="0.3851" y1="3.11" x2="0.8849" y2="4.4399" layer="51"/>
<rectangle x1="-0.8849" y1="3.11" x2="-0.3851" y2="4.4399" layer="51"/>
<rectangle x1="-2.1549" y1="3.11" x2="-1.6551" y2="4.4399" layer="51"/>
<rectangle x1="-3.4249" y1="3.11" x2="-2.9251" y2="4.4399" layer="51"/>
<rectangle x1="-4.6949" y1="3.11" x2="-4.1951" y2="4.4399" layer="51"/>
<rectangle x1="-5.9649" y1="3.11" x2="-5.4651" y2="4.4399" layer="51"/>
</package>
<package name="SSOP20" urn="urn:adsk.eagle:footprint:4172/1" locally_modified="yes">
<description>&lt;B&gt;Plastic Shrink Small Outline Package&lt;/B&gt;</description>
<wire x1="-3.556" y1="-2.591" x2="3.683" y2="-2.591" width="0.1524" layer="21"/>
<wire x1="3.683" y1="2.489" x2="3.683" y2="-2.591" width="0.1524" layer="21"/>
<wire x1="3.683" y1="2.489" x2="-3.556" y2="2.489" width="0.1524" layer="21"/>
<wire x1="-3.556" y1="-2.591" x2="-3.556" y2="2.489" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-2.337" x2="3.429" y2="-2.337" width="0.0508" layer="21"/>
<wire x1="3.429" y1="2.235" x2="3.429" y2="-2.337" width="0.0508" layer="21"/>
<wire x1="3.429" y1="2.235" x2="-3.302" y2="2.235" width="0.0508" layer="21"/>
<wire x1="-3.302" y1="-2.337" x2="-3.302" y2="2.235" width="0.0508" layer="21"/>
<circle x="-2.286" y="-1.4478" radius="0.635" width="0.1524" layer="21"/>
<smd name="1" x="-2.794" y="-3.3528" dx="0.3048" dy="1.2446" layer="1"/>
<smd name="2" x="-2.159" y="-3.3528" dx="0.3048" dy="1.2446" layer="1"/>
<smd name="3" x="-1.524" y="-3.3528" dx="0.3048" dy="1.2446" layer="1"/>
<smd name="4" x="-0.889" y="-3.3528" dx="0.3048" dy="1.2446" layer="1"/>
<smd name="5" x="-0.254" y="-3.3528" dx="0.3048" dy="1.2446" layer="1"/>
<smd name="6" x="0.381" y="-3.3528" dx="0.3048" dy="1.2446" layer="1"/>
<smd name="7" x="1.016" y="-3.3528" dx="0.3048" dy="1.2446" layer="1"/>
<smd name="8" x="1.651" y="-3.3528" dx="0.3048" dy="1.2446" layer="1"/>
<smd name="9" x="2.286" y="-3.3528" dx="0.3048" dy="1.2446" layer="1"/>
<smd name="10" x="2.921" y="-3.3528" dx="0.3048" dy="1.2446" layer="1"/>
<smd name="20" x="-2.794" y="3.2512" dx="0.3048" dy="1.2446" layer="1"/>
<smd name="19" x="-2.159" y="3.2512" dx="0.3048" dy="1.2446" layer="1"/>
<smd name="18" x="-1.524" y="3.2512" dx="0.3048" dy="1.2446" layer="1"/>
<smd name="17" x="-0.889" y="3.2512" dx="0.3048" dy="1.2446" layer="1"/>
<smd name="16" x="-0.254" y="3.2512" dx="0.3048" dy="1.2446" layer="1"/>
<smd name="15" x="0.381" y="3.2512" dx="0.3048" dy="1.2446" layer="1"/>
<smd name="14" x="1.016" y="3.2512" dx="0.3048" dy="1.2446" layer="1"/>
<smd name="13" x="1.651" y="3.2512" dx="0.3048" dy="1.2446" layer="1"/>
<smd name="12" x="2.286" y="3.2512" dx="0.3048" dy="1.2446" layer="1"/>
<smd name="11" x="2.921" y="3.2512" dx="0.3048" dy="1.2446" layer="1"/>
<text x="-3.8862" y="-2.0574" size="1.016" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-2.6924" y="-0.0762" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.8194" y1="2.54" x2="3.0226" y2="3.3782" layer="51"/>
<rectangle x1="2.1844" y1="2.54" x2="2.3876" y2="3.3782" layer="51"/>
<rectangle x1="1.5494" y1="2.54" x2="1.7526" y2="3.3782" layer="51"/>
<rectangle x1="0.9144" y1="2.54" x2="1.1176" y2="3.3782" layer="51"/>
<rectangle x1="0.2794" y1="2.54" x2="0.4826" y2="3.3782" layer="51"/>
<rectangle x1="-0.3556" y1="2.54" x2="-0.1524" y2="3.3782" layer="51"/>
<rectangle x1="-0.9906" y1="2.54" x2="-0.7874" y2="3.3782" layer="51"/>
<rectangle x1="-1.6256" y1="2.54" x2="-1.4224" y2="3.3782" layer="51"/>
<rectangle x1="-2.2606" y1="2.54" x2="-2.0574" y2="3.3782" layer="51"/>
<rectangle x1="-2.8956" y1="2.54" x2="-2.6924" y2="3.3782" layer="51"/>
<rectangle x1="-2.8956" y1="-3.4798" x2="-2.6924" y2="-2.6416" layer="51"/>
<rectangle x1="-2.2606" y1="-3.4798" x2="-2.0574" y2="-2.6416" layer="51"/>
<rectangle x1="-1.6256" y1="-3.4798" x2="-1.4224" y2="-2.6416" layer="51"/>
<rectangle x1="-0.9906" y1="-3.4798" x2="-0.7874" y2="-2.6416" layer="51"/>
<rectangle x1="-0.3556" y1="-3.4798" x2="-0.1524" y2="-2.6416" layer="51"/>
<rectangle x1="0.2794" y1="-3.4798" x2="0.4826" y2="-2.6416" layer="51"/>
<rectangle x1="0.9144" y1="-3.4798" x2="1.1176" y2="-2.6416" layer="51"/>
<rectangle x1="1.5494" y1="-3.4798" x2="1.7526" y2="-2.6416" layer="51"/>
<rectangle x1="2.1844" y1="-3.4798" x2="2.3876" y2="-2.6416" layer="51"/>
<rectangle x1="2.8194" y1="-3.4798" x2="3.0226" y2="-2.6416" layer="51"/>
</package>
<package name="TSSOP20" urn="urn:adsk.eagle:footprint:4239/1">
<description>&lt;b&gt;Thin Shrink Small Outline Plastic 20&lt;/b&gt;&lt;p&gt;
MAX3223-MAX3243.pdf</description>
<wire x1="-3.1646" y1="-2.2828" x2="3.1646" y2="-2.2828" width="0.1524" layer="21"/>
<wire x1="3.1646" y1="2.2828" x2="3.1646" y2="-2.2828" width="0.1524" layer="21"/>
<wire x1="3.1646" y1="2.2828" x2="-3.1646" y2="2.2828" width="0.1524" layer="21"/>
<wire x1="-3.1646" y1="-2.2828" x2="-3.1646" y2="2.2828" width="0.1524" layer="21"/>
<wire x1="-2.936" y1="-2.0542" x2="2.936" y2="-2.0542" width="0.0508" layer="21"/>
<wire x1="2.936" y1="2.0542" x2="2.936" y2="-2.0542" width="0.0508" layer="21"/>
<wire x1="2.936" y1="2.0542" x2="-2.936" y2="2.0542" width="0.0508" layer="21"/>
<wire x1="-2.936" y1="-2.0542" x2="-2.936" y2="2.0542" width="0.0508" layer="21"/>
<circle x="-2.2756" y="-1.2192" radius="0.4572" width="0.1524" layer="21"/>
<smd name="1" x="-2.925" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="2" x="-2.275" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="3" x="-1.625" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="4" x="-0.975" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="5" x="-0.325" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="6" x="0.325" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="7" x="0.975" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="8" x="1.625" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="9" x="2.275" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="10" x="2.925" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="11" x="2.925" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="12" x="2.275" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="13" x="1.625" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="14" x="0.975" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="15" x="0.325" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="16" x="-0.325" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="17" x="-0.975" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="18" x="-1.625" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="19" x="-2.275" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="20" x="-2.925" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<text x="-3.5456" y="-2.0828" size="1.016" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="4.5362" y="-2.0828" size="1.016" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-3.0266" y1="-3.121" x2="-2.8234" y2="-2.2828" layer="51"/>
<rectangle x1="-2.3766" y1="-3.121" x2="-2.1734" y2="-2.2828" layer="51"/>
<rectangle x1="-1.7266" y1="-3.121" x2="-1.5234" y2="-2.2828" layer="51"/>
<rectangle x1="-1.0766" y1="-3.121" x2="-0.8734" y2="-2.2828" layer="51"/>
<rectangle x1="-0.4266" y1="-3.121" x2="-0.2234" y2="-2.2828" layer="51"/>
<rectangle x1="0.2234" y1="-3.121" x2="0.4266" y2="-2.2828" layer="51"/>
<rectangle x1="0.8734" y1="-3.121" x2="1.0766" y2="-2.2828" layer="51"/>
<rectangle x1="1.5234" y1="-3.121" x2="1.7266" y2="-2.2828" layer="51"/>
<rectangle x1="2.1734" y1="-3.121" x2="2.3766" y2="-2.2828" layer="51"/>
<rectangle x1="2.8234" y1="-3.121" x2="3.0266" y2="-2.2828" layer="51"/>
<rectangle x1="2.8234" y1="2.2828" x2="3.0266" y2="3.121" layer="51"/>
<rectangle x1="2.1734" y1="2.2828" x2="2.3766" y2="3.121" layer="51"/>
<rectangle x1="1.5234" y1="2.2828" x2="1.7266" y2="3.121" layer="51"/>
<rectangle x1="0.8734" y1="2.2828" x2="1.0766" y2="3.121" layer="51"/>
<rectangle x1="0.2234" y1="2.2828" x2="0.4266" y2="3.121" layer="51"/>
<rectangle x1="-0.4266" y1="2.2828" x2="-0.2234" y2="3.121" layer="51"/>
<rectangle x1="-1.0766" y1="2.2828" x2="-0.8734" y2="3.121" layer="51"/>
<rectangle x1="-1.7266" y1="2.2828" x2="-1.5234" y2="3.121" layer="51"/>
<rectangle x1="-2.3766" y1="2.2828" x2="-2.1734" y2="3.121" layer="51"/>
<rectangle x1="-3.0266" y1="2.2828" x2="-2.8234" y2="3.121" layer="51"/>
</package>
</packages>
<packages3d>
<package3d name="SOP20" urn="urn:adsk.eagle:package:30933/2" type="model">
<description>SMALL OUTLINE PACKAGE</description>
<packageinstances>
<packageinstance name="SOP20"/>
</packageinstances>
</package3d>
<package3d name="SSOP20D8" urn="urn:adsk.eagle:package:4318/2" locally_modified="yes" type="model">
<description>Plastic Shrink Small Outline Package</description>
<packageinstances>
<packageinstance name="SSOP20"/>
</packageinstances>
</package3d>
<package3d name="TSSOP20" urn="urn:adsk.eagle:package:4349/2" type="model">
<description>Thin Shrink Small Outline Plastic 20
MAX3223-MAX3243.pdf</description>
<packageinstances>
<packageinstance name="TSSOP20"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="FT231XS-U">
<text x="-12.7304" y="25.4438" size="3.22921875" layer="95">&gt;NAME</text>
<text x="-12.7114" y="-27.9554" size="3.226640625" layer="96">&gt;VALUE</text>
<wire x1="-12.7" y1="22.86" x2="12.7" y2="22.86" width="0.254" layer="94"/>
<wire x1="12.7" y1="22.86" x2="12.7" y2="-22.86" width="0.254" layer="94"/>
<wire x1="12.7" y1="-22.86" x2="-12.7" y2="-22.86" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-22.86" x2="-12.7" y2="22.86" width="0.254" layer="94"/>
<pin name="RXD" x="-17.78" y="5.08" length="middle" direction="in"/>
<pin name="!RI#" x="-17.78" y="-5.08" length="middle" direction="in"/>
<pin name="!DSR#" x="-17.78" y="0" length="middle" direction="in"/>
<pin name="!DCD#" x="-17.78" y="-2.54" length="middle" direction="in"/>
<pin name="!CTS#" x="-17.78" y="2.54" length="middle" direction="in"/>
<pin name="CBUS2" x="-17.78" y="-15.24" length="middle"/>
<pin name="USBDP" x="-17.78" y="7.62" length="middle" direction="in"/>
<pin name="USBDM" x="-17.78" y="10.16" length="middle" direction="in"/>
<pin name="3V3OUT" x="17.78" y="15.24" length="middle" direction="pwr" rot="R180"/>
<pin name="!RESET#" x="-17.78" y="12.7" length="middle" direction="in"/>
<pin name="VCC" x="17.78" y="20.32" length="middle" direction="pwr" rot="R180"/>
<pin name="GND" x="17.78" y="-20.32" length="middle" direction="pwr" rot="R180"/>
<pin name="CBUS1" x="-17.78" y="-12.7" length="middle"/>
<pin name="CBUS0" x="-17.78" y="-10.16" length="middle"/>
<pin name="CBUS3" x="-17.78" y="-17.78" length="middle"/>
<pin name="TXD" x="17.78" y="2.54" length="middle" direction="out" rot="R180"/>
<pin name="!DTR#" x="17.78" y="-2.54" length="middle" direction="out" rot="R180"/>
<pin name="!RTS#" x="17.78" y="0" length="middle" direction="out" rot="R180"/>
<pin name="VCCIO" x="17.78" y="17.78" length="middle" direction="pwr" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="FT231XS-U" prefix="U">
<description>USB Full Speed to Full Handshake UART with USB Charger Detection, SSOP-20</description>
<gates>
<gate name="G$1" symbol="FT231XS-U" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOP20">
<connects>
<connect gate="G$1" pin="!CTS#" pad="9"/>
<connect gate="G$1" pin="!DCD#" pad="8"/>
<connect gate="G$1" pin="!DSR#" pad="7"/>
<connect gate="G$1" pin="!DTR#" pad="1"/>
<connect gate="G$1" pin="!RESET#" pad="14"/>
<connect gate="G$1" pin="!RI#" pad="5"/>
<connect gate="G$1" pin="!RTS#" pad="2"/>
<connect gate="G$1" pin="3V3OUT" pad="13"/>
<connect gate="G$1" pin="CBUS0" pad="18"/>
<connect gate="G$1" pin="CBUS1" pad="17"/>
<connect gate="G$1" pin="CBUS2" pad="10"/>
<connect gate="G$1" pin="CBUS3" pad="19"/>
<connect gate="G$1" pin="GND" pad="6 16"/>
<connect gate="G$1" pin="RXD" pad="4"/>
<connect gate="G$1" pin="TXD" pad="20"/>
<connect gate="G$1" pin="USBDM" pad="12"/>
<connect gate="G$1" pin="USBDP" pad="11"/>
<connect gate="G$1" pin="VCC" pad="15"/>
<connect gate="G$1" pin="VCCIO" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:30933/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value=" USB Full Speed to Full Handshake UART with USB Charger Detection, SSOP-20 "/>
<attribute name="DIGI-KEY_PART_NUMBER" value="768-1156-5-ND"/>
<attribute name="DIGI-KEY_PURCHASE_URL" value="https://www.digikey.com/product-detail/en/ftdi-future-technology-devices-international-ltd/FT231XS-U/768-1156-5-ND/3749471?utm_source=snapeda&amp;utm_medium=aggregator&amp;utm_campaign=symbol"/>
<attribute name="MF" value="FTDI,"/>
<attribute name="MP" value="FT231XS-U"/>
<attribute name="PACKAGE" value="SSOP-20 FTDI"/>
</technology>
</technologies>
</device>
<device name="3D" package="SSOP20">
<connects>
<connect gate="G$1" pin="!CTS#" pad="9"/>
<connect gate="G$1" pin="!DCD#" pad="8"/>
<connect gate="G$1" pin="!DSR#" pad="7"/>
<connect gate="G$1" pin="!DTR#" pad="1"/>
<connect gate="G$1" pin="!RESET#" pad="14"/>
<connect gate="G$1" pin="!RI#" pad="5"/>
<connect gate="G$1" pin="!RTS#" pad="2"/>
<connect gate="G$1" pin="3V3OUT" pad="13"/>
<connect gate="G$1" pin="CBUS0" pad="18"/>
<connect gate="G$1" pin="CBUS1" pad="17"/>
<connect gate="G$1" pin="CBUS2" pad="10"/>
<connect gate="G$1" pin="CBUS3" pad="19"/>
<connect gate="G$1" pin="GND" pad="6 16"/>
<connect gate="G$1" pin="RXD" pad="4"/>
<connect gate="G$1" pin="TXD" pad="20"/>
<connect gate="G$1" pin="USBDM" pad="12"/>
<connect gate="G$1" pin="USBDP" pad="11"/>
<connect gate="G$1" pin="VCC" pad="15"/>
<connect gate="G$1" pin="VCCIO" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:4318/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TSSOP20" package="TSSOP20">
<connects>
<connect gate="G$1" pin="!CTS#" pad="9"/>
<connect gate="G$1" pin="!DCD#" pad="8"/>
<connect gate="G$1" pin="!DSR#" pad="7"/>
<connect gate="G$1" pin="!DTR#" pad="1"/>
<connect gate="G$1" pin="!RESET#" pad="14"/>
<connect gate="G$1" pin="!RI#" pad="5"/>
<connect gate="G$1" pin="!RTS#" pad="2"/>
<connect gate="G$1" pin="3V3OUT" pad="13"/>
<connect gate="G$1" pin="CBUS0" pad="18"/>
<connect gate="G$1" pin="CBUS1" pad="17"/>
<connect gate="G$1" pin="CBUS2" pad="10"/>
<connect gate="G$1" pin="CBUS3" pad="19"/>
<connect gate="G$1" pin="GND" pad="6 16"/>
<connect gate="G$1" pin="RXD" pad="4"/>
<connect gate="G$1" pin="TXD" pad="20"/>
<connect gate="G$1" pin="USBDM" pad="12"/>
<connect gate="G$1" pin="USBDP" pad="11"/>
<connect gate="G$1" pin="VCC" pad="15"/>
<connect gate="G$1" pin="VCCIO" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:4349/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="MF_Discrete_Semiconductor">
<packages>
<package name="SOT-23-3" urn="urn:adsk.eagle:footprint:9990235/1">
<description>&lt;b&gt;Description:&lt;/b&gt; Standard SOT-23-3 footprint.&lt;br/&gt;</description>
<smd name="P$1" x="-1" y="0.95" dx="0.9" dy="0.8" layer="1"/>
<smd name="P$2" x="-1" y="-0.95" dx="0.9" dy="0.8" layer="1"/>
<smd name="P$3" x="1" y="0" dx="0.9" dy="0.8" layer="1"/>
<wire x1="-1.8" y1="1.6" x2="-1.8" y2="-1.6" width="0.127" layer="21"/>
<wire x1="-1.8" y1="-1.6" x2="1.8" y2="-1.6" width="0.127" layer="21"/>
<wire x1="1.8" y1="-1.6" x2="1.8" y2="1.6" width="0.127" layer="21"/>
<wire x1="1.8" y1="1.6" x2="-1.8" y2="1.6" width="0.127" layer="21"/>
<polygon width="0.127" layer="21">
<vertex x="-1.6" y="2.2" curve="-90"/>
<vertex x="-1.4" y="2.4" curve="-90"/>
<vertex x="-1.2" y="2.2" curve="-90"/>
<vertex x="-1.4" y="2" curve="-90"/>
</polygon>
</package>
</packages>
<packages3d>
<package3d name="SOT-23-3" urn="urn:adsk.eagle:package:9990244/2" type="model">
<description>&lt;b&gt;Description:&lt;/b&gt; Standard SOT-23-3 footprint.&lt;br/&gt;</description>
<packageinstances>
<packageinstance name="SOT-23-3"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="NPN_BJT">
<description>&lt;b&gt;Description:&lt;/b&gt; Symbol for BJT NPN Transistors.&lt;br/&gt;</description>
<pin name="BASE" x="-7.62" y="0" visible="off" length="short"/>
<pin name="COLLECTOR" x="2.54" y="7.62" visible="off" length="short" rot="R270"/>
<pin name="EMITTER" x="2.54" y="-7.62" visible="off" length="short" rot="R90"/>
<circle x="0" y="0" radius="3.81845625" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="0.508" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0.508" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-0.508" x2="-1.27" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="0" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="2.032" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.032" x2="-1.27" y2="0.508" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="2.54" y2="-2.032" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.032" x2="-1.27" y2="-0.508" width="0.1524" layer="94"/>
<polygon width="0.1524" layer="94">
<vertex x="2.54" y="-2.032"/>
<vertex x="1.905" y="-1.27"/>
<vertex x="1.524" y="-2.159"/>
</polygon>
<text x="-5.08" y="0.254" size="1.016" layer="94" font="vector">B</text>
<text x="2.794" y="3.81" size="1.016" layer="94" font="vector">C</text>
<text x="2.794" y="-4.826" size="1.016" layer="94" font="vector">E</text>
<text x="5.08" y="5.08" size="1.016" layer="95" font="vector" rot="R180" align="bottom-right">&gt;NAME</text>
<text x="5.08" y="2.54" size="1.016" layer="96" font="vector">&gt;VALUE</text>
</symbol>
<symbol name="P-CHANNEL_FET">
<description>&lt;b&gt;Description:&lt;/b&gt; Symbol for P-Channel Mosfets.&lt;br/&gt;</description>
<pin name="GATE" x="-7.62" y="0" visible="off" length="short"/>
<pin name="DRAIN" x="2.54" y="7.62" visible="off" length="short" rot="R270"/>
<pin name="SOURCE" x="2.54" y="-7.62" visible="off" length="short" rot="R90"/>
<circle x="0" y="0" radius="3.81845625" width="0.1524" layer="94"/>
<text x="-5.08" y="0.254" size="1.016" layer="94" font="vector">G</text>
<text x="2.794" y="3.81" size="1.016" layer="94" font="vector">D</text>
<text x="2.794" y="-4.826" size="1.016" layer="94" font="vector">S</text>
<text x="5.08" y="5.08" size="1.016" layer="95" font="vector" rot="R180" align="bottom-right">&gt;NAME</text>
<text x="5.08" y="2.54" size="1.016" layer="96" font="vector">&gt;VALUE</text>
<wire x1="-5.08" y1="0" x2="-1.778" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.778" y1="0" x2="-1.778" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-1.778" y1="0" x2="-1.778" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="1.778" width="0.1524" layer="94"/>
<wire x1="2.54" y1="1.778" x2="-1.016" y2="1.778" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="1.778" x2="-1.016" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="1.778" x2="-1.016" y2="1.016" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="2.54" y2="-1.778" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-1.778" x2="0.762" y2="-1.778" width="0.1524" layer="94"/>
<wire x1="0.762" y1="-1.778" x2="-1.016" y2="-1.778" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="-1.778" x2="-1.016" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="-1.778" x2="-1.016" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="0.508" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="0" x2="-1.016" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="-1.016" y1="0" x2="0.762" y2="0" width="0.1524" layer="94"/>
<wire x1="0.762" y1="0" x2="0.762" y2="-1.778" width="0.1524" layer="94"/>
<wire x1="2.54" y1="1.778" x2="2.54" y2="-1.778" width="0.1524" layer="94"/>
<wire x1="3.048" y1="-0.508" x2="2.032" y2="-0.508" width="0.1524" layer="94"/>
<polygon width="0.1524" layer="94">
<vertex x="2.54" y="-0.508"/>
<vertex x="3.048" y="0.254"/>
<vertex x="2.032" y="0.254"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-0.508" y="0.508"/>
<vertex x="-0.508" y="-0.508"/>
<vertex x="0.254" y="0"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="NPN_BJT" prefix="U" uservalue="yes">
<description>&lt;b&gt;Library:&lt;/b&gt;  MF_Discrete_Semiconductor&lt;br/&gt;
&lt;b&gt;Description:&lt;/b&gt; Device for Single NPN BJT Transistors. Manufacture part number (MFG#) can be added via Attributes.&lt;br/&gt;</description>
<gates>
<gate name="G$1" symbol="NPN_BJT" x="0" y="0"/>
</gates>
<devices>
<device name="_SOT-23-3" package="SOT-23-3">
<connects>
<connect gate="G$1" pin="BASE" pad="P$1"/>
<connect gate="G$1" pin="COLLECTOR" pad="P$3"/>
<connect gate="G$1" pin="EMITTER" pad="P$2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:9990244/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="HOUSEPART" value="NO" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="POPULATE" value="1" constant="no"/>
<attribute name="URL" value="" constant="no"/>
</technology>
<technology name="MMBT3904">
<attribute name="HOUSEPART" value="YES" constant="no"/>
<attribute name="MPN" value="MF-DSC-SOT233-MMBT3904" constant="no"/>
<attribute name="POPULATE" value="1" constant="no"/>
<attribute name="URL" value="https://factory.macrofab.com/part/MF-DSC-SOT233-MMBT3904" constant="no"/>
<attribute name="VALUE" value="MF-DSC-SOT233-MMBT3904" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="P-CHANNEL_FET" prefix="U" uservalue="yes">
<description>&lt;b&gt;Library:&lt;/b&gt;  MF_Discrete_Semiconductor&lt;br/&gt;
&lt;b&gt;Description:&lt;/b&gt; Device for Single P-Channel Mosfet. Manufacture part number (MFG#) can be added via Attributes.&lt;br/&gt;</description>
<gates>
<gate name="G$1" symbol="P-CHANNEL_FET" x="0" y="0"/>
</gates>
<devices>
<device name="_SOT-23-3" package="SOT-23-3">
<connects>
<connect gate="G$1" pin="DRAIN" pad="P$3"/>
<connect gate="G$1" pin="GATE" pad="P$1"/>
<connect gate="G$1" pin="SOURCE" pad="P$2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:9990244/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="HOUSEPART" value="NO" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="POPULATE" value="1" constant="no"/>
<attribute name="URL" value="" constant="no"/>
</technology>
<technology name="_BSS84">
<attribute name="HOUSEPART" value="YES" constant="no"/>
<attribute name="MPN" value="MF-DSC-SOT233-BSS84" constant="no"/>
<attribute name="POPULATE" value="1" constant="no"/>
<attribute name="URL" value="https://factory.macrofab.com/part/MF-DSC-SOT233-BSS84" constant="no"/>
<attribute name="VALUE" value="MF-DSC-SOT233-BSS84" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="myLib">
<packages>
<package name="RYLED" urn="urn:adsk.eagle:footprint:9990130/1">
<smd name="R-" x="-1.27" y="1.27" dx="1.778" dy="0.9144" layer="1"/>
<smd name="Y-" x="-1.27" y="-0.38" dx="1.778" dy="0.9144" layer="1"/>
<smd name="Y+" x="1.43" y="-0.38" dx="1.778" dy="0.9144" layer="1"/>
<smd name="R+" x="1.43" y="1.27" dx="1.778" dy="0.9144" layer="1"/>
<wire x1="-2.54" y1="1.905" x2="2.7" y2="1.905" width="0.000128125" layer="21"/>
<wire x1="2.7" y1="1.905" x2="2.7" y2="-1.015" width="0.000128125" layer="21"/>
<wire x1="2.7" y1="-1.015" x2="-2.54" y2="-1.015" width="0.000128125" layer="21"/>
<wire x1="-2.54" y1="-1.015" x2="-2.54" y2="1.905" width="0.000128125" layer="21"/>
<text x="-2.54" y="1.905209375" size="0.00126875" layer="25">&gt;NAME</text>
</package>
<package name="L80GPS" urn="urn:adsk.eagle:footprint:9990131/1">
<smd name="TIMER" x="-7.62" y="6.35" dx="2.54" dy="1.27" layer="1"/>
<smd name="ADDETN" x="-7.62" y="3.81" dx="2.54" dy="1.27" layer="1"/>
<smd name="NC" x="-7.62" y="1.27" dx="2.54" dy="1.27" layer="1"/>
<smd name="RESET" x="-7.62" y="-1.27" dx="2.54" dy="1.27" layer="1"/>
<smd name="EXANT" x="-7.62" y="-3.81" dx="2.54" dy="1.27" layer="1"/>
<smd name="GND2" x="-7.62" y="-6.35" dx="2.54" dy="1.27" layer="1"/>
<smd name="RXD1" x="8.38" y="-6.35" dx="2.54" dy="1.27" layer="1"/>
<smd name="TXD1" x="8.38" y="-3.81" dx="2.54" dy="1.27" layer="1"/>
<smd name="GND" x="8.38" y="-1.27" dx="2.54" dy="1.27" layer="1"/>
<smd name="VCC" x="8.38" y="1.27" dx="2.54" dy="1.27" layer="1"/>
<smd name="VBKP" x="8.38" y="3.81" dx="2.54" dy="1.27" layer="1"/>
<smd name="1PPS" x="8.38" y="6.35" dx="2.54" dy="1.27" layer="1"/>
<wire x1="-10.16" y1="7.62" x2="10.92" y2="7.62" width="0.000128125" layer="21"/>
<wire x1="10.92" y1="7.62" x2="10.92" y2="-7.62" width="0.000128125" layer="21"/>
<wire x1="10.92" y1="-7.62" x2="-10.16" y2="-7.62" width="0.000128125" layer="21"/>
<wire x1="-10.16" y1="-7.62" x2="-10.16" y2="7.62" width="0.000128125" layer="21"/>
<text x="-10.16" y="7.620209375" size="0.00126875" layer="25">&gt;NAME</text>
<text x="-10.16" y="-7.621478125" size="0.00126875" layer="27">&gt;VALUE</text>
</package>
<package name="FH12-10S-0.5SH">
<wire x1="-3.85" y1="2" x2="3.85" y2="2" width="0.127" layer="21"/>
<wire x1="4.6" y1="2" x2="3.85" y2="2" width="0.127" layer="21"/>
<wire x1="3.85" y1="2" x2="3.85" y2="5.1" width="0.127" layer="21"/>
<wire x1="3.85" y1="5.1" x2="-3.85" y2="5.1" width="0.127" layer="21"/>
<wire x1="-3.85" y1="5.1" x2="-3.85" y2="2" width="0.127" layer="21"/>
<smd name="P$1" x="-3.45" y="2.75" dx="1.5" dy="0.85" layer="1" rot="R90"/>
<smd name="P$2" x="3.45" y="2.75" dx="1.5" dy="0.85" layer="1" rot="R90"/>
<smd name="SI" x="-1.75" y="5.25" dx="0.3" dy="0.655" layer="1"/>
<smd name="DISP" x="-0.25" y="5.25" dx="0.3" dy="0.655" layer="1"/>
<smd name="VDD" x="0.75" y="5.25" dx="0.3" dy="0.655" layer="1"/>
<smd name="VSS" x="1.75" y="5.25" dx="0.3" dy="0.655" layer="1"/>
<smd name="VSSA" x="2.25" y="5.25" dx="0.3" dy="0.655" layer="1"/>
<smd name="EXTMODE" x="1.25" y="5.25" dx="0.3" dy="0.655" layer="1"/>
<smd name="VDDA" x="0.25" y="5.25" dx="0.3" dy="0.655" layer="1"/>
<smd name="EXTCOMIN" x="-0.75" y="5.25" dx="0.3" dy="0.655" layer="1"/>
<smd name="SCS" x="-1.25" y="5.25" dx="0.3" dy="0.655" layer="1"/>
<smd name="SCLK" x="-2.25" y="5.25" dx="0.3" dy="0.655" layer="1"/>
<wire x1="-4.85" y1="2" x2="-4.85" y2="-9" width="0.127" layer="21"/>
<wire x1="-4.85" y1="-9" x2="-31.4" y2="-9" width="0.127" layer="21"/>
<wire x1="-31.4" y1="-9" x2="-31.4" y2="-52" width="0.127" layer="21"/>
<wire x1="-31.4" y1="-52" x2="31.35" y2="-52" width="0.127" layer="21"/>
<wire x1="31.35" y1="-52" x2="31.35" y2="-9" width="0.127" layer="21"/>
<wire x1="31.35" y1="-9" x2="4.6" y2="-9" width="0.127" layer="21"/>
<wire x1="4.6" y1="-9" x2="4.6" y2="2" width="0.127" layer="21"/>
<wire x1="-3.85" y1="2" x2="-4.85" y2="2" width="0.127" layer="21"/>
</package>
</packages>
<packages3d>
<package3d name="L80GPS" urn="urn:adsk.eagle:package:9990138/2" type="model">
<packageinstances>
<packageinstance name="L80GPS"/>
</packageinstances>
</package3d>
<package3d name="RYLED" urn="urn:adsk.eagle:package:9990139/2" type="model">
<packageinstances>
<packageinstance name="RYLED"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="RYLED">
<wire x1="-5.08" y1="5.08" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
<pin name="P$1" x="-10.16" y="2.54" length="middle"/>
<pin name="P$2" x="-10.16" y="-2.54" length="middle"/>
<pin name="P$4" x="10.16" y="2.54" length="middle" rot="R180"/>
<pin name="P$3" x="10.16" y="-2.54" length="middle" rot="R180"/>
</symbol>
<symbol name="L80GPS">
<pin name="TIMER" x="-15.24" y="5.08" length="middle"/>
<pin name="AADET" x="-15.24" y="2.54" length="middle"/>
<pin name="RESET" x="-15.24" y="-2.54" length="middle"/>
<pin name="EXANT" x="-15.24" y="-5.08" length="middle"/>
<wire x1="-10.16" y1="7.62" x2="7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="7.62" x2="7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="-10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-7.62" x2="-10.16" y2="7.62" width="0.254" layer="94"/>
<pin name="1PPS" x="12.7" y="5.08" length="middle" rot="R180"/>
<pin name="VBKP" x="12.7" y="2.54" length="middle" rot="R180"/>
<pin name="VCC" x="12.7" y="0" length="middle" rot="R180"/>
<pin name="GND" x="-15.24" y="0" length="middle"/>
<pin name="TX" x="12.7" y="-5.08" length="middle" rot="R180"/>
<pin name="RX" x="12.7" y="-2.54" length="middle" rot="R180"/>
</symbol>
<symbol name="LS027B_LCD_SHARP">
<wire x1="12.7" y1="0" x2="12.7" y2="27.94" width="0.254" layer="94"/>
<wire x1="12.7" y1="27.94" x2="-2.54" y2="27.94" width="0.254" layer="94"/>
<wire x1="-2.54" y1="27.94" x2="-2.54" y2="0" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="12.7" y2="0" width="0.254" layer="94"/>
<pin name="VSSA" x="17.78" y="2.54" length="middle" rot="R180"/>
<pin name="VSS" x="17.78" y="5.08" length="middle" rot="R180"/>
<pin name="EXTMODE" x="17.78" y="7.62" length="middle" rot="R180"/>
<pin name="VDD" x="17.78" y="10.16" length="middle" rot="R180"/>
<pin name="VDDA" x="17.78" y="12.7" length="middle" rot="R180"/>
<pin name="DISP" x="17.78" y="15.24" length="middle" rot="R180"/>
<pin name="EXTCOMIN" x="17.78" y="17.78" length="middle" rot="R180"/>
<pin name="SCS" x="17.78" y="20.32" length="middle" rot="R180"/>
<pin name="SI" x="17.78" y="22.86" length="middle" rot="R180"/>
<pin name="SCLK" x="17.78" y="25.4" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="RYLED">
<gates>
<gate name="G$1" symbol="RYLED" x="0" y="0"/>
</gates>
<devices>
<device name="" package="RYLED">
<connects>
<connect gate="G$1" pin="P$1" pad="R-"/>
<connect gate="G$1" pin="P$2" pad="Y-"/>
<connect gate="G$1" pin="P$3" pad="Y+"/>
<connect gate="G$1" pin="P$4" pad="R+"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:9990139/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="L80GPS">
<gates>
<gate name="G$1" symbol="L80GPS" x="0" y="0"/>
</gates>
<devices>
<device name="" package="L80GPS">
<connects>
<connect gate="G$1" pin="1PPS" pad="1PPS"/>
<connect gate="G$1" pin="AADET" pad="ADDETN"/>
<connect gate="G$1" pin="EXANT" pad="EXANT"/>
<connect gate="G$1" pin="GND" pad="GND GND2"/>
<connect gate="G$1" pin="RESET" pad="RESET"/>
<connect gate="G$1" pin="RX" pad="RXD1"/>
<connect gate="G$1" pin="TIMER" pad="TIMER"/>
<connect gate="G$1" pin="TX" pad="TXD1"/>
<connect gate="G$1" pin="VBKP" pad="VBKP"/>
<connect gate="G$1" pin="VCC" pad="VCC"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:9990138/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LS027B_LCD_SHARP">
<gates>
<gate name="G$1" symbol="LS027B_LCD_SHARP" x="2.54" y="0"/>
</gates>
<devices>
<device name="FH12-10S-0.5SH" package="FH12-10S-0.5SH">
<connects>
<connect gate="G$1" pin="DISP" pad="DISP"/>
<connect gate="G$1" pin="EXTCOMIN" pad="EXTCOMIN"/>
<connect gate="G$1" pin="EXTMODE" pad="EXTMODE"/>
<connect gate="G$1" pin="SCLK" pad="SCLK"/>
<connect gate="G$1" pin="SCS" pad="SCS"/>
<connect gate="G$1" pin="SI" pad="SI"/>
<connect gate="G$1" pin="VDD" pad="VDD"/>
<connect gate="G$1" pin="VDDA" pad="VDDA"/>
<connect gate="G$1" pin="VSS" pad="VSS"/>
<connect gate="G$1" pin="VSSA" pad="VSSA"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Connectors">
<description>&lt;h3&gt;SparkFun Connectors&lt;/h3&gt;
This library contains electrically-functional connectors. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="JST-2-SMD" urn="urn:adsk.eagle:footprint:9948555/1">
<description>2mm SMD side-entry connector. tDocu layer indicates the actual physical plastic housing. +/- indicate SparkFun standard batteries and wiring.</description>
<wire x1="-4" y1="-1" x2="-4" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-4" y1="-4.5" x2="-3.2" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="-4.5" x2="-3.2" y2="-2" width="0.2032" layer="21"/>
<wire x1="-3.2" y1="-2" x2="-2" y2="-2" width="0.2032" layer="21"/>
<wire x1="2" y1="-2" x2="3.2" y2="-2" width="0.2032" layer="21"/>
<wire x1="3.2" y1="-2" x2="3.2" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="3.2" y1="-4.5" x2="4" y2="-4.5" width="0.2032" layer="21"/>
<wire x1="4" y1="-4.5" x2="4" y2="-1" width="0.2032" layer="21"/>
<wire x1="2" y1="3" x2="-2" y2="3" width="0.2032" layer="21"/>
<smd name="1" x="-1" y="-3.7" dx="1" dy="4.6" layer="1"/>
<smd name="2" x="1" y="-3.7" dx="1" dy="4.6" layer="1"/>
<smd name="NC1" x="-3.4" y="1.5" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<smd name="NC2" x="3.4" y="1.5" dx="3.4" dy="1.6" layer="1" rot="R90"/>
<text x="-1.27" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.27" y="0" size="0.4064" layer="27">&gt;Value</text>
<text x="2.159" y="-4.445" size="1.27" layer="51">+</text>
<text x="-2.921" y="-4.445" size="1.27" layer="51">-</text>
</package>
</packages>
<packages3d>
<package3d name="JST-2-SMD" urn="urn:adsk.eagle:package:9948904/2" type="model">
<description>2mm SMD side-entry connector. tDocu layer indicates the actual physical plastic housing. +/- indicate SparkFun standard batteries and wiring.</description>
<packageinstances>
<packageinstance name="JST-2-SMD"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="JST_2MM_MALE">
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="1.778" width="0.254" layer="94"/>
<wire x1="-2.54" y1="1.778" x2="-2.54" y2="3.302" width="0.254" layer="94"/>
<wire x1="-2.54" y1="3.302" x2="-2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="5.08" y2="3.302" width="0.254" layer="94"/>
<wire x1="5.08" y1="3.302" x2="5.08" y2="1.778" width="0.254" layer="94"/>
<wire x1="5.08" y1="1.778" x2="5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="4.064" y2="-2.54" width="0.254" layer="94"/>
<wire x1="4.064" y1="-2.54" x2="4.064" y2="0" width="0.254" layer="94"/>
<wire x1="4.064" y1="0" x2="-1.524" y2="0" width="0.254" layer="94"/>
<wire x1="-1.524" y1="0" x2="-1.524" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-1.524" y2="-2.54" width="0.254" layer="94"/>
<pin name="-" x="0" y="-5.08" visible="off" length="middle" rot="R90"/>
<pin name="+" x="2.54" y="-5.08" visible="off" length="middle" rot="R90"/>
<pin name="PAD2" x="5.08" y="2.54" visible="off" length="point" rot="R90"/>
<pin name="PAD1" x="-2.54" y="2.54" visible="off" length="point" rot="R90"/>
<wire x1="-2.54" y1="1.778" x2="-1.778" y2="1.778" width="0.254" layer="94"/>
<wire x1="-1.778" y1="1.778" x2="-1.778" y2="3.302" width="0.254" layer="94"/>
<wire x1="-1.778" y1="3.302" x2="-2.54" y2="3.302" width="0.254" layer="94"/>
<wire x1="5.08" y1="1.778" x2="4.318" y2="1.778" width="0.254" layer="94"/>
<wire x1="4.318" y1="1.778" x2="4.318" y2="3.302" width="0.254" layer="94"/>
<wire x1="4.318" y1="3.302" x2="5.08" y2="3.302" width="0.254" layer="94"/>
<wire x1="2.032" y1="1.016" x2="3.048" y2="1.016" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.508" x2="2.54" y2="1.524" width="0.254" layer="94"/>
<wire x1="0" y1="0.508" x2="0" y2="1.524" width="0.254" layer="94"/>
<text x="-2.54" y="5.842" size="1.778" layer="95">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="JST_2MM_MALE" prefix="J" uservalue="yes">
<description>Mates to single-cell LiPo batteries.&lt;br&gt;
CONN-08352</description>
<gates>
<gate name="G$1" symbol="JST_2MM_MALE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="JST-2-SMD">
<connects>
<connect gate="G$1" pin="+" pad="2"/>
<connect gate="G$1" pin="-" pad="1"/>
<connect gate="G$1" pin="PAD1" pad="NC1"/>
<connect gate="G$1" pin="PAD2" pad="NC2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:9948904/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08352"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="TPS79533DCQR">
<description>&lt;Ultralow-Noise, High-PSRR, Fast, RF, 500-mA Low-Dropout Linear Regulators&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="SOT127P706X180-6N" urn="urn:adsk.eagle:footprint:9988327/1">
<description>&lt;b&gt;TPS79533DCQR&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-3" y="2.54" dx="2" dy="0.65" layer="1"/>
<smd name="2" x="-3" y="1.27" dx="2" dy="0.65" layer="1"/>
<smd name="3" x="-3" y="0" dx="2" dy="0.65" layer="1"/>
<smd name="4" x="-3" y="-1.27" dx="2" dy="0.65" layer="1"/>
<smd name="5" x="-3" y="-2.54" dx="2" dy="0.65" layer="1"/>
<smd name="6" x="3" y="0" dx="3.2" dy="2" layer="1" rot="R90"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-4.25" y1="3.525" x2="4.25" y2="3.525" width="0.05" layer="51"/>
<wire x1="4.25" y1="3.525" x2="4.25" y2="-3.525" width="0.05" layer="51"/>
<wire x1="4.25" y1="-3.525" x2="-4.25" y2="-3.525" width="0.05" layer="51"/>
<wire x1="-4.25" y1="-3.525" x2="-4.25" y2="3.525" width="0.05" layer="51"/>
<wire x1="-1.75" y1="3.25" x2="1.75" y2="3.25" width="0.1" layer="51"/>
<wire x1="1.75" y1="3.25" x2="1.75" y2="-3.25" width="0.1" layer="51"/>
<wire x1="1.75" y1="-3.25" x2="-1.75" y2="-3.25" width="0.1" layer="51"/>
<wire x1="-1.75" y1="-3.25" x2="-1.75" y2="3.25" width="0.1" layer="51"/>
<wire x1="-1.75" y1="1.98" x2="-0.48" y2="3.25" width="0.1" layer="51"/>
<wire x1="-1.65" y1="3.25" x2="1.65" y2="3.25" width="0.2" layer="21"/>
<wire x1="1.65" y1="3.25" x2="1.65" y2="-3.25" width="0.2" layer="21"/>
<wire x1="1.65" y1="-3.25" x2="-1.65" y2="-3.25" width="0.2" layer="21"/>
<wire x1="-1.65" y1="-3.25" x2="-1.65" y2="3.25" width="0.2" layer="21"/>
<wire x1="-4" y1="3.215" x2="-2" y2="3.215" width="0.2" layer="21"/>
</package>
</packages>
<packages3d>
<package3d name="SOT127P706X180-6N" urn="urn:adsk.eagle:package:9988329/2" type="model">
<description>&lt;b&gt;TPS79533DCQR&lt;/b&gt;&lt;br&gt;
</description>
<packageinstances>
<packageinstance name="SOT127P706X180-6N"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="TPS79533DCQR">
<wire x1="5.08" y1="2.54" x2="25.4" y2="2.54" width="0.254" layer="94"/>
<wire x1="25.4" y1="-7.62" x2="25.4" y2="2.54" width="0.254" layer="94"/>
<wire x1="25.4" y1="-7.62" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<text x="26.67" y="7.62" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="26.67" y="5.08" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="EN" x="0" y="0" length="middle"/>
<pin name="IN" x="0" y="-2.54" length="middle"/>
<pin name="GND_1" x="0" y="-5.08" length="middle"/>
<pin name="OUT" x="30.48" y="0" length="middle" rot="R180"/>
<pin name="FB" x="30.48" y="-2.54" length="middle" rot="R180"/>
<pin name="GND_2" x="30.48" y="-5.08" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="TPS79533DCQR" prefix="IC">
<description>&lt;b&gt;Ultralow-Noise, High-PSRR, Fast, RF, 500-mA Low-Dropout Linear Regulators&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://www.ti.com/lit/gpn/tps795"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="TPS79533DCQR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT127P706X180-6N">
<connects>
<connect gate="G$1" pin="EN" pad="1"/>
<connect gate="G$1" pin="FB" pad="5"/>
<connect gate="G$1" pin="GND_1" pad="3"/>
<connect gate="G$1" pin="GND_2" pad="6"/>
<connect gate="G$1" pin="IN" pad="2"/>
<connect gate="G$1" pin="OUT" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:9988329/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="Ultralow-Noise, High-PSRR, Fast, RF, 500-mA Low-Dropout Linear Regulators" constant="no"/>
<attribute name="HEIGHT" value="1.8mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Texas Instruments" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="TPS79533DCQR" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="595-TPS79533DCQR" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="https://www.mouser.com/Search/Refine.aspx?Keyword=595-TPS79533DCQR" constant="no"/>
<attribute name="RS_PART_NUMBER" value="" constant="no"/>
<attribute name="RS_PRICE-STOCK" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="MF_Connectors">
<packages>
<package name="MICROUSB-RIGHT" urn="urn:adsk.eagle:footprint:9948143/1">
<description>&lt;b&gt;Description:&lt;/b&gt; Package for Micro USB Type B connector. Based on FCI 10118193-0001LF.&lt;br&gt;</description>
<smd name="3" x="0" y="1.27" dx="0.4" dy="1.35" layer="1" rot="R180"/>
<smd name="4" x="0.65" y="1.27" dx="0.4" dy="1.35" layer="1" rot="R180"/>
<smd name="2" x="-0.65" y="1.27" dx="0.4" dy="1.35" layer="1" rot="R180"/>
<smd name="1" x="-1.3" y="1.27" dx="0.4" dy="1.35" layer="1" rot="R180"/>
<smd name="5" x="1.3" y="1.27" dx="0.4" dy="1.35" layer="1" rot="R180"/>
<smd name="S3" x="-3.2" y="1.27" dx="1.4" dy="1.6" layer="1" rot="R90"/>
<smd name="S4" x="3.2" y="1.27" dx="1.4" dy="1.6" layer="1" rot="R90"/>
<smd name="S5" x="-1.2" y="-1.405" dx="1.9" dy="1.9" layer="1"/>
<smd name="S6" x="1.2" y="-1.405" dx="1.9" dy="1.9" layer="1"/>
<smd name="S1" x="-3.3" y="-1.4" dx="0.9" dy="1.6" layer="1" roundness="100" rot="R180"/>
<smd name="S2" x="3.3" y="-1.4" dx="0.9" dy="1.6" layer="1" roundness="100" rot="R180"/>
<smd name="S7" x="-3.3" y="-1.4" dx="0.9" dy="1.6" layer="16" roundness="100" rot="R180" cream="no"/>
<smd name="S8" x="3.3" y="-1.4" dx="0.9" dy="1.6" layer="16" roundness="100" rot="R180" cream="no"/>
<text x="-5.08" y="2.7" size="1.016" layer="25" font="vector" ratio="16">&gt;NAME</text>
<wire x1="-5.08" y1="-2.855" x2="5.08" y2="-2.855" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-2.855" x2="-5.08" y2="1.3" width="0.127" layer="21"/>
<wire x1="-5.08" y1="1.3" x2="-4.08" y2="2.3" width="0.127" layer="21"/>
<wire x1="-4.08" y1="2.3" x2="4.08" y2="2.3" width="0.127" layer="21"/>
<wire x1="4.08" y1="2.3" x2="5.08" y2="1.3" width="0.127" layer="21"/>
<wire x1="5.08" y1="1.3" x2="5.08" y2="-2.855" width="0.127" layer="21"/>
<wire x1="-3.55" y1="-1.05" x2="-3.55" y2="-1.75" width="0.01" layer="20"/>
<wire x1="-3.55" y1="-1.75" x2="-3.3" y2="-2" width="0.01" layer="20" curve="90"/>
<wire x1="-3.3" y1="-2" x2="-3.05" y2="-1.75" width="0.01" layer="20" curve="90"/>
<wire x1="-3.05" y1="-1.75" x2="-3.05" y2="-1.05" width="0.01" layer="20"/>
<wire x1="-3.05" y1="-1.05" x2="-3.3" y2="-0.8" width="0.01" layer="20" curve="90"/>
<wire x1="-3.3" y1="-0.8" x2="-3.55" y2="-1.05" width="0.01" layer="20" curve="90"/>
<wire x1="3.05" y1="-1.05" x2="3.05" y2="-1.75" width="0.01" layer="20"/>
<wire x1="3.05" y1="-1.75" x2="3.3" y2="-2" width="0.01" layer="20" curve="90"/>
<wire x1="3.3" y1="-2" x2="3.55" y2="-1.75" width="0.01" layer="20" curve="90"/>
<wire x1="3.55" y1="-1.75" x2="3.55" y2="-1.05" width="0.01" layer="20"/>
<wire x1="3.55" y1="-1.05" x2="3.3" y2="-0.8" width="0.01" layer="20" curve="90"/>
<wire x1="3.3" y1="-0.8" x2="3.05" y2="-1.05" width="0.01" layer="20" curve="90"/>
</package>
<package name="MINIUSB-RIGHT">
<description>&lt;b&gt;Description:&lt;/b&gt; Package for Mini USB Type B connector. Based on 4UCON 20236.&lt;br&gt;</description>
<smd name="1" x="-1.6" y="0" dx="0.5" dy="2.25" layer="1"/>
<smd name="2" x="-0.8" y="0" dx="0.5" dy="2.25" layer="1"/>
<smd name="3" x="0" y="0" dx="0.5" dy="2.25" layer="1"/>
<smd name="4" x="0.8" y="0" dx="0.5" dy="2.25" layer="1"/>
<smd name="5" x="1.6" y="0" dx="0.5" dy="2.25" layer="1"/>
<smd name="S1" x="-4.45" y="-0.125" dx="2" dy="2.5" layer="1"/>
<smd name="S3" x="-4.45" y="-5.5" dx="2" dy="2.5" layer="1"/>
<smd name="S4" x="4.45" y="-5.5" dx="2" dy="2.5" layer="1"/>
<smd name="S2" x="4.45" y="-0.125" dx="2" dy="2.5" layer="1"/>
<hole x="-2.2" y="-2.675" drill="0.9"/>
<hole x="2.2" y="-2.675" drill="0.9"/>
<wire x1="-3.8" y1="-1.75" x2="-3.8" y2="-4" width="0.127" layer="21"/>
<wire x1="-3.8" y1="-7" x2="-3.8" y2="-8.575" width="0.127" layer="21"/>
<wire x1="-3.8" y1="-8.575" x2="3.8" y2="-8.575" width="0.127" layer="21"/>
<wire x1="-2.25" y1="0.625" x2="-3.05" y2="0.625" width="0.127" layer="21"/>
<wire x1="2.25" y1="0.625" x2="3.05" y2="0.625" width="0.127" layer="21"/>
<wire x1="3.8" y1="-7" x2="3.8" y2="-8.575" width="0.127" layer="21"/>
<wire x1="3.8" y1="-1.75" x2="3.8" y2="-4" width="0.127" layer="21"/>
<text x="-3.8" y="1.5" size="1.016" layer="25" font="vector" ratio="16">&gt;NAME</text>
</package>
<package name="MF-MICROSD" urn="urn:adsk.eagle:footprint:9948142/1">
<description>&lt;b&gt;Description:&lt;/b&gt; Package for Micro SD Card Slot connector. Based on 4uconnector 15882.&lt;br&gt;</description>
<smd name="PIN1" x="-1.93875" y="3.15125" dx="0.8" dy="1.5" layer="1" rot="R180"/>
<smd name="PIN2" x="-0.83875" y="2.95125" dx="0.8" dy="1.5" layer="1" rot="R180"/>
<smd name="PIN3" x="0.26125" y="3.15125" dx="0.8" dy="1.5" layer="1" rot="R180"/>
<smd name="PIN4" x="1.36125" y="3.35125" dx="0.8" dy="1.5" layer="1" rot="R180"/>
<smd name="PIN5" x="2.46125" y="3.15125" dx="0.8" dy="1.5" layer="1" rot="R180"/>
<smd name="PIN6" x="3.56125" y="3.35125" dx="0.8" dy="1.5" layer="1" rot="R180"/>
<smd name="PIN7" x="4.66125" y="3.15125" dx="0.8" dy="1.5" layer="1" rot="R180"/>
<smd name="PIN8" x="5.76125" y="3.15125" dx="0.8" dy="1.5" layer="1" rot="R180"/>
<smd name="G0" x="-6.58875" y="7.00125" dx="1.4" dy="1.9" layer="1"/>
<smd name="G1" x="6.56125" y="6.00125" dx="1.4" dy="1.9" layer="1"/>
<smd name="PIN9" x="4.96125" y="-7.14875" dx="1.8" dy="1.4" layer="1"/>
<smd name="PIN10" x="-0.73875" y="-7.14875" dx="1.8" dy="1.4" layer="1"/>
<wire x1="-7.025" y1="5.77625" x2="-7.025" y2="-7.625" width="0.127" layer="21"/>
<wire x1="-7.025" y1="-7.625" x2="-1.91375" y2="-7.625" width="0.127" layer="21"/>
<wire x1="6.19875" y1="-7.625" x2="7.025" y2="-7.625" width="0.127" layer="21"/>
<wire x1="7.025" y1="-7.625" x2="7.025" y2="4.67625" width="0.127" layer="21"/>
<wire x1="7.025" y1="7.28875" x2="7.025" y2="7.625" width="0.127" layer="21"/>
<wire x1="7.025" y1="7.625" x2="-5.65" y2="7.625" width="0.127" layer="21"/>
<wire x1="-2.32625" y1="7.625" x2="-2.32625" y2="8.225" width="0.127" layer="51"/>
<wire x1="6.19875" y1="7.625" x2="6.19875" y2="8.225" width="0.127" layer="51"/>
<wire x1="-2.32625" y1="8.225" x2="6.19875" y2="8.225" width="0.127" layer="51"/>
<wire x1="-2.32625" y1="8.225" x2="-2.32625" y2="9.225" width="0.127" layer="51"/>
<wire x1="-2.32625" y1="9.225" x2="6.19875" y2="9.225" width="0.127" layer="51"/>
<wire x1="6.19875" y1="9.225" x2="6.19875" y2="8.25125" width="0.127" layer="51"/>
<wire x1="-2.32625" y1="9.225" x2="-2.32625" y2="12.6075" width="0.127" layer="51"/>
<wire x1="-2.32625" y1="12.6075" x2="-2.00875" y2="12.925" width="0.127" layer="51" curve="-90"/>
<wire x1="-2.00875" y1="12.925" x2="5.88125" y2="12.925" width="0.127" layer="51"/>
<wire x1="5.88125" y1="12.925" x2="6.19875" y2="12.6075" width="0.127" layer="51" curve="-90"/>
<wire x1="6.19875" y1="12.6075" x2="6.19875" y2="9.225" width="0.127" layer="51"/>
<wire x1="0.4" y1="-7.625" x2="3.72375" y2="-7.625" width="0.127" layer="21"/>
<text x="7.50125" y="-7.625" size="1.016" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="6.47375" y="12.925" size="0.6096" layer="51" font="vector" ratio="16">Card Push Out</text>
<text x="6.47375" y="9.225" size="0.6096" layer="51" font="vector" ratio="16">Card Lock Position</text>
<text x="6.47375" y="8.225" size="0.6096" layer="51" font="vector" ratio="16">Card Push In</text>
<polygon width="0.127" layer="21">
<vertex x="1.79875" y="7.42625"/>
<vertex x="2.07375" y="7.42625"/>
<vertex x="2.07375" y="6.87625"/>
<vertex x="2.34875" y="6.87625"/>
<vertex x="1.93625" y="6.46375"/>
<vertex x="1.52375" y="6.87625"/>
<vertex x="1.79875" y="6.87625"/>
</polygon>
</package>
</packages>
<packages3d>
<package3d name="MICROUSB-RIGHT" urn="urn:adsk.eagle:package:9948223/2" type="model">
<description>&lt;b&gt;Description:&lt;/b&gt; Package for Micro USB Type B connector. Based on FCI 10118193-0001LF.&lt;br&gt;</description>
<packageinstances>
<packageinstance name="MICROUSB-RIGHT"/>
</packageinstances>
</package3d>
<package3d name="MF-MICROSD" urn="urn:adsk.eagle:package:9948224/2" type="model">
<description>&lt;b&gt;Description:&lt;/b&gt; Package for Micro SD Card Slot connector. Based on 4uconnector 15882.&lt;br&gt;</description>
<packageinstances>
<packageinstance name="MF-MICROSD"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="USB_5PIN">
<description>&lt;b&gt;Description:&lt;/b&gt; Symbol for 5-Pin USB connectors.&lt;br&gt;</description>
<pin name="5V" x="7.62" y="5.08" visible="off" length="short" direction="pwr" rot="R180"/>
<pin name="D-" x="7.62" y="2.54" visible="off" length="short" rot="R180"/>
<pin name="D+" x="7.62" y="0" visible="off" length="short" rot="R180"/>
<pin name="GND" x="7.62" y="-5.08" visible="off" length="short" direction="pwr" rot="R180"/>
<text x="0" y="6.096" size="1.016" layer="97" font="vector" align="bottom-center">USB PORT</text>
<wire x1="5.08" y1="7.62" x2="5.08" y2="-10.16" width="0.127" layer="94"/>
<wire x1="5.08" y1="-10.16" x2="-5.08" y2="-10.16" width="0.127" layer="94"/>
<wire x1="-5.08" y1="-10.16" x2="-5.08" y2="7.62" width="0.127" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="5.08" y2="7.62" width="0.127" layer="94"/>
<text x="-5.08" y="11.43" size="1.016" layer="95" font="vector" align="top-left">&gt;NAME</text>
<text x="-5.08" y="8.89" size="1.016" layer="96" font="vector">&gt;VALUE</text>
<pin name="ID" x="7.62" y="-2.54" visible="off" length="short" rot="R180"/>
<pin name="SLD" x="7.62" y="-7.62" visible="off" length="short" direction="pwr" rot="R180"/>
<text x="4.572" y="5.08" size="1.016" layer="97" font="vector" align="center-right">5V</text>
<text x="4.572" y="2.54" size="1.016" layer="97" font="vector" align="center-right">D-</text>
<text x="4.572" y="0" size="1.016" layer="97" font="vector" align="center-right">D+</text>
<text x="4.572" y="-2.54" size="1.016" layer="97" font="vector" align="center-right">ID</text>
<text x="4.572" y="-5.08" size="1.016" layer="97" font="vector" align="center-right">GND</text>
<text x="4.572" y="-7.62" size="1.016" layer="97" font="vector" align="center-right">SHIELD</text>
<polygon width="0.254" layer="94">
<vertex x="-1.905" y="3.175"/>
<vertex x="-1.27" y="4.445"/>
<vertex x="-0.635" y="3.175"/>
</polygon>
<polygon width="0.254" layer="94">
<vertex x="-3.175" y="0.3175" curve="-90"/>
<vertex x="-2.8575" y="0" curve="-90"/>
<vertex x="-3.175" y="-0.3175" curve="-90"/>
<vertex x="-3.4925" y="0" curve="-90"/>
</polygon>
<polygon width="0.254" layer="94">
<vertex x="0.3175" y="1.5875"/>
<vertex x="0.3175" y="0.9525"/>
<vertex x="0.9525" y="0.9525"/>
<vertex x="0.9525" y="1.5875"/>
</polygon>
<wire x1="-1.27" y1="-3.595840625" x2="-1.27" y2="-2.325840625" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-2.325840625" x2="-1.27" y2="3.175" width="0.254" layer="94"/>
<wire x1="0.337421875" y1="-0.297578125" x2="-0.972421875" y2="-1.607421875" width="0.254" layer="94"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-0.849159375" width="0.254" layer="94"/>
<wire x1="-2.877421875" y1="-1.567578125" x2="-1.567578125" y2="-2.877421875" width="0.254" layer="94"/>
<wire x1="-3.175" y1="-0.849159375" x2="-2.877421875" y2="-1.567578125" width="0.254" layer="94" curve="44.999875"/>
<wire x1="0.635" y1="1.27" x2="0.635" y2="0.420840625" width="0.254" layer="94"/>
<wire x1="0.635" y1="0.420840625" x2="0.337421875" y2="-0.297578125" width="0.254" layer="94" curve="-44.999875"/>
<wire x1="-1.27" y1="-3.81" x2="-1.27" y2="-3.595840625" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-3.595840625" x2="-1.567578125" y2="-2.877421875" width="0.254" layer="94" curve="44.999875"/>
<wire x1="-1.27" y1="-2.325840625" x2="-0.972421875" y2="-1.607421875" width="0.254" layer="94" curve="-44.999875"/>
<polygon width="0.254" layer="94">
<vertex x="-1.2065" y="-5.715"/>
<vertex x="-1.3335" y="-5.715" curve="-90"/>
<vertex x="-2.2225" y="-4.826"/>
<vertex x="-2.2225" y="-4.699" curve="-90"/>
<vertex x="-1.3335" y="-3.81"/>
<vertex x="-1.2065" y="-3.81" curve="-90"/>
<vertex x="-0.3175" y="-4.699"/>
<vertex x="-0.3175" y="-4.826" curve="-90"/>
</polygon>
</symbol>
<symbol name="SD_CARD">
<description>&lt;b&gt;Description:&lt;/b&gt; Symbol for SD Card Slot connector.&lt;br&gt;</description>
<wire x1="0" y1="0" x2="15.24" y2="0" width="0.127" layer="94"/>
<wire x1="15.24" y1="0" x2="15.24" y2="-27.94" width="0.127" layer="94"/>
<wire x1="15.24" y1="-27.94" x2="0" y2="-27.94" width="0.127" layer="94"/>
<wire x1="0" y1="-27.94" x2="0" y2="0" width="0.127" layer="94"/>
<pin name="RW" x="-2.54" y="-2.54" visible="off" length="short"/>
<pin name="CS" x="-2.54" y="-5.08" visible="off" length="short"/>
<pin name="DI" x="-2.54" y="-7.62" visible="off" length="short"/>
<pin name="SCLK" x="-2.54" y="-10.16" visible="off" length="short"/>
<pin name="D0" x="-2.54" y="-12.7" visible="off" length="short"/>
<pin name="IRQ" x="-2.54" y="-15.24" visible="off" length="short"/>
<pin name="3.3V" x="-2.54" y="-22.86" visible="off" length="short" direction="pwr"/>
<pin name="GND" x="-2.54" y="-25.4" visible="off" length="short" direction="pwr"/>
<text x="0" y="3.81" size="1.016" layer="95" font="vector" align="top-left">&gt;NAME</text>
<text x="0" y="1.27" size="1.016" layer="96" font="vector">&gt;VALUE</text>
<pin name="CRD_DTCT" x="-2.54" y="-20.32" visible="off" length="short"/>
<text x="0.762" y="-2.54" size="1.016" layer="97" font="vector" align="center-left">RW</text>
<text x="0.762" y="-5.08" size="1.016" layer="97" font="vector" align="center-left">CS</text>
<text x="0.762" y="-7.62" size="1.016" layer="97" font="vector" align="center-left">DI</text>
<text x="0.762" y="-10.16" size="1.016" layer="97" font="vector" align="center-left">SCLK</text>
<text x="0.762" y="-12.7" size="1.016" layer="97" font="vector" align="center-left">D0</text>
<text x="0.762" y="-15.24" size="1.016" layer="97" font="vector" align="center-left">IRQ</text>
<text x="0.762" y="-20.32" size="1.016" layer="97" font="vector" align="center-left">CARD_DETECT</text>
<text x="0.762" y="-22.86" size="1.016" layer="97" font="vector" align="center-left">3.3V</text>
<text x="0.762" y="-25.4" size="1.016" layer="97" font="vector" align="center-left">GND</text>
<wire x1="5.08" y1="-3.81" x2="5.588" y2="-3.81" width="0.127" layer="94"/>
<wire x1="5.588" y1="-3.81" x2="6.858" y2="-3.81" width="0.127" layer="94"/>
<wire x1="6.858" y1="-3.81" x2="8.128" y2="-3.81" width="0.127" layer="94"/>
<wire x1="8.128" y1="-3.81" x2="9.398" y2="-3.81" width="0.127" layer="94"/>
<wire x1="9.398" y1="-3.81" x2="10.668" y2="-3.81" width="0.127" layer="94"/>
<wire x1="10.668" y1="-3.81" x2="10.7" y2="-3.81" width="0.127" layer="94"/>
<wire x1="10.7" y1="-3.81" x2="12.7" y2="-5.81" width="0.127" layer="94"/>
<wire x1="12.7" y1="-5.81" x2="12.7" y2="-13.97" width="0.127" layer="94"/>
<wire x1="12.7" y1="-13.97" x2="11.43" y2="-13.97" width="0.127" layer="94"/>
<wire x1="11.43" y1="-13.97" x2="6.35" y2="-13.97" width="0.127" layer="94"/>
<wire x1="6.35" y1="-13.97" x2="5.08" y2="-13.97" width="0.127" layer="94"/>
<wire x1="5.08" y1="-13.97" x2="5.08" y2="-3.81" width="0.127" layer="94"/>
<wire x1="10.668" y1="-3.81" x2="10.668" y2="-5.08" width="0.127" layer="94"/>
<wire x1="9.398" y1="-3.81" x2="9.398" y2="-5.08" width="0.127" layer="94"/>
<wire x1="8.128" y1="-3.81" x2="8.128" y2="-5.08" width="0.127" layer="94"/>
<wire x1="6.858" y1="-3.81" x2="6.858" y2="-5.08" width="0.127" layer="94"/>
<wire x1="5.588" y1="-3.81" x2="5.588" y2="-5.08" width="0.127" layer="94"/>
<wire x1="6.35" y1="-13.97" x2="6.35" y2="-9.89" width="0.127" layer="94"/>
<wire x1="6.35" y1="-9.89" x2="7.35" y2="-8.89" width="0.127" layer="94" curve="-90"/>
<wire x1="7.35" y1="-8.89" x2="10.43" y2="-8.89" width="0.127" layer="94"/>
<wire x1="10.43" y1="-8.89" x2="11.43" y2="-9.89" width="0.127" layer="94" curve="-90"/>
<wire x1="11.43" y1="-9.89" x2="11.43" y2="-13.97" width="0.127" layer="94"/>
<text x="7.62" y="-1.27" size="1.016" layer="97" font="vector" align="center">SD CARD SLOT</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="USB" prefix="J" uservalue="yes">
<description>&lt;b&gt;Library:&lt;/b&gt;  MF_Connectors&lt;br/&gt;
&lt;b&gt;Description:&lt;/b&gt; 5 Pin USB Connectors. SLD is the shield connection. Usually tied to ground through a small ferrite bead.&lt;br/&gt;</description>
<gates>
<gate name="G$1" symbol="USB_5PIN" x="0" y="0"/>
</gates>
<devices>
<device name="_MICRO_RIGHT" package="MICROUSB-RIGHT">
<connects>
<connect gate="G$1" pin="5V" pad="1"/>
<connect gate="G$1" pin="D+" pad="3"/>
<connect gate="G$1" pin="D-" pad="2"/>
<connect gate="G$1" pin="GND" pad="5"/>
<connect gate="G$1" pin="ID" pad="4"/>
<connect gate="G$1" pin="SLD" pad="S1 S2 S3 S4 S5 S6"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:9948223/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="HOUSEPART" value="YES" constant="no"/>
<attribute name="MPN" value="MF-CON-MICROUSB-RIGHT" constant="no"/>
<attribute name="POPULATE" value="1" constant="no"/>
<attribute name="URL" value="https://factory.macrofab.com/part/MF-CON-MICROUSB-RIGHT" constant="no"/>
<attribute name="VALUE" value="MF-CON-MICROUSB-RIGHT" constant="no"/>
</technology>
</technologies>
</device>
<device name="_MINI_RIGHT" package="MINIUSB-RIGHT">
<connects>
<connect gate="G$1" pin="5V" pad="1"/>
<connect gate="G$1" pin="D+" pad="3"/>
<connect gate="G$1" pin="D-" pad="2"/>
<connect gate="G$1" pin="GND" pad="5"/>
<connect gate="G$1" pin="ID" pad="4"/>
<connect gate="G$1" pin="SLD" pad="S1 S2 S3 S4"/>
</connects>
<technologies>
<technology name="">
<attribute name="HUOSEPART" value="NO" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="POPULATE" value="1" constant="no"/>
<attribute name="URL" value="http://www.4uconnector.com/online/object/4udrawing/20236.pdf" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SD_CARD_SLOT" prefix="SD">
<description>&lt;b&gt;Library:&lt;/b&gt;  MF_Connectors&lt;br/&gt;
&lt;b&gt;Description:&lt;/b&gt; SD Card Connector&lt;br/&gt;</description>
<gates>
<gate name="G$1" symbol="SD_CARD" x="0" y="0"/>
</gates>
<devices>
<device name="_MICRO_RIGHT" package="MF-MICROSD">
<connects>
<connect gate="G$1" pin="3.3V" pad="PIN4"/>
<connect gate="G$1" pin="CRD_DTCT" pad="PIN9 PIN10"/>
<connect gate="G$1" pin="CS" pad="PIN2"/>
<connect gate="G$1" pin="D0" pad="PIN7"/>
<connect gate="G$1" pin="DI" pad="PIN3"/>
<connect gate="G$1" pin="GND" pad="G0 G1 PIN6"/>
<connect gate="G$1" pin="IRQ" pad="PIN8"/>
<connect gate="G$1" pin="RW" pad="PIN1"/>
<connect gate="G$1" pin="SCLK" pad="PIN5"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:9948224/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="HOUSEPART" value="YES" constant="no"/>
<attribute name="MPN" value="MF-MICROSD" constant="no"/>
<attribute name="POPULATE" value="1" constant="no"/>
<attribute name="URL" value="https://factory.macrofab.com/part/MF-MICROSD" constant="no"/>
<attribute name="VALUE" value="MF-MICROSD" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="LSM303DLHC">
<packages>
<package name="ST_LGA14_3X5" urn="urn:adsk.eagle:footprint:9987750/1">
<wire x1="-1.5" y1="2.5" x2="1.5" y2="2.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="2.5" x2="1.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="-2.5" x2="-1.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-2.5" x2="-1.5" y2="2.5" width="0.127" layer="21"/>
<text x="0" y="-3.003090625" size="1.271309375" layer="25" align="top-center">&gt;NAME</text>
<smd name="P$1" x="-1.2" y="2" dx="0.5" dy="1" layer="1" rot="R90"/>
<smd name="P$2" x="-1.2" y="1.2" dx="0.5" dy="1" layer="1" rot="R90"/>
<smd name="P$3" x="-1.2" y="0.4" dx="0.5" dy="1" layer="1" rot="R90"/>
<smd name="P$4" x="-1.2" y="-0.4" dx="0.5" dy="1" layer="1" rot="R90"/>
<smd name="P$5" x="-1.2" y="-1.2" dx="0.5" dy="1" layer="1" rot="R90"/>
<smd name="P$6" x="-1.2" y="-2" dx="0.5" dy="1" layer="1" rot="R90"/>
<smd name="P$7" x="0" y="-2.2" dx="0.5" dy="1.2" layer="1" rot="R180"/>
<smd name="P$8" x="1.2" y="-2" dx="0.5" dy="1" layer="1" rot="R270"/>
<smd name="P$9" x="1.2" y="-1.2" dx="0.5" dy="1" layer="1" rot="R270"/>
<smd name="P$10" x="1.2" y="-0.4" dx="0.5" dy="1" layer="1" rot="R270"/>
<smd name="P$11" x="1.2" y="0.4" dx="0.5" dy="1" layer="1" rot="R270"/>
<smd name="P$12" x="1.2" y="1.2" dx="0.5" dy="1" layer="1" rot="R270"/>
<smd name="P$13" x="1.2" y="2" dx="0.5" dy="1" layer="1" rot="R270"/>
<smd name="P$14" x="0" y="2.2" dx="0.5" dy="1.2" layer="1"/>
</package>
</packages>
<packages3d>
<package3d name="ST_LGA14_3X5" urn="urn:adsk.eagle:package:9987752/2" type="model">
<packageinstances>
<packageinstance name="ST_LGA14_3X5"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="LMS303DLHC">
<wire x1="-12.7" y1="12.7" x2="7.62" y2="12.7" width="0.254" layer="94"/>
<wire x1="7.62" y1="12.7" x2="7.62" y2="-10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="-10.16" x2="-12.7" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-10.16" x2="-12.7" y2="12.7" width="0.254" layer="94"/>
<text x="0" y="-12.7143" size="1.78" layer="95">&gt;NAME</text>
<text x="0" y="15.2503" size="1.7792" layer="96" rot="MR180">&gt;VALUE</text>
<pin name="VDDIO" x="-15.24" y="7.62" visible="pin" length="short" direction="in"/>
<pin name="SCL" x="-15.24" y="5.08" visible="pin" length="short"/>
<pin name="SDA" x="-15.24" y="2.54" visible="pin" length="short"/>
<pin name="INT2" x="-15.24" y="0" visible="pin" length="short" direction="out"/>
<pin name="INT1" x="-15.24" y="-2.54" visible="pin" length="short" direction="out"/>
<pin name="C1" x="-15.24" y="-5.08" visible="pin" length="short"/>
<pin name="GND" x="-2.54" y="-12.7" visible="pin" length="short" direction="pwr" rot="R90"/>
<pin name="RES@1" x="10.16" y="-5.08" visible="pin" length="short" rot="R180"/>
<pin name="DRDY" x="10.16" y="-2.54" visible="pin" length="short" direction="out" rot="R180"/>
<pin name="RES@2" x="10.16" y="0" visible="pin" length="short" rot="R180"/>
<pin name="RES@3" x="10.16" y="2.54" visible="pin" length="short" rot="R180"/>
<pin name="SETP" x="10.16" y="5.08" visible="pin" length="short" rot="R180"/>
<pin name="SETC" x="10.16" y="7.62" visible="pin" length="short" rot="R180"/>
<pin name="VDD" x="-2.54" y="15.24" visible="pin" length="short" direction="pwr" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="LSM303DLHC" prefix="U">
<description>ST LSM303DLHC I2C MEMS Magnetometer and Accelerometer</description>
<gates>
<gate name="G$1" symbol="LMS303DLHC" x="2.54" y="-2.54"/>
</gates>
<devices>
<device name="" package="ST_LGA14_3X5">
<connects>
<connect gate="G$1" pin="C1" pad="P$6"/>
<connect gate="G$1" pin="DRDY" pad="P$9"/>
<connect gate="G$1" pin="GND" pad="P$7"/>
<connect gate="G$1" pin="INT1" pad="P$5"/>
<connect gate="G$1" pin="INT2" pad="P$4"/>
<connect gate="G$1" pin="RES@1" pad="P$8"/>
<connect gate="G$1" pin="RES@2" pad="P$10"/>
<connect gate="G$1" pin="RES@3" pad="P$11"/>
<connect gate="G$1" pin="SCL" pad="P$2"/>
<connect gate="G$1" pin="SDA" pad="P$3"/>
<connect gate="G$1" pin="SETC" pad="P$13"/>
<connect gate="G$1" pin="SETP" pad="P$12"/>
<connect gate="G$1" pin="VDD" pad="P$14"/>
<connect gate="G$1" pin="VDDIO" pad="P$1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:9987752/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="AVAILABILITY" value="Unavailable"/>
<attribute name="DESCRIPTION" value=" Ultra compact high performance e-compass: 3D accelerometer and 3D magnetometer module "/>
<attribute name="MF" value="STMicroelectronics"/>
<attribute name="MP" value="LSM303DLHC"/>
<attribute name="PACKAGE" value="LGA-14 STMicroelectronics"/>
<attribute name="PRICE" value="None"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="BME280">
<packages>
<package name="PSON65P250X250X100-8N" urn="urn:adsk.eagle:footprint:9948090/1">
<description>2.5 mm x 2.5 mm x 0.93 mm metal lid LGA</description>
<text x="-1.256709375" y="1.66246875" size="0.4085875" layer="25">&gt;NAME</text>
<text x="-1.204140625" y="-2.010759375" size="0.4078" layer="27">&gt;VALUE</text>
<wire x1="-1.3" y1="1.3" x2="-1.3" y2="-1.3" width="0.127" layer="51"/>
<wire x1="-1.3" y1="-1.3" x2="1.3" y2="-1.3" width="0.127" layer="51"/>
<wire x1="1.3" y1="-1.3" x2="1.3" y2="1.3" width="0.127" layer="51"/>
<wire x1="1.3" y1="1.3" x2="-1.3" y2="1.3" width="0.127" layer="51"/>
<wire x1="-1.3" y1="-1.385" x2="1.3" y2="-1.385" width="0.127" layer="21"/>
<wire x1="-1.3" y1="1.385" x2="1.3" y2="1.385" width="0.127" layer="21"/>
<wire x1="-1.55" y1="1.55" x2="1.55" y2="1.55" width="0.05" layer="39"/>
<wire x1="1.55" y1="1.55" x2="1.55" y2="-1.55" width="0.05" layer="39"/>
<wire x1="1.55" y1="-1.55" x2="-1.55" y2="-1.55" width="0.05" layer="39"/>
<wire x1="-1.55" y1="-1.55" x2="-1.55" y2="1.55" width="0.05" layer="39"/>
<circle x="1.6" y="1" radius="0.05" width="0.2" layer="21"/>
<circle x="1" y="1" radius="0.05" width="0.2" layer="51"/>
<smd name="3" x="0.97" y="-0.325" dx="0.49" dy="0.45" layer="1" roundness="25"/>
<smd name="2" x="0.97" y="0.325" dx="0.49" dy="0.45" layer="1" roundness="25"/>
<smd name="1" x="0.97" y="0.975" dx="0.49" dy="0.45" layer="1" roundness="25"/>
<smd name="4" x="0.97" y="-0.975" dx="0.49" dy="0.45" layer="1" roundness="25"/>
<smd name="6" x="-0.97" y="-0.325" dx="0.49" dy="0.45" layer="1" roundness="25"/>
<smd name="7" x="-0.97" y="0.325" dx="0.49" dy="0.45" layer="1" roundness="25"/>
<smd name="8" x="-0.97" y="0.975" dx="0.49" dy="0.45" layer="1" roundness="25"/>
<smd name="5" x="-0.97" y="-0.975" dx="0.49" dy="0.45" layer="1" roundness="25"/>
</package>
</packages>
<packages3d>
<package3d name="PSON65P250X250X100-8N" urn="urn:adsk.eagle:package:9948092/2" type="model">
<description>2.5 mm x 2.5 mm x 0.93 mm metal lid LGA</description>
<packageinstances>
<packageinstance name="PSON65P250X250X100-8N"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="BME280">
<wire x1="-10.16" y1="12.7" x2="10.16" y2="12.7" width="0.4064" layer="94"/>
<wire x1="10.16" y1="12.7" x2="10.16" y2="-12.7" width="0.4064" layer="94"/>
<wire x1="10.16" y1="-12.7" x2="-10.16" y2="-12.7" width="0.4064" layer="94"/>
<wire x1="-10.16" y1="-12.7" x2="-10.16" y2="12.7" width="0.4064" layer="94"/>
<text x="-10.1662" y="13.3431" size="1.786590625" layer="95">&gt;NAME</text>
<text x="-10.1683" y="-15.2524" size="1.782940625" layer="96">&gt;VALUE</text>
<pin name="VDD" x="15.24" y="7.62" length="middle" direction="pwr" rot="R180"/>
<pin name="GND" x="15.24" y="-10.16" length="middle" direction="pwr" rot="R180"/>
<pin name="VDDIO" x="15.24" y="10.16" length="middle" direction="pwr" rot="R180"/>
<pin name="CSB" x="-15.24" y="2.54" length="middle" direction="in"/>
<pin name="SDO" x="-15.24" y="-5.08" length="middle"/>
<pin name="SDI" x="-15.24" y="-2.54" length="middle"/>
<pin name="SCK" x="-15.24" y="0" length="middle" direction="in" function="clk"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="BME280" prefix="U">
<description>DIGITAL HUMIDITY, PRESSURE AND TEMPERATURE SENSOR&lt;br&gt;* Digital interface I²C (up to 3.4 MHz) and SPI (3 and 4 wire, up to 10 MHz)* Supply voltage VDD main supply voltage range: 1.71 V to 3.6 V*VDDIO interface voltage range: 1.2 V to 3.6 V&lt;br&gt;
Operating range -40…+85 °C, 0…100 % rel. humidity, 300…1100 hPa
&lt;br&gt;&lt;br&gt;

The library has been designed by&lt;a href="https://www.facebook.com/groups/eaglecadsoftUK"&gt; Richard Magdycz&lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="BME280" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PSON65P250X250X100-8N">
<connects>
<connect gate="G$1" pin="CSB" pad="2"/>
<connect gate="G$1" pin="GND" pad="1 7"/>
<connect gate="G$1" pin="SCK" pad="4"/>
<connect gate="G$1" pin="SDI" pad="3"/>
<connect gate="G$1" pin="SDO" pad="5"/>
<connect gate="G$1" pin="VDD" pad="8"/>
<connect gate="G$1" pin="VDDIO" pad="6"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:9948092/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="AVAILABILITY" value="Unavailable"/>
<attribute name="DESCRIPTION" value=" Integrated pressure, humidity and temperature sensor; 8-pin 2.5x2.5x0.93mm LGA "/>
<attribute name="MF" value="Bosch"/>
<attribute name="MP" value="BME280"/>
<attribute name="PACKAGE" value="LGA-8 Bosch"/>
<attribute name="PRICE" value="None"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26925/1" library_version="1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:26954/1" prefix="GND" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-DiscreteSemi">
<description>&lt;h3&gt;SparkFun Discrete Semiconductors&lt;/h3&gt;
This library contains diodes, optoisolators, TRIACs, MOSFETs, transistors, etc. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="SOD323" urn="urn:adsk.eagle:footprint:30969/1">
<description>&lt;b&gt;Small Outline Diode&lt;/b&gt;</description>
<wire x1="-0.8" y1="0.575" x2="0.8" y2="0.575" width="0.2032" layer="51"/>
<wire x1="0.8" y1="0.575" x2="0.8" y2="-0.575" width="0.2032" layer="51"/>
<wire x1="0.8" y1="-0.575" x2="-0.8" y2="-0.575" width="0.2032" layer="51"/>
<wire x1="-0.8" y1="-0.575" x2="-0.8" y2="0.575" width="0.2032" layer="51"/>
<smd name="1" x="-1.1" y="0" dx="1" dy="0.6" layer="1"/>
<smd name="2" x="1.1" y="0" dx="1" dy="0.6" layer="1"/>
<text x="-0.9" y="0.78" size="1.016" layer="25">&gt;NAME</text>
<text x="-0.9" y="-1.805" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.35" y1="-0.2" x2="-0.9" y2="0.2" layer="51"/>
<rectangle x1="0.9" y1="-0.2" x2="1.35" y2="0.2" layer="51"/>
<rectangle x1="-0.75" y1="-0.575" x2="-0.375" y2="0.575" layer="51"/>
</package>
<package name="SOD-523">
<description>SOD-523 (Small Outline Diode)</description>
<smd name="C" x="0.7" y="0" dx="0.4" dy="0.4" layer="1"/>
<smd name="A" x="-0.7" y="0" dx="0.4" dy="0.4" layer="1"/>
<wire x1="-0.625" y1="-0.425" x2="0.625" y2="-0.425" width="0.127" layer="21"/>
<wire x1="0.625" y1="0.425" x2="-0.625" y2="0.425" width="0.127" layer="21"/>
<wire x1="-0.6" y1="-0.4" x2="0.3" y2="-0.4" width="0.127" layer="51"/>
<wire x1="0.3" y1="-0.4" x2="0.6" y2="-0.4" width="0.127" layer="51"/>
<wire x1="0.6" y1="-0.4" x2="0.6" y2="-0.1" width="0.127" layer="51"/>
<wire x1="0.6" y1="-0.1" x2="0.6" y2="0.1" width="0.127" layer="51"/>
<wire x1="0.6" y1="0.1" x2="0.6" y2="0.4" width="0.127" layer="51"/>
<wire x1="0.6" y1="0.4" x2="0.3" y2="0.4" width="0.127" layer="51"/>
<wire x1="0.3" y1="0.4" x2="-0.6" y2="0.4" width="0.127" layer="51"/>
<wire x1="-0.6" y1="0.4" x2="-0.6" y2="0.1" width="0.127" layer="51"/>
<wire x1="-0.6" y1="0.1" x2="-0.6" y2="-0.1" width="0.127" layer="51"/>
<wire x1="-0.6" y1="-0.1" x2="-0.6" y2="-0.4" width="0.127" layer="51"/>
<wire x1="0.6" y1="0.1" x2="0.8" y2="0.1" width="0.127" layer="51"/>
<wire x1="0.8" y1="0.1" x2="0.8" y2="-0.1" width="0.127" layer="51"/>
<wire x1="0.8" y1="-0.1" x2="0.6" y2="-0.1" width="0.127" layer="51"/>
<wire x1="-0.6" y1="-0.1" x2="-0.8" y2="-0.1" width="0.127" layer="51"/>
<wire x1="-0.6" y1="0.1" x2="-0.8" y2="0.1" width="0.127" layer="51"/>
<wire x1="-0.8" y1="0.1" x2="-0.8" y2="-0.1" width="0.127" layer="51"/>
<wire x1="0.3" y1="0.4" x2="0.3" y2="-0.4" width="0.127" layer="51"/>
<wire x1="1.1176" y1="0.3048" x2="1.1176" y2="-0.3048" width="0.2032" layer="21"/>
</package>
<package name="SMA-DIODE">
<description>&lt;B&gt;Diode&lt;/B&gt;
&lt;p&gt;Basic SMA packaged diode. Good for reverse polarization protection. Common part #: MBRA140&lt;/p&gt;
&lt;p&gt;SMA is the smallest package in the DO-214 standard (DO-214AC)&lt;/p&gt;</description>
<wire x1="-2.3" y1="1" x2="-2.3" y2="1.45" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="1.45" x2="2.3" y2="1.45" width="0.2032" layer="21"/>
<wire x1="2.3" y1="1.45" x2="2.3" y2="1" width="0.2032" layer="21"/>
<wire x1="2.3" y1="-1" x2="2.3" y2="-1.45" width="0.2032" layer="21"/>
<wire x1="2.3" y1="-1.45" x2="-2.3" y2="-1.45" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.45" x2="-2.3" y2="-1" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1" x2="3.175" y2="-1" width="0.2032" layer="21"/>
<smd name="A" x="-2.15" y="0" dx="1.27" dy="1.47" layer="1" rot="R180"/>
<smd name="C" x="2.15" y="0" dx="1.27" dy="1.47" layer="1"/>
<text x="0" y="1.651" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.651" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
</package>
<package name="SOT23-3">
<description>SOT23-3</description>
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.4224" y1="-0.6604" x2="-1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="-0.6604" x2="-1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="0.6604" x2="1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-0.8" y1="0.7" x2="-1.4" y2="0.7" width="0.2032" layer="21"/>
<wire x1="-1.4" y1="0.7" x2="-1.4" y2="-0.1" width="0.2032" layer="21"/>
<wire x1="0.8" y1="0.7" x2="1.4" y2="0.7" width="0.2032" layer="21"/>
<wire x1="1.4" y1="0.7" x2="1.4" y2="-0.1" width="0.2032" layer="21"/>
<smd name="1" x="-0.95" y="-1" dx="0.8" dy="0.9" layer="1"/>
<smd name="2" x="0.95" y="-1" dx="0.8" dy="0.9" layer="1"/>
<smd name="3" x="0" y="1.1" dx="0.8" dy="0.9" layer="1"/>
<text x="-1.651" y="0" size="0.6096" layer="25" font="vector" ratio="20" rot="R90" align="bottom-center">&gt;NAME</text>
<text x="1.651" y="0" size="0.6096" layer="27" font="vector" ratio="20" rot="R90" align="top-center">&gt;VALUE</text>
</package>
<package name="DIODE-MICRO-SMP">
<wire x1="-1.3462" y1="0.4572" x2="-1.3462" y2="-0.4572" width="0.1524" layer="51"/>
<wire x1="1.3462" y1="-0.381" x2="1.3462" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-1.3462" y1="-0.7112" x2="1.3462" y2="-0.7112" width="0.1524" layer="51"/>
<wire x1="1.3462" y1="-0.7112" x2="1.3462" y2="0.7112" width="0.1524" layer="51"/>
<wire x1="1.3462" y1="0.7112" x2="-1.3462" y2="0.7112" width="0.1524" layer="51"/>
<wire x1="-1.3462" y1="0.7112" x2="-1.3462" y2="-0.7112" width="0.1524" layer="51"/>
<smd name="C" x="-0.6985" y="0" dx="2.0066" dy="1.102359375" layer="1"/>
<smd name="A" x="1.2192" y="0" dx="0.8128" dy="0.8128" layer="1"/>
<text x="0" y="1.016" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-1.6002" size="0.6096" layer="27" font="vector" ratio="20" align="bottom-center">&gt;Value</text>
<wire x1="-2.0574" y1="-0.6096" x2="-2.0574" y2="0.5842" width="0.127" layer="21"/>
</package>
</packages>
<packages3d>
<package3d name="SOD323" urn="urn:adsk.eagle:package:30992/2" type="model">
<description>Small Outline Diode</description>
<packageinstances>
<packageinstance name="SOD323"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="DIODE-SCHOTTKY">
<description>&lt;h3&gt; Schottky Diode&lt;/h3&gt;
Diode with low voltage drop</description>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.778" y2="1.27" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-1.27" x2="0.762" y2="-1.27" width="0.1524" layer="94"/>
<text x="-2.54" y="2.032" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="-2.54" y="-2.032" size="1.778" layer="96" font="vector" align="top-left">&gt;VALUE</text>
<pin name="A" x="-2.54" y="0" visible="off" length="point" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="point" direction="pas" rot="R180"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="0.762" y1="-1.27" x2="0.762" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="1.778" y1="1.27" x2="1.778" y2="1.016" width="0.1524" layer="94"/>
<polygon width="0.1524" layer="94">
<vertex x="-1.27" y="1.27"/>
<vertex x="1.27" y="0"/>
<vertex x="-1.27" y="-1.27"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="DIODE-SCHOTTKY" prefix="D">
<description>&lt;h3&gt;Schottky diode&lt;/h3&gt;
&lt;p&gt;A Schottky diode is a semiconductor diode which has a low forward voltage drop and a very fast switching action.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="DIODE-SCHOTTKY" x="0" y="0"/>
</gates>
<devices>
<device name="-BAT20J" package="SOD323">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="C" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:30992/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-11623"/>
<attribute name="VALUE" value="1A/23V/620mV"/>
</technology>
</technologies>
</device>
<device name="-RB751S40" package="SOD-523">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-11018"/>
<attribute name="VALUE" value="120mA/40V/370mV"/>
</technology>
</technologies>
</device>
<device name="-SS14" package="SMA-DIODE">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-08053"/>
<attribute name="VALUE" value="1A/40V/500mV"/>
</technology>
</technologies>
</device>
<device name="-PMEG4005EJ" package="SOD323">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="C" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:30992/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-10955"/>
<attribute name="VALUE" value="0.5A/40V/420mV"/>
</technology>
</technologies>
</device>
<device name="-B340A" package="SMA-DIODE">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-09886"/>
<attribute name="VALUE" value="3A/40V/500mV"/>
</technology>
</technologies>
</device>
<device name="-ZLLS500" package="SOT23-3">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-08411"/>
<attribute name="VALUE" value="700mA/40V/533mV"/>
</technology>
</technologies>
</device>
<device name="-MSS1P6-M3/89A" package="DIODE-MICRO-SMP">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-13919" constant="no"/>
<attribute name="VALUE" value="1A/60V/520mV" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="wirepad" urn="urn:adsk.eagle:library:412">
<description>&lt;b&gt;Single Pads&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="1,6/0,8" urn="urn:adsk.eagle:footprint:30809/1" library_version="1">
<description>&lt;b&gt;THROUGH-HOLE PAD&lt;/b&gt;</description>
<wire x1="-0.762" y1="0.762" x2="-0.508" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="0.762" x2="-0.762" y2="0.508" width="0.1524" layer="21"/>
<wire x1="0.762" y1="0.762" x2="0.762" y2="0.508" width="0.1524" layer="21"/>
<wire x1="0.762" y1="0.762" x2="0.508" y2="0.762" width="0.1524" layer="21"/>
<wire x1="0.762" y1="-0.508" x2="0.762" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="0.762" y1="-0.762" x2="0.508" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="-0.762" x2="-0.762" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-0.762" x2="-0.762" y2="-0.508" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="0.635" width="0.1524" layer="51"/>
<pad name="1" x="0" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<text x="-0.762" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="0.6" size="0.0254" layer="27">&gt;VALUE</text>
</package>
<package name="1,6/0,9" urn="urn:adsk.eagle:footprint:30812/1" library_version="1">
<description>&lt;b&gt;THROUGH-HOLE PAD&lt;/b&gt;</description>
<wire x1="-0.508" y1="0.762" x2="-0.762" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="0.762" x2="-0.762" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-0.508" x2="-0.762" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-0.762" x2="-0.508" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="0.508" y1="-0.762" x2="0.762" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="0.762" y1="-0.762" x2="0.762" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.762" y1="0.508" x2="0.762" y2="0.762" width="0.1524" layer="21"/>
<wire x1="0.762" y1="0.762" x2="0.508" y2="0.762" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="0.635" width="0.1524" layer="51"/>
<pad name="1" x="0" y="0" drill="0.9144" diameter="1.6002" shape="octagon"/>
<text x="-0.762" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="0.6" size="0.0254" layer="27">&gt;VALUE</text>
</package>
<package name="2,15/1,0" urn="urn:adsk.eagle:footprint:30813/1" library_version="1">
<description>&lt;b&gt;THROUGH-HOLE PAD&lt;/b&gt;</description>
<wire x1="1.143" y1="-1.143" x2="1.143" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-1.143" x2="0.635" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.635" x2="1.143" y2="1.143" width="0.1524" layer="21"/>
<wire x1="1.143" y1="1.143" x2="0.635" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.143" x2="-1.143" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="1.143" x2="-1.143" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-0.635" x2="-1.143" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-1.143" x2="-0.635" y2="-1.143" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="2.159" shape="octagon"/>
<text x="-1.143" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="1" size="0.0254" layer="27">&gt;VALUE</text>
</package>
<package name="2,54/0,8" urn="urn:adsk.eagle:footprint:30820/1" library_version="1">
<description>&lt;b&gt;THROUGH-HOLE PAD&lt;/b&gt;</description>
<wire x1="-1.27" y1="1.27" x2="-0.762" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0.762" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0.762" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="0.762" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.762" x2="1.27" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="0.762" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-1.27" x2="-1.27" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-0.762" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="0.635" width="0.1524" layer="51"/>
<pad name="1" x="0" y="0" drill="0.8128" diameter="2.54" shape="octagon"/>
<text x="-1.27" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="0.6" size="0.0254" layer="27">&gt;VALUE</text>
</package>
<package name="2,54/0,9" urn="urn:adsk.eagle:footprint:30821/1" library_version="1">
<description>&lt;b&gt;THROUGH-HOLE PAD&lt;/b&gt;</description>
<wire x1="-1.27" y1="1.27" x2="-0.762" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0.762" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0.762" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="0.762" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.762" x2="1.27" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="0.762" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-1.27" x2="-1.27" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-0.762" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="0.635" width="0.1524" layer="51"/>
<pad name="1" x="0" y="0" drill="0.9144" diameter="2.54" shape="octagon"/>
<text x="-1.27" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="0.6" size="0.0254" layer="27">&gt;VALUE</text>
</package>
<package name="2,54/1,0" urn="urn:adsk.eagle:footprint:30810/1" library_version="1">
<description>&lt;b&gt;THROUGH-HOLE PAD&lt;/b&gt;</description>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0.762" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="0.762" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-0.762" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-0.762" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.762" y1="-1.27" x2="1.27" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="-0.762" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="2.54" shape="octagon"/>
<text x="-1.27" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="1" size="0.0254" layer="27">&gt;VALUE</text>
</package>
<package name="2,54/1,1" urn="urn:adsk.eagle:footprint:30818/1" library_version="1">
<description>&lt;b&gt;THROUGH-HOLE PAD&lt;/b&gt;</description>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0.762" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="0.762" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-0.762" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-0.762" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="0.762" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="-0.762" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="0" y="0" drill="1.1176" diameter="2.54" shape="octagon"/>
<text x="-1.27" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="1" size="0.0254" layer="27">&gt;VALUE</text>
</package>
<package name="3,17/1,1" urn="urn:adsk.eagle:footprint:30814/1" library_version="1">
<description>&lt;b&gt;THROUGH-HOLE PAD&lt;/b&gt;</description>
<wire x1="1.524" y1="-1.016" x2="1.524" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="1.524" y1="-1.524" x2="1.016" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="-1.524" x2="-1.524" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-1.524" x2="-1.524" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="1.016" x2="-1.524" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="1.524" x2="-1.016" y2="1.524" width="0.1524" layer="21"/>
<wire x1="1.016" y1="1.524" x2="1.524" y2="1.524" width="0.1524" layer="21"/>
<wire x1="1.524" y1="1.524" x2="1.524" y2="1.016" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="1.27" width="0.1524" layer="51"/>
<pad name="1" x="0" y="0" drill="1.1176" diameter="3.175" shape="octagon"/>
<text x="-1.524" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="1.2" size="0.0254" layer="27">&gt;VALUE</text>
</package>
<package name="3,17/1,2" urn="urn:adsk.eagle:footprint:30824/1" library_version="1">
<description>&lt;b&gt;THROUGH-HOLE PAD&lt;/b&gt;</description>
<wire x1="1.524" y1="-1.016" x2="1.524" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="1.524" y1="-1.524" x2="1.016" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="-1.524" x2="-1.524" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-1.524" x2="-1.524" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="1.016" x2="-1.524" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="1.524" x2="-1.016" y2="1.524" width="0.1524" layer="21"/>
<wire x1="1.016" y1="1.524" x2="1.524" y2="1.524" width="0.1524" layer="21"/>
<wire x1="1.524" y1="1.524" x2="1.524" y2="1.016" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="1.27" width="0.1524" layer="51"/>
<pad name="1" x="0" y="0" drill="1.1938" diameter="3.175" shape="octagon"/>
<text x="-1.524" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="1.2" size="0.0254" layer="27">&gt;VALUE</text>
</package>
<package name="3,17/1,3" urn="urn:adsk.eagle:footprint:30815/1" library_version="1">
<description>&lt;b&gt;THROUGH-HOLE PAD&lt;/b&gt;</description>
<wire x1="1.524" y1="-1.016" x2="1.524" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="1.524" y1="-1.524" x2="1.016" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="-1.524" x2="-1.524" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-1.524" x2="-1.524" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="1.016" x2="-1.524" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="1.524" x2="-1.016" y2="1.524" width="0.1524" layer="21"/>
<wire x1="1.016" y1="1.524" x2="1.524" y2="1.524" width="0.1524" layer="21"/>
<wire x1="1.524" y1="1.524" x2="1.524" y2="1.016" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="1.27" width="0.1524" layer="51"/>
<pad name="1" x="0" y="0" drill="1.3208" diameter="3.175" shape="octagon"/>
<text x="-1.524" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="1.2" size="0.0254" layer="27">&gt;VALUE</text>
</package>
<package name="3,81/1,1" urn="urn:adsk.eagle:footprint:30811/1" library_version="1">
<description>&lt;b&gt;THROUGH-HOLE PAD&lt;/b&gt;</description>
<wire x1="1.905" y1="-1.27" x2="1.905" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.905" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-1.905" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.905" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-1.905" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.905" x2="-1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.905" y2="1.905" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.905" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="1.27" width="0.1524" layer="51"/>
<pad name="1" x="0" y="0" drill="1.1176" diameter="3.81" shape="octagon"/>
<text x="-1.905" y="2.286" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="1.2" size="0.0254" layer="27">&gt;VALUE</text>
</package>
<package name="3,81/1,3" urn="urn:adsk.eagle:footprint:30816/1" library_version="1">
<description>&lt;b&gt;THROUGH-HOLE PAD&lt;/b&gt;</description>
<wire x1="1.905" y1="-1.27" x2="1.905" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.905" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-1.905" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.905" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-1.905" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.905" x2="-1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.905" y2="1.905" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.905" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="1.27" width="0.1524" layer="51"/>
<pad name="1" x="0" y="0" drill="1.3208" diameter="3.81" shape="octagon"/>
<text x="-1.905" y="2.286" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="1.2" size="0.0254" layer="27">&gt;VALUE</text>
</package>
<package name="3,81/1,4" urn="urn:adsk.eagle:footprint:30817/1" library_version="1">
<description>&lt;b&gt;THROUGH-HOLE PAD&lt;/b&gt;</description>
<wire x1="1.905" y1="-1.27" x2="1.905" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.905" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-1.905" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.905" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-1.905" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.905" x2="-1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.905" y2="1.905" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.905" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="1.27" width="0.1524" layer="51"/>
<pad name="1" x="0" y="0" drill="1.397" diameter="3.81" shape="octagon"/>
<text x="-1.905" y="2.286" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="1.2" size="0.0254" layer="27">&gt;VALUE</text>
</package>
<package name="4,16O1,6" urn="urn:adsk.eagle:footprint:30825/1" library_version="1">
<description>&lt;b&gt;THROUGH-HOLE PAD&lt;/b&gt;</description>
<pad name="1" x="0" y="0" drill="1.6002" diameter="4.1656" shape="octagon"/>
<text x="0" y="0" size="0.0254" layer="27">&gt;VALUE</text>
<text x="-2.1" y="2.2" size="1.27" layer="25">&gt;NAME</text>
</package>
<package name="5-1,8" urn="urn:adsk.eagle:footprint:30826/1" library_version="1">
<description>&lt;b&gt;THROUGH-HOLE PAD&lt;/b&gt;</description>
<wire x1="1.1684" y1="2.794" x2="-1.1684" y2="2.794" width="0.1524" layer="21"/>
<wire x1="-1.1684" y1="-2.794" x2="-1.1684" y2="2.794" width="0.1524" layer="21"/>
<wire x1="-1.1684" y1="-2.794" x2="1.1684" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="1.1684" y1="2.794" x2="1.1684" y2="-2.794" width="0.1524" layer="21"/>
<smd name="1" x="0" y="0" dx="1.8288" dy="5.08" layer="1"/>
<text x="-1.524" y="-2.54" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-0.1" y="2.8" size="0.0254" layer="27">&gt;VALUE</text>
</package>
<package name="5-2,5" urn="urn:adsk.eagle:footprint:30827/1" library_version="1">
<description>&lt;b&gt;THROUGH-HOLE PAD&lt;/b&gt;</description>
<wire x1="1.524" y1="2.794" x2="-1.524" y2="2.794" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-2.794" x2="-1.524" y2="2.794" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-2.794" x2="1.524" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="1.524" y1="2.794" x2="1.524" y2="-2.794" width="0.1524" layer="21"/>
<smd name="1" x="0" y="0" dx="2.54" dy="5.08" layer="1"/>
<text x="-1.778" y="-2.54" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-0.1" y="2.8" size="0.0254" layer="27">&gt;VALUE</text>
</package>
<package name="SMD1,27-2,54" urn="urn:adsk.eagle:footprint:30822/1" library_version="1">
<description>&lt;b&gt;SMD PAD&lt;/b&gt;</description>
<smd name="1" x="0" y="0" dx="1.27" dy="2.54" layer="1"/>
<text x="0" y="0" size="0.0254" layer="27">&gt;VALUE</text>
<text x="-0.8" y="-2.4" size="1.27" layer="25" rot="R90">&gt;NAME</text>
</package>
<package name="SMD2,54-5,08" urn="urn:adsk.eagle:footprint:30823/1" library_version="1">
<description>&lt;b&gt;SMD PAD&lt;/b&gt;</description>
<smd name="1" x="0" y="0" dx="2.54" dy="5.08" layer="1"/>
<text x="0" y="0" size="0.0254" layer="27">&gt;VALUE</text>
<text x="-1.5" y="-2.5" size="1.27" layer="25" rot="R90">&gt;NAME</text>
</package>
</packages>
<packages3d>
<package3d name="1,6/0,8" urn="urn:adsk.eagle:package:30830/1" type="box" library_version="1">
<description>THROUGH-HOLE PAD</description>
<packageinstances>
<packageinstance name="1,6/0,8"/>
</packageinstances>
</package3d>
<package3d name="1,6/0,9" urn="urn:adsk.eagle:package:30840/1" type="box" library_version="1">
<description>THROUGH-HOLE PAD</description>
<packageinstances>
<packageinstance name="1,6/0,9"/>
</packageinstances>
</package3d>
<package3d name="2,15/1,0" urn="urn:adsk.eagle:package:30831/1" type="box" library_version="1">
<description>THROUGH-HOLE PAD</description>
<packageinstances>
<packageinstance name="2,15/1,0"/>
</packageinstances>
</package3d>
<package3d name="2,54/0,8" urn="urn:adsk.eagle:package:30838/1" type="box" library_version="1">
<description>THROUGH-HOLE PAD</description>
<packageinstances>
<packageinstance name="2,54/0,8"/>
</packageinstances>
</package3d>
<package3d name="2,54/0,9" urn="urn:adsk.eagle:package:30847/1" type="box" library_version="1">
<description>THROUGH-HOLE PAD</description>
<packageinstances>
<packageinstance name="2,54/0,9"/>
</packageinstances>
</package3d>
<package3d name="2,54/1,0" urn="urn:adsk.eagle:package:30828/1" type="box" library_version="1">
<description>THROUGH-HOLE PAD</description>
<packageinstances>
<packageinstance name="2,54/1,0"/>
</packageinstances>
</package3d>
<package3d name="2,54/1,1" urn="urn:adsk.eagle:package:30836/1" type="box" library_version="1">
<description>THROUGH-HOLE PAD</description>
<packageinstances>
<packageinstance name="2,54/1,1"/>
</packageinstances>
</package3d>
<package3d name="3,17/1,1" urn="urn:adsk.eagle:package:30832/1" type="box" library_version="1">
<description>THROUGH-HOLE PAD</description>
<packageinstances>
<packageinstance name="3,17/1,1"/>
</packageinstances>
</package3d>
<package3d name="3,17/1,2" urn="urn:adsk.eagle:package:30842/1" type="box" library_version="1">
<description>THROUGH-HOLE PAD</description>
<packageinstances>
<packageinstance name="3,17/1,2"/>
</packageinstances>
</package3d>
<package3d name="3,17/1,3" urn="urn:adsk.eagle:package:30833/1" type="box" library_version="1">
<description>THROUGH-HOLE PAD</description>
<packageinstances>
<packageinstance name="3,17/1,3"/>
</packageinstances>
</package3d>
<package3d name="3,81/1,1" urn="urn:adsk.eagle:package:30829/1" type="box" library_version="1">
<description>THROUGH-HOLE PAD</description>
<packageinstances>
<packageinstance name="3,81/1,1"/>
</packageinstances>
</package3d>
<package3d name="3,81/1,3" urn="urn:adsk.eagle:package:30834/1" type="box" library_version="1">
<description>THROUGH-HOLE PAD</description>
<packageinstances>
<packageinstance name="3,81/1,3"/>
</packageinstances>
</package3d>
<package3d name="3,81/1,4" urn="urn:adsk.eagle:package:30835/1" type="box" library_version="1">
<description>THROUGH-HOLE PAD</description>
<packageinstances>
<packageinstance name="3,81/1,4"/>
</packageinstances>
</package3d>
<package3d name="4,16O1,6" urn="urn:adsk.eagle:package:30843/1" type="box" library_version="1">
<description>THROUGH-HOLE PAD</description>
<packageinstances>
<packageinstance name="4,16O1,6"/>
</packageinstances>
</package3d>
<package3d name="5-1,8" urn="urn:adsk.eagle:package:30844/1" type="box" library_version="1">
<description>THROUGH-HOLE PAD</description>
<packageinstances>
<packageinstance name="5-1,8"/>
</packageinstances>
</package3d>
<package3d name="5-2,5" urn="urn:adsk.eagle:package:30845/1" type="box" library_version="1">
<description>THROUGH-HOLE PAD</description>
<packageinstances>
<packageinstance name="5-2,5"/>
</packageinstances>
</package3d>
<package3d name="SMD1,27-2,54" urn="urn:adsk.eagle:package:30839/1" type="box" library_version="1">
<description>SMD PAD</description>
<packageinstances>
<packageinstance name="SMD1,27-2,54"/>
</packageinstances>
</package3d>
<package3d name="SMD2,54-5,08" urn="urn:adsk.eagle:package:30841/1" type="box" library_version="1">
<description>SMD PAD</description>
<packageinstances>
<packageinstance name="SMD2,54-5,08"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="PAD" urn="urn:adsk.eagle:symbol:30808/1" library_version="1">
<wire x1="-1.016" y1="1.016" x2="1.016" y2="-1.016" width="0.254" layer="94"/>
<wire x1="-1.016" y1="-1.016" x2="1.016" y2="1.016" width="0.254" layer="94"/>
<text x="-1.143" y="1.8542" size="1.778" layer="95">&gt;NAME</text>
<text x="-1.143" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="P" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="WIREPAD" urn="urn:adsk.eagle:component:30861/1" prefix="PAD" library_version="1">
<description>&lt;b&gt;Wire PAD&lt;/b&gt; connect wire on PCB</description>
<gates>
<gate name="G$1" symbol="PAD" x="0" y="0"/>
</gates>
<devices>
<device name="1,6/0,8" package="1,6/0,8">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:30830/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1,6/0,9" package="1,6/0,9">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:30840/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2,15/1,0" package="2,15/1,0">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:30831/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2,54/0,8" package="2,54/0,8">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:30838/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2,54/0,9" package="2,54/0,9">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:30847/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2,54/1,0" package="2,54/1,0">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:30828/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2,54/1,1" package="2,54/1,1">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:30836/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3,17/1,1" package="3,17/1,1">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:30832/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3,17/1,2" package="3,17/1,2">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:30842/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3,17/1,3" package="3,17/1,3">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:30833/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3,81/1,1" package="3,81/1,1">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:30829/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3,81/1,3" package="3,81/1,3">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:30834/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3,81/1,4" package="3,81/1,4">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:30835/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="4,16O1,6" package="4,16O1,6">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:30843/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD5-1,8" package="5-1,8">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:30844/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD5-2,5" package="5-2,5">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:30845/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD1,27-254" package="SMD1,27-2,54">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:30839/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD2,54-5,08" package="SMD2,54-5,08">
<connects>
<connect gate="G$1" pin="P" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:30841/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="LM2750SD-5.0">
<packages>
<package name="DFN300X300X80-11N" urn="urn:adsk.eagle:footprint:9987728/1">
<wire x1="-1.5494" y1="-1.5494" x2="1.5494" y2="-1.5494" width="0.1524" layer="21"/>
<wire x1="1.5494" y1="1.5494" x2="-1.5494" y2="1.5494" width="0.1524" layer="21"/>
<wire x1="-2.1844" y1="0.9144" x2="-2.1844" y2="1.0668" width="0" layer="21" curve="-208"/>
<wire x1="-1.0414" y1="1.5494" x2="-1.5494" y2="1.0414" width="0.1524" layer="51"/>
<wire x1="-1.5494" y1="-1.5494" x2="1.5494" y2="-1.5494" width="0.1524" layer="51"/>
<wire x1="1.5494" y1="-1.5494" x2="1.5494" y2="1.5494" width="0.1524" layer="51"/>
<wire x1="1.5494" y1="1.5494" x2="-1.5494" y2="1.5494" width="0.1524" layer="51"/>
<wire x1="-1.5494" y1="1.5494" x2="-1.5494" y2="-1.5494" width="0.1524" layer="51"/>
<wire x1="-2.1336" y1="0.9906" x2="-2.286" y2="0.9906" width="0.1524" layer="51" curve="-180"/>
<wire x1="-2.286" y1="0.9906" x2="-2.1336" y2="0.9906" width="0.1524" layer="51" curve="-180"/>
<text x="-4.655109375" y="1.882390625" size="2.0859" layer="25" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-5.49203125" y="-3.7122" size="2.084940625" layer="27" ratio="10" rot="SR0">&gt;VALUE</text>
<smd name="1" x="-1.4478" y="0.9906" dx="0.8128" dy="0.3048" layer="1"/>
<smd name="2" x="-1.4478" y="0.508" dx="0.8128" dy="0.3048" layer="1"/>
<smd name="3" x="-1.4478" y="0" dx="0.8128" dy="0.3048" layer="1"/>
<smd name="4" x="-1.4478" y="-0.508" dx="0.8128" dy="0.3048" layer="1"/>
<smd name="5" x="-1.4478" y="-0.9906" dx="0.8128" dy="0.3048" layer="1"/>
<smd name="6" x="1.4478" y="-0.9906" dx="0.8128" dy="0.3048" layer="1" rot="R180"/>
<smd name="7" x="1.4478" y="-0.508" dx="0.8128" dy="0.3048" layer="1" rot="R180"/>
<smd name="8" x="1.4478" y="0" dx="0.8128" dy="0.3048" layer="1" rot="R180"/>
<smd name="9" x="1.4478" y="0.508" dx="0.8128" dy="0.3048" layer="1" rot="R180"/>
<smd name="10" x="1.4478" y="0.9906" dx="0.8128" dy="0.3048" layer="1" rot="R180"/>
<smd name="11" x="0" y="0" dx="1.7018" dy="2.1082" layer="1"/>
</package>
</packages>
<packages3d>
<package3d name="DFN300X300X80-11N" urn="urn:adsk.eagle:package:9987730/3" type="model">
<packageinstances>
<packageinstance name="DFN300X300X80-11N"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="LM2750SD-5.0">
<wire x1="-12.7" y1="15.24" x2="-12.7" y2="-20.32" width="0.4064" layer="94"/>
<wire x1="-12.7" y1="-20.32" x2="12.7" y2="-20.32" width="0.4064" layer="94"/>
<wire x1="12.7" y1="-20.32" x2="12.7" y2="15.24" width="0.4064" layer="94"/>
<wire x1="12.7" y1="15.24" x2="-12.7" y2="15.24" width="0.4064" layer="94"/>
<text x="-4.679709375" y="16.7351" size="2.08551875" layer="95" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-6.4805" y="-23.1519" size="2.08391875" layer="96" ratio="10" rot="SR0">&gt;VALUE</text>
<pin name="VIN_2" x="-17.78" y="10.16" length="middle" direction="in"/>
<pin name="VIN" x="-17.78" y="7.62" length="middle" direction="in"/>
<pin name="C+" x="-17.78" y="5.08" length="middle" direction="out"/>
<pin name="C-" x="-17.78" y="2.54" length="middle" direction="pas"/>
<pin name="FB" x="-17.78" y="-2.54" length="middle" direction="pas"/>
<pin name="SD" x="-17.78" y="-5.08" length="middle" direction="pas"/>
<pin name="GND_2" x="-17.78" y="-10.16" length="middle" direction="pas"/>
<pin name="GND_3" x="-17.78" y="-12.7" length="middle" direction="pas"/>
<pin name="GND" x="-17.78" y="-15.24" length="middle" direction="pas"/>
<pin name="VOUT_2" x="17.78" y="2.54" length="middle" direction="out" rot="R180"/>
<pin name="VOUT" x="17.78" y="5.08" length="middle" direction="out" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="LM2750SD-5.0" prefix="U">
<description>IC CONV REG SW CAP 5V</description>
<gates>
<gate name="A" symbol="LM2750SD-5.0" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DFN300X300X80-11N">
<connects>
<connect gate="A" pin="C+" pad="10"/>
<connect gate="A" pin="C-" pad="7"/>
<connect gate="A" pin="FB" pad="3"/>
<connect gate="A" pin="GND" pad="11"/>
<connect gate="A" pin="GND_2" pad="5"/>
<connect gate="A" pin="GND_3" pad="6"/>
<connect gate="A" pin="SD" pad="4"/>
<connect gate="A" pin="VIN" pad="9"/>
<connect gate="A" pin="VIN_2" pad="8"/>
<connect gate="A" pin="VOUT" pad="2"/>
<connect gate="A" pin="VOUT_2" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:9987730/3"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value=" Low Noise Regulator Switched Capacitor Boost Regulator 10-WSON -40 to 85 "/>
<attribute name="DIGI-KEY_PART_NUMBER" value="LM2750SD-5.0/NOPBCT-ND"/>
<attribute name="DIGI-KEY_PURCHASE_URL" value="https://www.digikey.com/product-detail/en/texas-instruments/LM2750SD-5.0-NOPB/LM2750SD-5.0-NOPBCT-ND/953485?utm_source=snapeda&amp;utm_medium=aggregator&amp;utm_campaign=symbol"/>
<attribute name="MF" value="Texas Instruments"/>
<attribute name="MP" value="LM2750SD-5.0/NOPB"/>
<attribute name="PACKAGE" value="WSON-10 Texas Instruments"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="HikeBuddy">
<packages>
<package name="ALPSSWITCH" urn="urn:adsk.eagle:footprint:9990208/1">
<smd name="P$3" x="0" y="0" dx="1.35" dy="1" layer="1"/>
<smd name="P$2" x="0" y="1.42" dx="1.35" dy="1" layer="1"/>
<smd name="P$1" x="0" y="2.84" dx="1.35" dy="1" layer="1"/>
<smd name="P$6" x="7.15" y="0" dx="1.35" dy="1" layer="1"/>
<smd name="P$5" x="7.15" y="1.42" dx="1.35" dy="1" layer="1"/>
<smd name="P$4" x="7.15" y="2.84" dx="1.35" dy="1" layer="1"/>
<smd name="P$7" x="3.575" y="-2.55" dx="2" dy="1.65" layer="1"/>
<smd name="P$8" x="3.575" y="5.4" dx="2" dy="1.65" layer="1"/>
<circle x="-1.651" y="2.794" radius="0.127" width="0.127" layer="21"/>
<hole x="3.575" y="-0.5" drill="1.05"/>
<hole x="3.575" y="3.35" drill="0.75"/>
<wire x1="-1.45" y1="1.3" x2="3.55" y2="6.3" width="0.127" layer="21"/>
<wire x1="3.55" y1="6.3" x2="8.55" y2="1.3" width="0.127" layer="21"/>
<wire x1="8.55" y1="1.3" x2="3.55" y2="-3.7" width="0.127" layer="21"/>
<wire x1="3.55" y1="-3.7" x2="-1.45" y2="1.3" width="0.127" layer="21"/>
</package>
<package name="GPSNEO7M">
<pad name="VIN" x="0" y="0" drill="1" diameter="1.778" shape="square"/>
<pad name="RX" x="2.54" y="0" drill="1" diameter="1.778"/>
<pad name="TX" x="5.08" y="0" drill="1" diameter="1.778"/>
<pad name="GND" x="7.62" y="0" drill="1" diameter="1.778"/>
<wire x1="-7.62" y1="1.27" x2="-7.62" y2="-25.4" width="0.127" layer="21"/>
<wire x1="-7.62" y1="-25.4" x2="15.24" y2="-25.4" width="0.127" layer="21"/>
<wire x1="15.24" y1="-25.4" x2="15.24" y2="1.27" width="0.127" layer="21"/>
<wire x1="15.24" y1="1.27" x2="-7.62" y2="1.27" width="0.127" layer="21"/>
<text x="-1.27" y="-5.08" size="1.27" layer="21">GPS NEO 7M</text>
</package>
</packages>
<packages3d>
<package3d name="ALPSSWITCH" urn="urn:adsk.eagle:package:9990219/2" type="model">
<packageinstances>
<packageinstance name="ALPSSWITCH"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="ALPSSWITCH">
<wire x1="0" y1="2.54" x2="0" y2="12.7" width="0.254" layer="94"/>
<wire x1="0" y1="12.7" x2="27.94" y2="12.7" width="0.254" layer="94"/>
<wire x1="27.94" y1="12.7" x2="27.94" y2="2.54" width="0.254" layer="94"/>
<wire x1="27.94" y1="2.54" x2="0" y2="2.54" width="0.254" layer="94"/>
<pin name="A" x="-5.08" y="10.16" length="middle"/>
<pin name="CENTER" x="-5.08" y="7.62" length="middle"/>
<pin name="C" x="-5.08" y="5.08" length="middle"/>
<pin name="B" x="33.02" y="10.16" length="middle" rot="R180"/>
<pin name="GND" x="33.02" y="7.62" length="middle" rot="R180"/>
<pin name="D" x="33.02" y="5.08" length="middle" rot="R180"/>
</symbol>
<symbol name="GPSNEO7M">
<wire x1="0" y1="0" x2="0" y2="20.32" width="0.254" layer="94"/>
<wire x1="0" y1="20.32" x2="10.16" y2="20.32" width="0.254" layer="94"/>
<wire x1="10.16" y1="20.32" x2="10.16" y2="0" width="0.254" layer="94"/>
<wire x1="10.16" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<pin name="VIN" x="-5.08" y="2.54" length="middle"/>
<pin name="TX" x="-5.08" y="12.7" length="middle"/>
<pin name="RX" x="-5.08" y="7.62" length="middle"/>
<pin name="GND" x="-5.08" y="17.78" length="middle"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ALPSSWITCH">
<gates>
<gate name="G$1" symbol="ALPSSWITCH" x="-12.7" y="-7.62"/>
</gates>
<devices>
<device name="" package="ALPSSWITCH">
<connects>
<connect gate="G$1" pin="A" pad="P$1"/>
<connect gate="G$1" pin="B" pad="P$4"/>
<connect gate="G$1" pin="C" pad="P$3"/>
<connect gate="G$1" pin="CENTER" pad="P$2"/>
<connect gate="G$1" pin="D" pad="P$6"/>
<connect gate="G$1" pin="GND" pad="P$5"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:9990219/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GPSNEO7M">
<gates>
<gate name="GPS" symbol="GPSNEO7M" x="-10.16" y="0"/>
</gates>
<devices>
<device name="" package="GPSNEO7M">
<connects>
<connect gate="GPS" pin="GND" pad="GND"/>
<connect gate="GPS" pin="RX" pad="RX"/>
<connect gate="GPS" pin="TX" pad="TX"/>
<connect gate="GPS" pin="VIN" pad="VIN"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="PCA9555">
<packages>
<package name="DIL24-6">
<description>&lt;b&gt;Dual In Line&lt;/b&gt;</description>
<wire x1="-15.113" y1="-1.27" x2="-15.113" y2="-6.604" width="0.1524" layer="21"/>
<wire x1="-15.113" y1="1.27" x2="-15.113" y2="-1.27" width="0.1524" layer="21" curve="-180"/>
<wire x1="15.113" y1="-6.604" x2="15.113" y2="6.604" width="0.1524" layer="21"/>
<wire x1="-15.113" y1="6.604" x2="-15.113" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-15.113" y1="6.604" x2="15.113" y2="6.604" width="0.1524" layer="21"/>
<wire x1="-15.113" y1="-6.604" x2="15.113" y2="-6.604" width="0.1524" layer="21"/>
<text x="-15.3757" y="-6.60775" size="1.779009375" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-11.4464" y="-2.54363125" size="1.780540625" layer="27" ratio="10">&gt;VALUE</text>
<pad name="1" x="-13.97" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="2" x="-11.43" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="3" x="-8.89" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="4" x="-6.35" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="5" x="-3.81" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="6" x="-1.27" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="7" x="1.27" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="8" x="3.81" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="9" x="6.35" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="10" x="8.89" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="11" x="11.43" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="12" x="13.97" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="13" x="13.97" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="14" x="11.43" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="15" x="8.89" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="16" x="6.35" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="17" x="3.81" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="18" x="1.27" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="19" x="-1.27" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="20" x="-3.81" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="21" x="-6.35" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="22" x="-8.89" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="23" x="-11.43" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="24" x="-13.97" y="7.62" drill="0.8128" shape="long" rot="R90"/>
</package>
<package name="SO24">
<description>&lt;b&gt;Small Outline Package&lt;/b&gt; 7.6 x 15.74 mm</description>
<wire x1="7.78" y1="-3.7" x2="-7.78" y2="-3.7" width="0.2032" layer="51"/>
<wire x1="-7.78" y1="-3.7" x2="-7.78" y2="-3.2" width="0.2032" layer="21"/>
<wire x1="-7.78" y1="-3.2" x2="-7.78" y2="3.7" width="0.2032" layer="21"/>
<wire x1="-7.78" y1="3.7" x2="7.78" y2="3.7" width="0.2032" layer="51"/>
<wire x1="7.78" y1="-3.2" x2="-7.78" y2="-3.2" width="0.2032" layer="21"/>
<wire x1="7.78" y1="3.7" x2="7.78" y2="-3.2" width="0.2032" layer="21"/>
<wire x1="7.78" y1="-3.2" x2="7.78" y2="-3.7" width="0.2032" layer="21"/>
<text x="-8.131640625" y="-3.68465" size="1.27056875" layer="25" rot="R90">&gt;NAME</text>
<text x="-6.998909375" y="-0.5090125" size="1.27253125" layer="27">&gt;VALUE</text>
<rectangle x1="-7.23991875" y1="-5.3273" x2="-6.74" y2="-3.8" layer="51"/>
<rectangle x1="-5.963659375" y1="-5.32326875" x2="-5.47" y2="-3.8" layer="51"/>
<rectangle x1="-4.69118125" y1="-5.321340625" x2="-4.2" y2="-3.8" layer="51"/>
<rectangle x1="-3.42095" y1="-5.32146875" x2="-2.93" y2="-3.8" layer="51"/>
<rectangle x1="-2.151" y1="-5.32246875" x2="-1.66" y2="-3.8" layer="51"/>
<rectangle x1="-0.8805" y1="-5.32301875" x2="-0.39" y2="-3.8" layer="51"/>
<rectangle x1="0.39049375" y1="-5.326759375" x2="0.88" y2="-3.8" layer="51"/>
<rectangle x1="1.66315" y1="-5.3301" x2="2.15" y2="-3.8" layer="51"/>
<rectangle x1="2.935559375" y1="-5.330090625" x2="3.42" y2="-3.8" layer="51"/>
<rectangle x1="4.208190625" y1="-5.33038125" x2="4.69" y2="-3.8" layer="51"/>
<rectangle x1="5.47246875" y1="-5.322409375" x2="5.96" y2="-3.8" layer="51"/>
<rectangle x1="6.74143125" y1="-5.32113125" x2="7.23" y2="-3.8" layer="51"/>
<rectangle x1="6.74906875" y1="3.805109375" x2="7.23" y2="5.32" layer="51"/>
<rectangle x1="5.470190625" y1="3.80013125" x2="5.96" y2="5.32" layer="51"/>
<rectangle x1="4.20468125" y1="3.804240625" x2="4.69" y2="5.32" layer="51"/>
<rectangle x1="2.93126875" y1="3.80165" x2="3.42" y2="5.32" layer="51"/>
<rectangle x1="1.66048125" y1="3.8011" x2="2.15" y2="5.32" layer="51"/>
<rectangle x1="0.39033125" y1="3.80323125" x2="0.88" y2="5.32" layer="51"/>
<rectangle x1="-0.8801625" y1="3.8007" x2="-0.39" y2="5.32" layer="51"/>
<rectangle x1="-2.15106875" y1="3.8019" x2="-1.66" y2="5.32" layer="51"/>
<rectangle x1="-3.4256" y1="3.80621875" x2="-2.93" y2="5.32" layer="51"/>
<rectangle x1="-4.693859375" y1="3.80313125" x2="-4.2" y2="5.32" layer="51"/>
<rectangle x1="-5.96238125" y1="3.80151875" x2="-5.47" y2="5.32" layer="51"/>
<rectangle x1="-7.24426875" y1="3.8075" x2="-6.74" y2="5.32" layer="51"/>
<smd name="2" x="-5.715" y="-4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="13" x="6.985" y="4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="1" x="-6.985" y="-4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="3" x="-4.445" y="-4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="4" x="-3.175" y="-4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="14" x="5.715" y="4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="12" x="6.985" y="-4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="11" x="5.715" y="-4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="6" x="-0.635" y="-4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="9" x="3.175" y="-4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="5" x="-1.905" y="-4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="7" x="0.635" y="-4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="10" x="4.445" y="-4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="8" x="1.905" y="-4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="15" x="4.445" y="4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="16" x="3.175" y="4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="17" x="1.905" y="4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="18" x="0.635" y="4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="19" x="-0.635" y="4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="20" x="-1.905" y="4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="21" x="-3.175" y="4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="22" x="-4.445" y="4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="23" x="-5.715" y="4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="24" x="-6.985" y="4.6" dx="0.6" dy="2.2" layer="1"/>
</package>
<package name="SSOP24">
<description>&lt;b&gt;Small Shrink Outline Package&lt;/b&gt;&lt;p&gt;
SOT340-1 / JEDEC MO-150AG</description>
<wire x1="-4.128" y1="-2.536" x2="4.128" y2="-2.536" width="0.1524" layer="21"/>
<wire x1="4.128" y1="-2.536" x2="4.128" y2="2.536" width="0.1524" layer="21"/>
<wire x1="4.128" y1="2.536" x2="-4.128" y2="2.536" width="0.1524" layer="21"/>
<wire x1="-4.128" y1="-2.536" x2="-4.128" y2="2.536" width="0.1524" layer="21"/>
<wire x1="-3.874" y1="-2.286" x2="3.874" y2="-2.286" width="0.0508" layer="21"/>
<wire x1="3.874" y1="2.286" x2="3.874" y2="-2.286" width="0.0508" layer="21"/>
<wire x1="3.874" y1="2.286" x2="-3.874" y2="2.286" width="0.0508" layer="21"/>
<wire x1="-3.874" y1="-2.286" x2="-3.874" y2="2.286" width="0.0508" layer="21"/>
<circle x="-2.8829" y="-1.397" radius="0.635" width="0.1524" layer="21"/>
<text x="-4.4754" y="-2.53013125" size="1.27141875" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-3.141790625" y="0" size="1.27198125" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.72741875" y1="-3.80246875" x2="-3.425" y2="-2.6" layer="51"/>
<rectangle x1="-3.07515" y1="-3.800190625" x2="-2.775" y2="-2.6" layer="51"/>
<rectangle x1="-2.42941875" y1="-3.80691875" x2="-2.125" y2="-2.6" layer="51"/>
<rectangle x1="-1.77505" y1="-3.800109375" x2="-1.475" y2="-2.6" layer="51"/>
<rectangle x1="-1.126159375" y1="-3.80393125" x2="-0.825" y2="-2.6" layer="51"/>
<rectangle x1="-0.47503125" y1="-3.800240625" x2="-0.175" y2="-2.6" layer="51"/>
<rectangle x1="0.1753125" y1="-3.80681875" x2="0.475" y2="-2.6" layer="51"/>
<rectangle x1="0.826296875" y1="-3.80596875" x2="1.125" y2="-2.6" layer="51"/>
<rectangle x1="1.47695" y1="-3.80503125" x2="1.775" y2="-2.6" layer="51"/>
<rectangle x1="2.126209375" y1="-3.802159375" x2="2.425" y2="-2.6" layer="51"/>
<rectangle x1="2.77943125" y1="-3.80606875" x2="3.075" y2="-2.6" layer="51"/>
<rectangle x1="3.42825" y1="-3.803609375" x2="3.725" y2="-2.6" layer="51"/>
<rectangle x1="3.42526875" y1="2.6002" x2="3.725" y2="3.8" layer="51"/>
<rectangle x1="2.77945" y1="2.60416875" x2="3.075" y2="3.8" layer="51"/>
<rectangle x1="2.125090625" y1="2.600109375" x2="2.425" y2="3.8" layer="51"/>
<rectangle x1="1.47625" y1="2.602209375" x2="1.775" y2="3.8" layer="51"/>
<rectangle x1="0.8252375" y1="2.60075" x2="1.125" y2="3.8" layer="51"/>
<rectangle x1="0.175146875" y1="2.60216875" x2="0.475" y2="3.8" layer="51"/>
<rectangle x1="-0.47555625" y1="2.603040625" x2="-0.175" y2="3.8" layer="51"/>
<rectangle x1="-1.125659375" y1="2.60151875" x2="-0.825" y2="3.8" layer="51"/>
<rectangle x1="-1.776709375" y1="2.602509375" x2="-1.475" y2="3.8" layer="51"/>
<rectangle x1="-2.42796875" y1="2.60318125" x2="-2.125" y2="3.8" layer="51"/>
<rectangle x1="-3.080340625" y1="2.60451875" x2="-2.775" y2="3.8" layer="51"/>
<rectangle x1="-3.72746875" y1="2.60171875" x2="-3.425" y2="3.8" layer="51"/>
<smd name="1" x="-3.575" y="-3.42" dx="0.4" dy="1.2" layer="1"/>
<smd name="2" x="-2.925" y="-3.42" dx="0.4" dy="1.2" layer="1"/>
<smd name="3" x="-2.275" y="-3.42" dx="0.4" dy="1.2" layer="1"/>
<smd name="4" x="-1.625" y="-3.42" dx="0.4" dy="1.2" layer="1"/>
<smd name="5" x="-0.975" y="-3.42" dx="0.4" dy="1.2" layer="1"/>
<smd name="6" x="-0.325" y="-3.42" dx="0.4" dy="1.2" layer="1"/>
<smd name="7" x="0.325" y="-3.42" dx="0.4" dy="1.2" layer="1"/>
<smd name="8" x="0.975" y="-3.42" dx="0.4" dy="1.2" layer="1"/>
<smd name="9" x="1.625" y="-3.42" dx="0.4" dy="1.2" layer="1"/>
<smd name="10" x="2.275" y="-3.42" dx="0.4" dy="1.2" layer="1"/>
<smd name="11" x="2.925" y="-3.42" dx="0.4" dy="1.2" layer="1"/>
<smd name="12" x="3.575" y="-3.42" dx="0.4" dy="1.2" layer="1"/>
<smd name="13" x="3.575" y="3.42" dx="0.4" dy="1.2" layer="1"/>
<smd name="14" x="2.925" y="3.42" dx="0.4" dy="1.2" layer="1"/>
<smd name="15" x="2.275" y="3.42" dx="0.4" dy="1.2" layer="1"/>
<smd name="16" x="1.625" y="3.42" dx="0.4" dy="1.2" layer="1"/>
<smd name="17" x="0.975" y="3.42" dx="0.4" dy="1.2" layer="1"/>
<smd name="18" x="0.325" y="3.42" dx="0.4" dy="1.2" layer="1"/>
<smd name="19" x="-0.325" y="3.42" dx="0.4" dy="1.2" layer="1"/>
<smd name="20" x="-0.975" y="3.42" dx="0.4" dy="1.2" layer="1"/>
<smd name="21" x="-1.625" y="3.42" dx="0.4" dy="1.2" layer="1"/>
<smd name="22" x="-2.275" y="3.42" dx="0.4" dy="1.2" layer="1"/>
<smd name="23" x="-2.925" y="3.42" dx="0.4" dy="1.2" layer="1"/>
<smd name="24" x="-3.575" y="3.42" dx="0.4" dy="1.2" layer="1"/>
</package>
<package name="HVQFN24">
<description>&lt;b&gt;plastic thermal enhanced very thin quad flat package&lt;/b&gt; SOT616-1&lt;p&gt;
no leads; 24 terminals; body 4 x 4 x 0.85 mm&lt;br&gt;
Source: PCA9555_5.pdf</description>
<wire x1="-1.7" y1="1.2" x2="-1.7" y2="1.3" width="0.15" layer="51" curve="180"/>
<wire x1="-1.7" y1="0.7" x2="-1.7" y2="0.8" width="0.15" layer="51" curve="180"/>
<wire x1="-1.7" y1="0.2" x2="-1.7" y2="0.3" width="0.15" layer="51" curve="180"/>
<wire x1="-1.7" y1="-0.3" x2="-1.7" y2="-0.2" width="0.15" layer="51" curve="180"/>
<wire x1="-1.7" y1="-0.8" x2="-1.7" y2="-0.7" width="0.15" layer="51" curve="180"/>
<wire x1="-1.7" y1="-1.3" x2="-1.7" y2="-1.2" width="0.15" layer="51" curve="180"/>
<wire x1="-1.95" y1="1.95" x2="1.95" y2="1.95" width="0.2032" layer="51"/>
<wire x1="1.95" y1="1.95" x2="1.95" y2="-1.95" width="0.2032" layer="51"/>
<wire x1="1.95" y1="-1.95" x2="-1.95" y2="-1.95" width="0.2032" layer="51"/>
<wire x1="-1.95" y1="-1.95" x2="-1.95" y2="1.95" width="0.2032" layer="51"/>
<wire x1="-1.2" y1="-1.7" x2="-1.3" y2="-1.7" width="0.15" layer="51" curve="180"/>
<wire x1="-0.7" y1="-1.7" x2="-0.8" y2="-1.7" width="0.15" layer="51" curve="180"/>
<wire x1="-0.2" y1="-1.7" x2="-0.3" y2="-1.7" width="0.15" layer="51" curve="180"/>
<wire x1="0.3" y1="-1.7" x2="0.2" y2="-1.7" width="0.15" layer="51" curve="180"/>
<wire x1="0.8" y1="-1.7" x2="0.7" y2="-1.7" width="0.15" layer="51" curve="180"/>
<wire x1="1.3" y1="-1.7" x2="1.2" y2="-1.7" width="0.15" layer="51" curve="180"/>
<wire x1="1.7" y1="-1.2" x2="1.7" y2="-1.3" width="0.15" layer="51" curve="180"/>
<wire x1="1.7" y1="-0.7" x2="1.7" y2="-0.8" width="0.15" layer="51" curve="180"/>
<wire x1="1.7" y1="-0.2" x2="1.7" y2="-0.3" width="0.15" layer="51" curve="180"/>
<wire x1="1.7" y1="0.3" x2="1.7" y2="0.2" width="0.15" layer="51" curve="180"/>
<wire x1="1.7" y1="0.8" x2="1.7" y2="0.7" width="0.15" layer="51" curve="180"/>
<wire x1="1.7" y1="1.3" x2="1.7" y2="1.2" width="0.15" layer="51" curve="180"/>
<wire x1="1.2" y1="1.7" x2="1.3" y2="1.7" width="0.15" layer="51" curve="180"/>
<wire x1="0.7" y1="1.7" x2="0.8" y2="1.7" width="0.15" layer="51" curve="180"/>
<wire x1="0.2" y1="1.7" x2="0.3" y2="1.7" width="0.15" layer="51" curve="180"/>
<wire x1="-0.3" y1="1.7" x2="-0.2" y2="1.7" width="0.15" layer="51" curve="180"/>
<wire x1="-0.8" y1="1.7" x2="-0.7" y2="1.7" width="0.15" layer="51" curve="180"/>
<wire x1="-1.3" y1="1.7" x2="-1.2" y2="1.7" width="0.15" layer="51" curve="180"/>
<text x="-2.03555" y="2.16276875" size="1.27221875" layer="25">&gt;NAME</text>
<text x="-2.0336" y="-3.431690625" size="1.271" layer="27">&gt;VALUE</text>
<rectangle x1="-2.00278125" y1="1.0765" x2="-1.75" y2="1.425" layer="51" rot="R90"/>
<rectangle x1="-2.00028125" y1="0.57508125" x2="-1.75" y2="0.925" layer="51" rot="R90"/>
<rectangle x1="-2.003809375" y1="0.07514375" x2="-1.75" y2="0.425" layer="51" rot="R90"/>
<rectangle x1="-2.0013" y1="-0.425275" x2="-1.75" y2="-0.075" layer="51" rot="R90"/>
<rectangle x1="-2.00171875" y1="-0.92579375" x2="-1.75" y2="-0.575" layer="51" rot="R90"/>
<rectangle x1="-2.001890625" y1="-1.42635" x2="-1.75" y2="-1.075" layer="51" rot="R90"/>
<rectangle x1="-1.9032" y1="0" x2="0" y2="1.9" layer="51"/>
<rectangle x1="-1.37716875" y1="-2.053240625" x2="-1.125" y2="-1.7" layer="51" rot="R180"/>
<rectangle x1="-0.875875" y1="-2.05205" x2="-0.625" y2="-1.7" layer="51" rot="R180"/>
<rectangle x1="-0.37555" y1="-2.053" x2="-0.125" y2="-1.7" layer="51" rot="R180"/>
<rectangle x1="0.12506875" y1="-2.05111875" x2="0.375" y2="-1.7" layer="51" rot="R180"/>
<rectangle x1="0.625375" y1="-2.05123125" x2="0.875" y2="-1.7" layer="51" rot="R180"/>
<rectangle x1="1.125309375" y1="-2.05056875" x2="1.375" y2="-1.7" layer="51" rot="R180"/>
<rectangle x1="1.75145" y1="-1.42618125" x2="2" y2="-1.075" layer="51" rot="R270"/>
<rectangle x1="1.75011875" y1="-0.9250625" x2="2" y2="-0.575" layer="51" rot="R270"/>
<rectangle x1="1.75296875" y1="-0.425721875" x2="2" y2="-0.075" layer="51" rot="R270"/>
<rectangle x1="1.75063125" y1="0.075028125" x2="2" y2="0.425" layer="51" rot="R270"/>
<rectangle x1="1.751359375" y1="0.575446875" x2="2" y2="0.925" layer="51" rot="R270"/>
<rectangle x1="1.752040625" y1="1.076259375" x2="2" y2="1.425" layer="51" rot="R270"/>
<rectangle x1="1.12645" y1="1.702190625" x2="1.375" y2="2.05" layer="51"/>
<rectangle x1="0.62615625" y1="1.70315" x2="0.875" y2="2.05" layer="51"/>
<rectangle x1="0.125028125" y1="1.70036875" x2="0.375" y2="2.05" layer="51"/>
<rectangle x1="-0.375675" y1="1.70305" x2="-0.125" y2="2.05" layer="51"/>
<rectangle x1="-0.876246875" y1="1.70241875" x2="-0.625" y2="2.05" layer="51"/>
<rectangle x1="-1.376040625" y1="1.70128125" x2="-1.125" y2="2.05" layer="51"/>
<smd name="1" x="-1.8" y="1.25" dx="0.3" dy="0.5" layer="1" rot="R90"/>
<smd name="2" x="-1.8" y="0.75" dx="0.3" dy="0.5" layer="1" rot="R90"/>
<smd name="3" x="-1.8" y="0.25" dx="0.3" dy="0.5" layer="1" rot="R90"/>
<smd name="4" x="-1.8" y="-0.25" dx="0.3" dy="0.5" layer="1" rot="R90"/>
<smd name="5" x="-1.8" y="-0.75" dx="0.3" dy="0.5" layer="1" rot="R90"/>
<smd name="6" x="-1.8" y="-1.25" dx="0.3" dy="0.5" layer="1" rot="R90"/>
<smd name="TH" x="0" y="0" dx="2.25" dy="2.25" layer="1"/>
<smd name="7" x="-1.25" y="-1.8" dx="0.3" dy="0.5" layer="1" rot="R180"/>
<smd name="8" x="-0.75" y="-1.8" dx="0.3" dy="0.5" layer="1" rot="R180"/>
<smd name="9" x="-0.25" y="-1.8" dx="0.3" dy="0.5" layer="1" rot="R180"/>
<smd name="10" x="0.25" y="-1.8" dx="0.3" dy="0.5" layer="1" rot="R180"/>
<smd name="11" x="0.75" y="-1.8" dx="0.3" dy="0.5" layer="1" rot="R180"/>
<smd name="12" x="1.25" y="-1.8" dx="0.3" dy="0.5" layer="1" rot="R180"/>
<smd name="13" x="1.8" y="-1.25" dx="0.3" dy="0.5" layer="1" rot="R270"/>
<smd name="14" x="1.8" y="-0.75" dx="0.3" dy="0.5" layer="1" rot="R270"/>
<smd name="15" x="1.8" y="-0.25" dx="0.3" dy="0.5" layer="1" rot="R270"/>
<smd name="16" x="1.8" y="0.25" dx="0.3" dy="0.5" layer="1" rot="R270"/>
<smd name="17" x="1.8" y="0.75" dx="0.3" dy="0.5" layer="1" rot="R270"/>
<smd name="18" x="1.8" y="1.25" dx="0.3" dy="0.5" layer="1" rot="R270"/>
<smd name="19" x="1.25" y="1.8" dx="0.3" dy="0.5" layer="1"/>
<smd name="20" x="0.75" y="1.8" dx="0.3" dy="0.5" layer="1"/>
<smd name="21" x="0.25" y="1.8" dx="0.3" dy="0.5" layer="1"/>
<smd name="22" x="-0.25" y="1.8" dx="0.3" dy="0.5" layer="1"/>
<smd name="23" x="-0.75" y="1.8" dx="0.3" dy="0.5" layer="1"/>
<smd name="24" x="-1.25" y="1.8" dx="0.3" dy="0.5" layer="1"/>
</package>
<package name="TSSOP24">
<description>&lt;b&gt;Thin Shrink Small Outline Plastic 24&lt;/b&gt;&lt;p&gt;
Source: MAX3223-MAX3243.pdf</description>
<wire x1="-3.8146" y1="-2.1558" x2="3.8146" y2="-2.1558" width="0.1524" layer="21"/>
<wire x1="3.8146" y1="2.1812" x2="3.8146" y2="-2.1558" width="0.1524" layer="21"/>
<wire x1="3.8146" y1="2.1812" x2="-3.8146" y2="2.1812" width="0.1524" layer="21"/>
<wire x1="-3.8146" y1="-2.1558" x2="-3.8146" y2="2.1812" width="0.1524" layer="21"/>
<wire x1="-3.586" y1="-1.9272" x2="3.586" y2="-1.9272" width="0.0508" layer="21"/>
<wire x1="3.586" y1="1.9526" x2="3.586" y2="-1.9272" width="0.0508" layer="21"/>
<wire x1="3.586" y1="1.9526" x2="-3.586" y2="1.9526" width="0.0508" layer="21"/>
<wire x1="-3.586" y1="-1.9272" x2="-3.586" y2="1.9526" width="0.0508" layer="21"/>
<circle x="-2.9256" y="-1.2192" radius="0.4572" width="0.1524" layer="21"/>
<text x="-4.19933125" y="-2.08465" size="1.0169" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="5.19211875" y="-2.08518125" size="1.017159375" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<smd name="1" x="-3.575" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="2" x="-2.925" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="3" x="-2.275" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="4" x="-1.625" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="5" x="-0.975" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="6" x="-0.325" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="7" x="0.325" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="8" x="0.975" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="9" x="1.625" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="10" x="2.275" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="11" x="2.925" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="12" x="3.575" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="13" x="3.575" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="14" x="2.925" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="15" x="2.275" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="16" x="1.625" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="17" x="0.975" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="18" x="0.325" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="19" x="-0.325" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="20" x="-0.975" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="21" x="-1.625" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="22" x="-2.275" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="23" x="-2.925" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="24" x="-3.575" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
</package>
<package name="SO24-2" urn="urn:adsk.eagle:footprint:18144/1">
<description>&lt;b&gt;Small Outline Package&lt;/b&gt;</description>
<wire x1="-7.6962" y1="-4.1148" x2="7.6708" y2="-4.1148" width="0.1524" layer="21"/>
<wire x1="7.6708" y1="-4.1148" x2="7.6708" y2="4.1148" width="0.1524" layer="21"/>
<wire x1="7.6708" y1="4.1148" x2="-7.6962" y2="4.1148" width="0.1524" layer="21"/>
<wire x1="-7.6962" y1="4.1148" x2="-7.6962" y2="-4.1148" width="0.1524" layer="21"/>
<circle x="-6.6802" y="-3.175" radius="0.5334" width="0.1524" layer="21"/>
<smd name="1" x="-6.985" y="-5.1562" dx="0.762" dy="1.524" layer="1"/>
<smd name="2" x="-5.715" y="-5.1562" dx="0.762" dy="1.524" layer="1"/>
<smd name="3" x="-4.445" y="-5.1562" dx="0.762" dy="1.524" layer="1"/>
<smd name="4" x="-3.175" y="-5.1562" dx="0.762" dy="1.524" layer="1"/>
<smd name="5" x="-1.905" y="-5.1562" dx="0.762" dy="1.524" layer="1"/>
<smd name="6" x="-0.635" y="-5.1562" dx="0.762" dy="1.524" layer="1"/>
<smd name="7" x="0.635" y="-5.1562" dx="0.762" dy="1.524" layer="1"/>
<smd name="8" x="1.905" y="-5.1562" dx="0.762" dy="1.524" layer="1"/>
<smd name="9" x="3.175" y="-5.1562" dx="0.762" dy="1.524" layer="1"/>
<smd name="10" x="4.445" y="-5.1562" dx="0.762" dy="1.524" layer="1"/>
<smd name="16" x="3.175" y="5.1562" dx="0.762" dy="1.524" layer="1"/>
<smd name="15" x="4.445" y="5.1562" dx="0.762" dy="1.524" layer="1"/>
<smd name="14" x="5.715" y="5.1562" dx="0.762" dy="1.524" layer="1"/>
<smd name="13" x="6.985" y="5.1562" dx="0.762" dy="1.524" layer="1"/>
<smd name="12" x="6.985" y="-5.1562" dx="0.762" dy="1.524" layer="1"/>
<smd name="11" x="5.715" y="-5.1562" dx="0.762" dy="1.524" layer="1"/>
<smd name="17" x="1.905" y="5.1562" dx="0.762" dy="1.524" layer="1"/>
<smd name="18" x="0.635" y="5.1562" dx="0.762" dy="1.524" layer="1"/>
<smd name="19" x="-0.635" y="5.1562" dx="0.762" dy="1.524" layer="1"/>
<smd name="20" x="-1.905" y="5.1562" dx="0.762" dy="1.524" layer="1"/>
<smd name="21" x="-3.175" y="5.1562" dx="0.762" dy="1.524" layer="1"/>
<smd name="22" x="-4.445" y="5.1562" dx="0.762" dy="1.524" layer="1"/>
<smd name="23" x="-5.715" y="5.1562" dx="0.762" dy="1.524" layer="1"/>
<smd name="24" x="-6.985" y="5.1562" dx="0.762" dy="1.524" layer="1"/>
<text x="-7.874" y="-4.191" size="1.778" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-6.985" y="-1.27" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-7.239" y1="-5.6642" x2="-6.731" y2="-4.1402" layer="51"/>
<rectangle x1="-5.969" y1="-5.6642" x2="-5.461" y2="-4.1402" layer="51"/>
<rectangle x1="-4.699" y1="-5.6642" x2="-4.191" y2="-4.1402" layer="51"/>
<rectangle x1="-3.429" y1="-5.6642" x2="-2.921" y2="-4.1402" layer="51"/>
<rectangle x1="-2.159" y1="-5.6642" x2="-1.651" y2="-4.1402" layer="51"/>
<rectangle x1="-0.889" y1="-5.6642" x2="-0.381" y2="-4.1402" layer="51"/>
<rectangle x1="0.381" y1="-5.6642" x2="0.889" y2="-4.1402" layer="51"/>
<rectangle x1="1.651" y1="-5.6642" x2="2.159" y2="-4.1402" layer="51"/>
<rectangle x1="2.921" y1="-5.6642" x2="3.429" y2="-4.1402" layer="51"/>
<rectangle x1="4.191" y1="-5.6642" x2="4.699" y2="-4.1402" layer="51"/>
<rectangle x1="5.461" y1="-5.6642" x2="5.969" y2="-4.1402" layer="51"/>
<rectangle x1="6.731" y1="-5.6642" x2="7.239" y2="-4.1402" layer="51"/>
<rectangle x1="6.731" y1="4.1402" x2="7.239" y2="5.6642" layer="51"/>
<rectangle x1="5.461" y1="4.1402" x2="5.969" y2="5.6642" layer="51"/>
<rectangle x1="4.191" y1="4.1402" x2="4.699" y2="5.6642" layer="51"/>
<rectangle x1="2.921" y1="4.1402" x2="3.429" y2="5.6642" layer="51"/>
<rectangle x1="1.651" y1="4.1402" x2="2.159" y2="5.6642" layer="51"/>
<rectangle x1="0.381" y1="4.1402" x2="0.889" y2="5.6642" layer="51"/>
<rectangle x1="-0.889" y1="4.1402" x2="-0.381" y2="5.6642" layer="51"/>
<rectangle x1="-2.159" y1="4.1402" x2="-1.651" y2="5.6642" layer="51"/>
<rectangle x1="-3.429" y1="4.1402" x2="-2.921" y2="5.6642" layer="51"/>
<rectangle x1="-4.699" y1="4.1402" x2="-4.191" y2="5.6642" layer="51"/>
<rectangle x1="-5.969" y1="4.1402" x2="-5.461" y2="5.6642" layer="51"/>
<rectangle x1="-7.239" y1="4.1402" x2="-6.731" y2="5.6642" layer="51"/>
</package>
</packages>
<packages3d>
<package3d name="SO24-2" urn="urn:adsk.eagle:package:18389/2" type="model">
<description>Small Outline Package</description>
<packageinstances>
<packageinstance name="SO24-2"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="PCA9555-35">
<wire x1="-10.16" y1="27.94" x2="10.16" y2="27.94" width="0.254" layer="94"/>
<wire x1="10.16" y1="27.94" x2="10.16" y2="-27.94" width="0.254" layer="94"/>
<wire x1="10.16" y1="-27.94" x2="-10.16" y2="-27.94" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-27.94" x2="-10.16" y2="27.94" width="0.254" layer="94"/>
<text x="-10.1802" y="29.268" size="1.78153125" layer="95">&gt;NAME</text>
<text x="-10.1658" y="-30.4974" size="1.779009375" layer="96">&gt;VALUE</text>
<text x="0" y="-22.8975" size="1.78091875" layer="94" rot="R90">I2C ADDR
0-1-0-0-A2-A1-A0-R/W
9555 HAS INTERNAL PULLUPS</text>
<pin name="SCL" x="-12.7" y="17.78" length="short" direction="in"/>
<pin name="SDA" x="-12.7" y="20.32" length="short"/>
<pin name="A0" x="-12.7" y="12.7" length="short" direction="in"/>
<pin name="A1" x="-12.7" y="10.16" length="short" direction="in"/>
<pin name="A2" x="-12.7" y="7.62" length="short" direction="in"/>
<pin name="!INT" x="-12.7" y="25.4" length="short"/>
<pin name="VSS" x="12.7" y="-25.4" length="short" rot="R180"/>
<pin name="VDD" x="12.7" y="25.4" length="short" rot="R180"/>
<pin name="I/O0.0" x="12.7" y="20.32" length="short" rot="R180"/>
<pin name="I/O0.1" x="12.7" y="17.78" length="short" rot="R180"/>
<pin name="I/O0.2" x="12.7" y="15.24" length="short" rot="R180"/>
<pin name="I/O0.3" x="12.7" y="12.7" length="short" rot="R180"/>
<pin name="I/O0.4" x="12.7" y="10.16" length="short" rot="R180"/>
<pin name="I/O0.5" x="12.7" y="7.62" length="short" rot="R180"/>
<pin name="I/O0.6" x="12.7" y="5.08" length="short" rot="R180"/>
<pin name="I/O0.7" x="12.7" y="2.54" length="short" rot="R180"/>
<pin name="I/O1.0" x="12.7" y="-2.54" length="short" rot="R180"/>
<pin name="I/O1.1" x="12.7" y="-5.08" length="short" rot="R180"/>
<pin name="I/O1.2" x="12.7" y="-7.62" length="short" rot="R180"/>
<pin name="I/O1.3" x="12.7" y="-10.16" length="short" rot="R180"/>
<pin name="I/O1.4" x="12.7" y="-12.7" length="short" rot="R180"/>
<pin name="I/O1.5" x="12.7" y="-15.24" length="short" rot="R180"/>
<pin name="I/O1.6" x="12.7" y="-17.78" length="short" rot="R180"/>
<pin name="I/O1.7" x="12.7" y="-20.32" length="short" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PCA9555" prefix="U">
<description>&lt;b&gt;16-bit I2C and SMBus I/O port with interrupt and internal 100kOhm Pullups&lt;/b&gt;&lt;p&gt;
Source: PCA9555_5.pdf</description>
<gates>
<gate name="U$1" symbol="PCA9555-35" x="0" y="0"/>
</gates>
<devices>
<device name="N" package="DIL24-6">
<connects>
<connect gate="U$1" pin="!INT" pad="1"/>
<connect gate="U$1" pin="A0" pad="21"/>
<connect gate="U$1" pin="A1" pad="2"/>
<connect gate="U$1" pin="A2" pad="3"/>
<connect gate="U$1" pin="I/O0.0" pad="4"/>
<connect gate="U$1" pin="I/O0.1" pad="5"/>
<connect gate="U$1" pin="I/O0.2" pad="6"/>
<connect gate="U$1" pin="I/O0.3" pad="7"/>
<connect gate="U$1" pin="I/O0.4" pad="8"/>
<connect gate="U$1" pin="I/O0.5" pad="9"/>
<connect gate="U$1" pin="I/O0.6" pad="10"/>
<connect gate="U$1" pin="I/O0.7" pad="11"/>
<connect gate="U$1" pin="I/O1.0" pad="13"/>
<connect gate="U$1" pin="I/O1.1" pad="14"/>
<connect gate="U$1" pin="I/O1.2" pad="15"/>
<connect gate="U$1" pin="I/O1.3" pad="16"/>
<connect gate="U$1" pin="I/O1.4" pad="17"/>
<connect gate="U$1" pin="I/O1.5" pad="18"/>
<connect gate="U$1" pin="I/O1.6" pad="19"/>
<connect gate="U$1" pin="I/O1.7" pad="20"/>
<connect gate="U$1" pin="SCL" pad="22"/>
<connect gate="U$1" pin="SDA" pad="23"/>
<connect gate="U$1" pin="VDD" pad="24"/>
<connect gate="U$1" pin="VSS" pad="12"/>
</connects>
<technologies>
<technology name="">
<attribute name="AVAILABILITY" value="Unavailable"/>
<attribute name="DESCRIPTION" value=" "/>
<attribute name="MF" value="NXP Semiconductors"/>
<attribute name="MP" value="PCA9555"/>
<attribute name="PACKAGE" value="None"/>
<attribute name="PRICE" value="None"/>
</technology>
</technologies>
</device>
<device name="D" package="SO24">
<connects>
<connect gate="U$1" pin="!INT" pad="1"/>
<connect gate="U$1" pin="A0" pad="21"/>
<connect gate="U$1" pin="A1" pad="2"/>
<connect gate="U$1" pin="A2" pad="3"/>
<connect gate="U$1" pin="I/O0.0" pad="4"/>
<connect gate="U$1" pin="I/O0.1" pad="5"/>
<connect gate="U$1" pin="I/O0.2" pad="6"/>
<connect gate="U$1" pin="I/O0.3" pad="7"/>
<connect gate="U$1" pin="I/O0.4" pad="8"/>
<connect gate="U$1" pin="I/O0.5" pad="9"/>
<connect gate="U$1" pin="I/O0.6" pad="10"/>
<connect gate="U$1" pin="I/O0.7" pad="11"/>
<connect gate="U$1" pin="I/O1.0" pad="13"/>
<connect gate="U$1" pin="I/O1.1" pad="14"/>
<connect gate="U$1" pin="I/O1.2" pad="15"/>
<connect gate="U$1" pin="I/O1.3" pad="16"/>
<connect gate="U$1" pin="I/O1.4" pad="17"/>
<connect gate="U$1" pin="I/O1.5" pad="18"/>
<connect gate="U$1" pin="I/O1.6" pad="19"/>
<connect gate="U$1" pin="I/O1.7" pad="20"/>
<connect gate="U$1" pin="SCL" pad="22"/>
<connect gate="U$1" pin="SDA" pad="23"/>
<connect gate="U$1" pin="VDD" pad="24"/>
<connect gate="U$1" pin="VSS" pad="12"/>
</connects>
<technologies>
<technology name="">
<attribute name="AVAILABILITY" value="Unavailable"/>
<attribute name="DESCRIPTION" value=" "/>
<attribute name="MF" value="NXP Semiconductors"/>
<attribute name="MP" value="PCA9555"/>
<attribute name="PACKAGE" value="None"/>
<attribute name="PRICE" value="None"/>
</technology>
</technologies>
</device>
<device name="DB" package="SSOP24">
<connects>
<connect gate="U$1" pin="!INT" pad="1"/>
<connect gate="U$1" pin="A0" pad="21"/>
<connect gate="U$1" pin="A1" pad="2"/>
<connect gate="U$1" pin="A2" pad="3"/>
<connect gate="U$1" pin="I/O0.0" pad="4"/>
<connect gate="U$1" pin="I/O0.1" pad="5"/>
<connect gate="U$1" pin="I/O0.2" pad="6"/>
<connect gate="U$1" pin="I/O0.3" pad="7"/>
<connect gate="U$1" pin="I/O0.4" pad="8"/>
<connect gate="U$1" pin="I/O0.5" pad="9"/>
<connect gate="U$1" pin="I/O0.6" pad="10"/>
<connect gate="U$1" pin="I/O0.7" pad="11"/>
<connect gate="U$1" pin="I/O1.0" pad="13"/>
<connect gate="U$1" pin="I/O1.1" pad="14"/>
<connect gate="U$1" pin="I/O1.2" pad="15"/>
<connect gate="U$1" pin="I/O1.3" pad="16"/>
<connect gate="U$1" pin="I/O1.4" pad="17"/>
<connect gate="U$1" pin="I/O1.5" pad="18"/>
<connect gate="U$1" pin="I/O1.6" pad="19"/>
<connect gate="U$1" pin="I/O1.7" pad="20"/>
<connect gate="U$1" pin="SCL" pad="22"/>
<connect gate="U$1" pin="SDA" pad="23"/>
<connect gate="U$1" pin="VDD" pad="24"/>
<connect gate="U$1" pin="VSS" pad="12"/>
</connects>
<technologies>
<technology name="">
<attribute name="AVAILABILITY" value="Unavailable"/>
<attribute name="DESCRIPTION" value=" "/>
<attribute name="MF" value="NXP Semiconductors"/>
<attribute name="MP" value="PCA9555"/>
<attribute name="PACKAGE" value="None"/>
<attribute name="PRICE" value="None"/>
</technology>
</technologies>
</device>
<device name="BS" package="HVQFN24">
<connects>
<connect gate="U$1" pin="!INT" pad="22"/>
<connect gate="U$1" pin="A0" pad="18"/>
<connect gate="U$1" pin="A1" pad="23"/>
<connect gate="U$1" pin="A2" pad="24"/>
<connect gate="U$1" pin="I/O0.0" pad="1"/>
<connect gate="U$1" pin="I/O0.1" pad="2"/>
<connect gate="U$1" pin="I/O0.2" pad="3"/>
<connect gate="U$1" pin="I/O0.3" pad="4"/>
<connect gate="U$1" pin="I/O0.4" pad="5"/>
<connect gate="U$1" pin="I/O0.5" pad="6"/>
<connect gate="U$1" pin="I/O0.6" pad="7"/>
<connect gate="U$1" pin="I/O0.7" pad="8"/>
<connect gate="U$1" pin="I/O1.0" pad="10"/>
<connect gate="U$1" pin="I/O1.1" pad="11"/>
<connect gate="U$1" pin="I/O1.2" pad="12"/>
<connect gate="U$1" pin="I/O1.3" pad="13"/>
<connect gate="U$1" pin="I/O1.4" pad="14"/>
<connect gate="U$1" pin="I/O1.5" pad="15"/>
<connect gate="U$1" pin="I/O1.6" pad="16"/>
<connect gate="U$1" pin="I/O1.7" pad="17"/>
<connect gate="U$1" pin="SCL" pad="19"/>
<connect gate="U$1" pin="SDA" pad="20"/>
<connect gate="U$1" pin="VDD" pad="21"/>
<connect gate="U$1" pin="VSS" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="AVAILABILITY" value="Unavailable"/>
<attribute name="DESCRIPTION" value=" "/>
<attribute name="MF" value="NXP Semiconductors"/>
<attribute name="MP" value="PCA9555"/>
<attribute name="PACKAGE" value="None"/>
<attribute name="PRICE" value="None"/>
</technology>
</technologies>
</device>
<device name="PW" package="TSSOP24">
<connects>
<connect gate="U$1" pin="!INT" pad="1"/>
<connect gate="U$1" pin="A0" pad="21"/>
<connect gate="U$1" pin="A1" pad="2"/>
<connect gate="U$1" pin="A2" pad="3"/>
<connect gate="U$1" pin="I/O0.0" pad="4"/>
<connect gate="U$1" pin="I/O0.1" pad="5"/>
<connect gate="U$1" pin="I/O0.2" pad="6"/>
<connect gate="U$1" pin="I/O0.3" pad="7"/>
<connect gate="U$1" pin="I/O0.4" pad="8"/>
<connect gate="U$1" pin="I/O0.5" pad="9"/>
<connect gate="U$1" pin="I/O0.6" pad="10"/>
<connect gate="U$1" pin="I/O0.7" pad="11"/>
<connect gate="U$1" pin="I/O1.0" pad="13"/>
<connect gate="U$1" pin="I/O1.1" pad="14"/>
<connect gate="U$1" pin="I/O1.2" pad="15"/>
<connect gate="U$1" pin="I/O1.3" pad="16"/>
<connect gate="U$1" pin="I/O1.4" pad="17"/>
<connect gate="U$1" pin="I/O1.5" pad="18"/>
<connect gate="U$1" pin="I/O1.6" pad="19"/>
<connect gate="U$1" pin="I/O1.7" pad="20"/>
<connect gate="U$1" pin="SCL" pad="22"/>
<connect gate="U$1" pin="SDA" pad="23"/>
<connect gate="U$1" pin="VDD" pad="24"/>
<connect gate="U$1" pin="VSS" pad="12"/>
</connects>
<technologies>
<technology name="">
<attribute name="AVAILABILITY" value="Unavailable"/>
<attribute name="DESCRIPTION" value=" "/>
<attribute name="MF" value="NXP Semiconductors"/>
<attribute name="MP" value="PCA9555"/>
<attribute name="PACKAGE" value="None"/>
<attribute name="PRICE" value="None"/>
</technology>
</technologies>
</device>
<device name="" package="SO24-2">
<connects>
<connect gate="U$1" pin="!INT" pad="1"/>
<connect gate="U$1" pin="A0" pad="21"/>
<connect gate="U$1" pin="A1" pad="2"/>
<connect gate="U$1" pin="A2" pad="3"/>
<connect gate="U$1" pin="I/O0.0" pad="4"/>
<connect gate="U$1" pin="I/O0.1" pad="5"/>
<connect gate="U$1" pin="I/O0.2" pad="6"/>
<connect gate="U$1" pin="I/O0.3" pad="7"/>
<connect gate="U$1" pin="I/O0.4" pad="8"/>
<connect gate="U$1" pin="I/O0.5" pad="9"/>
<connect gate="U$1" pin="I/O0.6" pad="10"/>
<connect gate="U$1" pin="I/O0.7" pad="11"/>
<connect gate="U$1" pin="I/O1.0" pad="13"/>
<connect gate="U$1" pin="I/O1.1" pad="14"/>
<connect gate="U$1" pin="I/O1.2" pad="15"/>
<connect gate="U$1" pin="I/O1.3" pad="16"/>
<connect gate="U$1" pin="I/O1.4" pad="17"/>
<connect gate="U$1" pin="I/O1.5" pad="18"/>
<connect gate="U$1" pin="I/O1.6" pad="19"/>
<connect gate="U$1" pin="I/O1.7" pad="20"/>
<connect gate="U$1" pin="SCL" pad="22"/>
<connect gate="U$1" pin="SDA" pad="23"/>
<connect gate="U$1" pin="VDD" pad="24"/>
<connect gate="U$1" pin="VSS" pad="12"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:18389/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="OPL_Connector">
<description>&lt;b&gt;Seeed Open Parts Library (OPL) for the Seeed Fusion PCB Assembly Service
&lt;br&gt;&lt;br&gt;
&lt;a href="https://www.seeedstudio.com/opl.html" title="https://www.seeedstudio.com/opl.html"&gt;Seeed Fusion PCBA OPL&lt;/a&gt;&lt;br&gt;
&lt;a href="https://www.seeedstudio.com/fusion_pcb.html"&gt;Order PCB/PCBA&lt;/a&gt;&lt;br&gt;&lt;br&gt;
&lt;a href="https://www.seeedstudio.com"&gt;www.seeedstudio.com&lt;/a&gt;
&lt;br&gt;&lt;/b&gt;</description>
<packages>
<package name="HW4-SMD-2.0-90D" urn="urn:adsk.eagle:footprint:8004440/1">
<wire x1="-6.1" y1="1.5" x2="-5" y2="1.5" width="0.254" layer="21"/>
<wire x1="5" y1="1.5" x2="6.1" y2="1.5" width="0.254" layer="21"/>
<wire x1="-6.1" y1="9.2" x2="-3.4" y2="9.2" width="0.254" layer="21"/>
<wire x1="3.4" y1="9.2" x2="6.1" y2="9.2" width="0.254" layer="21"/>
<wire x1="-6.1" y1="1.5" x2="-6.1" y2="0" width="0.254" layer="21"/>
<wire x1="-6.1" y1="0" x2="-5" y2="0" width="0.254" layer="21"/>
<wire x1="-5" y1="0" x2="-5" y2="1.5" width="0.254" layer="21"/>
<wire x1="-5" y1="1.5" x2="5" y2="1.5" width="0.254" layer="21"/>
<wire x1="5" y1="1.5" x2="5" y2="0" width="0.254" layer="21"/>
<wire x1="5" y1="0" x2="6.1" y2="0" width="0.254" layer="21"/>
<wire x1="6.1" y1="0" x2="6.1" y2="1.5" width="0.254" layer="21"/>
<wire x1="-6.053" y1="0" x2="-4.975" y2="0" width="0.254" layer="39"/>
<wire x1="4.975" y1="0" x2="6.053" y2="0" width="0.254" layer="39"/>
<wire x1="-6.1" y1="1.5" x2="-6.1" y2="5.465" width="0.254" layer="21"/>
<wire x1="6.1" y1="1.5" x2="6.1" y2="5.465" width="0.254" layer="21"/>
<wire x1="-6.1" y1="5.465" x2="-6.1" y2="0.2" width="0.254" layer="39"/>
<wire x1="6.1" y1="5.465" x2="6.1" y2="0.1" width="0.254" layer="39"/>
<wire x1="5" y1="0.1" x2="5" y2="-1.5" width="0.254" layer="39"/>
<wire x1="5" y1="-1.5" x2="-5" y2="-1.5" width="0.254" layer="39"/>
<wire x1="-5" y1="-1.5" x2="-5" y2="0.2" width="0.254" layer="39"/>
<wire x1="-3.4" y1="9.2" x2="-3.4" y2="8.2" width="0.254" layer="21"/>
<wire x1="-3.4" y1="8.2" x2="3.4" y2="8.2" width="0.254" layer="21"/>
<wire x1="3.4" y1="8.2" x2="3.4" y2="9.2" width="0.254" layer="21"/>
<wire x1="-6" y1="9.2" x2="-3.4" y2="9.2" width="0.254" layer="39"/>
<wire x1="-3.4" y1="9.2" x2="-3.4" y2="8.2" width="0.254" layer="39"/>
<wire x1="-3.4" y1="8.2" x2="3.4" y2="8.2" width="0.254" layer="39"/>
<wire x1="3.4" y1="8.2" x2="3.4" y2="9.2" width="0.254" layer="39"/>
<wire x1="3.4" y1="9.2" x2="6" y2="9.2" width="0.254" layer="39"/>
<smd name="1" x="-3" y="-0.1" dx="2.74" dy="1.01" layer="1" rot="R90"/>
<smd name="2" x="-1" y="-0.1" dx="2.74" dy="1.01" layer="1" rot="R90"/>
<smd name="3" x="1" y="-0.1" dx="2.74" dy="1.01" layer="1" rot="R90"/>
<smd name="4" x="3" y="-0.1" dx="2.74" dy="1.01" layer="1" rot="R90"/>
<smd name="SS1" x="-5.4" y="7.4" dx="3" dy="1.5" layer="1" rot="R90"/>
<smd name="SS2" x="5.4" y="7.4" dx="3" dy="1.5" layer="1" rot="R90"/>
<text x="-1.905" y="8.89" size="0.889" layer="25" font="vector" ratio="11">&gt;NAME</text>
<text x="-2.54" y="4.445" size="0.889" layer="27" font="vector" ratio="11">&gt;VALUE</text>
</package>
<package name="HW4-2.0-90D" urn="urn:adsk.eagle:footprint:8004460/1">
<wire x1="-5" y1="1" x2="-5" y2="-1" width="0.127" layer="21"/>
<wire x1="-5" y1="8.4" x2="-5" y2="1.6" width="0.254" layer="21"/>
<wire x1="-5" y1="1.6" x2="-5" y2="-1.5" width="0.254" layer="21"/>
<wire x1="5" y1="1.6" x2="5" y2="-1.5" width="0.254" layer="21"/>
<wire x1="5" y1="1.6" x2="5" y2="8.4" width="0.254" layer="21"/>
<wire x1="-5" y1="-1.5" x2="5" y2="-1.5" width="0.254" layer="21"/>
<wire x1="-5" y1="8.5" x2="5" y2="8.5" width="0.254" layer="39"/>
<wire x1="5" y1="8.5" x2="5" y2="-1.4" width="0.254" layer="39"/>
<wire x1="5" y1="-1.4" x2="-5" y2="-1.4" width="0.254" layer="39"/>
<wire x1="-5" y1="-1.4" x2="-5" y2="8.5" width="0.254" layer="39"/>
<wire x1="-5" y1="1.6" x2="5" y2="1.6" width="0.254" layer="21"/>
<wire x1="-5" y1="8.4" x2="-2.8" y2="8.4" width="0.254" layer="21"/>
<wire x1="-2.8" y1="8.4" x2="-2.8" y2="7.4" width="0.254" layer="21"/>
<wire x1="-2.8" y1="7.4" x2="2.8" y2="7.4" width="0.254" layer="21"/>
<wire x1="2.8" y1="7.4" x2="2.8" y2="8.4" width="0.254" layer="21"/>
<wire x1="2.8" y1="8.4" x2="5" y2="8.4" width="0.254" layer="21"/>
<pad name="1" x="-3" y="0" drill="0.8" diameter="1.27" shape="square"/>
<pad name="2" x="-1" y="0" drill="0.8" diameter="1.27"/>
<pad name="3" x="1" y="0" drill="0.8" diameter="1.27"/>
<pad name="4" x="3" y="0" drill="0.8" diameter="1.27"/>
<text x="-1.905" y="-3.175" size="0.889" layer="25" font="vector" ratio="11">&gt;NAME</text>
<text x="-1.905" y="3.81" size="0.889" layer="27" font="vector" ratio="11">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="HW4-SMD-2.0-90D" urn="urn:adsk.eagle:package:8004544/1" type="box">
<packageinstances>
<packageinstance name="HW4-SMD-2.0-90D"/>
</packageinstances>
</package3d>
<package3d name="HW4-2.0-90D" urn="urn:adsk.eagle:package:8004524/1" type="box">
<packageinstances>
<packageinstance name="HW4-2.0-90D"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="GROVE-CONNECTOR-SMD" urn="urn:adsk.eagle:symbol:8004428/1">
<wire x1="-2.54" y1="5.08" x2="2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="-2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="5.08" width="0.254" layer="94"/>
<text x="-7.62" y="6.35" size="1.27" layer="95" ratio="10">&gt;name</text>
<text x="1.27" y="6.35" size="1.27" layer="96" ratio="10">&gt;value</text>
<pin name="1" x="-5.08" y="3.81" visible="pad" length="middle" function="dotclk"/>
<pin name="2" x="-5.08" y="1.27" visible="pad" length="middle" function="dot"/>
<pin name="3" x="-5.08" y="-1.27" visible="pad" length="middle" function="dot"/>
<pin name="4" x="-5.08" y="-3.81" visible="pad" length="middle" function="dot"/>
<pin name="SS1" x="0" y="7.62" visible="off" length="short" rot="R270"/>
<pin name="SS2" x="0" y="-7.62" visible="off" length="short" rot="R90"/>
</symbol>
<symbol name="GROVE-CONNECTOR-DIP" urn="urn:adsk.eagle:symbol:8004427/1">
<wire x1="-1.27" y1="5.08" x2="3.81" y2="5.08" width="0.254" layer="94"/>
<wire x1="3.81" y1="5.08" x2="3.81" y2="-5.08" width="0.254" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="-1.27" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-5.08" x2="-1.27" y2="5.08" width="0.254" layer="94"/>
<text x="-6.35" y="6.35" size="1.27" layer="95" ratio="10">&gt;name</text>
<text x="0" y="6.35" size="1.27" layer="96" ratio="10">&gt;value</text>
<pin name="1" x="-3.81" y="3.81" visible="pad" length="middle" function="dotclk"/>
<pin name="2" x="-3.81" y="1.27" visible="pad" length="middle" function="dot"/>
<pin name="3" x="-3.81" y="-1.27" visible="pad" length="middle" function="dot"/>
<pin name="4" x="-3.81" y="-3.81" visible="pad" length="middle" function="dot"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GROVE-CONNECTOR-SMD-90D(4+2P-2.0)" urn="urn:adsk.eagle:component:8004641/1" prefix="J" uservalue="yes">
<description>320110032</description>
<gates>
<gate name="G$1" symbol="GROVE-CONNECTOR-SMD" x="0" y="0"/>
</gates>
<devices>
<device name="" package="HW4-SMD-2.0-90D">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="SS1" pad="SS1"/>
<connect gate="G$1" pin="SS2" pad="SS2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8004544/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="VALUE" value="4P-SMD-2.0-90D" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GROVE-CONNECTOR-DIP-90D(4P-2.0)" urn="urn:adsk.eagle:component:8004621/1" prefix="J" uservalue="yes">
<description>320110034</description>
<gates>
<gate name="G$1" symbol="GROVE-CONNECTOR-DIP" x="0" y="0"/>
</gates>
<devices>
<device name="" package="HW4-2.0-90D">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8004524/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="VALUE" value="4P-2.0-90D" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="MF_LEDs">
<packages>
<package name="PLCC4" urn="urn:adsk.eagle:footprint:10009048/1">
<description>&lt;b&gt;Description:&lt;/b&gt; Footprint for LEDs in PLCC4 Standard.&lt;br/&gt;</description>
<smd name="P$1" x="-1.55" y="0.75" dx="0.9" dy="1.5" layer="1" rot="R90"/>
<smd name="P$2" x="-1.55" y="-0.75" dx="0.9" dy="1.5" layer="1" rot="R90"/>
<smd name="P$3" x="1.55" y="-0.75" dx="0.9" dy="1.5" layer="1" rot="R90"/>
<smd name="P$4" x="1.55" y="0.75" dx="0.9" dy="1.5" layer="1" rot="R90"/>
<wire x1="-1.6" y1="1.4" x2="1.6" y2="1.4" width="0.127" layer="21"/>
<wire x1="1.6" y1="-1.4" x2="-1.6" y2="-1.4" width="0.127" layer="21"/>
<wire x1="-1.6" y1="-0.15" x2="-1.6" y2="0.1" width="0.127" layer="21"/>
<wire x1="1.6" y1="-0.15" x2="1.6" y2="0.15" width="0.127" layer="21"/>
<wire x1="1.6" y1="-1.4" x2="1.6" y2="-1.35" width="0.127" layer="21"/>
<wire x1="1.6" y1="1.4" x2="1.6" y2="1.35" width="0.127" layer="21"/>
<wire x1="-1.6" y1="-1.4" x2="-1.6" y2="-1.35" width="0.127" layer="21"/>
<wire x1="-1.6" y1="1.4" x2="-1.6" y2="1.35" width="0.127" layer="21"/>
<text x="-1.6" y="1.7" size="1.016" layer="25" font="vector" ratio="16">&gt;NAME</text>
<circle x="0" y="0" radius="0.559015625" width="0.127" layer="21"/>
<polygon width="0.127" layer="21">
<vertex x="-3.2" y="1.196" curve="90"/>
<vertex x="-2.954" y="0.95" curve="90"/>
<vertex x="-2.7" y="1.204" curve="90"/>
<vertex x="-2.946" y="1.45" curve="90"/>
</polygon>
</package>
</packages>
<packages3d>
<package3d name="PLCC4" urn="urn:adsk.eagle:package:10009056/2" type="model">
<description>&lt;b&gt;Description:&lt;/b&gt; Footprint for LEDs in PLCC4 Standard.&lt;br/&gt;</description>
<packageinstances>
<packageinstance name="PLCC4"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="LED_RGB_CA">
<description>&lt;b&gt;Description:&lt;/b&gt; Symbol for RGB LED with Common Anode&lt;br/&gt;</description>
<wire x1="1.27" y1="9.144" x2="1.27" y2="6.096" width="0.254" layer="94"/>
<wire x1="1.016" y1="7.62" x2="-0.508" y2="6.096" width="0.254" layer="94"/>
<wire x1="-0.508" y1="6.096" x2="-0.508" y2="9.144" width="0.254" layer="94"/>
<wire x1="-0.508" y1="9.144" x2="1.016" y2="7.62" width="0.254" layer="94"/>
<polygon width="0.254" layer="94">
<vertex x="-0.508" y="6.096"/>
<vertex x="-0.508" y="9.144"/>
<vertex x="1.016" y="7.62"/>
</polygon>
<wire x1="-0.508" y1="9.906" x2="0.508" y2="10.922" width="0.254" layer="94"/>
<wire x1="0.508" y1="10.922" x2="0" y2="11.43" width="0.254" layer="94"/>
<wire x1="0" y1="11.43" x2="1.016" y2="12.446" width="0.254" layer="94"/>
<wire x1="1.016" y1="12.446" x2="0.508" y2="12.446" width="0.254" layer="94"/>
<wire x1="0.508" y1="12.446" x2="1.016" y2="11.938" width="0.254" layer="94"/>
<wire x1="1.016" y1="11.938" x2="1.016" y2="12.446" width="0.254" layer="94"/>
<wire x1="0.762" y1="9.906" x2="1.778" y2="10.922" width="0.254" layer="94"/>
<wire x1="1.778" y1="10.922" x2="1.27" y2="11.43" width="0.254" layer="94"/>
<wire x1="1.27" y1="11.43" x2="2.286" y2="12.446" width="0.254" layer="94"/>
<wire x1="2.286" y1="12.446" x2="1.778" y2="12.446" width="0.254" layer="94"/>
<wire x1="1.778" y1="12.446" x2="2.286" y2="11.938" width="0.254" layer="94"/>
<wire x1="2.286" y1="11.938" x2="2.286" y2="12.446" width="0.254" layer="94"/>
<text x="-5.08" y="-10.16" size="1.016" layer="95" font="vector" align="top-left">&gt;NAME</text>
<text x="-5.08" y="-12.7" size="1.016" layer="96" font="vector">&gt;VALUE</text>
<pin name="ANODE" x="-7.62" y="0" visible="off" length="middle" direction="pwr"/>
<pin name="CATHODE_RED" x="5.08" y="7.62" visible="off" length="middle" direction="pwr" rot="R180"/>
<pin name="CATHODE_GREEN" x="5.08" y="0" visible="off" length="middle" direction="pwr" rot="R180"/>
<pin name="CATHODE_BLUE" x="5.08" y="-7.62" visible="off" length="middle" direction="pwr" rot="R180"/>
<wire x1="1.27" y1="1.524" x2="1.27" y2="-1.524" width="0.254" layer="94"/>
<wire x1="1.016" y1="0" x2="-0.508" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-0.508" y1="-1.524" x2="-0.508" y2="1.524" width="0.254" layer="94"/>
<wire x1="-0.508" y1="1.524" x2="1.016" y2="0" width="0.254" layer="94"/>
<polygon width="0.254" layer="94">
<vertex x="-0.508" y="-1.524"/>
<vertex x="-0.508" y="1.524"/>
<vertex x="1.016" y="0"/>
</polygon>
<wire x1="-0.508" y1="2.286" x2="0.508" y2="3.302" width="0.254" layer="94"/>
<wire x1="0.508" y1="3.302" x2="0" y2="3.81" width="0.254" layer="94"/>
<wire x1="0" y1="3.81" x2="1.016" y2="4.826" width="0.254" layer="94"/>
<wire x1="1.016" y1="4.826" x2="0.508" y2="4.826" width="0.254" layer="94"/>
<wire x1="0.508" y1="4.826" x2="1.016" y2="4.318" width="0.254" layer="94"/>
<wire x1="1.016" y1="4.318" x2="1.016" y2="4.826" width="0.254" layer="94"/>
<wire x1="0.762" y1="2.286" x2="1.778" y2="3.302" width="0.254" layer="94"/>
<wire x1="1.778" y1="3.302" x2="1.27" y2="3.81" width="0.254" layer="94"/>
<wire x1="1.27" y1="3.81" x2="2.286" y2="4.826" width="0.254" layer="94"/>
<wire x1="2.286" y1="4.826" x2="1.778" y2="4.826" width="0.254" layer="94"/>
<wire x1="1.778" y1="4.826" x2="2.286" y2="4.318" width="0.254" layer="94"/>
<wire x1="2.286" y1="4.318" x2="2.286" y2="4.826" width="0.254" layer="94"/>
<wire x1="1.27" y1="-6.096" x2="1.27" y2="-9.144" width="0.254" layer="94"/>
<wire x1="1.016" y1="-7.62" x2="-0.508" y2="-9.144" width="0.254" layer="94"/>
<wire x1="-0.508" y1="-9.144" x2="-0.508" y2="-6.096" width="0.254" layer="94"/>
<wire x1="-0.508" y1="-6.096" x2="1.016" y2="-7.62" width="0.254" layer="94"/>
<polygon width="0.254" layer="94">
<vertex x="-0.508" y="-9.144"/>
<vertex x="-0.508" y="-6.096"/>
<vertex x="1.016" y="-7.62"/>
</polygon>
<wire x1="-0.508" y1="-5.334" x2="0.508" y2="-4.318" width="0.254" layer="94"/>
<wire x1="0.508" y1="-4.318" x2="0" y2="-3.81" width="0.254" layer="94"/>
<wire x1="0" y1="-3.81" x2="1.016" y2="-2.794" width="0.254" layer="94"/>
<wire x1="1.016" y1="-2.794" x2="0.508" y2="-2.794" width="0.254" layer="94"/>
<wire x1="0.508" y1="-2.794" x2="1.016" y2="-3.302" width="0.254" layer="94"/>
<wire x1="1.016" y1="-3.302" x2="1.016" y2="-2.794" width="0.254" layer="94"/>
<wire x1="0.762" y1="-5.334" x2="1.778" y2="-4.318" width="0.254" layer="94"/>
<wire x1="1.778" y1="-4.318" x2="1.27" y2="-3.81" width="0.254" layer="94"/>
<wire x1="1.27" y1="-3.81" x2="2.286" y2="-2.794" width="0.254" layer="94"/>
<wire x1="2.286" y1="-2.794" x2="1.778" y2="-2.794" width="0.254" layer="94"/>
<wire x1="1.778" y1="-2.794" x2="2.286" y2="-3.302" width="0.254" layer="94"/>
<wire x1="2.286" y1="-3.302" x2="2.286" y2="-2.794" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="7.62" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="7.62" x2="0" y2="7.62" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-7.62" x2="0" y2="-7.62" width="0.1524" layer="94"/>
<text x="2.54" y="10.16" size="1.016" layer="97" font="vector">RED</text>
<text x="2.54" y="2.54" size="1.016" layer="97" font="vector">GREEN</text>
<text x="2.54" y="-5.08" size="1.016" layer="97" font="vector">BLUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED_RGB_CA" prefix="D" uservalue="yes">
<description>&lt;b&gt;Library:&lt;/b&gt;  MF_LEDs&lt;br/&gt;
&lt;b&gt;Description:&lt;/b&gt; Device for RGB LED Packages with Common Anodes. Manufacture part number (MFG#) can be added via Attributes.&lt;br/&gt;</description>
<gates>
<gate name="G$1" symbol="LED_RGB_CA" x="0" y="5.08"/>
</gates>
<devices>
<device name="_PLCC4" package="PLCC4">
<connects>
<connect gate="G$1" pin="ANODE" pad="P$1"/>
<connect gate="G$1" pin="CATHODE_BLUE" pad="P$4"/>
<connect gate="G$1" pin="CATHODE_GREEN" pad="P$3"/>
<connect gate="G$1" pin="CATHODE_RED" pad="P$2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:10009056/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="HOUSEPART" value="YES" constant="no"/>
<attribute name="MPN" value="MF-LED-3228-RGB" constant="no"/>
<attribute name="POPULATE" value="1" constant="no"/>
<attribute name="URL" value="https://factory.macrofab.com/part/MF-LED-3228-RGB" constant="no"/>
<attribute name="VALUE" value="MF-LED-3228-RGB" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="MCP73831T-3ACI_OT">
<packages>
<package name="SOT95P270X145-5N" urn="urn:adsk.eagle:footprint:9987755/1" locally_modified="yes">
<wire x1="1.905" y1="-1.524" x2="-1.905" y2="-1.524" width="0.1524" layer="39"/>
<wire x1="-1.905" y1="-1.524" x2="-1.905" y2="1.524" width="0.1524" layer="39"/>
<wire x1="-1.905" y1="1.524" x2="1.905" y2="1.524" width="0.1524" layer="39"/>
<wire x1="1.905" y1="1.524" x2="1.905" y2="-1.524" width="0.1524" layer="39"/>
<wire x1="0.635" y1="1.5494" x2="0.3048" y2="1.5494" width="0.1524" layer="21"/>
<wire x1="0.3048" y1="1.5494" x2="-0.3048" y2="1.5494" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="1.5494" x2="-0.635" y2="1.5494" width="0.1524" layer="21"/>
<wire x1="0.3048" y1="1.5494" x2="-0.3048" y2="1.5494" width="0.1524" layer="21" curve="-180"/>
<wire x1="-0.889" y1="-1.5494" x2="0.889" y2="-1.5494" width="0.1524" layer="51"/>
<wire x1="0.889" y1="-1.5494" x2="0.889" y2="-1.1938" width="0.1524" layer="51"/>
<wire x1="0.889" y1="-1.1938" x2="0.889" y2="-0.6858" width="0.1524" layer="51"/>
<wire x1="0.889" y1="-0.6858" x2="0.889" y2="0.6858" width="0.1524" layer="51"/>
<wire x1="0.889" y1="1.5494" x2="0.3048" y2="1.5494" width="0.1524" layer="51"/>
<wire x1="0.3048" y1="1.5494" x2="-0.3048" y2="1.5494" width="0.1524" layer="51"/>
<wire x1="-0.3048" y1="1.5494" x2="-0.889" y2="1.5494" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="1.5494" x2="-0.889" y2="1.1938" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="1.1938" x2="-0.889" y2="0.6858" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="0.6858" x2="-0.889" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="0.254" x2="-0.889" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="-0.254" x2="-0.889" y2="-0.6858" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="1.1938" x2="-1.6002" y2="1.1938" width="0.1524" layer="51"/>
<wire x1="-1.6002" y1="1.1938" x2="-1.6002" y2="0.6858" width="0.1524" layer="51"/>
<wire x1="-1.6002" y1="0.6858" x2="-0.889" y2="0.6858" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="0.254" x2="-1.6002" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-1.6002" y1="0.254" x2="-1.6002" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="-1.6002" y1="-0.254" x2="-0.889" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="-1.5494" x2="-0.889" y2="-1.1938" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="-1.1938" x2="-0.889" y2="-0.6858" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="-0.6858" x2="-1.6002" y2="-0.6858" width="0.1524" layer="51"/>
<wire x1="-1.6002" y1="-0.6858" x2="-1.6002" y2="-1.1938" width="0.1524" layer="51"/>
<wire x1="-1.6002" y1="-1.1938" x2="-0.889" y2="-1.1938" width="0.1524" layer="51"/>
<wire x1="0.889" y1="-1.1938" x2="1.6002" y2="-1.1938" width="0.1524" layer="51"/>
<wire x1="1.6002" y1="-1.1938" x2="1.6002" y2="-0.6858" width="0.1524" layer="51"/>
<wire x1="1.6002" y1="-0.6858" x2="0.889" y2="-0.6858" width="0.1524" layer="51"/>
<wire x1="0.889" y1="1.5494" x2="0.889" y2="1.1938" width="0.1524" layer="51"/>
<wire x1="0.889" y1="1.1938" x2="0.889" y2="0.6858" width="0.1524" layer="51"/>
<wire x1="0.889" y1="0.6858" x2="1.6002" y2="0.6858" width="0.1524" layer="51"/>
<wire x1="1.6002" y1="0.6858" x2="1.6002" y2="1.1938" width="0.1524" layer="51"/>
<wire x1="1.6002" y1="1.1938" x2="0.889" y2="1.1938" width="0.1524" layer="51"/>
<wire x1="0.3048" y1="1.5494" x2="-0.3048" y2="1.5494" width="0.1524" layer="51" curve="-180"/>
<text x="-4.170740625" y="2.16166875" size="2.08536875" layer="25" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-5.574690625" y="-4.25101875" size="2.08733125" layer="27" ratio="10" rot="SR0">&gt;VALUE</text>
<smd name="1" x="-1.3716" y="0.9398" dx="1.3208" dy="0.5588" layer="1"/>
<smd name="2" x="-1.3716" y="0" dx="1.3208" dy="0.5588" layer="1"/>
<smd name="3" x="-1.3716" y="-0.9398" dx="1.3208" dy="0.5588" layer="1"/>
<smd name="4" x="1.3716" y="-0.9398" dx="1.3208" dy="0.5588" layer="1"/>
<smd name="5" x="1.3716" y="0.9398" dx="1.3208" dy="0.5588" layer="1"/>
</package>
</packages>
<packages3d>
<package3d name="SOT95P270X145-5N" urn="urn:adsk.eagle:package:9987757/2" locally_modified="yes" type="model">
<packageinstances>
<packageinstance name="SOT95P270X145-5N"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="MCP73831T-3ACI/OT">
<wire x1="-7.62" y1="5.08" x2="-7.62" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="7.62" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="5.08" width="0.4064" layer="94"/>
<wire x1="7.62" y1="5.08" x2="-7.62" y2="5.08" width="0.4064" layer="94"/>
<text x="-4.532059375" y="6.445009375" size="2.0873" layer="95" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-3.3175" y="-7.9151" size="2.08831875" layer="96" ratio="10" rot="SR0">&gt;VALUE</text>
<pin name="PROG" x="12.7" y="0" length="middle" direction="in" rot="R180"/>
<pin name="STAT" x="-12.7" y="-2.54" length="middle" direction="out"/>
<pin name="VSS" x="12.7" y="-2.54" length="middle" direction="pwr" rot="R180"/>
<pin name="VBAT" x="12.7" y="2.54" length="middle" direction="pwr" rot="R180"/>
<pin name="VIN" x="-12.7" y="2.54" length="middle" direction="pwr"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MCP73831T-3ACI/OT" prefix="U">
<description>Li-Polymer Charge Management Controllers, SOT23-5</description>
<gates>
<gate name="A" symbol="MCP73831T-3ACI/OT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT95P270X145-5N">
<connects>
<connect gate="A" pin="PROG" pad="5"/>
<connect gate="A" pin="STAT" pad="1"/>
<connect gate="A" pin="VBAT" pad="3"/>
<connect gate="A" pin="VIN" pad="4"/>
<connect gate="A" pin="VSS" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:9987757/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value=" Tiny Integrated Li-Ion/Li-Poly Charge mgnt controller, 4.35V Vreg out5 SOT-23 T/R "/>
<attribute name="DIGI-KEY_PART_NUMBER" value="MCP73831T-3ACI/OTCT-ND"/>
<attribute name="DIGI-KEY_PURCHASE_URL" value="https://www.digikey.com/product-detail/en/microchip-technology/MCP73831T-3ACI-OT/MCP73831T-3ACI-OTCT-ND/5148863?utm_source=snapeda&amp;utm_medium=aggregator&amp;utm_campaign=symbol"/>
<attribute name="MF" value="Microchip"/>
<attribute name="MP" value="MCP73831T-3ACI/OT"/>
<attribute name="PACKAGE" value="SOT-23-5 Microchip"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Switches">
<description>&lt;h3&gt;SparkFun Switches, Buttons, Encoders&lt;/h3&gt;
In this library you'll find switches, buttons, joysticks, and anything that moves to create or disrupt an electrical connection.
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="TACTILE_SWITCH_SMD_5.2MM" urn="urn:adsk.eagle:footprint:9989133/1">
<description>&lt;h3&gt;Momentary Switch (Pushbutton) - SPST - SMD, 5.2mm Square&lt;/h3&gt;
&lt;p&gt;Normally-open (NO) SPST momentary switches (buttons, pushbuttons).&lt;/p&gt;
&lt;p&gt;&lt;a href="https://www.sparkfun.com/datasheets/Components/Buttons/SMD-Button.pdf"&gt;Dimensional Drawing&lt;/a&gt;&lt;/p&gt;</description>
<wire x1="-1.54" y1="-2.54" x2="-2.54" y2="-1.54" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-1.24" x2="-2.54" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="1.54" x2="-1.54" y2="2.54" width="0.2032" layer="51"/>
<wire x1="-1.54" y1="2.54" x2="1.54" y2="2.54" width="0.2032" layer="21"/>
<wire x1="1.54" y1="2.54" x2="2.54" y2="1.54" width="0.2032" layer="51"/>
<wire x1="2.54" y1="1.24" x2="2.54" y2="-1.24" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.54" x2="1.54" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="1.54" y1="-2.54" x2="-1.54" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="1.905" y2="0.445" width="0.127" layer="51"/>
<wire x1="1.905" y1="0.445" x2="2.16" y2="-0.01" width="0.127" layer="51"/>
<wire x1="1.905" y1="-0.23" x2="1.905" y2="-1.115" width="0.127" layer="51"/>
<circle x="0" y="0" radius="1.27" width="0.2032" layer="21"/>
<smd name="1" x="-2.794" y="1.905" dx="0.762" dy="1.524" layer="1" rot="R90"/>
<smd name="2" x="2.794" y="1.905" dx="0.762" dy="1.524" layer="1" rot="R90"/>
<smd name="3" x="-2.794" y="-1.905" dx="0.762" dy="1.524" layer="1" rot="R90"/>
<smd name="4" x="2.794" y="-1.905" dx="0.762" dy="1.524" layer="1" rot="R90"/>
<text x="0" y="2.667" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-2.667" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
</package>
<package name="TACTILE_SWITCH_PTH_6.0MM">
<description>&lt;h3&gt;Momentary Switch (Pushbutton) - SPST - PTH, 6.0mm Square&lt;/h3&gt;
&lt;p&gt;Normally-open (NO) SPST momentary switches (buttons, pushbuttons).&lt;/p&gt;
&lt;p&gt;&lt;a href="https://www.omron.com/ecb/products/pdf/en-b3f.pdf"&gt;Datasheet&lt;/a&gt; (B3F-1000)&lt;/p&gt;</description>
<wire x1="3.048" y1="1.016" x2="3.048" y2="2.54" width="0.2032" layer="51"/>
<wire x1="3.048" y1="2.54" x2="2.54" y2="3.048" width="0.2032" layer="51"/>
<wire x1="2.54" y1="-3.048" x2="3.048" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="3.048" y1="-2.54" x2="3.048" y2="-1.016" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="3.048" x2="-3.048" y2="2.54" width="0.2032" layer="51"/>
<wire x1="-3.048" y1="2.54" x2="-3.048" y2="1.016" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-3.048" x2="-3.048" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="-3.048" y1="-2.54" x2="-3.048" y2="-1.016" width="0.2032" layer="51"/>
<wire x1="2.54" y1="-3.048" x2="2.159" y2="-3.048" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-3.048" x2="-2.159" y2="-3.048" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="3.048" x2="-2.159" y2="3.048" width="0.2032" layer="51"/>
<wire x1="2.54" y1="3.048" x2="2.159" y2="3.048" width="0.2032" layer="51"/>
<wire x1="2.159" y1="3.048" x2="-2.159" y2="3.048" width="0.2032" layer="21"/>
<wire x1="-2.159" y1="-3.048" x2="2.159" y2="-3.048" width="0.2032" layer="21"/>
<wire x1="3.048" y1="0.998" x2="3.048" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="-3.048" y1="1.028" x2="-3.048" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="0.508" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-0.508" x2="-2.54" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="0.508" x2="-2.159" y2="-0.381" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.778" width="0.2032" layer="21"/>
<pad name="1" x="-3.2512" y="2.2606" drill="1.016" diameter="1.8796"/>
<pad name="2" x="3.2512" y="2.2606" drill="1.016" diameter="1.8796"/>
<pad name="3" x="-3.2512" y="-2.2606" drill="1.016" diameter="1.8796"/>
<pad name="4" x="3.2512" y="-2.2606" drill="1.016" diameter="1.8796"/>
<text x="0" y="3.302" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-3.175" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
</package>
<package name="TACTILE_SWITCH_SMD_4.5MM">
<description>&lt;h3&gt;Momentary Switch (Pushbutton) - SPST - SMD, 4.5mm Square&lt;/h3&gt;
&lt;p&gt;Normally-open (NO) SPST momentary switches (buttons, pushbuttons).&lt;/p&gt;
&lt;p&gt;&lt;a href="http://spec_sheets.e-switch.com/specs/P010338.pdf"&gt;Dimensional Drawing&lt;/a&gt;&lt;/p&gt;</description>
<wire x1="1.905" y1="1.27" x2="1.905" y2="0.445" width="0.127" layer="51"/>
<wire x1="1.905" y1="0.445" x2="2.16" y2="-0.01" width="0.127" layer="51"/>
<wire x1="1.905" y1="-0.23" x2="1.905" y2="-1.115" width="0.127" layer="51"/>
<wire x1="-2.25" y1="2.25" x2="2.25" y2="2.25" width="0.127" layer="51"/>
<wire x1="2.25" y1="2.25" x2="2.25" y2="-2.25" width="0.127" layer="51"/>
<wire x1="2.25" y1="-2.25" x2="-2.25" y2="-2.25" width="0.127" layer="51"/>
<wire x1="-2.25" y1="-2.25" x2="-2.25" y2="2.25" width="0.127" layer="51"/>
<wire x1="-2.2" y1="0.8" x2="-2.2" y2="-0.8" width="0.2032" layer="21"/>
<wire x1="1.3" y1="2.2" x2="-1.3" y2="2.2" width="0.2032" layer="21"/>
<wire x1="2.2" y1="-0.8" x2="2.2" y2="0.8" width="0.2032" layer="21"/>
<wire x1="-1.3" y1="-2.2" x2="1.3" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="2.2" y1="0.8" x2="1.8" y2="0.8" width="0.2032" layer="21"/>
<wire x1="2.2" y1="-0.8" x2="1.8" y2="-0.8" width="0.2032" layer="21"/>
<wire x1="-1.8" y1="0.8" x2="-2.2" y2="0.8" width="0.2032" layer="21"/>
<wire x1="-1.8" y1="-0.8" x2="-2.2" y2="-0.8" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="1.27" width="0.2032" layer="21"/>
<smd name="1" x="2.225" y="1.75" dx="1.1" dy="0.7" layer="1" rot="R90"/>
<smd name="2" x="2.225" y="-1.75" dx="1.1" dy="0.7" layer="1" rot="R90"/>
<smd name="3" x="-2.225" y="-1.75" dx="1.1" dy="0.7" layer="1" rot="R90"/>
<smd name="4" x="-2.225" y="1.75" dx="1.1" dy="0.7" layer="1" rot="R90"/>
<text x="0" y="2.413" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-2.413" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
</package>
<package name="TACTILE_SWITCH_PTH_12MM">
<description>&lt;h3&gt;Momentary Switch (Pushbutton) - SPST - PTH, 12mm Square&lt;/h3&gt;
&lt;p&gt;Normally-open (NO) SPST momentary switches (buttons, pushbuttons).&lt;/p&gt;
&lt;p&gt;&lt;a href="https://www.omron.com/ecb/products/pdf/en-b3f.pdf"&gt;Datasheet&lt;/a&gt; (B3F-5050)&lt;/p&gt;</description>
<wire x1="5" y1="-1.3" x2="5" y2="-0.7" width="0.2032" layer="51"/>
<wire x1="5" y1="-0.7" x2="4.5" y2="-0.2" width="0.2032" layer="51"/>
<wire x1="5" y1="0.2" x2="5" y2="1" width="0.2032" layer="51"/>
<wire x1="-6" y1="4" x2="-6" y2="5" width="0.2032" layer="21"/>
<wire x1="-5" y1="6" x2="5" y2="6" width="0.2032" layer="21"/>
<wire x1="6" y1="5" x2="6" y2="4" width="0.2032" layer="21"/>
<wire x1="6" y1="1" x2="6" y2="-1" width="0.2032" layer="21"/>
<wire x1="6" y1="-4" x2="6" y2="-5" width="0.2032" layer="21"/>
<wire x1="5" y1="-6" x2="-5" y2="-6" width="0.2032" layer="21"/>
<wire x1="-6" y1="-5" x2="-6" y2="-4" width="0.2032" layer="21"/>
<wire x1="-6" y1="-1" x2="-6" y2="1" width="0.2032" layer="21"/>
<wire x1="-6" y1="5" x2="-5" y2="6" width="0.2032" layer="21" curve="-90"/>
<wire x1="5" y1="6" x2="6" y2="5" width="0.2032" layer="21" curve="-90"/>
<wire x1="6" y1="-5" x2="5" y2="-6" width="0.2032" layer="21" curve="-90"/>
<wire x1="-5" y1="-6" x2="-6" y2="-5" width="0.2032" layer="21" curve="-90"/>
<circle x="0" y="0" radius="3.5" width="0.2032" layer="21"/>
<circle x="-4.5" y="4.5" radius="0.3" width="0.7" layer="21"/>
<circle x="4.5" y="4.5" radius="0.3" width="0.7" layer="21"/>
<circle x="4.5" y="-4.5" radius="0.3" width="0.7" layer="21"/>
<circle x="-4.5" y="-4.5" radius="0.3" width="0.7" layer="21"/>
<pad name="4" x="-6.25" y="2.5" drill="1.2" diameter="2.159"/>
<pad name="2" x="-6.25" y="-2.5" drill="1.2" diameter="2.159"/>
<pad name="1" x="6.25" y="-2.5" drill="1.2" diameter="2.159"/>
<pad name="3" x="6.25" y="2.5" drill="1.2" diameter="2.159"/>
<text x="0" y="6.223" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-6.223" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
</package>
<package name="TACTILE_SWITCH_SMD_6.0X3.5MM">
<description>&lt;h3&gt;Momentary Switch (Pushbutton) - SPST - SMD, 6.0 x 3.5 mm&lt;/h3&gt;
&lt;p&gt;Normally-open (NO) SPST momentary switches (buttons, pushbuttons).&lt;/p&gt;
&lt;p&gt;&lt;a href="https://www.sparkfun.com/datasheets/Components/1101.pdf"&gt;Datasheet&lt;/a&gt;&lt;/p&gt;</description>
<wire x1="-3" y1="1.1" x2="-3" y2="-1.1" width="0.127" layer="51"/>
<wire x1="3" y1="1.1" x2="3" y2="-1.1" width="0.127" layer="51"/>
<wire x1="-2.75" y1="1.75" x2="-3" y2="1.5" width="0.2032" layer="21" curve="90"/>
<wire x1="-2.75" y1="1.75" x2="2.75" y2="1.75" width="0.2032" layer="21"/>
<wire x1="2.75" y1="1.75" x2="3" y2="1.5" width="0.2032" layer="21" curve="-90"/>
<wire x1="3" y1="-1.5" x2="2.75" y2="-1.75" width="0.2032" layer="21" curve="-90"/>
<wire x1="2.75" y1="-1.75" x2="-2.75" y2="-1.75" width="0.2032" layer="21"/>
<wire x1="-3" y1="-1.5" x2="-2.75" y2="-1.75" width="0.2032" layer="21" curve="90"/>
<wire x1="-3" y1="-1.5" x2="-3" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="-3" y1="1.1" x2="-3" y2="1.5" width="0.2032" layer="21"/>
<wire x1="3" y1="1.1" x2="3" y2="1.5" width="0.2032" layer="21"/>
<wire x1="3" y1="-1.5" x2="3" y2="-1.1" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="0.75" x2="1.5" y2="0.75" width="0.2032" layer="21"/>
<wire x1="1.5" y1="-0.75" x2="-1.5" y2="-0.75" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="-0.75" x2="-1.5" y2="0.75" width="0.2032" layer="21"/>
<wire x1="1.5" y1="-0.75" x2="1.5" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-2" y1="0" x2="-1" y2="0" width="0.127" layer="51"/>
<wire x1="-1" y1="0" x2="0.1" y2="0.5" width="0.127" layer="51"/>
<wire x1="0.3" y1="0" x2="2" y2="0" width="0.127" layer="51"/>
<smd name="1" x="-3.15" y="0" dx="2.3" dy="1.6" layer="1" rot="R180"/>
<smd name="2" x="3.15" y="0" dx="2.3" dy="1.6" layer="1" rot="R180"/>
<text x="0" y="1.905" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-1.905" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
</package>
<package name="TACTILE_SWITCH_SMD_6.2MM_TALL">
<description>&lt;h3&gt;Momentary Switch (Pushbutton) - SPST - SMD, 6.2mm Square&lt;/h3&gt;
&lt;p&gt;Normally-open (NO) SPST momentary switches (buttons, pushbuttons).&lt;/p&gt;
&lt;p&gt;&lt;a href="http://www.apem.com/files/apem/brochures/ADTS6-ADTSM-KTSC6.pdf"&gt;Datasheet&lt;/a&gt; (ADTSM63NVTR)&lt;/p&gt;</description>
<wire x1="-3" y1="-3" x2="3" y2="-3" width="0.2032" layer="21"/>
<wire x1="3" y1="-3" x2="3" y2="3" width="0.2032" layer="21"/>
<wire x1="3" y1="3" x2="-3" y2="3" width="0.2032" layer="21"/>
<wire x1="-3" y1="3" x2="-3" y2="-3" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="1.75" width="0.2032" layer="21"/>
<smd name="A1" x="-3.975" y="-2.25" dx="1.3" dy="1.55" layer="1" rot="R90"/>
<smd name="A2" x="3.975" y="-2.25" dx="1.3" dy="1.55" layer="1" rot="R90"/>
<smd name="B1" x="-3.975" y="2.25" dx="1.3" dy="1.55" layer="1" rot="R90"/>
<smd name="B2" x="3.975" y="2.25" dx="1.3" dy="1.55" layer="1" rot="R90"/>
<text x="0" y="3.175" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-3.175" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
</package>
<package name="TACTILE_SWITCH_PTH_RIGHT_ANGLE_KIT">
<description>&lt;h3&gt;Momentary Switch (Pushbutton) - SPST - PTH, Right-angle&lt;/h3&gt;
&lt;p&gt;Normally-open (NO) SPST momentary switches (buttons, pushbuttons).&lt;/p&gt;
&lt;p&gt;&lt;a href="http://cdn.sparkfun.com/datasheets/Components/Switches/SW016.JPG"&gt;Dimensional Drawing&lt;/a&gt;&lt;/p&gt;</description>
<wire x1="1.5" y1="-3.8" x2="-1.5" y2="-3.8" width="0.127" layer="51"/>
<wire x1="-3.65" y1="-2" x2="-3.65" y2="3.5" width="0.127" layer="51"/>
<wire x1="-3.65" y1="3.5" x2="-3" y2="3.5" width="0.127" layer="51"/>
<wire x1="3" y1="3.5" x2="3.65" y2="3.5" width="0.127" layer="51"/>
<wire x1="3.65" y1="3.5" x2="3.65" y2="-2" width="0.127" layer="51"/>
<wire x1="-3" y1="2" x2="3" y2="2" width="0.127" layer="51"/>
<wire x1="-3" y1="2" x2="-3" y2="3.5" width="0.127" layer="51"/>
<wire x1="3" y1="2" x2="3" y2="3.5" width="0.127" layer="51"/>
<wire x1="-3.65" y1="-2" x2="-1.5" y2="-2" width="0.127" layer="51"/>
<wire x1="-1.5" y1="-2" x2="1.5" y2="-2" width="0.127" layer="51"/>
<wire x1="1.5" y1="-2" x2="3.65" y2="-2" width="0.127" layer="51"/>
<wire x1="1.5" y1="-2" x2="1.5" y2="-3.8" width="0.127" layer="51"/>
<wire x1="-1.5" y1="-2" x2="-1.5" y2="-3.8" width="0.127" layer="51"/>
<wire x1="-3.777" y1="1" x2="-3.777" y2="-2.127" width="0.2032" layer="21"/>
<wire x1="-3.777" y1="-2.127" x2="3.777" y2="-2.127" width="0.2032" layer="21"/>
<wire x1="3.777" y1="-2.127" x2="3.777" y2="1" width="0.2032" layer="21"/>
<wire x1="2" y1="2.127" x2="-2" y2="2.127" width="0.2032" layer="21"/>
<pad name="ANCHOR1" x="-3.5" y="2.5" drill="1.2" diameter="2.2" stop="no"/>
<pad name="ANCHOR2" x="3.5" y="2.5" drill="1.2" diameter="2.2" stop="no"/>
<pad name="1" x="-2.5" y="0" drill="0.8" diameter="1.7" stop="no"/>
<pad name="2" x="2.5" y="0" drill="0.8" diameter="1.7" stop="no"/>
<circle x="2.5" y="0" radius="0.4445" width="0" layer="29"/>
<circle x="-2.5" y="0" radius="0.4445" width="0" layer="29"/>
<circle x="-3.5" y="2.5" radius="0.635" width="0" layer="29"/>
<circle x="3.5" y="2.5" radius="0.635" width="0" layer="29"/>
<circle x="-3.5" y="2.5" radius="1.143" width="0" layer="30"/>
<circle x="2.5" y="0" radius="0.889" width="0" layer="30"/>
<circle x="-2.5" y="0" radius="0.889" width="0" layer="30"/>
<circle x="3.5" y="2.5" radius="1.143" width="0" layer="30"/>
<text x="0" y="2.286" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-2.286" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
</package>
<package name="TACTILE_SWITCH_SMD_12MM">
<description>&lt;h3&gt;Momentary Switch (Pushbutton) - SPST - SMD, 12mm Square&lt;/h3&gt;
&lt;p&gt;Normally-open (NO) SPST momentary switches (buttons, pushbuttons).&lt;/p&gt;
&lt;p&gt;&lt;a href="https://cdn.sparkfun.com/datasheets/Components/Switches/N301102.pdf"&gt;Datasheet&lt;/a&gt;&lt;/p&gt;</description>
<wire x1="5" y1="-1.3" x2="5" y2="-0.7" width="0.2032" layer="51"/>
<wire x1="5" y1="-0.7" x2="4.5" y2="-0.2" width="0.2032" layer="51"/>
<wire x1="5" y1="0.2" x2="5" y2="1" width="0.2032" layer="51"/>
<wire x1="-6" y1="4" x2="-6" y2="5" width="0.2032" layer="21"/>
<wire x1="-5" y1="6" x2="5" y2="6" width="0.2032" layer="21"/>
<wire x1="6" y1="5" x2="6" y2="4" width="0.2032" layer="21"/>
<wire x1="6" y1="1" x2="6" y2="-1" width="0.2032" layer="21"/>
<wire x1="6" y1="-4" x2="6" y2="-5" width="0.2032" layer="21"/>
<wire x1="5" y1="-6" x2="-5" y2="-6" width="0.2032" layer="21"/>
<wire x1="-6" y1="-5" x2="-6" y2="-4" width="0.2032" layer="21"/>
<wire x1="-6" y1="-1" x2="-6" y2="1" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="3.5" width="0.2032" layer="21"/>
<circle x="-4.5" y="4.5" radius="0.3" width="0.7" layer="21"/>
<circle x="4.5" y="4.5" radius="0.3" width="0.7" layer="21"/>
<circle x="4.5" y="-4.5" radius="0.3" width="0.7" layer="21"/>
<circle x="-4.5" y="-4.5" radius="0.3" width="0.7" layer="21"/>
<smd name="4" x="-6.975" y="2.5" dx="1.6" dy="1.55" layer="1"/>
<smd name="2" x="-6.975" y="-2.5" dx="1.6" dy="1.55" layer="1"/>
<smd name="1" x="6.975" y="-2.5" dx="1.6" dy="1.55" layer="1"/>
<smd name="3" x="6.975" y="2.5" dx="1.6" dy="1.55" layer="1"/>
<wire x1="-6" y1="-5" x2="-5" y2="-6" width="0.2032" layer="21"/>
<wire x1="6" y1="-5" x2="5" y2="-6" width="0.2032" layer="21"/>
<wire x1="6" y1="5" x2="5" y2="6" width="0.2032" layer="21"/>
<wire x1="-5" y1="6" x2="-6" y2="5" width="0.2032" layer="21"/>
<text x="0" y="6.223" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-6.223" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
</package>
<package name="TACTILE_SWITCH_PTH_6.0MM_KIT">
<description>&lt;h3&gt;Momentary Switch (Pushbutton) - SPST - PTH, 6.0mm Square&lt;/h3&gt;
&lt;p&gt;Normally-open (NO) SPST momentary switches (buttons, pushbuttons).&lt;/p&gt;
&lt;p&gt;&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of this package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.&lt;/p&gt;
&lt;p&gt;&lt;a href="https://www.omron.com/ecb/products/pdf/en-b3f.pdf"&gt;Datasheet&lt;/a&gt; (B3F-1000)&lt;/p&gt;</description>
<wire x1="3.048" y1="1.016" x2="3.048" y2="2.54" width="0.2032" layer="51"/>
<wire x1="3.048" y1="2.54" x2="2.54" y2="3.048" width="0.2032" layer="51"/>
<wire x1="2.54" y1="-3.048" x2="3.048" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="3.048" y1="-2.54" x2="3.048" y2="-1.016" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="3.048" x2="-3.048" y2="2.54" width="0.2032" layer="51"/>
<wire x1="-3.048" y1="2.54" x2="-3.048" y2="1.016" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-3.048" x2="-3.048" y2="-2.54" width="0.2032" layer="51"/>
<wire x1="-3.048" y1="-2.54" x2="-3.048" y2="-1.016" width="0.2032" layer="51"/>
<wire x1="2.54" y1="-3.048" x2="2.159" y2="-3.048" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-3.048" x2="-2.159" y2="-3.048" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="3.048" x2="-2.159" y2="3.048" width="0.2032" layer="51"/>
<wire x1="2.54" y1="3.048" x2="2.159" y2="3.048" width="0.2032" layer="51"/>
<wire x1="2.159" y1="3.048" x2="-2.159" y2="3.048" width="0.2032" layer="21"/>
<wire x1="-2.159" y1="-3.048" x2="2.159" y2="-3.048" width="0.2032" layer="21"/>
<wire x1="3.048" y1="0.998" x2="3.048" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="-3.048" y1="1.028" x2="-3.048" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="0.508" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="-0.508" x2="-2.54" y2="-1.27" width="0.2032" layer="51"/>
<wire x1="-2.54" y1="0.508" x2="-2.159" y2="-0.381" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.778" width="0.2032" layer="21"/>
<pad name="1" x="-3.2512" y="2.2606" drill="1.016" diameter="1.8796" stop="no"/>
<pad name="2" x="3.2512" y="2.2606" drill="1.016" diameter="1.8796" stop="no"/>
<pad name="3" x="-3.2512" y="-2.2606" drill="1.016" diameter="1.8796" stop="no"/>
<pad name="4" x="3.2512" y="-2.2606" drill="1.016" diameter="1.8796" stop="no"/>
<polygon width="0.127" layer="30">
<vertex x="-3.2664" y="3.142"/>
<vertex x="-3.2589" y="3.1445" curve="89.986886"/>
<vertex x="-4.1326" y="2.286"/>
<vertex x="-4.1351" y="2.2657" curve="90.00652"/>
<vertex x="-3.2563" y="1.392"/>
<vertex x="-3.2487" y="1.3869" curve="90.006616"/>
<vertex x="-2.3826" y="2.2403"/>
<vertex x="-2.3775" y="2.2683" curve="89.98711"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-3.2462" y="2.7026"/>
<vertex x="-3.2589" y="2.7051" curve="90.026544"/>
<vertex x="-3.6881" y="2.2733"/>
<vertex x="-3.6881" y="2.2632" curve="89.974074"/>
<vertex x="-3.2562" y="1.8213"/>
<vertex x="-3.2259" y="1.8186" curve="90.051271"/>
<vertex x="-2.8093" y="2.2658"/>
<vertex x="-2.8093" y="2.2606" curve="90.012964"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="3.2411" y="3.1395"/>
<vertex x="3.2486" y="3.142" curve="89.986886"/>
<vertex x="2.3749" y="2.2835"/>
<vertex x="2.3724" y="2.2632" curve="90.00652"/>
<vertex x="3.2512" y="1.3895"/>
<vertex x="3.2588" y="1.3844" curve="90.006616"/>
<vertex x="4.1249" y="2.2378"/>
<vertex x="4.13" y="2.2658" curve="89.98711"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="3.2613" y="2.7001"/>
<vertex x="3.2486" y="2.7026" curve="90.026544"/>
<vertex x="2.8194" y="2.2708"/>
<vertex x="2.8194" y="2.2607" curve="89.974074"/>
<vertex x="3.2513" y="1.8188"/>
<vertex x="3.2816" y="1.8161" curve="90.051271"/>
<vertex x="3.6982" y="2.2633"/>
<vertex x="3.6982" y="2.2581" curve="90.012964"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="-3.2613" y="-1.3868"/>
<vertex x="-3.2538" y="-1.3843" curve="89.986886"/>
<vertex x="-4.1275" y="-2.2428"/>
<vertex x="-4.13" y="-2.2631" curve="90.00652"/>
<vertex x="-3.2512" y="-3.1368"/>
<vertex x="-3.2436" y="-3.1419" curve="90.006616"/>
<vertex x="-2.3775" y="-2.2885"/>
<vertex x="-2.3724" y="-2.2605" curve="89.98711"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-3.2411" y="-1.8262"/>
<vertex x="-3.2538" y="-1.8237" curve="90.026544"/>
<vertex x="-3.683" y="-2.2555"/>
<vertex x="-3.683" y="-2.2656" curve="89.974074"/>
<vertex x="-3.2511" y="-2.7075"/>
<vertex x="-3.2208" y="-2.7102" curve="90.051271"/>
<vertex x="-2.8042" y="-2.263"/>
<vertex x="-2.8042" y="-2.2682" curve="90.012964"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="3.2411" y="-1.3843"/>
<vertex x="3.2486" y="-1.3818" curve="89.986886"/>
<vertex x="2.3749" y="-2.2403"/>
<vertex x="2.3724" y="-2.2606" curve="90.00652"/>
<vertex x="3.2512" y="-3.1343"/>
<vertex x="3.2588" y="-3.1394" curve="90.006616"/>
<vertex x="4.1249" y="-2.286"/>
<vertex x="4.13" y="-2.258" curve="89.98711"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="3.2613" y="-1.8237"/>
<vertex x="3.2486" y="-1.8212" curve="90.026544"/>
<vertex x="2.8194" y="-2.253"/>
<vertex x="2.8194" y="-2.2631" curve="89.974074"/>
<vertex x="3.2513" y="-2.705"/>
<vertex x="3.2816" y="-2.7077" curve="90.051271"/>
<vertex x="3.6982" y="-2.2605"/>
<vertex x="3.6982" y="-2.2657" curve="90.012964"/>
</polygon>
<text x="0" y="3.175" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-3.175" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
</package>
<package name="TACTILE_SWITCH_SMD_RIGHT_ANGLE">
<description>&lt;h3&gt;Momentary Switch (Pushbutton) - SPST - SMD, Right-angle&lt;/h3&gt;
&lt;p&gt;Normally-open (NO) SPST momentary switches (buttons, pushbuttons).&lt;/p&gt;</description>
<hole x="0" y="0.9" drill="0.7"/>
<hole x="0" y="-0.9" drill="0.7"/>
<smd name="1" x="-1.95" y="0" dx="2" dy="1.1" layer="1" rot="R90"/>
<smd name="2" x="1.95" y="0" dx="2" dy="1.1" layer="1" rot="R90"/>
<wire x1="-2" y1="1.2" x2="-2" y2="1.5" width="0.2032" layer="21"/>
<wire x1="-2" y1="1.5" x2="2" y2="1.5" width="0.2032" layer="21"/>
<wire x1="2" y1="1.5" x2="2" y2="1.2" width="0.2032" layer="21"/>
<wire x1="-2" y1="-1.2" x2="-2" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="-2" y1="-1.5" x2="-0.7" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="-0.7" y1="-1.5" x2="0.7" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="0.7" y1="-1.5" x2="2" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="2" y1="-1.5" x2="2" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="-0.7" y1="-2.1" x2="0.7" y2="-2.1" width="0.2032" layer="21"/>
<wire x1="0.7" y1="-2.1" x2="0.7" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="-0.7" y1="-2.1" x2="-0.7" y2="-1.5" width="0.2032" layer="21"/>
<text x="0" y="1.651" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-2.286" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
</package>
<package name="TACTILE_SWITCH_SMD_4.6X2.8MM">
<description>&lt;h3&gt;Momentary Switch (Pushbutton) - SPST - SMD, 4.6 x 2.8mm&lt;/h3&gt;
&lt;p&gt;Normally-open (NO) SPST momentary switches (buttons, pushbuttons).&lt;/p&gt;
&lt;p&gt;&lt;a href="http://www.ck-components.com/media/1479/kmr2.pdf"&gt;Datasheet&lt;/a&gt;&lt;/p&gt;</description>
<smd name="3" x="2.05" y="0.8" dx="0.9" dy="1" layer="1"/>
<smd name="2" x="2.05" y="-0.8" dx="0.9" dy="1" layer="1"/>
<smd name="1" x="-2.05" y="-0.8" dx="0.9" dy="1" layer="1"/>
<smd name="4" x="-2.05" y="0.8" dx="0.9" dy="1" layer="1"/>
<wire x1="-2.1" y1="1.4" x2="-2.1" y2="-1.4" width="0.127" layer="51"/>
<wire x1="2.1" y1="-1.4" x2="2.1" y2="1.4" width="0.127" layer="51"/>
<wire x1="-2.1" y1="1.4" x2="2.1" y2="1.4" width="0.127" layer="51"/>
<wire x1="-2.1" y1="-1.4" x2="2.1" y2="-1.4" width="0.127" layer="51"/>
<circle x="0" y="0" radius="0.805" width="0.127" layer="21"/>
<wire x1="1.338" y1="-1.4" x2="-1.338" y2="-1.4" width="0.2032" layer="21"/>
<wire x1="-1.338" y1="1.4" x2="1.338" y2="1.4" width="0.2032" layer="21"/>
<wire x1="-2.1" y1="0.13" x2="-2.1" y2="-0.13" width="0.2032" layer="21"/>
<wire x1="2.1" y1="-0.13" x2="2.1" y2="0.13" width="0.2032" layer="21"/>
<rectangle x1="-2.3" y1="0.5" x2="-2.1" y2="1.1" layer="51"/>
<rectangle x1="-2.3" y1="-1.1" x2="-2.1" y2="-0.5" layer="51"/>
<rectangle x1="2.1" y1="-1.1" x2="2.3" y2="-0.5" layer="51" rot="R180"/>
<rectangle x1="2.1" y1="0.5" x2="2.3" y2="1.1" layer="51" rot="R180"/>
<text x="0" y="1.524" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-1.524" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
</package>
<package name="TACTILE_SWITCH_PTH_RIGHT_ANGLE">
<description>&lt;h3&gt;Momentary Switch (Pushbutton) - SPST - PTH, Right-angle&lt;/h3&gt;
&lt;p&gt;Normally-open (NO) SPST momentary switches (buttons, pushbuttons).&lt;/p&gt;
&lt;p&gt;&lt;a href="http://cdn.sparkfun.com/datasheets/Components/Switches/SW016.JPG"&gt;Dimensional Drawing&lt;/a&gt;&lt;/p&gt;</description>
<wire x1="1.5" y1="-3.8" x2="-1.5" y2="-3.8" width="0.127" layer="51"/>
<wire x1="-3.65" y1="-2" x2="-3.65" y2="3.5" width="0.127" layer="51"/>
<wire x1="-3.65" y1="3.5" x2="-3" y2="3.5" width="0.127" layer="51"/>
<wire x1="3" y1="3.5" x2="3.65" y2="3.5" width="0.127" layer="51"/>
<wire x1="3.65" y1="3.5" x2="3.65" y2="-2" width="0.127" layer="51"/>
<wire x1="-3" y1="2" x2="3" y2="2" width="0.127" layer="51"/>
<wire x1="-3" y1="2" x2="-3" y2="3.5" width="0.127" layer="51"/>
<wire x1="3" y1="2" x2="3" y2="3.5" width="0.127" layer="51"/>
<wire x1="-3.65" y1="-2" x2="-1.5" y2="-2" width="0.127" layer="51"/>
<wire x1="-1.5" y1="-2" x2="1.5" y2="-2" width="0.127" layer="51"/>
<wire x1="1.5" y1="-2" x2="3.65" y2="-2" width="0.127" layer="51"/>
<wire x1="1.5" y1="-2" x2="1.5" y2="-3.8" width="0.127" layer="51"/>
<wire x1="-1.5" y1="-2" x2="-1.5" y2="-3.8" width="0.127" layer="51"/>
<wire x1="-3.777" y1="1" x2="-3.777" y2="-2.445003125" width="0.2032" layer="21"/>
<wire x1="-3.777" y1="-2.445003125" x2="3.777" y2="-2.445003125" width="0.2032" layer="21"/>
<wire x1="3.777" y1="-2.445003125" x2="3.777" y2="1" width="0.2032" layer="21"/>
<wire x1="2" y1="2.127" x2="-2" y2="2.127" width="0.2032" layer="21"/>
<pad name="ANCHOR1" x="-3.7032" y="2.5" drill="1.2" diameter="2.2"/>
<pad name="ANCHOR2" x="3.7032" y="2.5" drill="1.2" diameter="2.2"/>
<pad name="1" x="-2.5" y="0" drill="0.8" diameter="1.7" stop="no"/>
<pad name="2" x="2.5" y="0" drill="0.8" diameter="1.7" stop="no"/>
<circle x="2.5" y="0" radius="0.4445" width="0" layer="29"/>
<circle x="-2.5" y="0" radius="0.4445" width="0" layer="29"/>
<circle x="2.5" y="0" radius="0.889" width="0" layer="30"/>
<circle x="-2.5" y="0" radius="0.889" width="0" layer="30"/>
<text x="0" y="2.286" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;Name</text>
<text x="0" y="-2.286" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;Value</text>
<wire x1="-0.635" y1="-2.445003125" x2="0.635" y2="-2.445003125" width="0.4064" layer="51"/>
<wire x1="3.777" y1="1.905" x2="3.777" y2="3.683" width="0.4064" layer="51"/>
<wire x1="-3.777" y1="1.905" x2="-3.777" y2="3.683" width="0.4064" layer="51"/>
<text x="-1.27" y="-0.254" size="0.8128" layer="51" font="vector" ratio="15">Foot</text>
<wire x1="0" y1="-0.635" x2="0" y2="-1.651" width="0.127" layer="51"/>
<wire x1="0" y1="-1.651" x2="-0.127" y2="-1.27" width="0.127" layer="51"/>
<wire x1="-0.127" y1="-1.27" x2="0.127" y2="-1.27" width="0.127" layer="51"/>
<wire x1="0.127" y1="-1.27" x2="0" y2="-1.651" width="0.127" layer="51"/>
<wire x1="1.905" y1="3.683" x2="2.921" y2="3.683" width="0.127" layer="51"/>
<wire x1="2.921" y1="3.683" x2="2.54" y2="3.556" width="0.127" layer="51"/>
<wire x1="2.54" y1="3.556" x2="2.54" y2="3.81" width="0.127" layer="51"/>
<wire x1="2.54" y1="3.81" x2="2.921" y2="3.683" width="0.127" layer="51"/>
<wire x1="-1.905" y1="3.683" x2="-2.921" y2="3.683" width="0.127" layer="51"/>
<wire x1="-2.921" y1="3.683" x2="-2.54" y2="3.81" width="0.127" layer="51"/>
<wire x1="-2.54" y1="3.81" x2="-2.54" y2="3.556" width="0.127" layer="51"/>
<wire x1="-2.54" y1="3.556" x2="-2.921" y2="3.683" width="0.127" layer="51"/>
<text x="-1.27" y="3.302" size="0.8128" layer="51" font="vector" ratio="15">Feet</text>
</package>
</packages>
<packages3d>
<package3d name="TACTILE_SWITCH_SMD_5.2MM" urn="urn:adsk.eagle:package:9989186/2" type="model">
<description>&lt;h3&gt;Momentary Switch (Pushbutton) - SPST - SMD, 5.2mm Square&lt;/h3&gt;
&lt;p&gt;Normally-open (NO) SPST momentary switches (buttons, pushbuttons).&lt;/p&gt;
&lt;p&gt;&lt;a href="https://www.sparkfun.com/datasheets/Components/Buttons/SMD-Button.pdf"&gt;Dimensional Drawing&lt;/a&gt;&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="TACTILE_SWITCH_SMD_5.2MM"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="SWITCH-MOMENTARY-2">
<description>&lt;h3&gt;Momentary Switch (Pushbutton) - SPST&lt;/h3&gt;
&lt;p&gt;Normally-open (NO) SPST momentary switches (buttons, pushbuttons).&lt;/p&gt;</description>
<wire x1="1.905" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="1.905" y2="1.27" width="0.254" layer="94"/>
<circle x="-2.54" y="0" radius="0.127" width="0.4064" layer="94"/>
<circle x="2.54" y="0" radius="0.127" width="0.4064" layer="94"/>
<text x="0" y="1.524" size="1.778" layer="95" font="vector" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.508" size="1.778" layer="96" font="vector" align="top-center">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="2"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MOMENTARY-SWITCH-SPST" prefix="S">
<description>&lt;h3&gt;Momentary Switch (Pushbutton) - SPST&lt;/h3&gt;
&lt;p&gt;Normally-open (NO) SPST momentary switches (buttons, pushbuttons).&lt;/p&gt;
&lt;h4&gt;Variants&lt;/h4&gt;
&lt;h5&gt;PTH-12MM - 12mm square, through-hole&lt;/h5&gt;
&lt;ul&gt;&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/9190"&gt;Momentary Pushbutton Switch - 12mm Square&lt;/a&gt; (COM-09190)&lt;/li&gt;&lt;/ul&gt;
&lt;h5&gt;PTH-6.0MM, PTH-6.0MM-KIT - 6.0mm square, through-hole&lt;/h5&gt;
&lt;ul&gt;&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/97"&gt;Mini Pushbutton Switch&lt;/a&gt; (COM-00097)&lt;/li&gt;
&lt;li&gt;KIT package intended for soldering kit's - only one side of pads' copper is exposed.&lt;/li&gt;&lt;/ul&gt;
&lt;h5&gt;PTH-RIGHT-ANGLE-KIT - Right-angle, through-hole&lt;/h5&gt;
&lt;ul&gt;&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/10791"&gt;Right Angle Tactile Button&lt;/a&gt; - Used on &lt;a href="https://www.sparkfun.com/products/11734"&gt;
SparkFun BigTime Watch Kit&lt;/a&gt;&lt;/li&gt;&lt;/ul&gt;
&lt;h5&gt;SMD-12MM - 12mm square, surface-mount&lt;/h5&gt;
&lt;ul&gt;&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/12993"&gt;Tactile Button - SMD (12mm)&lt;/a&gt; (COM-12993)&lt;/li&gt;
&lt;li&gt;Used on &lt;a href="https://www.sparkfun.com/products/11888"&gt;SparkFun PicoBoard&lt;/a&gt;&lt;/li&gt;&lt;/ul&gt;
&lt;h5&gt;SMD-4.5MM - 4.5mm Square Trackball Switch&lt;/h5&gt;
&lt;ul&gt;&lt;li&gt;Used on &lt;a href="https://www.sparkfun.com/products/13169"&gt;SparkFun Blackberry Trackballer Breakout&lt;/a&gt;&lt;/li&gt;&lt;/ul&gt;
&lt;h5&gt;SMD-4.6MMX2.8MM -  4.60mm x 2.80mm, surface mount&lt;/h5&gt;
&lt;ul&gt;&lt;li&gt;Used on &lt;a href="https://www.sparkfun.com/products/13664"&gt;SparkFun SAMD21 Mini Breakout&lt;/a&gt;&lt;/li&gt;&lt;/ul&gt;
&lt;h5&gt;SMD-5.2MM, SMD-5.2-REDUNDANT - 5.2mm square, surface-mount&lt;/h5&gt;
&lt;ul&gt;&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/8720"&gt;Mini Pushbutton Switch - SMD&lt;/a&gt; (COM-08720)&lt;/li&gt;
&lt;li&gt;Used on &lt;a href="https://www.sparkfun.com/products/11114"&gt;Arduino Pro Mini&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;REDUNDANT package connects both switch circuits together&lt;/li&gt;&lt;/ul&gt;
&lt;h5&gt;SMD-6.0X3.5MM - 6.0 x 3.5mm, surface mount&lt;/h5&gt;
&lt;ul&gt;&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/8229"&gt;Momentary Reset Switch SMD&lt;/a&gt; (COM-08229)&lt;/li&gt;&lt;/ul&gt;
&lt;h5&gt;SMD-6.2MM-TALL - 6.2mm square, surface mount&lt;/h5&gt;
&lt;ul&gt;&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/12992"&gt;Tactile Button - SMD (6mm)&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;Used on &lt;a href="https://www.sparkfun.com/products/12651"&gt;SparkFun Digital Sandbox&lt;/a&gt;&lt;/li&gt;&lt;/ul&gt;
&lt;h5&gt;SMD-RIGHT-ANGLE - Right-angle, surface mount&lt;/h5&gt;
&lt;ul&gt;&lt;li&gt;Used on &lt;a href="https://www.sparkfun.com/products/13036"&gt;SparkFun Block for Intel® Edison - Arduino&lt;/a&gt;&lt;/li&gt;&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="SWITCH-MOMENTARY-2" x="0" y="0"/>
</gates>
<devices>
<device name="-PTH-6.0MM" package="TACTILE_SWITCH_PTH_6.0MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value=" SWCH-08441"/>
<attribute name="SF_SKU" value="COM-00097"/>
</technology>
</technologies>
</device>
<device name="-SMD-4.5MM" package="TACTILE_SWITCH_SMD_4.5MM">
<connects>
<connect gate="G$1" pin="1" pad="2 3"/>
<connect gate="G$1" pin="2" pad="1 4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="SWCH-09213"/>
</technology>
</technologies>
</device>
<device name="-PTH-12MM" package="TACTILE_SWITCH_PTH_12MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="SWCH-09185"/>
<attribute name="SF_SKU" value="COM-09190"/>
</technology>
</technologies>
</device>
<device name="-SMD-6.0X3.5MM" package="TACTILE_SWITCH_SMD_6.0X3.5MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="SWCH-00815"/>
<attribute name="SF_SKU" value="COM-08229"/>
</technology>
</technologies>
</device>
<device name="-SMD-6.2MM-TALL" package="TACTILE_SWITCH_SMD_6.2MM_TALL">
<connects>
<connect gate="G$1" pin="1" pad="A2"/>
<connect gate="G$1" pin="2" pad="B2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="SWCH-11966"/>
<attribute name="SF_SKU" value="COM-12992"/>
</technology>
</technologies>
</device>
<device name="-PTH-RIGHT-ANGLE-KIT" package="TACTILE_SWITCH_PTH_RIGHT_ANGLE_KIT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-10672"/>
<attribute name="SF_SKU" value="COM-10791"/>
</technology>
</technologies>
</device>
<device name="-SMD-12MM" package="TACTILE_SWITCH_SMD_12MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="SWCH-11967"/>
<attribute name="SF_SKU" value="COM-12993"/>
</technology>
</technologies>
</device>
<device name="-PTH-6.0MM-KIT" package="TACTILE_SWITCH_PTH_6.0MM_KIT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="SWCH-08441"/>
<attribute name="SF_SKU" value="COM-00097 "/>
</technology>
</technologies>
</device>
<device name="-SMD-5.2MM" package="TACTILE_SWITCH_SMD_5.2MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:9989186/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="SWCH-08247"/>
<attribute name="SF_SKU" value="COM-08720"/>
</technology>
</technologies>
</device>
<device name="-SMD-5.2-REDUNDANT" package="TACTILE_SWITCH_SMD_5.2MM">
<connects>
<connect gate="G$1" pin="1" pad="1 2"/>
<connect gate="G$1" pin="2" pad="3 4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:9989186/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="SWCH-08247"/>
<attribute name="SF_SKU" value="COM-08720"/>
</technology>
</technologies>
</device>
<device name="-SMD-RIGHT-ANGLE" package="TACTILE_SWITCH_SMD_RIGHT_ANGLE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="COMP-12265" constant="no"/>
</technology>
</technologies>
</device>
<device name="-SMD-4.6X2.8MM" package="TACTILE_SWITCH_SMD_4.6X2.8MM">
<connects>
<connect gate="G$1" pin="1" pad="1 2"/>
<connect gate="G$1" pin="2" pad="3 4"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="SWCH-13065"/>
</technology>
</technologies>
</device>
<device name="-PTH-RIGHT-ANGLE" package="TACTILE_SWITCH_PTH_RIGHT_ANGLE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-10672" constant="no"/>
<attribute name="SF_SKU" value="COM-10791" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="FH12-10S-0.5SH(1)(98)">
<description>&lt;FFC &amp; FPC Connectors 10P SMT HORIZONTAL .5MM PITCH&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="FH1210S05SH198">
<description>&lt;b&gt;FH12-10S-0.5SH(1)(98)&lt;/b&gt;&lt;br&gt;
</description>
<smd name="1" x="-2.25" y="2.8" dx="1.3" dy="0.3" layer="1" rot="R90"/>
<smd name="2" x="-1.75" y="2.8" dx="1.3" dy="0.3" layer="1" rot="R90"/>
<smd name="3" x="-1.25" y="2.8" dx="1.3" dy="0.3" layer="1" rot="R90"/>
<smd name="4" x="-0.75" y="2.8" dx="1.3" dy="0.3" layer="1" rot="R90"/>
<smd name="5" x="-0.25" y="2.8" dx="1.3" dy="0.3" layer="1" rot="R90"/>
<smd name="6" x="0.25" y="2.8" dx="1.3" dy="0.3" layer="1" rot="R90"/>
<smd name="7" x="0.75" y="2.8" dx="1.3" dy="0.3" layer="1" rot="R90"/>
<smd name="8" x="1.25" y="2.8" dx="1.3" dy="0.3" layer="1" rot="R90"/>
<smd name="9" x="1.75" y="2.8" dx="1.3" dy="0.3" layer="1" rot="R90"/>
<smd name="10" x="2.25" y="2.8" dx="1.3" dy="0.3" layer="1" rot="R90"/>
<smd name="11" x="-4.15" y="-0.45" dx="2.2" dy="1.8" layer="1" rot="R90"/>
<smd name="12" x="4.15" y="-0.45" dx="2.2" dy="1.8" layer="1" rot="R90"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-4.05" y1="2.15" x2="4.05" y2="2.15" width="0.2" layer="51"/>
<wire x1="4.05" y1="2.15" x2="4.05" y2="-3.45" width="0.2" layer="51"/>
<wire x1="4.05" y1="-3.45" x2="-4.05" y2="-3.45" width="0.2" layer="51"/>
<wire x1="-4.05" y1="-3.45" x2="-4.05" y2="2.15" width="0.2" layer="51"/>
<wire x1="-6.05" y1="4.45" x2="6.05" y2="4.45" width="0.1" layer="51"/>
<wire x1="6.05" y1="4.45" x2="6.05" y2="-4.45" width="0.1" layer="51"/>
<wire x1="6.05" y1="-4.45" x2="-6.05" y2="-4.45" width="0.1" layer="51"/>
<wire x1="-6.05" y1="-4.45" x2="-6.05" y2="4.45" width="0.1" layer="51"/>
<wire x1="-4.05" y1="-1.85" x2="-4.05" y2="-3.45" width="0.1" layer="21"/>
<wire x1="-4.05" y1="-3.45" x2="4.05" y2="-3.45" width="0.1" layer="21"/>
<wire x1="4.05" y1="-3.45" x2="4.05" y2="-1.85" width="0.1" layer="21"/>
<wire x1="-4.05" y1="0.9" x2="-4.05" y2="2.15" width="0.1" layer="21"/>
<wire x1="-4.05" y1="2.15" x2="-3" y2="2.15" width="0.1" layer="21"/>
<wire x1="3" y1="2.15" x2="4.05" y2="2.15" width="0.1" layer="21"/>
<wire x1="4.05" y1="2.15" x2="4.05" y2="0.9" width="0.1" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="FH12-10S-0.5SH(1)(98)">
<wire x1="5.08" y1="2.54" x2="17.78" y2="2.54" width="0.254" layer="94"/>
<wire x1="17.78" y1="-15.24" x2="17.78" y2="2.54" width="0.254" layer="94"/>
<wire x1="17.78" y1="-15.24" x2="5.08" y2="-15.24" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-15.24" width="0.254" layer="94"/>
<text x="19.05" y="7.62" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="19.05" y="5.08" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="1" x="0" y="0" length="middle"/>
<pin name="3" x="0" y="-2.54" length="middle"/>
<pin name="5" x="0" y="-5.08" length="middle"/>
<pin name="7" x="0" y="-7.62" length="middle"/>
<pin name="9" x="0" y="-10.16" length="middle"/>
<pin name="11" x="0" y="-12.7" length="middle"/>
<pin name="2" x="22.86" y="0" length="middle" rot="R180"/>
<pin name="4" x="22.86" y="-2.54" length="middle" rot="R180"/>
<pin name="6" x="22.86" y="-5.08" length="middle" rot="R180"/>
<pin name="8" x="22.86" y="-7.62" length="middle" rot="R180"/>
<pin name="10" x="22.86" y="-10.16" length="middle" rot="R180"/>
<pin name="12" x="22.86" y="-12.7" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="FH12-10S-0.5SH(1)(98)" prefix="J">
<description>&lt;b&gt;FFC &amp; FPC Connectors 10P SMT HORIZONTAL .5MM PITCH&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://www.hirose.com/product/en/download_file/key_name/FH12-10S-0.5SH(1)(98)/category/Drawing (2D)/doc_file_id/144497/?file_category_id=6&amp;item_id=05280003598&amp;is_series="&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="FH12-10S-0.5SH(1)(98)" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FH1210S05SH198">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="FFC &amp; FPC Connectors 10P SMT HORIZONTAL .5MM PITCH" constant="no"/>
<attribute name="HEIGHT" value="2mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Hirose" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="FH12-10S-0.5SH(1)(98)" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="798-FH1210S05SH198" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="https://www.mouser.com/Search/Refine.aspx?Keyword=798-FH1210S05SH198" constant="no"/>
<attribute name="RS_PART_NUMBER" value="" constant="no"/>
<attribute name="RS_PRICE-STOCK" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="ESP32-PICO-D4" library="ESP32-PICO-D4" deviceset="ESP32-PICO-D4" device="" package3d_urn="urn:adsk.eagle:package:9948345/2"/>
<part name="3V3_ESP" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="VCC" device="" value="3V3_ESP"/>
<part name="GND1" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="L1" library="SparkFun-Coils" deviceset="FERRITE_BEAD" device="-0603" package3d_urn="urn:adsk.eagle:package:5829817/3" value="30Ω/1.8A"/>
<part name="C1" library="SparkFun-Capacitors" deviceset="4.7PF" device="-0603-50V-5%" package3d_urn="urn:adsk.eagle:package:23616/2" value="4.7pF"/>
<part name="C2" library="SparkFun-Capacitors" deviceset="4.7PF" device="-0603-50V-5%" package3d_urn="urn:adsk.eagle:package:23616/2" value="4.7pF"/>
<part name="GND2" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="ANT" library="PRO-OB-440" deviceset="PRO-OB-440" device="" package3d_urn="urn:adsk.eagle:package:9989957/2"/>
<part name="R1" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" package3d_urn="urn:adsk.eagle:package:23555/3" value="10k"/>
<part name="C3" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0603-25V-(+80/-20%)" package3d_urn="urn:adsk.eagle:package:23616/2" value="0.1uF"/>
<part name="GND3" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="3V3_ESP1" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="VCC" device="" value="3V3_ESP"/>
<part name="R2" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" package3d_urn="urn:adsk.eagle:package:23555/3" value="10k"/>
<part name="3V3_ESP2" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="VCC" device="" value="3V3_ESP"/>
<part name="FT231" library="FT231XS-U" deviceset="FT231XS-U" device="TSSOP20" package3d_urn="urn:adsk.eagle:package:4349/2" value="FT231XS-UTSSOP20">
<attribute name="SPICEPREFIX" value="F"/>
</part>
<part name="GND4" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="R3" library="SparkFun-Resistors" deviceset="27OHM" device="-0603-1/10W-1%" package3d_urn="urn:adsk.eagle:package:23555/3" value="27"/>
<part name="R4" library="SparkFun-Resistors" deviceset="27OHM" device="-0603-1/10W-1%" package3d_urn="urn:adsk.eagle:package:23555/3" value="27"/>
<part name="C4" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0603-25V-(+80/-20%)" package3d_urn="urn:adsk.eagle:package:23616/2" value="0.1uF"/>
<part name="C5" library="SparkFun-Capacitors" deviceset="47PF" device="-0603-50V-5%" package3d_urn="urn:adsk.eagle:package:23616/2" value="47pF"/>
<part name="C6" library="SparkFun-Capacitors" deviceset="47PF" device="-0603-50V-5%" package3d_urn="urn:adsk.eagle:package:23616/2" value="47pF"/>
<part name="GND5" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="SUPPLY1" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="V_USB" device=""/>
<part name="C7" library="SparkFun-Capacitors" deviceset="2.2UF" device="-0603-10V-20%" package3d_urn="urn:adsk.eagle:package:23616/2" value="2.2uF"/>
<part name="R5" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" package3d_urn="urn:adsk.eagle:package:23555/3" value="10k"/>
<part name="R6" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" package3d_urn="urn:adsk.eagle:package:23555/3" value="10k"/>
<part name="U3" library="MF_Discrete_Semiconductor" deviceset="NPN_BJT" device="_SOT-23-3" package3d_urn="urn:adsk.eagle:package:9990244/2" technology="MMBT3904" value="MF-DSC-SOT233-MMBT3904"/>
<part name="U4" library="MF_Discrete_Semiconductor" deviceset="NPN_BJT" device="_SOT-23-3" package3d_urn="urn:adsk.eagle:package:9990244/2" technology="MMBT3904" value="MF-DSC-SOT233-MMBT3904"/>
<part name="FLASH" library="SparkFun-Switches" deviceset="MOMENTARY-SWITCH-SPST" device="-SMD-5.2MM" package3d_urn="urn:adsk.eagle:package:9989186/2" value="MOMENTARY-SWITCH-SPST-SMD-5.2MM"/>
<part name="RESET" library="SparkFun-Switches" deviceset="MOMENTARY-SWITCH-SPST" device="-SMD-5.2MM" package3d_urn="urn:adsk.eagle:package:9989186/2" value="MOMENTARY-SWITCH-SPST-SMD-5.2MM"/>
<part name="C8" library="SparkFun-Capacitors" deviceset="10UF" device="-1206-6.3V-20%" package3d_urn="urn:adsk.eagle:package:23618/2" value="10uF"/>
<part name="GND6" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="MCP73831" library="MCP73831T-3ACI_OT" deviceset="MCP73831T-3ACI/OT" device="" package3d_urn="urn:adsk.eagle:package:9987757/2"/>
<part name="SUPPLY2" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="V_USB" device=""/>
<part name="C9" library="SparkFun-Capacitors" deviceset="2.2UF" device="-0603-10V-20%" package3d_urn="urn:adsk.eagle:package:23616/2" value="2.2uF"/>
<part name="GND7" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="GND8" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="U$1" library="myLib" deviceset="RYLED" device="" package3d_urn="urn:adsk.eagle:package:9990139/2"/>
<part name="C10" library="SparkFun-Capacitors" deviceset="10UF" device="-0603-6.3V-20%" package3d_urn="urn:adsk.eagle:package:23616/2" value="10uF"/>
<part name="BATTERY" library="SparkFun-Connectors" deviceset="JST_2MM_MALE" device="" package3d_urn="urn:adsk.eagle:package:9948904/2"/>
<part name="SUPPLY3" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="V_BATT" device=""/>
<part name="TPS79533" library="TPS79533DCQR" deviceset="TPS79533DCQR" device="" package3d_urn="urn:adsk.eagle:package:9988329/2"/>
<part name="GND9" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="GND10" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="C11" library="SparkFun-Capacitors" deviceset="1.0UF" device="-0603-16V-10%" package3d_urn="urn:adsk.eagle:package:23616/2" value="1.0uF"/>
<part name="C12" library="SparkFun-Capacitors" deviceset="1.0UF" device="-0603-16V-10%" package3d_urn="urn:adsk.eagle:package:23616/2" value="1.0uF"/>
<part name="C13" library="SparkFun-Capacitors" deviceset="10NF" device="-0603-50V-10%" package3d_urn="urn:adsk.eagle:package:23616/2" value="10nF"/>
<part name="SUPPLY4" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="V_BATT" device=""/>
<part name="3V3_ESP3" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="VCC" device="" value="3V3_ESP"/>
<part name="GND11" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="SUPPLY5" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="V_USB" device=""/>
<part name="LSM303" library="LSM303DLHC" deviceset="LSM303DLHC" device="" package3d_urn="urn:adsk.eagle:package:9987752/2"/>
<part name="BME280" library="BME280" deviceset="BME280" device="" package3d_urn="urn:adsk.eagle:package:9948092/2"/>
<part name="SD-CARD" library="MF_Connectors" deviceset="SD_CARD_SLOT" device="_MICRO_RIGHT" package3d_urn="urn:adsk.eagle:package:9948224/2" value="MF-MICROSD"/>
<part name="U$2" library="myLib" deviceset="L80GPS" device="" package3d_urn="urn:adsk.eagle:package:9990138/2"/>
<part name="R8" library="SparkFun-Resistors" deviceset="1KOHM" device="-0603-1/10W-1%" package3d_urn="urn:adsk.eagle:package:23555/3" value="1k"/>
<part name="R9" library="SparkFun-Resistors" deviceset="1KOHM" device="-0603-1/10W-1%" package3d_urn="urn:adsk.eagle:package:23555/3" value="1k"/>
<part name="R10" library="SparkFun-Resistors" deviceset="1KOHM" device="-0603-1/10W-1%" package3d_urn="urn:adsk.eagle:package:23555/3" value="1k"/>
<part name="R11" library="SparkFun-Resistors" deviceset="1KOHM" device="-0603-1/10W-1%" package3d_urn="urn:adsk.eagle:package:23555/3" value="1k"/>
<part name="GND12" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R12" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" package3d_urn="urn:adsk.eagle:package:23555/3" value="10k"/>
<part name="R13" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" package3d_urn="urn:adsk.eagle:package:23555/3" value="10k"/>
<part name="R14" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" package3d_urn="urn:adsk.eagle:package:23555/3" value="10k"/>
<part name="R16" library="SparkFun-Resistors" deviceset="330KOHM" device="-0603-1/10W-1%" package3d_urn="urn:adsk.eagle:package:23555/3" value="330k"/>
<part name="C14" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0603-25V-5%" package3d_urn="urn:adsk.eagle:package:23616/2" value="0.1uF"/>
<part name="C15" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0603-25V-5%" package3d_urn="urn:adsk.eagle:package:23616/2" value="0.1uF"/>
<part name="GND13" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="U8" library="MF_Discrete_Semiconductor" deviceset="NPN_BJT" device="_SOT-23-3" package3d_urn="urn:adsk.eagle:package:9990244/2">
<attribute name="MPN" value="MMBT2222A"/>
</part>
<part name="R17" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" package3d_urn="urn:adsk.eagle:package:23555/3" value="10k"/>
<part name="GND14" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R18" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" package3d_urn="urn:adsk.eagle:package:23555/3" value="10k"/>
<part name="U9" library="MF_Discrete_Semiconductor" deviceset="P-CHANNEL_FET" device="_SOT-23-3" package3d_urn="urn:adsk.eagle:package:9990244/2" technology="_BSS84" value="MF-DSC-SOT233-BSS84"/>
<part name="C16" library="SparkFun-Capacitors" deviceset="4.7UF" device="-0603-6.3V-(10%)" package3d_urn="urn:adsk.eagle:package:23616/2" value="4.7uF"/>
<part name="GND15" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C17" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0603-25V-5%" package3d_urn="urn:adsk.eagle:package:23616/2" value="0.1uF"/>
<part name="GND16" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C18" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0603-25V-5%" package3d_urn="urn:adsk.eagle:package:23616/2" value="0.1uF"/>
<part name="C19" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0603-25V-5%" package3d_urn="urn:adsk.eagle:package:23616/2" value="0.1uF"/>
<part name="GND17" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="3V3_ESP4" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="VCC" device="" value="3V3_ESP"/>
<part name="3V3_ESP5" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="VCC" device="" value="3V3_ESP"/>
<part name="GND26" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="3V3_ESP6" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="VCC" device="" value="3V3_ESP"/>
<part name="3V3_ESP7" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="VCC" device="" value="3V3_ESP"/>
<part name="GND18" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C20" library="SparkFun-Capacitors" deviceset="0.22UF" device="-0603-25V-10%" package3d_urn="urn:adsk.eagle:package:23616/2" value="0.22uF"/>
<part name="C21" library="SparkFun-Capacitors" deviceset="4.7UF" device="-0603-6.3V-(10%)" package3d_urn="urn:adsk.eagle:package:23616/2" value="4.7uF"/>
<part name="3V3_ESP8" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="VCC" device="" value="3V3_ESP"/>
<part name="GND19" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C22" library="SparkFun-Capacitors" deviceset="10UF" device="-0603-6.3V-20%" package3d_urn="urn:adsk.eagle:package:23616/2" value="10uF"/>
<part name="C23" library="SparkFun-Capacitors" deviceset="10UF" device="-0603-6.3V-20%" package3d_urn="urn:adsk.eagle:package:23616/2" value="10uF"/>
<part name="SUPPLY6" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="V_BATT" device=""/>
<part name="R19" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" package3d_urn="urn:adsk.eagle:package:23555/3" value="10k"/>
<part name="GND20" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="R20" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" package3d_urn="urn:adsk.eagle:package:23555/3" value="10k"/>
<part name="R21" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" package3d_urn="urn:adsk.eagle:package:23555/3" value="10k"/>
<part name="C24" library="SparkFun-Capacitors" deviceset="10UF" device="-1206-6.3V-20%" package3d_urn="urn:adsk.eagle:package:23618/2" value="10uF"/>
<part name="C25" library="SparkFun-Capacitors" deviceset="0.1UF" device="-0603-25V-5%" package3d_urn="urn:adsk.eagle:package:23616/2" value="0.1uF"/>
<part name="U10" library="MF_Discrete_Semiconductor" deviceset="NPN_BJT" device="_SOT-23-3" package3d_urn="urn:adsk.eagle:package:9990244/2">
<attribute name="MPN" value="MMBT2222A"/>
</part>
<part name="R22" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" package3d_urn="urn:adsk.eagle:package:23555/3" value="10k"/>
<part name="GND21" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R23" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" package3d_urn="urn:adsk.eagle:package:23555/3" value="10k"/>
<part name="U11" library="MF_Discrete_Semiconductor" deviceset="P-CHANNEL_FET" device="_SOT-23-3" package3d_urn="urn:adsk.eagle:package:9990244/2" technology="_BSS84" value="MF-DSC-SOT233-BSS84"/>
<part name="C26" library="SparkFun-Capacitors" deviceset="4.7UF" device="-0603-6.3V-(10%)" package3d_urn="urn:adsk.eagle:package:23616/2" value="4.7uF"/>
<part name="GND22" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="3V3_ESP9" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="VCC" device="" value="3V3_ESP"/>
<part name="R24" library="SparkFun-Resistors" deviceset="47KOHM" device="-0603-1/10W-1%" package3d_urn="urn:adsk.eagle:package:23555/3" value="47k"/>
<part name="D1" library="SparkFun-DiscreteSemi" deviceset="DIODE-SCHOTTKY" device="-BAT20J" package3d_urn="urn:adsk.eagle:package:30992/2" value="1A/23V/620mV"/>
<part name="D2" library="SparkFun-DiscreteSemi" deviceset="DIODE-SCHOTTKY" device="-BAT20J" package3d_urn="urn:adsk.eagle:package:30992/2" value="1A/23V/620mV"/>
<part name="SUPPLY7" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="V_USB" device=""/>
<part name="R26" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" package3d_urn="urn:adsk.eagle:package:23555/3" value="10k"/>
<part name="GND23" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="SUPPLY8" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="V_BATT" device=""/>
<part name="R27" library="SparkFun-Resistors" deviceset="200KOHM" device="-0603-1/10W-1%" package3d_urn="urn:adsk.eagle:package:23555/3" value="200k"/>
<part name="R28" library="SparkFun-Resistors" deviceset="200KOHM" device="-0603-1/10W-1%" package3d_urn="urn:adsk.eagle:package:23555/3" value="200k"/>
<part name="GND24" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="C27" library="SparkFun-Capacitors" deviceset="10NF" device="-0603-50V-10%" package3d_urn="urn:adsk.eagle:package:23616/2" value="10nF"/>
<part name="R29" library="SparkFun-Resistors" deviceset="22OHM" device="-0603-1/10W-1%" package3d_urn="urn:adsk.eagle:package:23555/3" value="22"/>
<part name="U12" library="MF_Discrete_Semiconductor" deviceset="NPN_BJT" device="_SOT-23-3" package3d_urn="urn:adsk.eagle:package:9990244/2">
<attribute name="MPN" value="MMBT2222A"/>
</part>
<part name="GND25" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R30" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" package3d_urn="urn:adsk.eagle:package:23555/3" value="10k"/>
<part name="PIEZO+" library="wirepad" library_urn="urn:adsk.eagle:library:412" deviceset="WIREPAD" device="SMD1,27-254" package3d_urn="urn:adsk.eagle:package:30839/1" value=""/>
<part name="PIEZO-" library="wirepad" library_urn="urn:adsk.eagle:library:412" deviceset="WIREPAD" device="SMD1,27-254" package3d_urn="urn:adsk.eagle:package:30839/1" value=""/>
<part name="SUPPLY9" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="V_BATT" device=""/>
<part name="LM2750" library="LM2750SD-5.0" deviceset="LM2750SD-5.0" device="" package3d_urn="urn:adsk.eagle:package:9987730/3"/>
<part name="GND27" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="SUPPLY10" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="V_BATT" device=""/>
<part name="C28" library="SparkFun-Capacitors" deviceset="2.2UF" device="-0603-10V-20%" package3d_urn="urn:adsk.eagle:package:23616/2" value="2.2uF"/>
<part name="C29" library="SparkFun-Capacitors" deviceset="2.2UF" device="-0603-10V-20%" package3d_urn="urn:adsk.eagle:package:23616/2" value="2.2uF"/>
<part name="C30" library="SparkFun-Capacitors" deviceset="1.0UF" device="-0603-16V-10%" package3d_urn="urn:adsk.eagle:package:23616/2" value="1.0uF"/>
<part name="GND28" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="SUPPLY11" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="5V" device=""/>
<part name="U$3" library="HikeBuddy" deviceset="ALPSSWITCH" device="" package3d_urn="urn:adsk.eagle:package:9990219/2" value="SKRHAAE010"/>
<part name="R31" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" package3d_urn="urn:adsk.eagle:package:23555/3" value="10k"/>
<part name="U$4" library="HikeBuddy" deviceset="GPSNEO7M" device=""/>
<part name="GND29" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="U$5" library="myLib" deviceset="LS027B_LCD_SHARP" device="FH12-10S-0.5SH" value="FH12-10S-0.5SH"/>
<part name="GND30" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="PCA9555D" library="PCA9555" deviceset="PCA9555" device="" package3d_urn="urn:adsk.eagle:package:18389/2" value="PCA9555"/>
<part name="GND31" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="3V3_ESP10" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="VCC" device="" value="3V3_ESP"/>
<part name="3V3_ESP11" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="VCC" device="" value="3V3_ESP"/>
<part name="R15" library="SparkFun-Resistors" deviceset="22OHM" device="-0603-1/10W-1%" package3d_urn="urn:adsk.eagle:package:23555/3" value="22"/>
<part name="U15" library="MF_Discrete_Semiconductor" deviceset="NPN_BJT" device="_SOT-23-3" package3d_urn="urn:adsk.eagle:package:9990244/2">
<attribute name="MPN" value="MMBT2222A"/>
</part>
<part name="GND32" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R32" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" package3d_urn="urn:adsk.eagle:package:23555/3" value="10k"/>
<part name="MOTOR+" library="wirepad" library_urn="urn:adsk.eagle:library:412" deviceset="WIREPAD" device="SMD1,27-254" package3d_urn="urn:adsk.eagle:package:30839/1" value=""/>
<part name="MOTOR-" library="wirepad" library_urn="urn:adsk.eagle:library:412" deviceset="WIREPAD" device="SMD1,27-254" package3d_urn="urn:adsk.eagle:package:30839/1" value=""/>
<part name="SUPPLY12" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="V_BATT" device=""/>
<part name="R33" library="SparkFun-Resistors" deviceset="1KOHM" device="-0603-1/10W-1%" package3d_urn="urn:adsk.eagle:package:23555/3" value="1k"/>
<part name="R34" library="SparkFun-Resistors" deviceset="1KOHM" device="-0603-1/10W-1%" package3d_urn="urn:adsk.eagle:package:23555/3" value="1k"/>
<part name="R35" library="SparkFun-Resistors" deviceset="1KOHM" device="-0603-1/10W-1%" package3d_urn="urn:adsk.eagle:package:23555/3" value="1k"/>
<part name="R36" library="SparkFun-Resistors" deviceset="1KOHM" device="-0603-1/10W-1%" package3d_urn="urn:adsk.eagle:package:23555/3" value="1k"/>
<part name="R39" library="SparkFun-Resistors" deviceset="1KOHM" device="-0603-1/10W-1%" package3d_urn="urn:adsk.eagle:package:23555/3" value="1k"/>
<part name="3V3_ESP12" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="VCC" device="" value="3V3_ESP"/>
<part name="GND33" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R40" library="SparkFun-Resistors" deviceset="1KOHM" device="-0603-1/10W-1%" package3d_urn="urn:adsk.eagle:package:23555/3" value="1k"/>
<part name="R41" library="SparkFun-Resistors" deviceset="1KOHM" device="-0603-1/10W-1%" package3d_urn="urn:adsk.eagle:package:23555/3" value="1k"/>
<part name="R42" library="SparkFun-Resistors" deviceset="1KOHM" device="-0603-1/10W-1%" package3d_urn="urn:adsk.eagle:package:23555/3" value="1k"/>
<part name="3V3_ESP13" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="VCC" device="" value="3V3_ESP"/>
<part name="R43" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" package3d_urn="urn:adsk.eagle:package:23555/3" value="10k"/>
<part name="R7" library="SparkFun-Resistors" deviceset="1KOHM" device="-0603-1/10W-1%" package3d_urn="urn:adsk.eagle:package:23555/3" value="1k"/>
<part name="R44" library="SparkFun-Resistors" deviceset="1KOHM" device="-0603-1/10W-1%" package3d_urn="urn:adsk.eagle:package:23555/3" value="1k"/>
<part name="R37" library="SparkFun-Resistors" deviceset="1KOHM" device="-0603-1/10W-1%" package3d_urn="urn:adsk.eagle:package:23555/3" value="1k"/>
<part name="R38" library="SparkFun-Resistors" deviceset="1KOHM" device="-0603-1/10W-1%" package3d_urn="urn:adsk.eagle:package:23555/3" value="1k"/>
<part name="R45" library="SparkFun-Resistors" deviceset="1KOHM" device="-0603-1/10W-1%" package3d_urn="urn:adsk.eagle:package:23555/3" value="1k"/>
<part name="R46" library="SparkFun-Resistors" deviceset="1KOHM" device="-0603-1/10W-1%" package3d_urn="urn:adsk.eagle:package:23555/3" value="1k"/>
<part name="R25" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" package3d_urn="urn:adsk.eagle:package:23555/3" value="10k"/>
<part name="R47" library="SparkFun-Resistors" deviceset="10KOHM" device="-0603-1/10W-1%" package3d_urn="urn:adsk.eagle:package:23555/3" value="10k"/>
<part name="J1" library="OPL_Connector" deviceset="GROVE-CONNECTOR-SMD-90D(4+2P-2.0)" device="" package3d_urn="urn:adsk.eagle:package:8004544/1" value="4P-SMD-2.0-90D"/>
<part name="J2" library="OPL_Connector" deviceset="GROVE-CONNECTOR-DIP-90D(4P-2.0)" device="" package3d_urn="urn:adsk.eagle:package:8004524/1" value="4P-2.0-90D"/>
<part name="3V3_ESP14" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="VCC" device="" value="3V3_ESP"/>
<part name="3V3_ESP15" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="VCC" device="" value="3V3_ESP"/>
<part name="GND34" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND35" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="D4" library="MF_LEDs" deviceset="LED_RGB_CA" device="_PLCC4" package3d_urn="urn:adsk.eagle:package:10009056/2" value="MF-LED-3228-RGB"/>
<part name="R48" library="SparkFun-Resistors" deviceset="1KOHM" device="-0603-1/10W-1%" package3d_urn="urn:adsk.eagle:package:23555/3" value="1k"/>
<part name="3V3_ESP16" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="VCC" device="" value="3V3_ESP"/>
<part name="J3" library="MF_Connectors" deviceset="USB" device="_MICRO_RIGHT" package3d_urn="urn:adsk.eagle:package:9948223/2" value="MF-CON-MICROUSB-RIGHT"/>
<part name="J4" library="FH12-10S-0.5SH(1)(98)" deviceset="FH12-10S-0.5SH(1)(98)" device=""/>
<part name="GND36" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND37" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="SUPPLY13" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="5V" device=""/>
<part name="SUPPLY14" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="5V" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="-5.08" y="2.54" size="1.27" layer="91">GPIO2 and GPIO12 must be low at power-up.</text>
</plain>
<instances>
<instance part="ESP32-PICO-D4" gate="G$1" x="53.34" y="40.64" smashed="yes">
<attribute name="NAME" x="38.0948" y="76.2122" size="1.778609375" layer="95" ratio="10"/>
<attribute name="VALUE" x="38.0831" y="2.4978" size="1.77996875" layer="96" ratio="10"/>
</instance>
<instance part="3V3_ESP" gate="G$1" x="73.66" y="78.74" smashed="yes">
<attribute name="VALUE" x="73.66" y="81.534" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="GND1" gate="1" x="73.66" y="5.08" smashed="yes">
<attribute name="VALUE" x="73.66" y="4.826" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="L1" gate="G$1" x="96.52" y="48.26" smashed="yes">
<attribute name="NAME" x="97.79" y="50.8" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="97.79" y="45.72" size="1.778" layer="96" font="vector" align="top-left"/>
</instance>
<instance part="C1" gate="G$1" x="106.68" y="53.34" smashed="yes" rot="R90">
<attribute name="NAME" x="103.759" y="54.864" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="108.839" y="54.864" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="C2" gate="G$1" x="114.3" y="48.26" smashed="yes" rot="R180">
<attribute name="NAME" x="112.776" y="45.339" size="1.778" layer="95" font="vector" rot="R180"/>
<attribute name="VALUE" x="112.776" y="50.419" size="1.778" layer="96" font="vector" rot="R180"/>
</instance>
<instance part="GND2" gate="1" x="106.68" y="38.1" smashed="yes">
<attribute name="VALUE" x="106.68" y="37.846" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="ANT" gate="G$1" x="124.46" y="40.64" smashed="yes" rot="R90">
<attribute name="NAME" x="116.84" y="69.85" size="1.778" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="119.38" y="69.85" size="1.778" layer="96" rot="R90" align="center-left"/>
</instance>
<instance part="R1" gate="G$1" x="17.78" y="53.34" smashed="yes" rot="R90">
<attribute name="NAME" x="16.256" y="53.34" size="1.778" layer="95" font="vector" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="19.304" y="53.34" size="1.778" layer="96" font="vector" rot="R90" align="top-center"/>
</instance>
<instance part="C3" gate="G$1" x="17.78" y="45.72" smashed="yes" rot="R180">
<attribute name="NAME" x="16.256" y="42.799" size="1.778" layer="95" font="vector" rot="R180"/>
<attribute name="VALUE" x="16.256" y="47.879" size="1.778" layer="96" font="vector" rot="R180"/>
</instance>
<instance part="GND3" gate="1" x="17.78" y="38.1" smashed="yes">
<attribute name="VALUE" x="17.78" y="37.846" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="3V3_ESP1" gate="G$1" x="17.78" y="58.42" smashed="yes">
<attribute name="VALUE" x="17.78" y="61.214" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="R2" gate="G$1" x="91.44" y="40.64" smashed="yes" rot="R90">
<attribute name="NAME" x="89.916" y="40.64" size="1.778" layer="95" font="vector" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="92.964" y="40.64" size="1.778" layer="96" font="vector" rot="R90" align="top-center"/>
</instance>
<instance part="3V3_ESP2" gate="G$1" x="91.44" y="48.26" smashed="yes">
<attribute name="VALUE" x="91.44" y="51.054" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="FT231" gate="G$1" x="60.96" y="-40.64" smashed="yes">
<attribute name="VALUE" x="45.974" y="-67.056" size="3.226640625" layer="96"/>
</instance>
<instance part="GND4" gate="1" x="88.9" y="-63.5" smashed="yes">
<attribute name="VALUE" x="88.9" y="-63.754" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="R3" gate="G$1" x="33.02" y="-30.48" smashed="yes">
<attribute name="NAME" x="33.02" y="-28.956" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="33.02" y="-32.004" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="R4" gate="G$1" x="27.94" y="-33.02" smashed="yes">
<attribute name="NAME" x="27.94" y="-31.496" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="27.94" y="-34.544" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="C4" gate="G$1" x="88.9" y="-27.94" smashed="yes">
<attribute name="NAME" x="90.424" y="-25.019" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="90.424" y="-30.099" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="C5" gate="G$1" x="20.32" y="-35.56" smashed="yes">
<attribute name="NAME" x="21.844" y="-32.639" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="21.844" y="-37.719" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="C6" gate="G$1" x="15.24" y="-38.1" smashed="yes">
<attribute name="NAME" x="16.764" y="-35.179" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="16.764" y="-40.259" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="GND5" gate="1" x="17.78" y="-43.18" smashed="yes">
<attribute name="VALUE" x="17.78" y="-43.434" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="SUPPLY1" gate="G$1" x="78.74" y="-17.78" smashed="yes">
<attribute name="VALUE" x="78.74" y="-14.986" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="C7" gate="G$1" x="81.28" y="-22.86" smashed="yes">
<attribute name="NAME" x="82.804" y="-19.939" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="82.804" y="-25.019" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="R5" gate="G$1" x="104.14" y="-38.1" smashed="yes">
<attribute name="NAME" x="104.14" y="-36.576" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="104.14" y="-39.624" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="R6" gate="G$1" x="104.14" y="-55.88" smashed="yes">
<attribute name="NAME" x="104.14" y="-54.356" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="104.14" y="-57.404" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="U3" gate="G$1" x="116.84" y="-38.1" smashed="yes">
<attribute name="NAME" x="121.92" y="-33.02" size="1.016" layer="95" font="vector" rot="R180" align="bottom-right"/>
<attribute name="VALUE" x="121.92" y="-35.56" size="1.016" layer="96" font="vector"/>
</instance>
<instance part="U4" gate="G$1" x="116.84" y="-55.88" smashed="yes" rot="MR180">
<attribute name="NAME" x="121.92" y="-60.96" size="1.016" layer="95" font="vector" rot="MR0" align="bottom-right"/>
<attribute name="VALUE" x="121.92" y="-58.42" size="1.016" layer="96" font="vector" rot="MR180"/>
</instance>
<instance part="FLASH" gate="G$1" x="20.32" y="-53.34" smashed="yes">
<attribute name="NAME" x="20.32" y="-51.816" size="1.778" layer="95" font="vector" align="bottom-center"/>
</instance>
<instance part="RESET" gate="G$1" x="20.32" y="-58.42" smashed="yes">
<attribute name="NAME" x="20.32" y="-56.896" size="1.778" layer="95" font="vector" align="bottom-center"/>
</instance>
<instance part="C8" gate="G$1" x="106.68" y="73.66" smashed="yes">
<attribute name="NAME" x="108.204" y="76.581" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="108.204" y="71.501" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="GND6" gate="1" x="106.68" y="68.58" smashed="yes">
<attribute name="VALUE" x="106.68" y="68.326" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="MCP73831" gate="A" x="198.12" y="-45.72" smashed="yes">
<attribute name="NAME" x="190.5" y="-40.132" size="1.778" layer="95"/>
<attribute name="VALUE" x="190.5" y="-53.34" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY2" gate="G$1" x="185.42" y="-40.64" smashed="yes">
<attribute name="VALUE" x="180.594" y="-40.64" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="C9" gate="G$1" x="154.94" y="-55.88" smashed="yes">
<attribute name="NAME" x="156.464" y="-52.959" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="156.464" y="-58.039" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="GND7" gate="1" x="154.94" y="-63.5" smashed="yes">
<attribute name="VALUE" x="154.94" y="-63.754" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="GND8" gate="1" x="210.82" y="-63.5" smashed="yes">
<attribute name="VALUE" x="210.82" y="-63.754" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="U$1" gate="G$1" x="175.26" y="-55.88" smashed="yes" rot="MR0"/>
<instance part="C10" gate="G$1" x="218.44" y="-50.8" smashed="yes">
<attribute name="NAME" x="219.964" y="-47.879" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="219.964" y="-52.959" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="BATTERY" gate="G$1" x="228.6" y="-45.72" smashed="yes" rot="MR90">
<attribute name="NAME" x="234.442" y="-48.26" size="1.778" layer="95" rot="MR90"/>
</instance>
<instance part="SUPPLY3" gate="G$1" x="218.44" y="-43.18" smashed="yes">
<attribute name="VALUE" x="218.44" y="-40.386" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="TPS79533" gate="G$1" x="189.992" y="-21.336" smashed="yes">
<attribute name="NAME" x="195.326" y="-30.226" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="195.072" y="-17.526" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="GND9" gate="1" x="189.992" y="-28.956" smashed="yes">
<attribute name="VALUE" x="189.992" y="-29.21" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="GND10" gate="1" x="220.472" y="-28.956" smashed="yes">
<attribute name="VALUE" x="220.472" y="-29.21" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="C11" gate="G$1" x="182.372" y="-26.416" smashed="yes">
<attribute name="NAME" x="183.896" y="-23.495" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="183.896" y="-28.575" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="C12" gate="G$1" x="230.632" y="-21.336" smashed="yes" rot="R270">
<attribute name="NAME" x="233.553" y="-22.86" size="1.778" layer="95" font="vector" rot="R270"/>
<attribute name="VALUE" x="228.473" y="-22.86" size="1.778" layer="96" font="vector" rot="R270"/>
</instance>
<instance part="C13" gate="G$1" x="223.012" y="-23.876" smashed="yes" rot="R270">
<attribute name="NAME" x="225.933" y="-25.4" size="1.778" layer="95" font="vector" rot="R270"/>
<attribute name="VALUE" x="220.853" y="-25.4" size="1.778" layer="96" font="vector" rot="R270"/>
</instance>
<instance part="SUPPLY4" gate="G$1" x="182.372" y="-21.336" smashed="yes">
<attribute name="VALUE" x="182.372" y="-18.542" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="3V3_ESP3" gate="G$1" x="225.552" y="-21.336" smashed="yes">
<attribute name="VALUE" x="225.552" y="-18.542" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="GND11" gate="1" x="169.672" y="-39.37" smashed="yes">
<attribute name="VALUE" x="169.672" y="-39.624" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="SUPPLY5" gate="G$1" x="170.18" y="-23.114" smashed="yes">
<attribute name="VALUE" x="174.244" y="-21.844" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="LSM303" gate="G$1" x="347.98" y="-48.26" smashed="yes">
<attribute name="NAME" x="347.98" y="-60.9743" size="1.78" layer="95"/>
<attribute name="VALUE" x="347.98" y="-33.0097" size="1.7792" layer="96" rot="MR180"/>
</instance>
<instance part="BME280" gate="G$1" x="274.32" y="-48.26" smashed="yes" rot="MR0">
<attribute name="NAME" x="284.4862" y="-34.9169" size="1.786590625" layer="95" rot="MR0"/>
<attribute name="VALUE" x="284.4883" y="-63.5124" size="1.782940625" layer="96" rot="MR0"/>
</instance>
<instance part="SD-CARD" gate="G$1" x="330.2" y="17.78" smashed="yes">
<attribute name="NAME" x="330.2" y="21.59" size="1.016" layer="95" font="vector" align="top-left"/>
<attribute name="VALUE" x="330.2" y="19.05" size="1.016" layer="96" font="vector"/>
</instance>
<instance part="U$2" gate="G$1" x="193.04" y="17.78" smashed="yes"/>
<instance part="R8" gate="G$1" x="190.5" y="-58.42" smashed="yes">
<attribute name="NAME" x="190.5" y="-56.896" size="1.778" layer="95" font="vector" align="bottom-center"/>
<attribute name="VALUE" x="190.5" y="-59.944" size="1.778" layer="96" font="vector" align="top-center"/>
</instance>
<instance part="R9" gate="G$1" x="165.1" y="-48.26" smashed="yes" rot="R90">
<attribute name="NAME" x="163.576" y="-48.26" size="1.778" layer="95" font="vector" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="166.624" y="-48.26" size="1.778" layer="96" font="vector" rot="R90" align="top-center"/>
</instance>
<instance part="R10" gate="G$1" x="78.74" y="63.5" smashed="yes" rot="R90">
<attribute name="NAME" x="77.216" y="63.5" size="1.778" layer="95" font="vector" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="80.264" y="63.5" size="1.778" layer="96" font="vector" rot="R90" align="top-center"/>
</instance>
<instance part="R11" gate="G$1" x="86.36" y="60.96" smashed="yes" rot="R90">
<attribute name="NAME" x="84.836" y="60.96" size="1.778" layer="95" font="vector" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="87.884" y="60.96" size="1.778" layer="96" font="vector" rot="R90" align="top-center"/>
</instance>
<instance part="GND12" gate="1" x="325.12" y="-12.7" smashed="yes">
<attribute name="VALUE" x="322.58" y="-15.24" size="1.778" layer="96"/>
</instance>
<instance part="R12" gate="G$1" x="307.34" y="22.86" smashed="yes" rot="R90">
<attribute name="NAME" x="305.816" y="22.86" size="1.778" layer="95" font="vector" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="308.864" y="22.86" size="1.778" layer="96" font="vector" rot="R90" align="top-center"/>
</instance>
<instance part="R13" gate="G$1" x="299.72" y="22.86" smashed="yes" rot="R90">
<attribute name="NAME" x="298.196" y="22.86" size="1.778" layer="95" font="vector" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="301.244" y="22.86" size="1.778" layer="96" font="vector" rot="R90" align="top-center"/>
</instance>
<instance part="R14" gate="G$1" x="292.1" y="22.86" smashed="yes" rot="R90">
<attribute name="NAME" x="290.576" y="22.86" size="1.778" layer="95" font="vector" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="293.624" y="22.86" size="1.778" layer="96" font="vector" rot="R90" align="top-center"/>
</instance>
<instance part="R16" gate="G$1" x="325.12" y="22.86" smashed="yes" rot="R90">
<attribute name="NAME" x="323.596" y="22.86" size="1.778" layer="95" font="vector" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="326.644" y="22.86" size="1.778" layer="96" font="vector" rot="R90" align="top-center"/>
</instance>
<instance part="C14" gate="G$1" x="269.24" y="22.86" smashed="yes">
<attribute name="NAME" x="270.764" y="25.781" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="270.764" y="20.701" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="C15" gate="G$1" x="259.08" y="22.86" smashed="yes">
<attribute name="NAME" x="260.604" y="25.781" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="260.604" y="20.701" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="GND13" gate="1" x="264.16" y="15.24" smashed="yes">
<attribute name="VALUE" x="261.62" y="12.7" size="1.778" layer="96"/>
</instance>
<instance part="U8" gate="G$1" x="276.86" y="45.72" smashed="yes">
<attribute name="NAME" x="281.94" y="50.8" size="1.016" layer="95" font="vector" rot="R180" align="bottom-right"/>
<attribute name="VALUE" x="281.94" y="48.26" size="1.016" layer="96" font="vector"/>
</instance>
<instance part="R17" gate="G$1" x="264.16" y="45.72" smashed="yes" rot="R180">
<attribute name="NAME" x="264.16" y="44.196" size="1.778" layer="95" font="vector" rot="R180" align="bottom-center"/>
<attribute name="VALUE" x="264.16" y="47.244" size="1.778" layer="96" font="vector" rot="R180" align="top-center"/>
</instance>
<instance part="GND14" gate="1" x="279.4" y="33.02" smashed="yes">
<attribute name="VALUE" x="276.86" y="30.48" size="1.778" layer="96"/>
</instance>
<instance part="R18" gate="G$1" x="284.48" y="60.96" smashed="yes" rot="R180">
<attribute name="NAME" x="284.48" y="59.436" size="1.778" layer="95" font="vector" rot="R180" align="bottom-center"/>
<attribute name="VALUE" x="284.48" y="62.484" size="1.778" layer="96" font="vector" rot="R180" align="top-center"/>
</instance>
<instance part="U9" gate="G$1" x="287.02" y="53.34" smashed="yes" rot="MR180">
<attribute name="NAME" x="292.1" y="48.26" size="1.016" layer="95" font="vector" rot="MR0" align="bottom-right"/>
<attribute name="VALUE" x="292.1" y="50.8" size="1.016" layer="96" font="vector" rot="MR180"/>
</instance>
<instance part="C16" gate="G$1" x="289.56" y="38.1" smashed="yes">
<attribute name="NAME" x="291.084" y="41.021" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="291.084" y="35.941" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="GND15" gate="1" x="289.56" y="33.02" smashed="yes">
<attribute name="VALUE" x="287.02" y="30.48" size="1.778" layer="96"/>
</instance>
<instance part="C17" gate="G$1" x="320.04" y="38.1" smashed="yes">
<attribute name="NAME" x="321.564" y="41.021" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="321.564" y="35.941" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="GND16" gate="1" x="327.66" y="53.34" smashed="yes">
<attribute name="VALUE" x="325.12" y="50.8" size="1.778" layer="96"/>
</instance>
<instance part="C18" gate="G$1" x="327.66" y="58.42" smashed="yes">
<attribute name="NAME" x="329.184" y="61.341" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="329.184" y="56.261" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="C19" gate="G$1" x="330.2" y="38.1" smashed="yes">
<attribute name="NAME" x="331.724" y="41.021" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="331.724" y="35.941" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="GND17" gate="1" x="330.2" y="30.48" smashed="yes">
<attribute name="VALUE" x="327.66" y="27.94" size="1.778" layer="96"/>
</instance>
<instance part="3V3_ESP4" gate="G$1" x="289.56" y="66.04" smashed="yes">
<attribute name="VALUE" x="289.56" y="68.834" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="3V3_ESP5" gate="G$1" x="327.66" y="63.5" smashed="yes">
<attribute name="VALUE" x="327.66" y="66.294" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="GND26" gate="1" x="259.08" y="-60.96" smashed="yes">
<attribute name="VALUE" x="256.54" y="-63.5" size="1.778" layer="96"/>
</instance>
<instance part="3V3_ESP6" gate="G$1" x="259.08" y="-35.56" smashed="yes">
<attribute name="VALUE" x="259.08" y="-32.766" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="3V3_ESP7" gate="G$1" x="289.56" y="-35.56" smashed="yes">
<attribute name="VALUE" x="289.56" y="-32.766" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="GND18" gate="1" x="289.56" y="-60.96" smashed="yes">
<attribute name="VALUE" x="287.02" y="-63.5" size="1.778" layer="96"/>
</instance>
<instance part="C20" gate="G$1" x="368.3" y="-40.64" smashed="yes" rot="R90">
<attribute name="NAME" x="365.379" y="-39.116" size="1.778" layer="95" font="vector" rot="R90"/>
<attribute name="VALUE" x="370.459" y="-39.116" size="1.778" layer="96" font="vector" rot="R90"/>
</instance>
<instance part="C21" gate="G$1" x="330.2" y="-55.88" smashed="yes" rot="R180">
<attribute name="NAME" x="328.676" y="-58.801" size="1.778" layer="95" font="vector" rot="R180"/>
<attribute name="VALUE" x="328.676" y="-53.721" size="1.778" layer="96" font="vector" rot="R180"/>
</instance>
<instance part="3V3_ESP8" gate="G$1" x="345.44" y="-33.02" smashed="yes">
<attribute name="VALUE" x="345.44" y="-30.226" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="GND19" gate="1" x="345.44" y="-63.5" smashed="yes">
<attribute name="VALUE" x="342.9" y="-66.04" size="1.778" layer="96"/>
</instance>
<instance part="C22" gate="G$1" x="325.12" y="-38.1" smashed="yes">
<attribute name="NAME" x="326.644" y="-35.179" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="326.644" y="-40.259" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="C23" gate="G$1" x="251.46" y="-40.64" smashed="yes">
<attribute name="NAME" x="252.984" y="-37.719" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="252.984" y="-42.799" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="SUPPLY6" gate="G$1" x="213.36" y="27.94" smashed="yes">
<attribute name="VALUE" x="213.36" y="30.734" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="R19" gate="G$1" x="170.18" y="20.32" smashed="yes" rot="R270">
<attribute name="NAME" x="171.704" y="20.32" size="1.778" layer="95" font="vector" rot="R270" align="bottom-center"/>
<attribute name="VALUE" x="168.656" y="20.32" size="1.778" layer="96" font="vector" rot="R270" align="top-center"/>
</instance>
<instance part="GND20" gate="1" x="172.72" y="7.62" smashed="yes">
<attribute name="VALUE" x="172.72" y="7.366" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="R20" gate="G$1" x="215.9" y="17.78" smashed="yes" rot="R270">
<attribute name="NAME" x="217.424" y="17.78" size="1.778" layer="95" font="vector" rot="R270" align="bottom-center"/>
<attribute name="VALUE" x="214.376" y="17.78" size="1.778" layer="96" font="vector" rot="R270" align="top-center"/>
</instance>
<instance part="R21" gate="G$1" x="223.52" y="20.32" smashed="yes" rot="R270">
<attribute name="NAME" x="225.044" y="20.32" size="1.778" layer="95" font="vector" rot="R270" align="bottom-center"/>
<attribute name="VALUE" x="221.996" y="20.32" size="1.778" layer="96" font="vector" rot="R270" align="top-center"/>
</instance>
<instance part="C24" gate="G$1" x="160.02" y="20.32" smashed="yes">
<attribute name="NAME" x="161.544" y="23.241" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="161.544" y="18.161" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="C25" gate="G$1" x="152.4" y="20.32" smashed="yes">
<attribute name="NAME" x="153.924" y="23.241" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="153.924" y="18.161" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="U10" gate="G$1" x="167.64" y="45.72" smashed="yes">
<attribute name="NAME" x="172.72" y="50.8" size="1.016" layer="95" font="vector" rot="R180" align="bottom-right"/>
<attribute name="VALUE" x="172.72" y="48.26" size="1.016" layer="96" font="vector"/>
</instance>
<instance part="R22" gate="G$1" x="154.94" y="45.72" smashed="yes" rot="R180">
<attribute name="NAME" x="154.94" y="44.196" size="1.778" layer="95" font="vector" rot="R180" align="bottom-center"/>
<attribute name="VALUE" x="154.94" y="47.244" size="1.778" layer="96" font="vector" rot="R180" align="top-center"/>
</instance>
<instance part="GND21" gate="1" x="170.18" y="33.02" smashed="yes">
<attribute name="VALUE" x="167.64" y="30.48" size="1.778" layer="96"/>
</instance>
<instance part="R23" gate="G$1" x="175.26" y="60.96" smashed="yes" rot="R180">
<attribute name="NAME" x="175.26" y="59.436" size="1.778" layer="95" font="vector" rot="R180" align="bottom-center"/>
<attribute name="VALUE" x="175.26" y="62.484" size="1.778" layer="96" font="vector" rot="R180" align="top-center"/>
</instance>
<instance part="U11" gate="G$1" x="177.8" y="53.34" smashed="yes" rot="MR180">
<attribute name="NAME" x="182.88" y="48.26" size="1.016" layer="95" font="vector" rot="MR0" align="bottom-right"/>
<attribute name="VALUE" x="182.88" y="50.8" size="1.016" layer="96" font="vector" rot="MR180"/>
</instance>
<instance part="C26" gate="G$1" x="180.34" y="38.1" smashed="yes">
<attribute name="NAME" x="181.864" y="41.021" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="181.864" y="35.941" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="GND22" gate="1" x="180.34" y="33.02" smashed="yes">
<attribute name="VALUE" x="177.8" y="30.48" size="1.778" layer="96"/>
</instance>
<instance part="3V3_ESP9" gate="G$1" x="180.34" y="66.04" smashed="yes">
<attribute name="VALUE" x="180.34" y="68.834" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="R24" gate="G$1" x="162.56" y="55.88" smashed="yes" rot="R90">
<attribute name="NAME" x="161.036" y="55.88" size="1.778" layer="95" font="vector" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="164.084" y="55.88" size="1.778" layer="96" font="vector" rot="R90" align="top-center"/>
</instance>
<instance part="D1" gate="G$1" x="144.78" y="38.1" smashed="yes" rot="R90">
<attribute name="NAME" x="142.748" y="35.56" size="1.778" layer="95" font="vector" rot="R90"/>
</instance>
<instance part="D2" gate="G$1" x="152.4" y="38.1" smashed="yes" rot="R90">
<attribute name="NAME" x="150.368" y="35.56" size="1.778" layer="95" font="vector" rot="R90"/>
</instance>
<instance part="SUPPLY7" gate="G$1" x="45.72" y="-73.66" smashed="yes">
<attribute name="VALUE" x="49.784" y="-72.39" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="R26" gate="G$1" x="45.72" y="-99.06" smashed="yes" rot="R90">
<attribute name="NAME" x="44.196" y="-99.06" size="1.778" layer="95" font="vector" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="47.244" y="-99.06" size="1.778" layer="96" font="vector" rot="R90" align="top-center"/>
</instance>
<instance part="GND23" gate="1" x="45.72" y="-106.68" smashed="yes">
<attribute name="VALUE" x="45.72" y="-106.934" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="SUPPLY8" gate="G$1" x="71.12" y="-83.82" smashed="yes">
<attribute name="VALUE" x="71.12" y="-81.026" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="R27" gate="G$1" x="71.12" y="-88.9" smashed="yes" rot="R90">
<attribute name="NAME" x="69.596" y="-88.9" size="1.778" layer="95" font="vector" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="72.644" y="-88.9" size="1.778" layer="96" font="vector" rot="R90" align="top-center"/>
</instance>
<instance part="R28" gate="G$1" x="71.12" y="-99.06" smashed="yes" rot="R90">
<attribute name="NAME" x="69.596" y="-99.06" size="1.778" layer="95" font="vector" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="72.644" y="-99.06" size="1.778" layer="96" font="vector" rot="R90" align="top-center"/>
</instance>
<instance part="GND24" gate="1" x="71.12" y="-106.68" smashed="yes">
<attribute name="VALUE" x="71.12" y="-106.934" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="C27" gate="G$1" x="77.216" y="-99.568" smashed="yes">
<attribute name="NAME" x="78.74" y="-96.647" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="78.74" y="-101.727" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="R29" gate="G$1" x="124.46" y="-83.82" smashed="yes" rot="R90">
<attribute name="NAME" x="122.936" y="-83.82" size="1.778" layer="95" font="vector" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="125.984" y="-83.82" size="1.778" layer="96" font="vector" rot="R90" align="top-center"/>
</instance>
<instance part="U12" gate="G$1" x="121.92" y="-101.6" smashed="yes">
<attribute name="NAME" x="127" y="-96.52" size="1.016" layer="95" font="vector" rot="R180" align="bottom-right"/>
<attribute name="VALUE" x="127" y="-99.06" size="1.016" layer="96" font="vector"/>
</instance>
<instance part="GND25" gate="1" x="124.46" y="-111.76" smashed="yes">
<attribute name="VALUE" x="121.92" y="-114.3" size="1.778" layer="96"/>
</instance>
<instance part="R30" gate="G$1" x="109.22" y="-111.76" smashed="yes" rot="R180">
<attribute name="NAME" x="109.22" y="-113.284" size="1.778" layer="95" font="vector" rot="R180" align="bottom-center"/>
<attribute name="VALUE" x="109.22" y="-110.236" size="1.778" layer="96" font="vector" rot="R180" align="top-center"/>
</instance>
<instance part="PIEZO+" gate="G$1" x="127" y="-88.9" smashed="yes" rot="R180">
<attribute name="NAME" x="128.143" y="-90.7542" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="128.143" y="-85.598" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="PIEZO-" gate="G$1" x="127" y="-93.98" smashed="yes" rot="R180">
<attribute name="NAME" x="128.143" y="-95.8342" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="128.143" y="-90.678" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="SUPPLY9" gate="G$1" x="124.46" y="-78.74" smashed="yes">
<attribute name="VALUE" x="124.46" y="-75.946" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="LM2750" gate="A" x="215.9" y="-111.76" smashed="yes">
<attribute name="NAME" x="211.220290625" y="-95.0249" size="2.08551875" layer="95" ratio="10" rot="SR0"/>
<attribute name="VALUE" x="209.4195" y="-134.9119" size="2.08391875" layer="96" ratio="10" rot="SR0"/>
</instance>
<instance part="GND27" gate="1" x="198.12" y="-129.54" smashed="yes">
<attribute name="VALUE" x="195.58" y="-132.08" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY10" gate="G$1" x="195.58" y="-99.314" smashed="yes">
<attribute name="VALUE" x="195.58" y="-96.52" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="C28" gate="G$1" x="180.34" y="-106.68" smashed="yes">
<attribute name="NAME" x="181.864" y="-103.759" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="181.864" y="-108.839" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="C29" gate="G$1" x="233.68" y="-114.3" smashed="yes">
<attribute name="NAME" x="235.204" y="-111.379" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="235.204" y="-116.459" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="C30" gate="G$1" x="190.5" y="-109.22" smashed="yes">
<attribute name="NAME" x="192.024" y="-106.299" size="1.778" layer="95" font="vector"/>
<attribute name="VALUE" x="192.024" y="-111.379" size="1.778" layer="96" font="vector"/>
</instance>
<instance part="GND28" gate="1" x="233.68" y="-119.38" smashed="yes">
<attribute name="VALUE" x="231.14" y="-121.92" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY11" gate="G$1" x="238.76" y="-106.68" smashed="yes">
<attribute name="VALUE" x="238.76" y="-103.886" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="U$3" gate="G$1" x="132.08" y="-182.88" smashed="yes"/>
<instance part="R31" gate="G$1" x="314.96" y="22.86" smashed="yes" rot="R90">
<attribute name="NAME" x="313.436" y="22.86" size="1.778" layer="95" font="vector" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="316.484" y="22.86" size="1.778" layer="96" font="vector" rot="R90" align="top-center"/>
</instance>
<instance part="U$4" gate="GPS" x="226.06" y="58.42" smashed="yes" rot="R90"/>
<instance part="GND29" gate="1" x="208.28" y="48.26" smashed="yes">
<attribute name="VALUE" x="208.28" y="48.006" size="1.778" layer="96" align="top-center"/>
</instance>
<instance part="U$5" gate="G$1" x="281.94" y="-96.52" smashed="yes" rot="R180"/>
<instance part="GND30" gate="1" x="259.08" y="-101.6" smashed="yes">
<attribute name="VALUE" x="256.54" y="-104.14" size="1.778" layer="96"/>
</instance>
<instance part="PCA9555D" gate="U$1" x="73.66" y="-175.26" smashed="yes">
<attribute name="NAME" x="63.4798" y="-145.992" size="1.78153125" layer="95"/>
<attribute name="VALUE" x="63.4942" y="-205.7574" size="1.779009375" layer="96"/>
</instance>
<instance part="GND31" gate="1" x="86.36" y="-203.2" smashed="yes">
<attribute name="VALUE" x="83.82" y="-205.74" size="1.778" layer="96"/>
</instance>
<instance part="3V3_ESP10" gate="G$1" x="88.9" y="-149.86" smashed="yes">
<attribute name="VALUE" x="88.9" y="-147.066" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="3V3_ESP11" gate="G$1" x="58.42" y="-167.64" smashed="yes" rot="R180">
<attribute name="VALUE" x="58.42" y="-170.434" size="1.778" layer="96" rot="R180" align="bottom-center"/>
</instance>
<instance part="R15" gate="G$1" x="162.56" y="-83.82" smashed="yes" rot="R90">
<attribute name="NAME" x="161.036" y="-83.82" size="1.778" layer="95" font="vector" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="164.084" y="-83.82" size="1.778" layer="96" font="vector" rot="R90" align="top-center"/>
</instance>
<instance part="U15" gate="G$1" x="160.02" y="-101.6" smashed="yes">
<attribute name="NAME" x="165.1" y="-96.52" size="1.016" layer="95" font="vector" rot="R180" align="bottom-right"/>
<attribute name="VALUE" x="165.1" y="-99.06" size="1.016" layer="96" font="vector"/>
</instance>
<instance part="GND32" gate="1" x="162.56" y="-111.76" smashed="yes">
<attribute name="VALUE" x="160.02" y="-114.3" size="1.778" layer="96"/>
</instance>
<instance part="R32" gate="G$1" x="147.32" y="-111.76" smashed="yes" rot="R180">
<attribute name="NAME" x="147.32" y="-113.284" size="1.778" layer="95" font="vector" rot="R180" align="bottom-center"/>
<attribute name="VALUE" x="147.32" y="-110.236" size="1.778" layer="96" font="vector" rot="R180" align="top-center"/>
</instance>
<instance part="MOTOR+" gate="G$1" x="165.1" y="-88.9" smashed="yes" rot="R180">
<attribute name="NAME" x="166.243" y="-90.7542" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="166.243" y="-85.598" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="MOTOR-" gate="G$1" x="165.1" y="-93.98" smashed="yes" rot="R180">
<attribute name="NAME" x="166.243" y="-95.8342" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="166.243" y="-90.678" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="SUPPLY12" gate="G$1" x="162.56" y="-78.74" smashed="yes">
<attribute name="VALUE" x="162.56" y="-75.946" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="R33" gate="G$1" x="124.46" y="-167.64" smashed="yes" rot="R90">
<attribute name="NAME" x="122.936" y="-167.64" size="1.778" layer="95" font="vector" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="125.984" y="-167.64" size="1.778" layer="96" font="vector" rot="R90" align="top-center"/>
</instance>
<instance part="R34" gate="G$1" x="116.84" y="-170.18" smashed="yes" rot="R90">
<attribute name="NAME" x="115.316" y="-170.18" size="1.778" layer="95" font="vector" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="118.364" y="-170.18" size="1.778" layer="96" font="vector" rot="R90" align="top-center"/>
</instance>
<instance part="R35" gate="G$1" x="109.22" y="-172.72" smashed="yes" rot="R90">
<attribute name="NAME" x="107.696" y="-172.72" size="1.778" layer="95" font="vector" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="110.744" y="-172.72" size="1.778" layer="96" font="vector" rot="R90" align="top-center"/>
</instance>
<instance part="R36" gate="G$1" x="167.64" y="-167.64" smashed="yes" rot="R90">
<attribute name="NAME" x="166.116" y="-167.64" size="1.778" layer="95" font="vector" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="169.164" y="-167.64" size="1.778" layer="96" font="vector" rot="R90" align="top-center"/>
</instance>
<instance part="R39" gate="G$1" x="175.26" y="-172.72" smashed="yes" rot="R90">
<attribute name="NAME" x="173.736" y="-172.72" size="1.778" layer="95" font="vector" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="176.784" y="-172.72" size="1.778" layer="96" font="vector" rot="R90" align="top-center"/>
</instance>
<instance part="3V3_ESP12" gate="G$1" x="144.78" y="-162.56" smashed="yes">
<attribute name="VALUE" x="144.78" y="-159.766" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="GND33" gate="1" x="167.64" y="-182.88" smashed="yes">
<attribute name="VALUE" x="165.1" y="-185.42" size="1.778" layer="96"/>
</instance>
<instance part="R40" gate="G$1" x="160.02" y="-193.04" smashed="yes" rot="R180">
<attribute name="NAME" x="160.02" y="-194.564" size="1.778" layer="95" font="vector" rot="R180" align="bottom-center"/>
<attribute name="VALUE" x="160.02" y="-191.516" size="1.778" layer="96" font="vector" rot="R180" align="top-center"/>
</instance>
<instance part="R41" gate="G$1" x="160.02" y="-198.12" smashed="yes" rot="R180">
<attribute name="NAME" x="160.02" y="-199.644" size="1.778" layer="95" font="vector" rot="R180" align="bottom-center"/>
<attribute name="VALUE" x="160.02" y="-196.596" size="1.778" layer="96" font="vector" rot="R180" align="top-center"/>
</instance>
<instance part="R42" gate="G$1" x="160.02" y="-195.58" smashed="yes" rot="R180">
<attribute name="NAME" x="160.02" y="-197.104" size="1.778" layer="95" font="vector" rot="R180" align="bottom-center"/>
<attribute name="VALUE" x="160.02" y="-194.056" size="1.778" layer="96" font="vector" rot="R180" align="top-center"/>
</instance>
<instance part="3V3_ESP13" gate="G$1" x="137.16" y="-195.58" smashed="yes">
<attribute name="VALUE" x="137.16" y="-192.786" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="R43" gate="G$1" x="284.48" y="22.86" smashed="yes" rot="R90">
<attribute name="NAME" x="282.956" y="22.86" size="1.778" layer="95" font="vector" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="286.004" y="22.86" size="1.778" layer="96" font="vector" rot="R90" align="top-center"/>
</instance>
<instance part="R7" gate="G$1" x="213.36" y="-50.8" smashed="yes" rot="R90">
<attribute name="NAME" x="211.836" y="-50.8" size="1.778" layer="95" font="vector" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="214.884" y="-50.8" size="1.778" layer="96" font="vector" rot="R90" align="top-center"/>
</instance>
<instance part="R44" gate="G$1" x="218.44" y="-58.42" smashed="yes" rot="R180">
<attribute name="NAME" x="218.44" y="-59.944" size="1.778" layer="95" font="vector" rot="R180" align="bottom-center"/>
<attribute name="VALUE" x="218.44" y="-56.896" size="1.778" layer="96" font="vector" rot="R180" align="top-center"/>
</instance>
<instance part="R37" gate="G$1" x="294.64" y="-38.1" smashed="yes" rot="R180">
<attribute name="NAME" x="294.64" y="-39.624" size="1.778" layer="95" font="vector" rot="R180" align="bottom-center"/>
<attribute name="VALUE" x="294.64" y="-36.576" size="1.778" layer="96" font="vector" rot="R180" align="top-center"/>
</instance>
<instance part="R38" gate="G$1" x="294.64" y="-45.72" smashed="yes" rot="R180">
<attribute name="NAME" x="294.64" y="-47.244" size="1.778" layer="95" font="vector" rot="R180" align="bottom-center"/>
<attribute name="VALUE" x="294.64" y="-44.196" size="1.778" layer="96" font="vector" rot="R180" align="top-center"/>
</instance>
<instance part="R45" gate="G$1" x="299.72" y="-50.8" smashed="yes" rot="R270">
<attribute name="NAME" x="301.244" y="-50.8" size="1.778" layer="95" font="vector" rot="R270" align="bottom-center"/>
<attribute name="VALUE" x="298.196" y="-50.8" size="1.778" layer="96" font="vector" rot="R270" align="top-center"/>
</instance>
<instance part="R46" gate="G$1" x="302.26" y="-43.18" smashed="yes" rot="R270">
<attribute name="NAME" x="303.784" y="-43.18" size="1.778" layer="95" font="vector" rot="R270" align="bottom-center"/>
<attribute name="VALUE" x="300.736" y="-43.18" size="1.778" layer="96" font="vector" rot="R270" align="top-center"/>
</instance>
<instance part="R25" gate="G$1" x="45.72" y="-88.9" smashed="yes" rot="R90">
<attribute name="NAME" x="44.196" y="-88.9" size="1.778" layer="95" font="vector" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="47.244" y="-88.9" size="1.778" layer="96" font="vector" rot="R90" align="top-center"/>
</instance>
<instance part="R47" gate="G$1" x="45.72" y="-78.74" smashed="yes" rot="R90">
<attribute name="NAME" x="44.196" y="-78.74" size="1.778" layer="95" font="vector" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="47.244" y="-78.74" size="1.778" layer="96" font="vector" rot="R90" align="top-center"/>
</instance>
<instance part="J1" gate="G$1" x="335.28" y="-102.87" smashed="yes">
<attribute name="NAME" x="327.66" y="-96.52" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="336.55" y="-96.52" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="J2" gate="G$1" x="311.15" y="-102.87" smashed="yes">
<attribute name="NAME" x="304.8" y="-96.52" size="1.27" layer="95" ratio="10"/>
<attribute name="VALUE" x="311.15" y="-96.52" size="1.27" layer="96" ratio="10"/>
</instance>
<instance part="3V3_ESP14" gate="G$1" x="299.72" y="-99.06" smashed="yes">
<attribute name="VALUE" x="299.72" y="-96.266" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="3V3_ESP15" gate="G$1" x="322.58" y="-99.06" smashed="yes">
<attribute name="VALUE" x="322.58" y="-96.266" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="GND34" gate="1" x="302.26" y="-111.76" smashed="yes">
<attribute name="VALUE" x="299.72" y="-114.3" size="1.778" layer="96"/>
</instance>
<instance part="GND35" gate="1" x="325.12" y="-111.76" smashed="yes">
<attribute name="VALUE" x="322.58" y="-114.3" size="1.778" layer="96"/>
</instance>
<instance part="D4" gate="G$1" x="144.78" y="-195.58" smashed="yes">
<attribute name="NAME" x="139.7" y="-205.74" size="1.016" layer="95" font="vector" align="top-left"/>
<attribute name="VALUE" x="139.7" y="-208.28" size="1.016" layer="96" font="vector"/>
</instance>
<instance part="R48" gate="G$1" x="60.96" y="-144.78" smashed="yes" rot="R90">
<attribute name="NAME" x="59.436" y="-144.78" size="1.778" layer="95" font="vector" rot="R90" align="bottom-center"/>
<attribute name="VALUE" x="62.484" y="-144.78" size="1.778" layer="96" font="vector" rot="R90" align="top-center"/>
</instance>
<instance part="3V3_ESP16" gate="G$1" x="60.96" y="-139.7" smashed="yes">
<attribute name="VALUE" x="60.96" y="-136.906" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="J3" gate="G$1" x="160.02" y="-28.194" smashed="yes">
<attribute name="NAME" x="154.94" y="-16.764" size="1.016" layer="95" font="vector" align="top-left"/>
<attribute name="VALUE" x="154.94" y="-19.304" size="1.016" layer="96" font="vector"/>
</instance>
<instance part="J4" gate="G$1" x="261.62" y="-142.24" smashed="yes">
<attribute name="NAME" x="280.67" y="-134.62" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="280.67" y="-137.16" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="GND36" gate="1" x="259.08" y="-157.48" smashed="yes">
<attribute name="VALUE" x="256.54" y="-160.02" size="1.778" layer="96"/>
</instance>
<instance part="GND37" gate="1" x="287.02" y="-157.48" smashed="yes">
<attribute name="VALUE" x="284.48" y="-160.02" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY13" gate="G$1" x="302.26" y="-147.32" smashed="yes">
<attribute name="VALUE" x="302.26" y="-144.526" size="1.778" layer="96" align="bottom-center"/>
</instance>
<instance part="SUPPLY14" gate="G$1" x="241.3" y="-147.32" smashed="yes">
<attribute name="VALUE" x="241.3" y="-144.526" size="1.778" layer="96" align="bottom-center"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="VCC" class="0">
<segment>
<pinref part="ESP32-PICO-D4" gate="G$1" pin="VDD3P3_CPU"/>
<pinref part="ESP32-PICO-D4" gate="G$1" pin="VDD_SDIO"/>
<wire x1="73.66" y1="63.5" x2="73.66" y2="66.04" width="0.1524" layer="91"/>
<pinref part="ESP32-PICO-D4" gate="G$1" pin="VDD3P3_RTC"/>
<wire x1="73.66" y1="66.04" x2="73.66" y2="68.58" width="0.1524" layer="91"/>
<junction x="73.66" y="66.04"/>
<pinref part="ESP32-PICO-D4" gate="G$1" pin="VDDA3P3"/>
<wire x1="73.66" y1="68.58" x2="73.66" y2="71.12" width="0.1524" layer="91"/>
<junction x="73.66" y="68.58"/>
<pinref part="ESP32-PICO-D4" gate="G$1" pin="VDDA"/>
<wire x1="73.66" y1="71.12" x2="73.66" y2="73.66" width="0.1524" layer="91"/>
<junction x="73.66" y="71.12"/>
<pinref part="3V3_ESP" gate="G$1" pin="VCC"/>
<wire x1="73.66" y1="78.74" x2="73.66" y2="73.66" width="0.1524" layer="91"/>
<junction x="73.66" y="73.66"/>
<pinref part="C8" gate="G$1" pin="1"/>
<wire x1="73.66" y1="78.74" x2="78.74" y2="78.74" width="0.1524" layer="91"/>
<junction x="73.66" y="78.74"/>
<pinref part="R10" gate="G$1" pin="2"/>
<wire x1="78.74" y1="78.74" x2="86.36" y2="78.74" width="0.1524" layer="91"/>
<wire x1="86.36" y1="78.74" x2="106.68" y2="78.74" width="0.1524" layer="91"/>
<wire x1="78.74" y1="68.58" x2="78.74" y2="78.74" width="0.1524" layer="91"/>
<junction x="78.74" y="78.74"/>
<pinref part="R11" gate="G$1" pin="2"/>
<wire x1="86.36" y1="66.04" x2="86.36" y2="78.74" width="0.1524" layer="91"/>
<junction x="86.36" y="78.74"/>
</segment>
<segment>
<pinref part="R1" gate="G$1" pin="2"/>
<pinref part="3V3_ESP1" gate="G$1" pin="VCC"/>
</segment>
<segment>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="91.44" y1="45.72" x2="91.44" y2="48.26" width="0.1524" layer="91"/>
<pinref part="3V3_ESP2" gate="G$1" pin="VCC"/>
</segment>
<segment>
<pinref part="TPS79533" gate="G$1" pin="OUT"/>
<pinref part="C12" gate="G$1" pin="2"/>
<wire x1="220.472" y1="-21.336" x2="225.552" y2="-21.336" width="0.1524" layer="91"/>
<pinref part="3V3_ESP3" gate="G$1" pin="VCC"/>
<wire x1="225.552" y1="-21.336" x2="228.092" y2="-21.336" width="0.1524" layer="91"/>
<junction x="225.552" y="-21.336"/>
</segment>
<segment>
<pinref part="R18" gate="G$1" pin="1"/>
<wire x1="289.56" y1="60.96" x2="289.56" y2="66.04" width="0.1524" layer="91"/>
<pinref part="U9" gate="G$1" pin="SOURCE"/>
<junction x="289.56" y="60.96"/>
<pinref part="3V3_ESP4" gate="G$1" pin="VCC"/>
</segment>
<segment>
<pinref part="C18" gate="G$1" pin="1"/>
<pinref part="3V3_ESP5" gate="G$1" pin="VCC"/>
</segment>
<segment>
<pinref part="BME280" gate="G$1" pin="VDD"/>
<pinref part="BME280" gate="G$1" pin="VDDIO"/>
<wire x1="259.08" y1="-40.64" x2="259.08" y2="-38.1" width="0.1524" layer="91"/>
<pinref part="3V3_ESP6" gate="G$1" pin="VCC"/>
<wire x1="259.08" y1="-38.1" x2="259.08" y2="-35.56" width="0.1524" layer="91"/>
<junction x="259.08" y="-38.1"/>
<pinref part="C23" gate="G$1" pin="1"/>
<wire x1="259.08" y1="-35.56" x2="251.46" y2="-35.56" width="0.1524" layer="91"/>
<junction x="259.08" y="-35.56"/>
</segment>
<segment>
<pinref part="3V3_ESP7" gate="G$1" pin="VCC"/>
<pinref part="BME280" gate="G$1" pin="CSB"/>
<wire x1="289.56" y1="-35.56" x2="289.56" y2="-38.1" width="0.1524" layer="91"/>
<pinref part="R37" gate="G$1" pin="2"/>
<wire x1="289.56" y1="-38.1" x2="289.56" y2="-45.72" width="0.1524" layer="91"/>
<junction x="289.56" y="-38.1"/>
<pinref part="R38" gate="G$1" pin="2"/>
<junction x="289.56" y="-45.72"/>
</segment>
<segment>
<pinref part="LSM303" gate="G$1" pin="VDDIO"/>
<wire x1="332.74" y1="-40.64" x2="332.74" y2="-33.02" width="0.1524" layer="91"/>
<pinref part="LSM303" gate="G$1" pin="VDD"/>
<wire x1="332.74" y1="-33.02" x2="345.44" y2="-33.02" width="0.1524" layer="91"/>
<pinref part="3V3_ESP8" gate="G$1" pin="VCC"/>
<junction x="345.44" y="-33.02"/>
<pinref part="C22" gate="G$1" pin="1"/>
<wire x1="332.74" y1="-33.02" x2="325.12" y2="-33.02" width="0.1524" layer="91"/>
<junction x="332.74" y="-33.02"/>
</segment>
<segment>
<pinref part="R23" gate="G$1" pin="1"/>
<wire x1="180.34" y1="60.96" x2="180.34" y2="66.04" width="0.1524" layer="91"/>
<pinref part="U11" gate="G$1" pin="SOURCE"/>
<junction x="180.34" y="60.96"/>
<pinref part="3V3_ESP9" gate="G$1" pin="VCC"/>
<pinref part="R24" gate="G$1" pin="2"/>
<wire x1="162.56" y1="60.96" x2="162.56" y2="66.04" width="0.1524" layer="91"/>
<wire x1="162.56" y1="66.04" x2="180.34" y2="66.04" width="0.1524" layer="91"/>
<junction x="180.34" y="66.04"/>
</segment>
<segment>
<pinref part="PCA9555D" gate="U$1" pin="VDD"/>
<wire x1="86.36" y1="-149.86" x2="88.9" y2="-149.86" width="0.1524" layer="91"/>
<pinref part="3V3_ESP10" gate="G$1" pin="VCC"/>
</segment>
<segment>
<pinref part="PCA9555D" gate="U$1" pin="A2"/>
<wire x1="60.96" y1="-167.64" x2="58.42" y2="-167.64" width="0.1524" layer="91"/>
<pinref part="PCA9555D" gate="U$1" pin="A1"/>
<wire x1="60.96" y1="-165.1" x2="58.42" y2="-165.1" width="0.1524" layer="91"/>
<wire x1="58.42" y1="-167.64" x2="58.42" y2="-165.1" width="0.1524" layer="91"/>
<pinref part="PCA9555D" gate="U$1" pin="A0"/>
<wire x1="60.96" y1="-162.56" x2="58.42" y2="-162.56" width="0.1524" layer="91"/>
<wire x1="58.42" y1="-165.1" x2="58.42" y2="-162.56" width="0.1524" layer="91"/>
<junction x="58.42" y="-165.1"/>
<pinref part="3V3_ESP11" gate="G$1" pin="VCC"/>
<junction x="58.42" y="-167.64"/>
</segment>
<segment>
<pinref part="R35" gate="G$1" pin="2"/>
<pinref part="R33" gate="G$1" pin="2"/>
<wire x1="109.22" y1="-167.64" x2="109.22" y2="-162.56" width="0.1524" layer="91"/>
<wire x1="109.22" y1="-162.56" x2="116.84" y2="-162.56" width="0.1524" layer="91"/>
<pinref part="R34" gate="G$1" pin="2"/>
<wire x1="116.84" y1="-162.56" x2="124.46" y2="-162.56" width="0.1524" layer="91"/>
<wire x1="116.84" y1="-165.1" x2="116.84" y2="-162.56" width="0.1524" layer="91"/>
<junction x="116.84" y="-162.56"/>
<pinref part="R36" gate="G$1" pin="2"/>
<wire x1="124.46" y1="-162.56" x2="144.78" y2="-162.56" width="0.1524" layer="91"/>
<junction x="124.46" y="-162.56"/>
<pinref part="R39" gate="G$1" pin="2"/>
<wire x1="144.78" y1="-162.56" x2="167.64" y2="-162.56" width="0.1524" layer="91"/>
<wire x1="167.64" y1="-162.56" x2="175.26" y2="-162.56" width="0.1524" layer="91"/>
<wire x1="175.26" y1="-162.56" x2="175.26" y2="-167.64" width="0.1524" layer="91"/>
<junction x="167.64" y="-162.56"/>
<pinref part="3V3_ESP12" gate="G$1" pin="VCC"/>
<junction x="144.78" y="-162.56"/>
</segment>
<segment>
<pinref part="3V3_ESP15" gate="G$1" pin="VCC"/>
<pinref part="J1" gate="G$1" pin="2"/>
<wire x1="322.58" y1="-99.06" x2="322.58" y2="-101.6" width="0.1524" layer="91"/>
<wire x1="322.58" y1="-101.6" x2="330.2" y2="-101.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="3V3_ESP14" gate="G$1" pin="VCC"/>
<pinref part="J2" gate="G$1" pin="2"/>
<wire x1="299.72" y1="-99.06" x2="299.72" y2="-101.6" width="0.1524" layer="91"/>
<wire x1="299.72" y1="-101.6" x2="307.34" y2="-101.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D4" gate="G$1" pin="ANODE"/>
<pinref part="3V3_ESP13" gate="G$1" pin="VCC"/>
</segment>
<segment>
<pinref part="R48" gate="G$1" pin="2"/>
<pinref part="3V3_ESP16" gate="G$1" pin="VCC"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="ESP32-PICO-D4" gate="G$1" pin="GND"/>
<pinref part="GND1" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="L1" gate="G$1" pin="2"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="96.52" y1="43.18" x2="106.68" y2="43.18" width="0.1524" layer="91"/>
<wire x1="106.68" y1="43.18" x2="114.3" y2="43.18" width="0.1524" layer="91"/>
<wire x1="106.68" y1="43.18" x2="106.68" y2="40.64" width="0.1524" layer="91"/>
<junction x="106.68" y="43.18"/>
<pinref part="GND2" gate="1" pin="GND"/>
<pinref part="ANT" gate="G$1" pin="GND"/>
<wire x1="106.68" y1="40.64" x2="124.46" y2="40.64" width="0.1524" layer="91"/>
<junction x="106.68" y="40.64"/>
</segment>
<segment>
<pinref part="C3" gate="G$1" pin="1"/>
<pinref part="GND3" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="FT231" gate="G$1" pin="GND"/>
<pinref part="GND4" gate="1" pin="GND"/>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="88.9" y1="-30.48" x2="88.9" y2="-60.96" width="0.1524" layer="91"/>
<junction x="88.9" y="-60.96"/>
<pinref part="C7" gate="G$1" pin="2"/>
<wire x1="81.28" y1="-25.4" x2="81.28" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="81.28" y1="-30.48" x2="88.9" y2="-30.48" width="0.1524" layer="91"/>
<junction x="88.9" y="-30.48"/>
<wire x1="88.9" y1="-60.96" x2="78.74" y2="-60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C6" gate="G$1" pin="2"/>
<pinref part="C5" gate="G$1" pin="2"/>
<wire x1="15.24" y1="-40.64" x2="17.78" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="17.78" y1="-40.64" x2="20.32" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="20.32" y1="-40.64" x2="20.32" y2="-38.1" width="0.1524" layer="91"/>
<pinref part="GND5" gate="1" pin="GND"/>
<junction x="17.78" y="-40.64"/>
<pinref part="RESET" gate="G$1" pin="1"/>
<pinref part="FLASH" gate="G$1" pin="1"/>
<wire x1="15.24" y1="-58.42" x2="15.24" y2="-53.34" width="0.1524" layer="91"/>
<wire x1="15.24" y1="-53.34" x2="15.24" y2="-40.64" width="0.1524" layer="91"/>
<junction x="15.24" y="-53.34"/>
<junction x="15.24" y="-40.64"/>
</segment>
<segment>
<pinref part="C8" gate="G$1" pin="2"/>
<pinref part="GND6" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C9" gate="G$1" pin="2"/>
<wire x1="154.94" y1="-58.42" x2="154.94" y2="-60.96" width="0.1524" layer="91"/>
<pinref part="GND7" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="MCP73831" gate="A" pin="VSS"/>
<wire x1="210.82" y1="-48.26" x2="210.82" y2="-60.96" width="0.1524" layer="91"/>
<pinref part="GND8" gate="1" pin="GND"/>
<junction x="210.82" y="-60.96"/>
<wire x1="195.58" y1="-58.42" x2="195.58" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="195.58" y1="-60.96" x2="210.82" y2="-60.96" width="0.1524" layer="91"/>
<pinref part="C10" gate="G$1" pin="2"/>
<wire x1="218.44" y1="-53.34" x2="218.44" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="218.44" y1="-60.96" x2="210.82" y2="-60.96" width="0.1524" layer="91"/>
<pinref part="BATTERY" gate="G$1" pin="-"/>
<wire x1="223.52" y1="-45.72" x2="223.52" y2="-58.42" width="0.1524" layer="91"/>
<junction x="218.44" y="-60.96"/>
<pinref part="R8" gate="G$1" pin="2"/>
<pinref part="R44" gate="G$1" pin="1"/>
<wire x1="223.52" y1="-58.42" x2="223.52" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="223.52" y1="-60.96" x2="218.44" y2="-60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="TPS79533" gate="G$1" pin="GND_1"/>
<pinref part="GND9" gate="1" pin="GND"/>
<pinref part="C11" gate="G$1" pin="2"/>
<wire x1="182.372" y1="-28.956" x2="187.452" y2="-28.956" width="0.1524" layer="91"/>
<wire x1="187.452" y1="-28.956" x2="187.452" y2="-26.416" width="0.1524" layer="91"/>
<wire x1="187.452" y1="-26.416" x2="189.992" y2="-26.416" width="0.1524" layer="91"/>
<junction x="189.992" y="-26.416"/>
</segment>
<segment>
<pinref part="TPS79533" gate="G$1" pin="GND_2"/>
<pinref part="GND10" gate="1" pin="GND"/>
<pinref part="C13" gate="G$1" pin="1"/>
<pinref part="C12" gate="G$1" pin="1"/>
<wire x1="228.092" y1="-23.876" x2="235.712" y2="-23.876" width="0.1524" layer="91"/>
<wire x1="235.712" y1="-23.876" x2="235.712" y2="-21.336" width="0.1524" layer="91"/>
<wire x1="220.472" y1="-26.416" x2="235.712" y2="-26.416" width="0.1524" layer="91"/>
<wire x1="235.712" y1="-26.416" x2="235.712" y2="-23.876" width="0.1524" layer="91"/>
<junction x="220.472" y="-26.416"/>
<junction x="235.712" y="-23.876"/>
</segment>
<segment>
<pinref part="GND11" gate="1" pin="GND"/>
<wire x1="167.64" y1="-35.814" x2="169.672" y2="-35.814" width="0.1524" layer="91"/>
<wire x1="169.672" y1="-35.814" x2="169.672" y2="-36.83" width="0.1524" layer="91"/>
<wire x1="167.64" y1="-35.814" x2="167.64" y2="-34.6202" width="0.1524" layer="91"/>
<pinref part="J3" gate="G$1" pin="SLD"/>
<junction x="167.64" y="-35.814"/>
<pinref part="J3" gate="G$1" pin="GND"/>
<wire x1="167.64" y1="-35.814" x2="167.64" y2="-33.274" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="327.66" y1="-7.62" x2="325.12" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="325.12" y1="-7.62" x2="325.12" y2="-10.16" width="0.1524" layer="91"/>
<pinref part="GND12" gate="1" pin="GND"/>
<pinref part="SD-CARD" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="C15" gate="G$1" pin="2"/>
<pinref part="C14" gate="G$1" pin="2"/>
<wire x1="259.08" y1="20.32" x2="264.16" y2="20.32" width="0.1524" layer="91"/>
<wire x1="264.16" y1="20.32" x2="269.24" y2="20.32" width="0.1524" layer="91"/>
<wire x1="264.16" y1="20.32" x2="264.16" y2="17.78" width="0.1524" layer="91"/>
<junction x="264.16" y="20.32"/>
<pinref part="GND13" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U8" gate="G$1" pin="EMITTER"/>
<wire x1="279.4" y1="38.1" x2="279.4" y2="35.56" width="0.1524" layer="91"/>
<pinref part="GND14" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C16" gate="G$1" pin="2"/>
<pinref part="GND15" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND16" gate="1" pin="GND"/>
<pinref part="C18" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="BME280" gate="G$1" pin="GND"/>
<pinref part="GND26" gate="1" pin="GND"/>
<pinref part="C23" gate="G$1" pin="2"/>
<wire x1="251.46" y1="-43.18" x2="251.46" y2="-58.42" width="0.1524" layer="91"/>
<wire x1="251.46" y1="-58.42" x2="259.08" y2="-58.42" width="0.1524" layer="91"/>
<junction x="259.08" y="-58.42"/>
</segment>
<segment>
<pinref part="BME280" gate="G$1" pin="SDO"/>
<pinref part="GND18" gate="1" pin="GND"/>
<wire x1="289.56" y1="-53.34" x2="289.56" y2="-58.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C21" gate="G$1" pin="1"/>
<pinref part="LSM303" gate="G$1" pin="GND"/>
<wire x1="330.2" y1="-60.96" x2="345.44" y2="-60.96" width="0.1524" layer="91"/>
<pinref part="GND19" gate="1" pin="GND"/>
<junction x="345.44" y="-60.96"/>
<pinref part="LSM303" gate="G$1" pin="RES@3"/>
<pinref part="LSM303" gate="G$1" pin="RES@2"/>
<wire x1="358.14" y1="-45.72" x2="358.14" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="358.14" y1="-48.26" x2="363.22" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="363.22" y1="-48.26" x2="363.22" y2="-60.96" width="0.1524" layer="91"/>
<junction x="358.14" y="-48.26"/>
<wire x1="363.22" y1="-60.96" x2="345.44" y2="-60.96" width="0.1524" layer="91"/>
<pinref part="C22" gate="G$1" pin="2"/>
<wire x1="325.12" y1="-40.64" x2="325.12" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="325.12" y1="-60.96" x2="330.2" y2="-60.96" width="0.1524" layer="91"/>
<junction x="330.2" y="-60.96"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="GND"/>
<wire x1="177.8" y1="17.78" x2="172.72" y2="17.78" width="0.1524" layer="91"/>
<wire x1="172.72" y1="17.78" x2="172.72" y2="10.16" width="0.1524" layer="91"/>
<pinref part="GND20" gate="1" pin="GND"/>
<pinref part="C25" gate="G$1" pin="2"/>
<pinref part="C24" gate="G$1" pin="2"/>
<wire x1="152.4" y1="17.78" x2="160.02" y2="17.78" width="0.1524" layer="91"/>
<wire x1="160.02" y1="17.78" x2="160.02" y2="10.16" width="0.1524" layer="91"/>
<wire x1="160.02" y1="10.16" x2="172.72" y2="10.16" width="0.1524" layer="91"/>
<junction x="160.02" y="17.78"/>
<junction x="172.72" y="10.16"/>
</segment>
<segment>
<pinref part="U10" gate="G$1" pin="EMITTER"/>
<wire x1="170.18" y1="38.1" x2="170.18" y2="35.56" width="0.1524" layer="91"/>
<pinref part="GND21" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C26" gate="G$1" pin="2"/>
<pinref part="GND22" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R26" gate="G$1" pin="1"/>
<pinref part="GND23" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R28" gate="G$1" pin="1"/>
<pinref part="GND24" gate="1" pin="GND"/>
<pinref part="C27" gate="G$1" pin="2"/>
<wire x1="77.216" y1="-102.108" x2="77.216" y2="-104.14" width="0.1524" layer="91"/>
<wire x1="77.216" y1="-104.14" x2="71.12" y2="-104.14" width="0.1524" layer="91"/>
<junction x="71.12" y="-104.14"/>
</segment>
<segment>
<pinref part="U12" gate="G$1" pin="EMITTER"/>
<pinref part="GND25" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="LM2750" gate="A" pin="GND_2"/>
<pinref part="LM2750" gate="A" pin="GND_3"/>
<wire x1="198.12" y1="-121.92" x2="198.12" y2="-124.46" width="0.1524" layer="91"/>
<pinref part="LM2750" gate="A" pin="GND"/>
<wire x1="198.12" y1="-124.46" x2="198.12" y2="-127" width="0.1524" layer="91"/>
<junction x="198.12" y="-124.46"/>
<pinref part="GND27" gate="1" pin="GND"/>
<junction x="198.12" y="-127"/>
<pinref part="C28" gate="G$1" pin="2"/>
<wire x1="180.34" y1="-109.22" x2="180.34" y2="-114.3" width="0.1524" layer="91"/>
<wire x1="180.34" y1="-114.3" x2="180.34" y2="-127" width="0.1524" layer="91"/>
<wire x1="180.34" y1="-127" x2="198.12" y2="-127" width="0.1524" layer="91"/>
<pinref part="LM2750" gate="A" pin="FB"/>
<wire x1="198.12" y1="-114.3" x2="180.34" y2="-114.3" width="0.1524" layer="91"/>
<junction x="180.34" y="-114.3"/>
</segment>
<segment>
<pinref part="C29" gate="G$1" pin="2"/>
<pinref part="GND28" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U$4" gate="GPS" pin="GND"/>
<wire x1="208.28" y1="53.34" x2="208.28" y2="50.8" width="0.1524" layer="91"/>
<pinref part="GND29" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="EXTMODE"/>
<pinref part="U$5" gate="G$1" pin="VSS"/>
<wire x1="264.16" y1="-104.14" x2="264.16" y2="-101.6" width="0.1524" layer="91"/>
<pinref part="U$5" gate="G$1" pin="VSSA"/>
<wire x1="264.16" y1="-101.6" x2="264.16" y2="-99.06" width="0.1524" layer="91"/>
<junction x="264.16" y="-101.6"/>
<wire x1="264.16" y1="-99.06" x2="264.16" y2="-96.52" width="0.1524" layer="91"/>
<wire x1="264.16" y1="-96.52" x2="259.08" y2="-96.52" width="0.1524" layer="91"/>
<junction x="264.16" y="-99.06"/>
<wire x1="259.08" y1="-96.52" x2="259.08" y2="-99.06" width="0.1524" layer="91"/>
<pinref part="GND30" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="PCA9555D" gate="U$1" pin="VSS"/>
<pinref part="GND31" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U15" gate="G$1" pin="EMITTER"/>
<pinref part="GND32" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="GND"/>
<wire x1="165.1" y1="-175.26" x2="167.64" y2="-175.26" width="0.1524" layer="91"/>
<pinref part="GND33" gate="1" pin="GND"/>
<wire x1="167.64" y1="-175.26" x2="167.64" y2="-180.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C17" gate="G$1" pin="2"/>
<pinref part="C19" gate="G$1" pin="2"/>
<wire x1="320.04" y1="35.56" x2="330.2" y2="35.56" width="0.1524" layer="91"/>
<pinref part="GND17" gate="1" pin="GND"/>
<wire x1="330.2" y1="33.02" x2="330.2" y2="35.56" width="0.1524" layer="91"/>
<junction x="330.2" y="35.56"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="1"/>
<pinref part="GND35" gate="1" pin="GND"/>
<wire x1="330.2" y1="-99.06" x2="325.12" y2="-99.06" width="0.1524" layer="91"/>
<wire x1="325.12" y1="-99.06" x2="325.12" y2="-109.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="1"/>
<pinref part="GND34" gate="1" pin="GND"/>
<wire x1="307.34" y1="-99.06" x2="302.26" y2="-99.06" width="0.1524" layer="91"/>
<wire x1="302.26" y1="-99.06" x2="302.26" y2="-109.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J4" gate="G$1" pin="11"/>
<wire x1="261.62" y1="-154.94" x2="259.08" y2="-154.94" width="0.1524" layer="91"/>
<pinref part="GND36" gate="1" pin="GND"/>
<pinref part="J4" gate="G$1" pin="9"/>
<wire x1="261.62" y1="-152.4" x2="259.08" y2="-152.4" width="0.1524" layer="91"/>
<wire x1="259.08" y1="-152.4" x2="259.08" y2="-154.94" width="0.1524" layer="91"/>
<junction x="259.08" y="-154.94"/>
</segment>
<segment>
<pinref part="J4" gate="G$1" pin="12"/>
<wire x1="284.48" y1="-154.94" x2="287.02" y2="-154.94" width="0.1524" layer="91"/>
<pinref part="GND37" gate="1" pin="GND"/>
<pinref part="J4" gate="G$1" pin="8"/>
<wire x1="284.48" y1="-149.86" x2="287.02" y2="-149.86" width="0.1524" layer="91"/>
<pinref part="J4" gate="G$1" pin="10"/>
<wire x1="284.48" y1="-152.4" x2="287.02" y2="-152.4" width="0.1524" layer="91"/>
<wire x1="287.02" y1="-149.86" x2="287.02" y2="-152.4" width="0.1524" layer="91"/>
<wire x1="287.02" y1="-152.4" x2="287.02" y2="-154.94" width="0.1524" layer="91"/>
<junction x="287.02" y="-152.4"/>
<junction x="287.02" y="-154.94"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="ESP32-PICO-D4" gate="G$1" pin="LNA_IN"/>
<pinref part="L1" gate="G$1" pin="1"/>
<wire x1="73.66" y1="53.34" x2="96.52" y2="53.34" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="96.52" y1="53.34" x2="101.6" y2="53.34" width="0.1524" layer="91"/>
<junction x="96.52" y="53.34"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="C1" gate="G$1" pin="2"/>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="109.22" y1="53.34" x2="114.3" y2="53.34" width="0.1524" layer="91"/>
<wire x1="114.3" y1="53.34" x2="114.3" y2="50.8" width="0.1524" layer="91"/>
<wire x1="114.3" y1="53.34" x2="119.38" y2="53.34" width="0.1524" layer="91"/>
<junction x="114.3" y="53.34"/>
<wire x1="119.38" y1="53.34" x2="119.38" y2="38.1" width="0.1524" layer="91"/>
<pinref part="ANT" gate="G$1" pin="RF_FEED"/>
<wire x1="119.38" y1="38.1" x2="127" y2="38.1" width="0.1524" layer="91"/>
<wire x1="127" y1="38.1" x2="127" y2="40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ESP_TX" class="0">
<segment>
<pinref part="ESP32-PICO-D4" gate="G$1" pin="U0TXD"/>
<wire x1="73.66" y1="58.42" x2="78.74" y2="58.42" width="0.1524" layer="91"/>
<label x="91.44" y="58.42" size="1.27" layer="95" xref="yes"/>
<pinref part="R10" gate="G$1" pin="1"/>
<wire x1="78.74" y1="58.42" x2="91.44" y2="58.42" width="0.1524" layer="91"/>
<junction x="78.74" y="58.42"/>
</segment>
<segment>
<pinref part="FT231" gate="G$1" pin="RXD"/>
<wire x1="43.18" y1="-35.56" x2="40.64" y2="-35.56" width="0.1524" layer="91"/>
<label x="40.64" y="-35.56" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="ESP_RX" class="0">
<segment>
<pinref part="ESP32-PICO-D4" gate="G$1" pin="U0RXD"/>
<wire x1="73.66" y1="55.88" x2="86.36" y2="55.88" width="0.1524" layer="91"/>
<label x="91.44" y="55.88" size="1.27" layer="95" xref="yes"/>
<pinref part="R11" gate="G$1" pin="1"/>
<wire x1="86.36" y1="55.88" x2="91.44" y2="55.88" width="0.1524" layer="91"/>
<junction x="86.36" y="55.88"/>
</segment>
<segment>
<pinref part="FT231" gate="G$1" pin="TXD"/>
<wire x1="78.74" y1="-38.1" x2="81.28" y2="-38.1" width="0.1524" layer="91"/>
<label x="81.28" y="-38.1" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="ESP_EN" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="1"/>
<pinref part="ESP32-PICO-D4" gate="G$1" pin="EN"/>
<wire x1="17.78" y1="48.26" x2="33.02" y2="48.26" width="0.1524" layer="91"/>
<label x="33.02" y="48.26" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="C3" gate="G$1" pin="2"/>
<junction x="17.78" y="48.26"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="COLLECTOR"/>
<wire x1="119.38" y1="-63.5" x2="121.92" y2="-63.5" width="0.1524" layer="91"/>
<label x="121.92" y="-63.5" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="RESET" gate="G$1" pin="2"/>
<wire x1="25.4" y1="-58.42" x2="27.94" y2="-58.42" width="0.1524" layer="91"/>
<label x="27.94" y="-58.42" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="ESP_FLASH" class="0">
<segment>
<pinref part="ESP32-PICO-D4" gate="G$1" pin="IO0"/>
<label x="76.2" y="35.56" size="1.27" layer="95" xref="yes"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="91.44" y1="35.56" x2="73.66" y2="35.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="COLLECTOR"/>
<wire x1="119.38" y1="-30.48" x2="121.92" y2="-30.48" width="0.1524" layer="91"/>
<label x="121.92" y="-30.48" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="FLASH" gate="G$1" pin="2"/>
<wire x1="25.4" y1="-53.34" x2="27.94" y2="-53.34" width="0.1524" layer="91"/>
<label x="27.94" y="-53.34" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="FT231" gate="G$1" pin="VCCIO"/>
<pinref part="FT231" gate="G$1" pin="3V3OUT"/>
<wire x1="78.74" y1="-22.86" x2="78.74" y2="-25.4" width="0.1524" layer="91"/>
<pinref part="FT231" gate="G$1" pin="!RESET#"/>
<wire x1="43.18" y1="-27.94" x2="43.18" y2="-22.86" width="0.1524" layer="91"/>
<wire x1="43.18" y1="-22.86" x2="78.74" y2="-22.86" width="0.1524" layer="91"/>
<junction x="78.74" y="-22.86"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="78.74" y1="-22.86" x2="88.9" y2="-22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="FT231" gate="G$1" pin="USBDM"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="38.1" y1="-30.48" x2="43.18" y2="-30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="2"/>
<pinref part="FT231" gate="G$1" pin="USBDP"/>
<wire x1="33.02" y1="-33.02" x2="43.18" y2="-33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="D-" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="27.94" y1="-30.48" x2="20.32" y2="-30.48" width="0.1524" layer="91"/>
<label x="12.7" y="-30.48" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="20.32" y1="-30.48" x2="12.7" y2="-30.48" width="0.1524" layer="91"/>
<junction x="20.32" y="-30.48"/>
</segment>
<segment>
<wire x1="167.64" y1="-25.654" x2="170.18" y2="-25.654" width="0.1524" layer="91"/>
<label x="170.18" y="-25.654" size="1.27" layer="95" xref="yes"/>
<pinref part="J3" gate="G$1" pin="D-"/>
</segment>
</net>
<net name="D+" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="22.86" y1="-33.02" x2="15.24" y2="-33.02" width="0.1524" layer="91"/>
<label x="12.7" y="-33.02" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="15.24" y1="-33.02" x2="12.7" y2="-33.02" width="0.1524" layer="91"/>
<junction x="15.24" y="-33.02"/>
</segment>
<segment>
<wire x1="167.64" y1="-28.194" x2="170.18" y2="-28.194" width="0.1524" layer="91"/>
<label x="170.18" y="-28.194" size="1.27" layer="95" xref="yes"/>
<pinref part="J3" gate="G$1" pin="D+"/>
</segment>
</net>
<net name="V_USB" class="0">
<segment>
<pinref part="FT231" gate="G$1" pin="VCC"/>
<wire x1="78.74" y1="-20.32" x2="78.74" y2="-17.78" width="0.1524" layer="91"/>
<pinref part="SUPPLY1" gate="G$1" pin="V_USB"/>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="78.74" y1="-17.78" x2="81.28" y2="-17.78" width="0.1524" layer="91"/>
<junction x="78.74" y="-17.78"/>
</segment>
<segment>
<pinref part="MCP73831" gate="A" pin="VIN"/>
<wire x1="185.42" y1="-43.18" x2="185.42" y2="-40.64" width="0.1524" layer="91"/>
<pinref part="SUPPLY2" gate="G$1" pin="V_USB"/>
<wire x1="185.42" y1="-43.18" x2="165.1" y2="-43.18" width="0.1524" layer="91"/>
<junction x="185.42" y="-43.18"/>
<pinref part="C9" gate="G$1" pin="1"/>
<wire x1="165.1" y1="-43.18" x2="154.94" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="154.94" y1="-43.18" x2="154.94" y2="-50.8" width="0.1524" layer="91"/>
<pinref part="R9" gate="G$1" pin="2"/>
<junction x="165.1" y="-43.18"/>
</segment>
<segment>
<wire x1="167.64" y1="-23.114" x2="170.18" y2="-23.114" width="0.1524" layer="91"/>
<pinref part="SUPPLY5" gate="G$1" pin="V_USB"/>
<pinref part="J3" gate="G$1" pin="5V"/>
</segment>
<segment>
<pinref part="R47" gate="G$1" pin="2"/>
<pinref part="SUPPLY7" gate="G$1" pin="V_USB"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="BASE"/>
<pinref part="R5" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="BASE"/>
<pinref part="R6" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="EMITTER"/>
<wire x1="119.38" y1="-48.26" x2="99.06" y2="-48.26" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="99.06" y1="-48.26" x2="99.06" y2="-40.64" width="0.1524" layer="91"/>
<pinref part="FT231" gate="G$1" pin="!RTS#"/>
<wire x1="99.06" y1="-40.64" x2="99.06" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="78.74" y1="-40.64" x2="99.06" y2="-40.64" width="0.1524" layer="91"/>
<junction x="99.06" y="-40.64"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="EMITTER"/>
<wire x1="119.38" y1="-45.72" x2="96.52" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="96.52" y1="-45.72" x2="96.52" y2="-55.88" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="96.52" y1="-55.88" x2="99.06" y2="-55.88" width="0.1524" layer="91"/>
<pinref part="FT231" gate="G$1" pin="!DTR#"/>
<junction x="96.52" y="-45.72"/>
<wire x1="96.52" y1="-43.18" x2="96.52" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="78.74" y1="-43.18" x2="96.52" y2="-43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="MCP73831" gate="A" pin="PROG"/>
<wire x1="210.82" y1="-45.72" x2="213.36" y2="-45.72" width="0.1524" layer="91"/>
<pinref part="R7" gate="G$1" pin="2"/>
</segment>
</net>
<net name="IO7" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$1"/>
<pinref part="MCP73831" gate="A" pin="STAT"/>
<wire x1="185.42" y1="-53.34" x2="185.42" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="185.42" y1="-53.34" x2="185.42" y2="-55.88" width="0.1524" layer="91"/>
<junction x="185.42" y="-53.34"/>
<wire x1="185.42" y1="-55.88" x2="165.1" y2="-55.88" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="P$3"/>
<wire x1="165.1" y1="-55.88" x2="165.1" y2="-58.42" width="0.1524" layer="91"/>
<label x="165.1" y="-63.5" size="1.27" layer="95" xref="yes"/>
<wire x1="165.1" y1="-58.42" x2="165.1" y2="-63.5" width="0.1524" layer="91"/>
<junction x="165.1" y="-58.42"/>
</segment>
<segment>
<pinref part="ESP32-PICO-D4" gate="G$1" pin="IO34"/>
<wire x1="33.02" y1="43.18" x2="30.48" y2="43.18" width="0.1524" layer="91"/>
<label x="30.48" y="43.18" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="V_BATT" class="0">
<segment>
<pinref part="MCP73831" gate="A" pin="VBAT"/>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="210.82" y1="-43.18" x2="218.44" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="218.44" y1="-43.18" x2="218.44" y2="-45.72" width="0.1524" layer="91"/>
<pinref part="BATTERY" gate="G$1" pin="+"/>
<wire x1="218.44" y1="-43.18" x2="223.52" y2="-43.18" width="0.1524" layer="91"/>
<junction x="218.44" y="-43.18"/>
<pinref part="SUPPLY3" gate="G$1" pin="V_BATT"/>
</segment>
<segment>
<pinref part="TPS79533" gate="G$1" pin="IN"/>
<pinref part="TPS79533" gate="G$1" pin="EN"/>
<wire x1="189.992" y1="-23.876" x2="189.992" y2="-21.336" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="189.992" y1="-21.336" x2="182.372" y2="-21.336" width="0.1524" layer="91"/>
<junction x="189.992" y="-21.336"/>
<pinref part="SUPPLY4" gate="G$1" pin="V_BATT"/>
<junction x="182.372" y="-21.336"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="VBKP"/>
<pinref part="SUPPLY6" gate="G$1" pin="V_BATT"/>
<wire x1="205.74" y1="20.32" x2="213.36" y2="20.32" width="0.1524" layer="91"/>
<wire x1="213.36" y1="20.32" x2="213.36" y2="27.94" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LM2750" gate="A" pin="VIN_2"/>
<pinref part="LM2750" gate="A" pin="VIN"/>
<wire x1="198.12" y1="-101.6" x2="198.12" y2="-104.14" width="0.1524" layer="91"/>
<wire x1="198.12" y1="-101.6" x2="195.58" y2="-101.6" width="0.1524" layer="91"/>
<junction x="198.12" y="-101.6"/>
<wire x1="195.58" y1="-101.6" x2="195.58" y2="-99.314" width="0.1524" layer="91"/>
<pinref part="SUPPLY10" gate="G$1" pin="V_BATT"/>
<wire x1="195.58" y1="-99.314" x2="195.58" y2="-99.06" width="0.1524" layer="91"/>
<junction x="195.58" y="-99.314"/>
<pinref part="C28" gate="G$1" pin="1"/>
<wire x1="180.34" y1="-101.6" x2="195.58" y2="-101.6" width="0.1524" layer="91"/>
<junction x="195.58" y="-101.6"/>
</segment>
<segment>
<pinref part="R15" gate="G$1" pin="2"/>
<pinref part="SUPPLY12" gate="G$1" pin="V_BATT"/>
</segment>
<segment>
<pinref part="R29" gate="G$1" pin="2"/>
<pinref part="SUPPLY9" gate="G$1" pin="V_BATT"/>
</segment>
<segment>
<pinref part="R27" gate="G$1" pin="2"/>
<pinref part="SUPPLY8" gate="G$1" pin="V_BATT"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="TPS79533" gate="G$1" pin="FB"/>
<pinref part="C13" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$2"/>
<pinref part="R8" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="P$4"/>
<pinref part="R9" gate="G$1" pin="1"/>
</segment>
</net>
<net name="IO12" class="0">
<segment>
<wire x1="327.66" y1="-2.54" x2="325.12" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="R16" gate="G$1" pin="1"/>
<wire x1="325.12" y1="-2.54" x2="314.96" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="325.12" y1="17.78" x2="325.12" y2="-2.54" width="0.1524" layer="91"/>
<junction x="325.12" y="-2.54"/>
<pinref part="SD-CARD" gate="G$1" pin="CRD_DTCT"/>
<label x="314.96" y="-2.54" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SD_CS" class="0">
<segment>
<wire x1="327.66" y1="12.7" x2="325.12" y2="12.7" width="0.1524" layer="91"/>
<wire x1="325.12" y1="12.7" x2="314.96" y2="12.7" width="0.1524" layer="91"/>
<pinref part="R12" gate="G$1" pin="1"/>
<wire x1="314.96" y1="12.7" x2="307.34" y2="12.7" width="0.1524" layer="91"/>
<wire x1="307.34" y1="12.7" x2="307.34" y2="17.78" width="0.1524" layer="91"/>
<pinref part="SD-CARD" gate="G$1" pin="CS"/>
<label x="307.34" y="12.7" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="ESP32-PICO-D4" gate="G$1" pin="IO5"/>
<wire x1="73.66" y1="27.94" x2="76.2" y2="27.94" width="0.1524" layer="91"/>
<label x="76.2" y="27.94" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SD_MOSI" class="0">
<segment>
<wire x1="327.66" y1="10.16" x2="325.12" y2="10.16" width="0.1524" layer="91"/>
<wire x1="325.12" y1="10.16" x2="314.96" y2="10.16" width="0.1524" layer="91"/>
<pinref part="R13" gate="G$1" pin="1"/>
<wire x1="314.96" y1="10.16" x2="299.72" y2="10.16" width="0.1524" layer="91"/>
<wire x1="299.72" y1="10.16" x2="299.72" y2="17.78" width="0.1524" layer="91"/>
<pinref part="SD-CARD" gate="G$1" pin="DI"/>
<label x="299.72" y="10.16" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="ESP32-PICO-D4" gate="G$1" pin="IO23"/>
<wire x1="33.02" y1="25.4" x2="30.48" y2="25.4" width="0.1524" layer="91"/>
<label x="30.48" y="25.4" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SD_CLK" class="0">
<segment>
<wire x1="327.66" y1="7.62" x2="325.12" y2="7.62" width="0.1524" layer="91"/>
<wire x1="325.12" y1="7.62" x2="314.96" y2="7.62" width="0.1524" layer="91"/>
<pinref part="R14" gate="G$1" pin="1"/>
<wire x1="314.96" y1="7.62" x2="292.1" y2="7.62" width="0.1524" layer="91"/>
<wire x1="292.1" y1="7.62" x2="292.1" y2="17.78" width="0.1524" layer="91"/>
<pinref part="SD-CARD" gate="G$1" pin="SCLK"/>
<label x="292.1" y="7.62" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="ESP32-PICO-D4" gate="G$1" pin="IO18"/>
<wire x1="33.02" y1="15.24" x2="30.48" y2="15.24" width="0.1524" layer="91"/>
<label x="30.48" y="15.24" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SD_MISO" class="0">
<segment>
<wire x1="327.66" y1="5.08" x2="284.48" y2="5.08" width="0.1524" layer="91"/>
<wire x1="284.48" y1="5.08" x2="284.48" y2="17.78" width="0.1524" layer="91"/>
<pinref part="SD-CARD" gate="G$1" pin="D0"/>
<label x="284.48" y="5.08" size="1.27" layer="95" rot="R270" xref="yes"/>
<pinref part="R43" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="ESP32-PICO-D4" gate="G$1" pin="IO19"/>
<wire x1="33.02" y1="17.78" x2="30.48" y2="17.78" width="0.1524" layer="91"/>
<label x="30.48" y="17.78" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="3V3_SDC" class="0">
<segment>
<pinref part="R16" gate="G$1" pin="2"/>
<pinref part="R12" gate="G$1" pin="2"/>
<wire x1="325.12" y1="27.94" x2="320.04" y2="27.94" width="0.1524" layer="91"/>
<pinref part="R13" gate="G$1" pin="2"/>
<wire x1="320.04" y1="27.94" x2="314.96" y2="27.94" width="0.1524" layer="91"/>
<wire x1="314.96" y1="27.94" x2="312.42" y2="27.94" width="0.1524" layer="91"/>
<wire x1="312.42" y1="27.94" x2="307.34" y2="27.94" width="0.1524" layer="91"/>
<wire x1="307.34" y1="27.94" x2="299.72" y2="27.94" width="0.1524" layer="91"/>
<junction x="307.34" y="27.94"/>
<pinref part="R14" gate="G$1" pin="2"/>
<wire x1="299.72" y1="27.94" x2="292.1" y2="27.94" width="0.1524" layer="91"/>
<junction x="299.72" y="27.94"/>
<junction x="292.1" y="27.94"/>
<wire x1="327.66" y1="-5.08" x2="320.04" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="320.04" y1="-5.08" x2="320.04" y2="27.94" width="0.1524" layer="91"/>
<junction x="320.04" y="27.94"/>
<pinref part="C14" gate="G$1" pin="1"/>
<wire x1="292.1" y1="27.94" x2="284.48" y2="27.94" width="0.1524" layer="91"/>
<pinref part="C15" gate="G$1" pin="1"/>
<wire x1="284.48" y1="27.94" x2="269.24" y2="27.94" width="0.1524" layer="91"/>
<wire x1="269.24" y1="27.94" x2="259.08" y2="27.94" width="0.1524" layer="91"/>
<junction x="269.24" y="27.94"/>
<pinref part="C16" gate="G$1" pin="1"/>
<pinref part="U9" gate="G$1" pin="DRAIN"/>
<wire x1="289.56" y1="43.18" x2="289.56" y2="45.72" width="0.1524" layer="91"/>
<wire x1="289.56" y1="43.18" x2="312.42" y2="43.18" width="0.1524" layer="91"/>
<wire x1="312.42" y1="43.18" x2="312.42" y2="27.94" width="0.1524" layer="91"/>
<junction x="289.56" y="43.18"/>
<junction x="312.42" y="27.94"/>
<pinref part="C17" gate="G$1" pin="1"/>
<wire x1="312.42" y1="43.18" x2="320.04" y2="43.18" width="0.1524" layer="91"/>
<junction x="312.42" y="43.18"/>
<wire x1="320.04" y1="43.18" x2="330.2" y2="43.18" width="0.1524" layer="91"/>
<junction x="320.04" y="43.18"/>
<pinref part="SD-CARD" gate="G$1" pin="3.3V"/>
<pinref part="C19" gate="G$1" pin="1"/>
<label x="312.42" y="43.18" size="1.27" layer="95" rot="R90" xref="yes"/>
<pinref part="R31" gate="G$1" pin="2"/>
<junction x="314.96" y="27.94"/>
<pinref part="R43" gate="G$1" pin="2"/>
<junction x="284.48" y="27.94"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="BASE"/>
<pinref part="R17" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="COLLECTOR"/>
<pinref part="R18" gate="G$1" pin="2"/>
<wire x1="279.4" y1="53.34" x2="279.4" y2="60.96" width="0.1524" layer="91"/>
<pinref part="U9" gate="G$1" pin="GATE"/>
<junction x="279.4" y="53.34"/>
</segment>
</net>
<net name="I2C_SDA" class="0">
<segment>
<pinref part="BME280" gate="G$1" pin="SDI"/>
<wire x1="289.56" y1="-50.8" x2="302.26" y2="-50.8" width="0.1524" layer="91"/>
<label x="304.8" y="-50.8" size="1.27" layer="95" xref="yes"/>
<pinref part="R45" gate="G$1" pin="2"/>
<wire x1="302.26" y1="-50.8" x2="304.8" y2="-50.8" width="0.1524" layer="91"/>
<wire x1="299.72" y1="-55.88" x2="302.26" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="302.26" y1="-55.88" x2="302.26" y2="-50.8" width="0.1524" layer="91"/>
<junction x="302.26" y="-50.8"/>
</segment>
<segment>
<pinref part="LSM303" gate="G$1" pin="SDA"/>
<wire x1="332.74" y1="-45.72" x2="330.2" y2="-45.72" width="0.1524" layer="91"/>
<label x="330.2" y="-45.72" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="PCA9555D" gate="U$1" pin="SDA"/>
<wire x1="60.96" y1="-154.94" x2="58.42" y2="-154.94" width="0.1524" layer="91"/>
<label x="58.42" y="-154.94" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="ESP32-PICO-D4" gate="G$1" pin="IO22"/>
<wire x1="33.02" y1="22.86" x2="30.48" y2="22.86" width="0.1524" layer="91"/>
<label x="30.48" y="22.86" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="3"/>
<wire x1="307.34" y1="-104.14" x2="304.8" y2="-104.14" width="0.1524" layer="91"/>
<label x="304.8" y="-104.14" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="3"/>
<wire x1="330.2" y1="-104.14" x2="327.66" y2="-104.14" width="0.1524" layer="91"/>
<label x="327.66" y="-104.14" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="I2C_SCL" class="0">
<segment>
<pinref part="BME280" gate="G$1" pin="SCK"/>
<wire x1="289.56" y1="-48.26" x2="302.26" y2="-48.26" width="0.1524" layer="91"/>
<label x="304.8" y="-48.26" size="1.27" layer="95" xref="yes"/>
<pinref part="R46" gate="G$1" pin="2"/>
<wire x1="302.26" y1="-48.26" x2="304.8" y2="-48.26" width="0.1524" layer="91"/>
<junction x="302.26" y="-48.26"/>
</segment>
<segment>
<pinref part="LSM303" gate="G$1" pin="SCL"/>
<wire x1="332.74" y1="-43.18" x2="330.2" y2="-43.18" width="0.1524" layer="91"/>
<label x="330.2" y="-43.18" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="ESP32-PICO-D4" gate="G$1" pin="IO21"/>
<wire x1="33.02" y1="20.32" x2="30.48" y2="20.32" width="0.1524" layer="91"/>
<label x="30.48" y="20.32" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="PCA9555D" gate="U$1" pin="SCL"/>
<wire x1="60.96" y1="-157.48" x2="58.42" y2="-157.48" width="0.1524" layer="91"/>
<label x="58.42" y="-157.48" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="4"/>
<wire x1="307.34" y1="-106.68" x2="304.8" y2="-106.68" width="0.1524" layer="91"/>
<label x="304.8" y="-106.68" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="4"/>
<wire x1="330.2" y1="-106.68" x2="327.66" y2="-106.68" width="0.1524" layer="91"/>
<label x="327.66" y="-106.68" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="LSM303" gate="G$1" pin="C1"/>
<pinref part="C21" gate="G$1" pin="2"/>
<wire x1="332.74" y1="-53.34" x2="330.2" y2="-53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="LSM303" gate="G$1" pin="SETC"/>
<pinref part="C20" gate="G$1" pin="1"/>
<wire x1="358.14" y1="-40.64" x2="363.22" y2="-40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="C20" gate="G$1" pin="2"/>
<pinref part="LSM303" gate="G$1" pin="SETP"/>
<wire x1="370.84" y1="-40.64" x2="370.84" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="370.84" y1="-43.18" x2="358.14" y2="-43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="TX_GPS" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="RX"/>
<wire x1="205.74" y1="15.24" x2="223.52" y2="15.24" width="0.1524" layer="91"/>
<label x="223.52" y="15.24" size="1.27" layer="95" xref="yes"/>
<pinref part="R21" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="ESP32-PICO-D4" gate="G$1" pin="IO33"/>
<wire x1="33.02" y1="38.1" x2="30.48" y2="38.1" width="0.1524" layer="91"/>
<label x="30.48" y="38.1" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U$4" gate="GPS" pin="TX"/>
<wire x1="213.36" y1="53.34" x2="213.36" y2="50.8" width="0.1524" layer="91"/>
<label x="213.36" y="50.8" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="RX_GPS" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="TX"/>
<wire x1="205.74" y1="12.7" x2="215.9" y2="12.7" width="0.1524" layer="91"/>
<label x="223.52" y="12.7" size="1.27" layer="95" xref="yes"/>
<pinref part="R20" gate="G$1" pin="2"/>
<wire x1="215.9" y1="12.7" x2="223.52" y2="12.7" width="0.1524" layer="91"/>
<junction x="215.9" y="12.7"/>
</segment>
<segment>
<pinref part="ESP32-PICO-D4" gate="G$1" pin="IO32"/>
<wire x1="33.02" y1="35.56" x2="30.48" y2="35.56" width="0.1524" layer="91"/>
<label x="30.48" y="35.56" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U$4" gate="GPS" pin="RX"/>
<wire x1="218.44" y1="53.34" x2="218.44" y2="50.8" width="0.1524" layer="91"/>
<label x="218.44" y="50.8" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="IO6" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="1PPS"/>
<wire x1="205.74" y1="22.86" x2="208.28" y2="22.86" width="0.1524" layer="91"/>
<label x="208.28" y="22.86" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="3V3_GPS" class="0">
<segment>
<pinref part="C26" gate="G$1" pin="1"/>
<pinref part="U11" gate="G$1" pin="DRAIN"/>
<wire x1="180.34" y1="43.18" x2="180.34" y2="45.72" width="0.1524" layer="91"/>
<wire x1="180.34" y1="43.18" x2="218.44" y2="43.18" width="0.1524" layer="91"/>
<junction x="180.34" y="43.18"/>
<label x="195.58" y="43.18" size="1.27" layer="95" rot="R90" xref="yes"/>
<pinref part="U$2" gate="G$1" pin="VCC"/>
<wire x1="205.74" y1="17.78" x2="218.44" y2="17.78" width="0.1524" layer="91"/>
<wire x1="218.44" y1="17.78" x2="218.44" y2="25.4" width="0.1524" layer="91"/>
<pinref part="R19" gate="G$1" pin="1"/>
<wire x1="170.18" y1="25.4" x2="215.9" y2="25.4" width="0.1524" layer="91"/>
<junction x="218.44" y="25.4"/>
<pinref part="R20" gate="G$1" pin="1"/>
<wire x1="215.9" y1="25.4" x2="218.44" y2="25.4" width="0.1524" layer="91"/>
<wire x1="215.9" y1="22.86" x2="215.9" y2="25.4" width="0.1524" layer="91"/>
<junction x="215.9" y="25.4"/>
<pinref part="R21" gate="G$1" pin="1"/>
<wire x1="223.52" y1="25.4" x2="218.44" y2="25.4" width="0.1524" layer="91"/>
<pinref part="C25" gate="G$1" pin="1"/>
<pinref part="C24" gate="G$1" pin="1"/>
<wire x1="152.4" y1="25.4" x2="160.02" y2="25.4" width="0.1524" layer="91"/>
<wire x1="160.02" y1="25.4" x2="170.18" y2="25.4" width="0.1524" layer="91"/>
<junction x="160.02" y="25.4"/>
<junction x="170.18" y="25.4"/>
<wire x1="218.44" y1="43.18" x2="218.44" y2="25.4" width="0.1524" layer="91"/>
<pinref part="U$4" gate="GPS" pin="VIN"/>
<wire x1="223.52" y1="53.34" x2="223.52" y2="25.4" width="0.1524" layer="91"/>
<junction x="223.52" y="25.4"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="U10" gate="G$1" pin="BASE"/>
<pinref part="R22" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="U10" gate="G$1" pin="COLLECTOR"/>
<pinref part="R23" gate="G$1" pin="2"/>
<wire x1="170.18" y1="53.34" x2="170.18" y2="60.96" width="0.1524" layer="91"/>
<pinref part="U11" gate="G$1" pin="GATE"/>
<junction x="170.18" y="53.34"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="TIMER"/>
<wire x1="177.8" y1="22.86" x2="162.56" y2="22.86" width="0.1524" layer="91"/>
<wire x1="162.56" y1="22.86" x2="162.56" y2="33.02" width="0.1524" layer="91"/>
<pinref part="R24" gate="G$1" pin="1"/>
<pinref part="D2" gate="G$1" pin="A"/>
<wire x1="162.56" y1="33.02" x2="162.56" y2="50.8" width="0.1524" layer="91"/>
<wire x1="152.4" y1="35.56" x2="152.4" y2="33.02" width="0.1524" layer="91"/>
<wire x1="152.4" y1="33.02" x2="162.56" y2="33.02" width="0.1524" layer="91"/>
<junction x="162.56" y="33.02"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="D1" gate="G$1" pin="C"/>
<pinref part="D2" gate="G$1" pin="C"/>
<wire x1="144.78" y1="40.64" x2="149.86" y2="40.64" width="0.1524" layer="91"/>
<pinref part="R22" gate="G$1" pin="2"/>
<wire x1="149.86" y1="40.64" x2="152.4" y2="40.64" width="0.1524" layer="91"/>
<wire x1="149.86" y1="45.72" x2="149.86" y2="40.64" width="0.1524" layer="91"/>
<junction x="149.86" y="40.64"/>
</segment>
</net>
<net name="AI1" class="0">
<segment>
<pinref part="R27" gate="G$1" pin="1"/>
<pinref part="R28" gate="G$1" pin="2"/>
<junction x="71.12" y="-93.98"/>
<label x="83.82" y="-93.98" size="1.27" layer="95" xref="yes"/>
<wire x1="71.12" y1="-93.98" x2="76.2" y2="-93.98" width="0.1524" layer="91"/>
<pinref part="C27" gate="G$1" pin="1"/>
<wire x1="76.2" y1="-93.98" x2="83.82" y2="-93.98" width="0.1524" layer="91"/>
<wire x1="76.2" y1="-93.98" x2="76.2" y2="-94.488" width="0.1524" layer="91"/>
<wire x1="76.2" y1="-94.488" x2="77.216" y2="-94.488" width="0.1524" layer="91"/>
<junction x="76.2" y="-93.98"/>
</segment>
<segment>
<pinref part="ESP32-PICO-D4" gate="G$1" pin="IO35"/>
<wire x1="33.02" y1="45.72" x2="30.48" y2="45.72" width="0.1524" layer="91"/>
<label x="30.48" y="45.72" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="U12" gate="G$1" pin="BASE"/>
<wire x1="114.3" y1="-101.6" x2="114.3" y2="-111.76" width="0.1524" layer="91"/>
<pinref part="R30" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="R29" gate="G$1" pin="1"/>
<pinref part="PIEZO+" gate="G$1" pin="P"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="U12" gate="G$1" pin="COLLECTOR"/>
<pinref part="PIEZO-" gate="G$1" pin="P"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="LM2750" gate="A" pin="C+"/>
<pinref part="C30" gate="G$1" pin="1"/>
<wire x1="198.12" y1="-106.68" x2="190.5" y2="-106.68" width="0.1524" layer="91"/>
<wire x1="190.5" y1="-106.68" x2="190.5" y2="-104.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="LM2750" gate="A" pin="C-"/>
<pinref part="C30" gate="G$1" pin="2"/>
<wire x1="198.12" y1="-109.22" x2="190.5" y2="-109.22" width="0.1524" layer="91"/>
<wire x1="190.5" y1="-109.22" x2="190.5" y2="-111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="5V" class="0">
<segment>
<pinref part="LM2750" gate="A" pin="VOUT"/>
<pinref part="LM2750" gate="A" pin="VOUT_2"/>
<wire x1="233.68" y1="-106.68" x2="233.68" y2="-109.22" width="0.1524" layer="91"/>
<pinref part="C29" gate="G$1" pin="1"/>
<junction x="233.68" y="-109.22"/>
<wire x1="233.68" y1="-106.68" x2="238.76" y2="-106.68" width="0.1524" layer="91"/>
<junction x="233.68" y="-106.68"/>
<pinref part="SUPPLY11" gate="G$1" pin="5V"/>
<pinref part="U$5" gate="G$1" pin="VDD"/>
<wire x1="238.76" y1="-106.68" x2="264.16" y2="-106.68" width="0.1524" layer="91"/>
<junction x="238.76" y="-106.68"/>
<pinref part="U$5" gate="G$1" pin="VDDA"/>
<wire x1="264.16" y1="-106.68" x2="264.16" y2="-109.22" width="0.1524" layer="91"/>
<junction x="264.16" y="-106.68"/>
</segment>
<segment>
<pinref part="J4" gate="G$1" pin="6"/>
<pinref part="SUPPLY13" gate="G$1" pin="5V"/>
<wire x1="284.48" y1="-147.32" x2="302.26" y2="-147.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J4" gate="G$1" pin="7"/>
<pinref part="SUPPLY14" gate="G$1" pin="5V"/>
<wire x1="261.62" y1="-149.86" x2="241.3" y2="-149.86" width="0.1524" layer="91"/>
<wire x1="241.3" y1="-149.86" x2="241.3" y2="-147.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="NC2" class="0">
<segment>
<pinref part="ESP32-PICO-D4" gate="G$1" pin="CMD"/>
<wire x1="73.66" y1="48.26" x2="76.2" y2="48.26" width="0.1524" layer="91"/>
<label x="76.2" y="48.26" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="NC3" class="0">
<segment>
<pinref part="ESP32-PICO-D4" gate="G$1" pin="CLK"/>
<wire x1="73.66" y1="50.8" x2="76.2" y2="50.8" width="0.1524" layer="91"/>
<label x="76.2" y="50.8" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="NC0" class="0">
<segment>
<pinref part="ESP32-PICO-D4" gate="G$1" pin="SD0"/>
<wire x1="73.66" y1="38.1" x2="76.2" y2="38.1" width="0.1524" layer="91"/>
<label x="76.2" y="38.1" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="NC1" class="0">
<segment>
<pinref part="ESP32-PICO-D4" gate="G$1" pin="SD1"/>
<wire x1="73.66" y1="40.64" x2="76.2" y2="40.64" width="0.1524" layer="91"/>
<label x="76.2" y="40.64" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="NC4" class="0">
<segment>
<pinref part="ESP32-PICO-D4" gate="G$1" pin="IO16"/>
<wire x1="73.66" y1="15.24" x2="76.2" y2="15.24" width="0.1524" layer="91"/>
<label x="76.2" y="15.24" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="NC5" class="0">
<segment>
<pinref part="ESP32-PICO-D4" gate="G$1" pin="IO17"/>
<wire x1="33.02" y1="12.7" x2="30.48" y2="12.7" width="0.1524" layer="91"/>
<label x="30.48" y="12.7" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="SD-CARD" gate="G$1" pin="IRQ"/>
<pinref part="R31" gate="G$1" pin="1"/>
<wire x1="327.66" y1="2.54" x2="314.96" y2="2.54" width="0.1524" layer="91"/>
<wire x1="314.96" y1="2.54" x2="314.96" y2="17.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DAC2" class="0">
<segment>
<pinref part="ESP32-PICO-D4" gate="G$1" pin="IO25"/>
<wire x1="33.02" y1="27.94" x2="30.48" y2="27.94" width="0.1524" layer="91"/>
<label x="30.48" y="27.94" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R32" gate="G$1" pin="2"/>
<wire x1="139.7" y1="-111.76" x2="142.24" y2="-111.76" width="0.1524" layer="91"/>
<label x="139.7" y="-111.76" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="DAC1" class="0">
<segment>
<pinref part="ESP32-PICO-D4" gate="G$1" pin="IO26"/>
<wire x1="33.02" y1="30.48" x2="30.48" y2="30.48" width="0.1524" layer="91"/>
<label x="30.48" y="30.48" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R30" gate="G$1" pin="2"/>
<wire x1="101.6" y1="-111.76" x2="104.14" y2="-111.76" width="0.1524" layer="91"/>
<label x="101.6" y="-111.76" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="LCD_MOSI" class="0">
<segment>
<pinref part="U$5" gate="G$1" pin="SI"/>
<wire x1="264.16" y1="-119.38" x2="261.62" y2="-119.38" width="0.1524" layer="91"/>
<label x="261.62" y="-119.38" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="ESP32-PICO-D4" gate="G$1" pin="IO13"/>
<wire x1="73.66" y1="22.86" x2="76.2" y2="22.86" width="0.1524" layer="91"/>
<label x="76.2" y="22.86" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="J4" gate="G$1" pin="2"/>
<wire x1="284.48" y1="-142.24" x2="287.02" y2="-142.24" width="0.1524" layer="91"/>
<label x="287.02" y="-142.24" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="LCD_CLK" class="0">
<segment>
<pinref part="U$5" gate="G$1" pin="SCLK"/>
<wire x1="264.16" y1="-121.92" x2="261.62" y2="-121.92" width="0.1524" layer="91"/>
<label x="261.62" y="-121.92" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="ESP32-PICO-D4" gate="G$1" pin="IO14"/>
<wire x1="73.66" y1="20.32" x2="76.2" y2="20.32" width="0.1524" layer="91"/>
<label x="76.2" y="20.32" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="J4" gate="G$1" pin="1"/>
<wire x1="261.62" y1="-142.24" x2="259.08" y2="-142.24" width="0.1524" layer="91"/>
<label x="259.08" y="-142.24" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="LCD_CS" class="0">
<segment>
<pinref part="U$5" gate="G$1" pin="SCS"/>
<wire x1="264.16" y1="-116.84" x2="261.62" y2="-116.84" width="0.1524" layer="91"/>
<label x="261.62" y="-116.84" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="ESP32-PICO-D4" gate="G$1" pin="IO15"/>
<wire x1="73.66" y1="17.78" x2="76.2" y2="17.78" width="0.1524" layer="91"/>
<label x="76.2" y="17.78" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="J4" gate="G$1" pin="3"/>
<wire x1="261.62" y1="-144.78" x2="259.08" y2="-144.78" width="0.1524" layer="91"/>
<label x="259.08" y="-144.78" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="GPIO_INT" class="0">
<segment>
<pinref part="PCA9555D" gate="U$1" pin="!INT"/>
<wire x1="60.96" y1="-149.86" x2="58.42" y2="-149.86" width="0.1524" layer="91"/>
<label x="58.42" y="-149.86" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="R48" gate="G$1" pin="1"/>
<junction x="60.96" y="-149.86"/>
</segment>
<segment>
<pinref part="ESP32-PICO-D4" gate="G$1" pin="IO4"/>
<wire x1="73.66" y1="30.48" x2="76.2" y2="30.48" width="0.1524" layer="91"/>
<label x="76.2" y="30.48" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="GPS_RESET" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="RESET"/>
<wire x1="177.8" y1="15.24" x2="170.18" y2="15.24" width="0.1524" layer="91"/>
<label x="170.18" y="15.24" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="R19" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="PCA9555D" gate="U$1" pin="I/O0.7"/>
<wire x1="86.36" y1="-172.72" x2="88.9" y2="-172.72" width="0.1524" layer="91"/>
<label x="88.9" y="-172.72" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SD_POWER" class="0">
<segment>
<pinref part="R17" gate="G$1" pin="2"/>
<wire x1="259.08" y1="45.72" x2="259.08" y2="55.88" width="0.1524" layer="91"/>
<label x="259.08" y="55.88" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="PCA9555D" gate="U$1" pin="I/O0.4"/>
<wire x1="86.36" y1="-165.1" x2="88.9" y2="-165.1" width="0.1524" layer="91"/>
<label x="88.9" y="-165.1" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="GPS_POWER" class="0">
<segment>
<pinref part="D1" gate="G$1" pin="A"/>
<wire x1="144.78" y1="35.56" x2="144.78" y2="30.48" width="0.1524" layer="91"/>
<label x="144.78" y="30.48" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
<segment>
<pinref part="PCA9555D" gate="U$1" pin="I/O0.3"/>
<wire x1="86.36" y1="-162.56" x2="88.9" y2="-162.56" width="0.1524" layer="91"/>
<label x="88.9" y="-162.56" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="LCD_POWER" class="0">
<segment>
<pinref part="LM2750" gate="A" pin="SD"/>
<wire x1="198.12" y1="-116.84" x2="195.58" y2="-116.84" width="0.1524" layer="91"/>
<label x="195.58" y="-116.84" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="PCA9555D" gate="U$1" pin="I/O0.6"/>
<wire x1="86.36" y1="-170.18" x2="88.9" y2="-170.18" width="0.1524" layer="91"/>
<label x="88.9" y="-170.18" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="LCD_DISPLAY" class="0">
<segment>
<pinref part="U$5" gate="G$1" pin="DISP"/>
<wire x1="264.16" y1="-111.76" x2="261.62" y2="-111.76" width="0.1524" layer="91"/>
<label x="261.62" y="-111.76" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="PCA9555D" gate="U$1" pin="I/O0.5"/>
<wire x1="86.36" y1="-167.64" x2="88.9" y2="-167.64" width="0.1524" layer="91"/>
<label x="88.9" y="-167.64" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="J4" gate="G$1" pin="5"/>
<wire x1="261.62" y1="-147.32" x2="259.08" y2="-147.32" width="0.1524" layer="91"/>
<label x="259.08" y="-147.32" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="USB_DETECT" class="0">
<segment>
<pinref part="R26" gate="G$1" pin="2"/>
<wire x1="45.72" y1="-93.98" x2="50.8" y2="-93.98" width="0.1524" layer="91"/>
<label x="50.8" y="-93.98" size="1.27" layer="95" xref="yes"/>
<pinref part="R25" gate="G$1" pin="1"/>
<junction x="45.72" y="-93.98"/>
</segment>
<segment>
<pinref part="ESP32-PICO-D4" gate="G$1" pin="IO27"/>
<wire x1="33.02" y1="33.02" x2="30.48" y2="33.02" width="0.1524" layer="91"/>
<label x="30.48" y="33.02" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="ACCL1" class="0">
<segment>
<pinref part="LSM303" gate="G$1" pin="INT2"/>
<wire x1="332.74" y1="-48.26" x2="330.2" y2="-48.26" width="0.1524" layer="91"/>
<label x="330.2" y="-48.26" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="PCA9555D" gate="U$1" pin="I/O1.5"/>
<wire x1="86.36" y1="-190.5" x2="88.9" y2="-190.5" width="0.1524" layer="91"/>
<label x="88.9" y="-190.5" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="ACCL2" class="0">
<segment>
<pinref part="LSM303" gate="G$1" pin="INT1"/>
<wire x1="332.74" y1="-50.8" x2="330.2" y2="-50.8" width="0.1524" layer="91"/>
<label x="330.2" y="-50.8" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="PCA9555D" gate="U$1" pin="I/O1.6"/>
<wire x1="86.36" y1="-193.04" x2="88.9" y2="-193.04" width="0.1524" layer="91"/>
<label x="88.9" y="-193.04" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="MAGDRDY" class="0">
<segment>
<pinref part="LSM303" gate="G$1" pin="DRDY"/>
<wire x1="358.14" y1="-50.8" x2="360.68" y2="-50.8" width="0.1524" layer="91"/>
<label x="360.68" y="-50.8" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PCA9555D" gate="U$1" pin="I/O1.7"/>
<wire x1="86.36" y1="-195.58" x2="88.9" y2="-195.58" width="0.1524" layer="91"/>
<label x="88.9" y="-195.58" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="B4" class="0">
<segment>
<pinref part="U$3" gate="G$1" pin="B"/>
<wire x1="165.1" y1="-172.72" x2="167.64" y2="-172.72" width="0.1524" layer="91"/>
<pinref part="R36" gate="G$1" pin="1"/>
<label x="167.64" y="-172.72" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PCA9555D" gate="U$1" pin="I/O1.0"/>
<wire x1="86.36" y1="-177.8" x2="88.9" y2="-177.8" width="0.1524" layer="91"/>
<label x="88.9" y="-177.8" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="U15" gate="G$1" pin="BASE"/>
<wire x1="152.4" y1="-101.6" x2="152.4" y2="-111.76" width="0.1524" layer="91"/>
<pinref part="R32" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<pinref part="R15" gate="G$1" pin="1"/>
<pinref part="MOTOR+" gate="G$1" pin="P"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="U15" gate="G$1" pin="COLLECTOR"/>
<pinref part="MOTOR-" gate="G$1" pin="P"/>
</segment>
</net>
<net name="B1" class="0">
<segment>
<pinref part="PCA9555D" gate="U$1" pin="I/O1.1"/>
<wire x1="86.36" y1="-180.34" x2="88.9" y2="-180.34" width="0.1524" layer="91"/>
<label x="88.9" y="-180.34" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="A"/>
<wire x1="127" y1="-172.72" x2="124.46" y2="-172.72" width="0.1524" layer="91"/>
<pinref part="R33" gate="G$1" pin="1"/>
<label x="124.46" y="-172.72" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B2" class="0">
<segment>
<pinref part="PCA9555D" gate="U$1" pin="I/O1.2"/>
<wire x1="86.36" y1="-182.88" x2="88.9" y2="-182.88" width="0.1524" layer="91"/>
<label x="88.9" y="-182.88" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="CENTER"/>
<pinref part="R34" gate="G$1" pin="1"/>
<wire x1="116.84" y1="-175.26" x2="127" y2="-175.26" width="0.1524" layer="91"/>
<label x="116.84" y="-175.26" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B3" class="0">
<segment>
<pinref part="PCA9555D" gate="U$1" pin="I/O1.3"/>
<wire x1="86.36" y1="-185.42" x2="88.9" y2="-185.42" width="0.1524" layer="91"/>
<label x="88.9" y="-185.42" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="C"/>
<pinref part="R35" gate="G$1" pin="1"/>
<wire x1="109.22" y1="-177.8" x2="127" y2="-177.8" width="0.1524" layer="91"/>
<label x="109.22" y="-177.8" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B5" class="0">
<segment>
<pinref part="U$3" gate="G$1" pin="D"/>
<pinref part="R39" gate="G$1" pin="1"/>
<wire x1="165.1" y1="-177.8" x2="175.26" y2="-177.8" width="0.1524" layer="91"/>
<label x="175.26" y="-177.8" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PCA9555D" gate="U$1" pin="I/O1.4"/>
<wire x1="86.36" y1="-187.96" x2="88.9" y2="-187.96" width="0.1524" layer="91"/>
<label x="88.9" y="-187.96" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="LED1" class="0">
<segment>
<pinref part="R40" gate="G$1" pin="1"/>
<wire x1="165.1" y1="-193.04" x2="167.64" y2="-193.04" width="0.1524" layer="91"/>
<label x="167.64" y="-193.04" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PCA9555D" gate="U$1" pin="I/O0.2"/>
<wire x1="86.36" y1="-160.02" x2="88.9" y2="-160.02" width="0.1524" layer="91"/>
<label x="88.9" y="-160.02" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="LED2" class="0">
<segment>
<pinref part="R41" gate="G$1" pin="1"/>
<wire x1="165.1" y1="-198.12" x2="167.64" y2="-198.12" width="0.1524" layer="91"/>
<label x="167.64" y="-198.12" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PCA9555D" gate="U$1" pin="I/O0.0"/>
<wire x1="86.36" y1="-154.94" x2="88.9" y2="-154.94" width="0.1524" layer="91"/>
<label x="88.9" y="-154.94" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="LED3" class="0">
<segment>
<pinref part="R42" gate="G$1" pin="1"/>
<wire x1="165.1" y1="-195.58" x2="167.64" y2="-195.58" width="0.1524" layer="91"/>
<label x="167.64" y="-195.58" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="PCA9555D" gate="U$1" pin="I/O0.1"/>
<wire x1="86.36" y1="-157.48" x2="88.9" y2="-157.48" width="0.1524" layer="91"/>
<label x="88.9" y="-157.48" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="R7" gate="G$1" pin="1"/>
<pinref part="R44" gate="G$1" pin="2"/>
<wire x1="213.36" y1="-55.88" x2="213.36" y2="-58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<pinref part="R38" gate="G$1" pin="1"/>
<pinref part="R45" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="R37" gate="G$1" pin="1"/>
<pinref part="R46" gate="G$1" pin="1"/>
<wire x1="299.72" y1="-38.1" x2="302.26" y2="-38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<pinref part="R25" gate="G$1" pin="2"/>
<pinref part="R47" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="R40" gate="G$1" pin="2"/>
<pinref part="D4" gate="G$1" pin="CATHODE_RED"/>
<wire x1="154.94" y1="-193.04" x2="149.86" y2="-193.04" width="0.1524" layer="91"/>
<wire x1="149.86" y1="-193.04" x2="149.86" y2="-187.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<pinref part="D4" gate="G$1" pin="CATHODE_BLUE"/>
<wire x1="149.86" y1="-198.12" x2="149.86" y2="-203.2" width="0.1524" layer="91"/>
<pinref part="R41" gate="G$1" pin="2"/>
<wire x1="154.94" y1="-198.12" x2="149.86" y2="-198.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="D4" gate="G$1" pin="CATHODE_GREEN"/>
<pinref part="R42" gate="G$1" pin="2"/>
<wire x1="149.86" y1="-195.58" x2="154.94" y2="-195.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="4"/>
<wire x1="284.48" y1="-144.78" x2="287.02" y2="-144.78" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="104,1,73.66,73.66,ESP32-PICO-D4,VDDA,VCC,,,"/>
<approved hash="104,1,73.66,71.12,ESP32-PICO-D4,VDDA3P3,VCC,,,"/>
<approved hash="202,1,33.02,58.42,ESP32-PICO-D4,SENSOR_VP,,,,"/>
<approved hash="202,1,33.02,55.88,ESP32-PICO-D4,SENSOR_CAPP,,,,"/>
<approved hash="202,1,33.02,53.34,ESP32-PICO-D4,SENSOR_CAPN,,,,"/>
<approved hash="202,1,33.02,50.8,ESP32-PICO-D4,SENSOR_VN,,,,"/>
<approved hash="104,1,73.66,68.58,ESP32-PICO-D4,VDD3P3_RTC,VCC,,,"/>
<approved hash="104,1,73.66,66.04,ESP32-PICO-D4,VDD_SDIO,VCC,,,"/>
<approved hash="104,1,73.66,63.5,ESP32-PICO-D4,VDD3P3_CPU,VCC,,,"/>
<approved hash="202,1,43.18,-45.72,FT231,!RI#,,,,"/>
<approved hash="202,1,43.18,-40.64,FT231,!DSR#,,,,"/>
<approved hash="202,1,43.18,-43.18,FT231,!DCD#,,,,"/>
<approved hash="202,1,43.18,-38.1,FT231,!CTS#,,,,"/>
<approved hash="104,1,78.74,-25.4,FT231,3V3OUT,N$5,,,"/>
<approved hash="104,1,78.74,-20.32,FT231,VCC,V_USB,,,"/>
<approved hash="104,1,78.74,-22.86,FT231,VCCIO,N$5,,,"/>
<approved hash="104,1,187.96,-43.18,MCP73831,VIN,V_USB,,,"/>
<approved hash="104,1,208.28,-43.18,MCP73831,VBAT,V_BATT,,,"/>
<approved hash="104,1,208.28,-48.26,MCP73831,VSS,GND,,,"/>
<approved hash="104,1,167.64,-23.114,USB,5V,V_USB,,,"/>
<approved hash="104,1,167.64,-35.814,USB,SLD,GND,,,"/>
<approved hash="104,1,345.44,-33.02,LSM303,VDD,VCC,,,"/>
<approved hash="104,1,259.08,-40.64,BME280,VDD,VCC,,,"/>
<approved hash="104,1,259.08,-38.1,BME280,VDDIO,VCC,,,"/>
<approved hash="104,1,327.66,-5.08,SD-CARD,3.3V,3V3_SDC,,,"/>
<approved hash="206,1,233.68,-106.68,5V,,,,,"/>
<approved hash="206,1,233.68,-109.22,5V,,,,,"/>
<approved hash="208,1,233.68,-106.68,5V,out,,,,"/>
<approved hash="208,1,233.68,-109.22,5V,out,,,,"/>
<approved hash="208,1,238.76,-106.68,5V,sup,,,,"/>
<approved hash="106,1,73.66,38.1,NC0,,,,,"/>
<approved hash="106,1,73.66,40.64,NC1,,,,,"/>
<approved hash="106,1,73.66,48.26,NC2,,,,,"/>
<approved hash="106,1,73.66,50.8,NC3,,,,,"/>
<approved hash="106,1,73.66,15.24,NC4,,,,,"/>
<approved hash="106,1,33.02,12.7,NC5,,,,,"/>
<approved hash="110,1,325.12,12.7,IO12,SD_CS,,,,"/>
<approved hash="110,1,325.12,12.7,IO12,SD_CS,,,,"/>
<approved hash="110,1,325.12,10.16,IO12,SD_MOSI,,,,"/>
<approved hash="110,1,325.12,10.16,IO12,SD_MOSI,,,,"/>
<approved hash="110,1,325.12,7.62,IO12,SD_CLK,,,,"/>
<approved hash="110,1,325.12,7.62,IO12,SD_CLK,,,,"/>
<approved hash="110,1,314.96,12.7,N$23,SD_CS,,,,"/>
<approved hash="110,1,314.96,12.7,N$23,SD_CS,,,,"/>
<approved hash="110,1,314.96,10.16,N$23,SD_MOSI,,,,"/>
<approved hash="110,1,314.96,10.16,N$23,SD_MOSI,,,,"/>
<approved hash="110,1,314.96,7.62,N$23,SD_CLK,,,,"/>
<approved hash="110,1,314.96,7.62,N$23,SD_CLK,,,,"/>
<approved hash="111,1,20.32,-33.02,D+,,,,,"/>
<approved hash="111,1,81.28,-22.86,N$5,,,,,"/>
<approved hash="111,1,86.36,58.42,ESP_TX,,,,,"/>
<approved hash="111,1,294.64,-48.26,I2C_SCL,,,,,"/>
<approved hash="111,1,170.18,22.86,N$16,,,,,"/>
<approved hash="111,1,215.9,15.24,TX_GPS,,,,,"/>
<approved hash="111,1,162.56,45.72,N$16,,,,,"/>
<approved hash="113,1,228.102,-43.9644,BATTERY,,,,,"/>
<approved hash="113,1,274.372,45.72,U8,,,,,"/>
<approved hash="113,1,165.609,45.72,U10,,,,,"/>
<approved hash="113,1,119.889,-101.6,U12,,,,,"/>
<approved hash="113,1,157.989,-101.6,U15,,,,,"/>
</errors>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
